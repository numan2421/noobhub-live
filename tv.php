<?php include_once "Mobile_Detect.php" ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Josefin+Sans" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether-theme-arrows.min.css">
<link href="http://vjs.zencdn.net/6.2.0/video-js.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="video.css">
<link rel="stylesheet" type="text/css" href="style.css">
<script  type="text/javascript"  src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<?php 
include 'search.php';
	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '<div class="row page_body">
				<div id="backdrop" class="playhide">
					<div id="bImage">
						<img src="https://image.tmdb.org/t/p/w1280/gfTQaH7h09IaSZw9ubZgP2c7syr.jpg" />
						<div id="overlay">
							<h1>Power Rangers<small>(2017)</small></h1>
						</div>
					</div>
				</div>


				<div id="player" class="playshow">
					<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
				    	<source src="https://vjs.zencdn.net/v/oceans.mp4" type="video/mp4" />
					    <source src="https://vjs.zencdn.net/v/oceans.webm" type="video/webm" />
				    
				  	</video>
				</div>

				<div id="details row">
					<div class="buttons">
						<div id="btn1">
							<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						</div>
						<div id="btn2">
							<a href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
							<a href="javascript:void(0)"><button><i class="fa fa-youtube" aria-hidden="true"></i> Trailer</button></a>
						</div>
					</div>

					<div id="playHeading" class="playshow">
						<h1>Power Rangers<small>(2017)</small></h1><small id="ep">S01E02</small>

					</div>
					
					<div class="basic_info">
						<div id="rated">
							<h3>Rated</h3>
							<a href="javascript:void(0)">PG-13</a>
						</div>
						<div id="imdb">
							<h3>Imdb</h3>
							<a href="javascript:void(0)"><i class="fa fa-star"></i> 6.4</a>
						</div>
						<div id="runtime">
							<h3>Runtime</h3>
							<a href="javascript:void(0)">2h 40m</a>
						</div>
						<div id="Genre">
							<h3>genre</h3>
							<a href="javascript:void(0)">Action</a>
							<a href="javascript:void(0)">Comedy</a>
							<a href="javascript:void(0)">Romance</a>
							<a href="javascript:void(0)">Action</a>
							<a href="javascript:void(0)">Action</a>

						</div>
						<div id="storyLine">
							<h3>StoryLine</h3>
							<p>Sabans Power Rangers follows five ordinary teens who must become something extraordinary when they learn that their small town of Angel Grove — and the world — is on the verge of being obliterated by an alien threat. Chosen by destiny, our heroes quickly discover they are the only ones who can save the planet. But to do so, they will have to overcome their real-life issues and before it’s too late, band together as the Power Rangers.</p>
						</div>
						<div class="seasons">
							<div class="snav">
							<h3>Seasons</h3>
								<a class="selected" href="javascript:void(0)">1</a>
								<a href="javascript:void(0)">2</a>
								<a href="javascript:void(0)">3</a>
								<a href="javascript:void(0)">4</a>
								<a href="javascript:void(0)">5</a>
								<a href="javascript:void(0)">6</a>
								<a href="javascript:void(0)">7</a>
								<a href="javascript:void(0)">8</a>
								<a href="javascript:void(0)">9</a>
								<a href="javascript:void(0)">10</a>
								<a href="javascript:void(0)">11</a>
							</div>
						</div>
					</div>
				</div>
			</div>';
			echo '<div class="row">
					<div class="episodes">
					<h3>Season Name <small>(2048-01-17)</small></h3>
						<div class="row">
							<div class="col-md-3 hover10 column" id="episode">
								<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
								<h2>1.Pilot</h2>
								<p class="ttip">2011-04-17</p>
								<div class="play_buttons">
									<div class="row">
										<a href="javascript:void(0)" onclick="play();">
											<div class="col-md-6" id="play_button">	
												<div class="row">
													<i class="fa fa-2x fa-play-circle-o"></i>
												</div>
												
												<div class="row">
													<a href="javascript:void()">Play</a>
												</div>	
											</div>
										</a>
										<a href="javascript:void(0)"><div class="col-md-6" id="down_button">
											<div class="row">
												<i class="fa fa-2x fa-download"></i>
											</div>
											<div class="row">
												<a href="javascript:void()">Download</a>
											</div></a>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>';
		echo '<div class="row"><div id="cast">
					<div id="cast_details">
					<h3>Cast</h3>
						<ul id="lightSlider2">
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
						</ul>
					</div>
				
				</div></div>';
	}
	else
	{
echo '<div class="container page_body" style="padding-top: 1%">
	
		<div class="main">
		<!-- backdrop -->
			<div id="backdrop" class="playhide">
				<div id="bImage">
					<img src="https://image.tmdb.org/t/p/w1280/gfTQaH7h09IaSZw9ubZgP2c7syr.jpg" />
					<div id="overlay">
						<h1>Power Rangers<small>(2017)</small></h1>
					</div>
				</div>
			</div>
			


			<div id="player" class="playshow">
				<video id="my-video" class="video-js" controls preload="auto"
			 data-setup="{}">
			    	<source src="https://vjs.zencdn.net/v/oceans.mp4" type="video/mp4" />
				    <source src="https://vjs.zencdn.net/v/oceans.webm" type="video/webm" />
			    
			  </video>
			  </div>

				<!-- All Details -->
				<div id="details">
					<div class="buttons">
						<div id="btn1">
							<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						</div>
						<div id="btn2">
							<a href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
							<a class="playhide" href="javascript:void(0)"><button><i class="fa fa-youtube" aria-hidden="true"></i> Trailer</button></a>
						</div>
					
					</div>

					<div id="playHeading" class="playshow" style="display: block;padding: 20px;">
						<div><b>PLAYING</b></div>
						<div style="margin-top: 10px;">Power Rangers - S01E02 - Pata nai kya naam hai :D</div>

					</div>

					<div class="basic_info playhide">
						<div id="rated">
							<h3>Rated</h3>
							<a href="javascript:void(0)">PG-13</a>
						</div>
						<div id="imdb">
							<h3>Imdb</h3>
							<a href="javascript:void(0)"><i class="fa fa-star"></i> 6.4</a>
						</div>
						<div id="runtime">
							<h3>Runtime</h3>
							<a href="javascript:void(0)">2h 40m</a>
						</div>
						<div id="Genre">
							<h3>genre</h3>
							<a href="javascript:void(0)">Action</a>
							<a href="javascript:void(0)">Comedy</a>
							<a href="javascript:void(0)">Romance</a>
							<a href="javascript:void(0)">Action</a>
							<a href="javascript:void(0)">Action</a>

						</div>
						<div id="storyLine">
							<h3>StoryLine</h3>
							<p>Sabans Power Rangers follows five ordinary teens who must become something extraordinary when they learn that their small town of Angel Grove — and the world — is on the verge of being obliterated by an alien threat. Chosen by destiny, our heroes quickly discover they are the only ones who can save the planet. But to do so, they will have to overcome their real-life issues and before it’s too late, band together as the Power Rangers.</p>
						</div>
					
					</div>





					<!-- Basic info end -->
					



		<div class="episodes">
		
				<div class="seasons">
							<div class="snav">
							<h3>Seasons</h3>
								<a class="selected" href="javascript:void(0)">1</a>
								<a href="javascript:void(0)">2</a>
								<a href="javascript:void(0)">3</a>
								<a href="javascript:void(0)">4</a>
								<a href="javascript:void(0)">5</a>
								<a href="javascript:void(0)">6</a>
								<a href="javascript:void(0)">7</a>
								<a href="javascript:void(0)">8</a>
								<a href="javascript:void(0)">9</a>
								<a href="javascript:void(0)">10</a>
								<a href="javascript:void(0)">11</a>
							</div>
						</div>
			<div class="row">
				<div class="col-md-3 hover10 column" id="episode">
					<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
					<h2>1.Pilot</h2>
					<p class="ttip">2011-04-17</p>
					<div class="play_buttons">
						<div class="row">
							<a href="javascript:void(0)" onclick="play();">
								<div class="col-md-6" id="play_button">	
									<div class="row">
										<i class="fa fa-2x fa-play-circle-o"></i>
									</div>
									
									<div class="row">
										<a href="javascript:void()">Play</a>
									</div>	
								</div>
							</a>
							<a href="javascript:void(0)"><div class="col-md-6" id="down_button">
								<div class="row">
									<i class="fa fa-2x fa-download"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Download</a>
								</div></a>
								
							</div>
						</div>
					</div>
				</div>

					<div class="col-md-3 hover10 column" id="episode">
					<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
					<h2>1.Pilot</h2>
					<p class="ttip">2011-04-17</p>
					<div class="play_buttons">
						<div class="row">
							<div class="col-md-6" id="play_button">
								<div class="row">
									<i class="fa fa-2x fa-play-circle-o"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Play</a>
								</div>
								
							</div>
							<div class="col-md-6" id="down_button">
								<div class="row">
									<i class="fa fa-2x fa-download"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Download</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>

					<div class="col-md-3 hover10 column" id="episode">
					<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
					<h2>1.Pilot</h2>
					<p class="ttip">2011-04-17</p>
					<div class="play_buttons">
						<div class="row">
							<div class="col-md-6" id="play_button">
								<div class="row">
									<i class="fa fa-2x fa-play-circle-o"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Play</a>
								</div>
								
							</div>
							<div class="col-md-6" id="down_button">
								<div class="row">
									<i class="fa fa-2x fa-download"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Download</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>

					<div class="col-md-3 hover10 column" id="episode">
					<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
					<h2>1.Pilot</h2>
					<p class="ttip">2011-04-17</p>
					<div class="play_buttons">
						<div class="row">
							<div class="col-md-6" id="play_button">
								<a href="javascript:void(0)"><div class="row">
									<i class="fa fa-2x fa-play-circle-o"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Play</a>
								</div></a>
								
							</div>
							<div class="col-md-6" id="down_button">
								<a href="javascript:void(0)"><div class="row">
									<i class="fa fa-2x fa-download"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Download</a>
								</div></a>
								
							</div>
						</div>
					</div>
				</div>

							<div class="col-md-3 hover10 column" id="episode">
					<figure><a href="javascript:void(0)" ><img width="300" src="https://image.tmdb.org/t/p/w300/rxWlBXZhGWhumbLB8gAHyyW3ITD.jpg"></a></figure>
					<h2>1.Pilot</h2>
					<p class="ttip">2011-04-17</p>
					<div class="play_buttons">
						<div class="row">
							<div class="col-md-6" id="play_button">
								<a href="javascript:void(0)"><div class="row">
									<i class="fa fa-2x fa-play-circle-o"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Play</a>
								</div></a>
								
							</div>
							<div class="col-md-6" id="down_button">
								<a href="javascript:void(0)"><div class="row">
									<i class="fa fa-2x fa-download"></i>
								</div>
								<div class="row">
									<a href="javascript:void()">Download</a>
								</div></a>
								
							</div>
						</div>
					</div>
				</div>
			
		

			</div>



					<!-- Related -->
					<div class="related">
					<h3>You May Also like </h3>
					<ul id="lightSlider">
						  <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ytkaL1wZcqgQKHLuU7Go3UxGpON.jpg"></a>
						  </li>
								
							  <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//bQHhpTHiys0CZRrdDRKvXBmM5KL.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//iZThUkOSnmlIjq39C5Sn2azobrb.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ytkaL1wZcqgQKHLuU7Go3UxGpON.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//qDBDaMv2lr2UIZE1lyV0CvRIUeh.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//apBhaUivEub9Ghx7sj7UqQCFYH4.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ytkaL1wZcqgQKHLuU7Go3UxGpON.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ytkaL1wZcqgQKHLuU7Go3UxGpON.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//91maoa7TTMsrRDOn4As2Kpd4me2.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//inkHD37MJBx0RXjQYvuYLgjodNo.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ytkaL1wZcqgQKHLuU7Go3UxGpON.jpg"></a>
						  </li>
						    <li>
						      <a><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2//ddQZ7xUBPBcj9vA1LiLyEzfFQK5.jpg"></a>
						  </li>
						</ul>
					</div>
					<!-- ENd Related -->
				</div>


			
			<!-- End Content -->
		


	



		</div>



				<div id="cast">
					

					<div id="cast_details">
					<h3>Cast</h3>
						<ul id="lightSlider2">
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
							<li>
								<div class="person">
									<a href="javascript:void(0)"><img src="https://image.tmdb.org/t/p/w185/mB1NYXRyCPvWc20LKuEPhix6S9z.jpg"></a>
									<h2>Name</h2>
									<p>Character</p>
								</div>
							</li>
				
				
					
						
						</ul>
					</div>
				
				</div';
}
?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<<script src="http://vjs.zencdn.net/6.2.0/video.js"></script>
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() 
  {
  	$('.playshow').hide();

    $("#lightSlider").lightSlider(
	{
		pager:false,
		autoWidth:true,
		loop:true,
		slideMargin:1,
		adaptiveHeight:true
	}); 
	$('#lightSlider2').lightSlider(
	{
		pager:false,
		autoWidth:true,
		loop:true,
		slideMargin:10,
		adaptiveHeight:true
	}); 


	$('.hover10').click(function()
	{
		$('.play_buttons').hide();
		$('.hover10 img').removeClass('blur_active');
		$(this).find('img').addClass('blur_active');
		$(this).find('.play_buttons').show();
	})

		var video = videojs('my-video');

		video.on('pause', function() 
		{
		  	this.bigPlayButton.show();

		  // Now the issue is that we need to hide it again if we start playing
		  // So every time we do this, we can create a one-time listener for play events.
		  video.one('play', function() {
		    this.bigPlayButton.hide();
		  });
		});

		$('#getBack').click(function()
		{
			var video=videojs('my-video');
			video.pause();
			$('.playshow').hide();
  			$('.playhide').show();
		})
  });
  function play()
  {
  	$('html,body').animate({ scrollTop: 0 }, 'slow');
  	$('.playshow').show();
  	$('.playhide').hide();
  }
</script>