<?php
 	include 'header.php';
	include_once 'includes/mobile_detect/Mobile_Detect.php';
 ?>

 <?php 
	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '
	<div class="row options filters" style="margin-right:0;margin-left:0;background: #2f3444;">
		
		<div class="all">
		<label style="margin-top: 10px">Type: </label><br>
			<div class="btn-group" role="group" aria-label="...">
			  <a  href="javascript:void(0)"  onclick="typefilter(1,this)"	class="btn btn-default activefilter">all</a>
			  <a  href="javascript:void(0)"  onclick="typefilter(2,this)" 	class="btn btn-default">tv</a>
			  <a  href="javascript:void(0)"  onclick="typefilter(3,this)" 	class="btn btn-default">movies</a>
			</div>
		</div>

			<div class="sorter">
			<label style="margin-top: 10px">Sort By: </label><br>
			<div class="btn-group" role="group" aria-label="...">
			  <a  href="javascript:void(0)" onclick="filter(1,this)" class="btn btn-default activefilter">Trending</a>
			  <a  href="javascript:void(0)" onclick="filter(2,this)" class="btn btn-default">Popularity</a>
			  <a  href="javascript:void(0)" onclick="filter(3,this)" class="btn btn-default">Recently Added</a>
			</div>
		</div>
		
		
		<div class="order">
		<label style="margin-top: 10px">Order By: </label><br>
			<div class="btn-group" role="group" aria-label="...">
			   <a  class="btn btn-default" onclick="filter(4,this)">asc</a>
		  <a  class="btn btn-default" onclick="filter(5,this)">desc</a>
			</div>
		</div>
		
	</div><br>'; 
	}
	else
	{
		echo '<div class="filters" style="padding-top: 10px;padding-left: 15px;padding-right: 15px;">

	<div class="options pull-right">
		<div class="all">
			<div class="btn-group" role="group" aria-label="...">
			  <a  href="javascript:void(0)"  onclick="typefilter(1,this)"	class="btn btn-default activefilter">all</a>
			  <a  href="javascript:void(0)"  onclick="typefilter(2,this)" 	class="btn btn-default">tv</a>
			  <a  href="javascript:void(0)"  onclick="typefilter(3,this)" 	class="btn btn-default">movies</a>
			</div>
		</div>
		<div class="sorter">
			<label>Sort By: </label>
			<div class="btn-group" role="group" aria-label="...">
			  <a  href="javascript:void(0)" onclick="filter(1,this)" class="btn btn-default activefilter">Trending</a>
			  <a  href="javascript:void(0)" onclick="filter(2,this)" class="btn btn-default">Popularity</a>
			  <a  href="javascript:void(0)" onclick="filter(3,this)" class="btn btn-default">Recently Added</a>
			</div>
		</div>
		<div class="order">
		<label>Order By: </label>
			<div class="btn-group" role="group" aria-label="...">
			   <a  class="btn btn-default" onclick="filter(4,this)">asc</a>
			  <a  class="btn btn-default" onclick="filter(5,this)">desc</a>
			</div>
		</div>
	</div>
</div>';
	}
	?>

