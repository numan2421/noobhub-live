<?php

include 'header.php'; 
include_once "includes/mobile_detect/Mobile_Detect.php";
include_once 'includes/processes/mvdetails_process.php';
?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether-theme-arrows.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
<link href="http://vjs.zencdn.net/6.2.0/video-js.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/video.css">

<style type="text/css">

.subtitle
{
	display: none;
}
#lang
{
	    width: 150px;
    margin: 12px;
}
 .vjs-progress-holder {
    height: 0.5em !important;}
   .video-js .vjs-play-progress:before
   {
   	top:-.2em !important;
   }
.select2-container {
    width: 150px !important;
}
.select2-selection--single{
	height: 30px !important;
	background: none !important;
    
    border-radius: 35px !important;
    border: 2px solid rgba(243,249,255,0.7) !important;
    opacity: .9;
}
.select2-selection__rendered
{
	color: white !important;
}
.select2-search__field
{
	color: black !important;
}

	iframe


{
	width: 100%;
	height: 90%;
}
.vjs-menu-button-popup .vjs-menu
{
 width:10.5em;
}
.modal
{

}
.modal-header
{
	border-bottom: none !important;
}
.modal-dialog
{
	width: 100% !important;
}
.modal-content
{
	background-color: rgb(0, 0, 0) !important;
}
.modal-fullscreen .modal-dialog
{
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
@media (min-width: 768px) {
  .modal-fullscreen .modal-dialog {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .modal-fullscreen .modal-dialog {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .modal-fullscreen .modal-dialog {
     width: 1170px;
  }
}
.close {
    color: #fff !important;
    opacity: 9 !important;
}
#myModalLabel
{
	color: white;
	font-weight: bolder;
}
h1
{
	font-size: 2em;
}
 @media screen and (min-width: 1024px) and (max-width: 1900px){
        body { padding-top: 135px !important; }
</style>

<input type="hidden" name="mv_code" id="mv_code" value=<?php echo $_REQUEST['mv_code'] ?> >
<input type="hidden" name="premium" id="premium" value=<?php echo $_SESSION['is_premium'] ?> >
<?php 
include 'search.php';

	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '
			<div class="page_body">
			<div class="alert alert-info fade in alert-video">
  <div class="alert-Text"><strong>Warning!</strong> Your Daily video play limit reached. <a href="'.$absolutepath.'upgrade">Click</a> to become premium user.</div>
</div>
	<!-- backdrop -->
		<div id="backdrop"  class="playhide">
			<div id="bImage">
				<img src="'.$backDrop_path.'">
				<div style="    width: 95%;
    text-align: center;"><a href="javascript:void(0)" id="play_button1" onclick="play(\''.$title.'\')"><i class="fa fa-play fa-4x"></i></a></div>
				<div id="mvoverlay">

					<h1>'.$title.'<small>('.$ryear.')</small></h1>
				</div>
			</div>
		</div>
		

		<div id="player" class="playshow">
					<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
				    	<source src="'.$video.'" type="video/mp4" />';
				    	if($subtitle=="")
				  		{

				  		}
				  		else
				  		{
				  			for($i=0;$i<count($subtitle);$i++)
				  			{
				  				echo '<track kind="captions" src='.$absolutepath.'uploads/srt/'.$subtitle[$i]->subtitle.' srclang='.$subtitle[$i]->language.' label='.$subtitle[$i]->language.' />';
				  			}
				  		}
				    	
				  	echo '</video>
				</div>

			<!-- All Details -->
			<div id="details">
				<div class="buttons" style="padding-right:0px;">
					<div>
						<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						<a href="javascript:void(0)" onclick="play(\''.$title.'\')" class="playhide"><button><i class="fa fa-play-circle"></i> Play</button></a>';
						if($_SESSION['is_premium']==1)
						{
							echo '<a class="downshow" href="javascript:void(0)"  id="downLink" name="downLink"><button><i class="fa fa-download"></i> Download</button></a>
						<a class="downhide" href="'.$absolutepath.'download/movie/'.$mId.'"  title="Download '.$title.'"><button><i class="fa fa-download"></i> '.$size.' - mp4</button></a>
						';
						}
						else
						{

						}
						

					echo $trailerBody.'</div>
					<div >
					<a class="playshow" href="javascript:void(0)" id="upsub" name="upsub"><button><i class="fa fa-upload"></i>Upload Subtitle</button></a>
						<a d="'.$mId.'" e="movie" id="playlistadd" href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
					</div>
				
				</div>
					<div id="playHeading" class="playshow" style="display: block;padding: 20px;">
						<span>PLAYING - </span>
						<span style="margin-top: 10px;">'.$title.'</span>

					</div>

					<div class="subtitle" style="padding:20px;">
					<h3>Upload Subtitle</h3>
					<p id="subInfo">You can download  .vtt subtitles for '.$title.' from the internet and upload it here.</p>
						<div>
							<div class="buttons" style="padding:0;    background: none;display: inline-flex;">
								
							<div id="lang">
								<select id="select2" data-placeholder="Choose a Language...">
								  <option value="AF">Afrikanns</option>
								  <option value="SQ">Albanian</option>
								  <option value="AR">Arabic</option>
								  <option value="HY">Armenian</option>
								  <option value="EU">Basque</option>
								  <option value="BN">Bengali</option>
								  <option value="BG">Bulgarian</option>
								  <option value="CA">Catalan</option>
								  <option value="KM">Cambodian</option>
								  <option value="ZH">Chinese (Mandarin)</option>
								  <option value="HR">Croation</option>
								  <option value="CS">Czech</option>
								  <option value="DA">Danish</option>
								  <option value="NL">Dutch</option>
								  <option value="EN" selected="selected">English</option>
								  <option value="ET" >Estonian</option>
								  <option value="FJ">Fiji</option>
								  <option value="FI">Finnish</option>
								  <option value="FR">French</option>
								  <option value="KA">Georgian</option>
								  <option value="DE">German</option>
								  <option value="EL">Greek</option>
								  <option value="GU">Gujarati</option>
								  <option value="HE">Hebrew</option>
								  <option value="HI">Hindi</option>
								  <option value="HU">Hungarian</option>
								  <option value="IS">Icelandic</option>
								  <option value="ID">Indonesian</option>
								  <option value="GA">Irish</option>
								  <option value="IT">Italian</option>
								  <option value="JA">Japanese</option>
								  <option value="JW">Javanese</option>
								  <option value="KO">Korean</option>
								  <option value="LA">Latin</option>
								  <option value="LV">Latvian</option>
								  <option value="LT">Lithuanian</option>
								  <option value="MK">Macedonian</option>
								  <option value="MS">Malay</option>
								  <option value="ML">Malayalam</option>
								  <option value="MT">Maltese</option>
								  <option value="MI">Maori</option>
								  <option value="MR">Marathi</option>
								  <option value="MN">Mongolian</option>
								  <option value="NE">Nepali</option>
								  <option value="NO">Norwegian</option>
								  <option value="FA">Persian</option>
								  <option value="PL">Polish</option>
								  <option value="PT">Portuguese</option>
								  <option value="PA">Punjabi</option>
								  <option value="QU">Quechua</option>
								  <option value="RO">Romanian</option>
								  <option value="RU">Russian</option>
								  <option value="SM">Samoan</option>
								  <option value="SR">Serbian</option>
								  <option value="SK">Slovak</option>
								  <option value="SL">Slovenian</option>
								  <option value="ES">Spanish</option>
								  <option value="SW">Swahili</option>
								  <option value="SV">Swedish </option>
								  <option value="TA">Tamil</option>
								  <option value="TT">Tatar</option>
								  <option value="TE">Telugu</option>
								  <option value="TH">Thai</option>
								  <option value="BO">Tibetan</option>
								  <option value="TO">Tonga</option>
								  <option value="TR">Turkish</option>
								  <option value="UK">Ukranian</option>
								  <option value="UR">Urdu</option>
								  <option value="UZ">Uzbek</option>
								  <option value="VI">Vietnamese</option>
								  <option value="CY">Welsh</option>
								  <option value="XH">Xhosa</option>
								</select>
							</div>

								<a d="'.$mId.'" e="movie" id="uploadSub" name="uploadSub" href="javascript:void(0)"><button style="border-radius:35px;"><i class="fa fa-upload"></i> Upload Subtitles</button></a>


								<p class="pull-right error" id="sub_error" style="display:none;color:red">Only .vtt formate supported at the time.</p>
								<p class="pull-right error" id="sub_success" style="display:none;color:green">Subtitles Uploded Successfully. Enjoy!</p>
							</div>
							<div class="error-msg pull-right">
							
							</div>
						</div>
					</div>

				<div class="basic_info">
					<div id="rated">
						<h3>Rated</h3>
						<a href="javascript:void(0)">'.$rated.'</a>
					</div>
					<div id="imdb">
						<h3>Imdb</h3>
						<a href="javascript:void(0)"><i class="fa fa-star"></i> '.$Imdb_rating.'</a>
					</div>
					<div id="runtime">
						<h3>Runtime</h3>
						<a href="javascript:void(0)">'.$hour.' '.$minutes.'</a>
					</div>
					<div id="Genre" style="margin-right:0 !important;">
						<h3>genre</h3>
						'.$allgen.'

					</div>
					<div id="storyLine" style="margin-right: 0px !important;">
						<h3>StoryLine</h3>
						<p>'.$overview.'</p>
					</div>
				</div>
				<!-- Basic info end -->
				<!-- Related -->
				<div class="related">
					  '.$relatedBody.'
				</div>
				<!-- ENd Related -->
			</div>


		
		<!-- End Content -->



	

				<div id="cast" style="width:100%;padding:0px;margin-left:0px;">
					

					
							'.$castBody.'
					
		</div>	</div>';
		
	}
	else
	{
echo '<div class="page_body">
<div class="alert alert-info fade in alert-video">
  <div class="alert-Text"><strong>Warning!</strong> Your Daily video play limit reached. <a href="'.$absolutepath.'upgrade">Click</a> to become premium user.</div>
</div>
<div class="container">
	<div class="main">

	<!-- backdrop -->
		<div id="backdrop"  class="playhide">
			<div id="bImage">
				<img src="'.$backDrop_path.'">
				<div style="    width: 95%;
    text-align: center;"><a href="javascript:void(0)" id="play_button1" onclick="play(\''.$title.'\')"><i class="fa fa-play fa-4x"></i></a></div>
				<div id="mvoverlay">

					<h1>'.$title.'<small>('.$ryear.')</small></h1>
				</div>
			</div>
		</div>
		

		<div id="player" class="playshow">
					<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
				    	<source src="'.$video.'" type="video/mp4" />';

				  		if($subtitle=="")
				  		{

				  		}
				  		else
				  		{
				  			for($i=0;$i<count($subtitle);$i++)
				  			{
				  				echo '<track kind="captions" src='.$absolutepath.'uploads/srt/'.$subtitle[$i]->subtitle.' srclang='.$subtitle[$i]->language.' label='.$subtitle[$i]->language.' />';
				  			}
				  		}

				  	echo '</video>
				</div>
			<!-- All Details -->
			<div id="details">
				<div class="buttons">
					<div>
						<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						<a href="javascript:void(0)" onclick="play(\''.$title.'\')" class="playhide"><button><i class="fa fa-play-circle"></i> Play</button></a>';
						if($_SESSION['is_premium']==1)
						{
							echo '<a class="downshow" href="javascript:void(0)"  id="downLink" name="downLink"><button><i class="fa fa-download"></i> Download</button></a>
						<a class="downhide" href="'.$absolutepath.'download/movie/'.$mId.'"  title="Download '.$title.'"><button><i class="fa fa-download"></i> '.$size.' - mp4</button></a>
						';
						}
						else
						{

						}

	

					echo $trailerBody.'</div>
					<div >
					<a class="playshow" href="javascript:void(0)" id="upsub" name="upsub"><button><i class="fa fa-upload"></i>Upload Subtitle</button></a>
						<a d="'.$mId.'" e="movie" id="playlistadd" href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
					</div>
				
				</div>
					<div id="playHeading" class="playshow" style="display: block;padding: 20px;">
						<span>PLAYING - </span>
						<span style="margin-top: 10px;" id="title" name="title">'.$title.'</span>
					</div>

					<div class="subtitle" style="padding:20px;">
					<h3>Upload Subtitle</h3>
					<p id="subInfo">You can download  .vtt subtitles for '.$title.' from the internet and upload it here.</p>
						<div>
							<div class="buttons" style="padding:0;    background: none;display: inline-flex;">
								
							<div id="lang">
								<select id="select2" data-placeholder="Choose a Language...">
								  <option value="AF">Afrikanns</option>
								  <option value="SQ">Albanian</option>
								  <option value="AR">Arabic</option>
								  <option value="HY">Armenian</option>
								  <option value="EU">Basque</option>
								  <option value="BN">Bengali</option>
								  <option value="BG">Bulgarian</option>
								  <option value="CA">Catalan</option>
								  <option value="KM">Cambodian</option>
								  <option value="ZH">Chinese (Mandarin)</option>
								  <option value="HR">Croation</option>
								  <option value="CS">Czech</option>
								  <option value="DA">Danish</option>
								  <option value="NL">Dutch</option>
								  <option value="EN" selected="selected">English</option>
								  <option value="ET" >Estonian</option>
								  <option value="FJ">Fiji</option>
								  <option value="FI">Finnish</option>
								  <option value="FR">French</option>
								  <option value="KA">Georgian</option>
								  <option value="DE">German</option>
								  <option value="EL">Greek</option>
								  <option value="GU">Gujarati</option>
								  <option value="HE">Hebrew</option>
								  <option value="HI">Hindi</option>
								  <option value="HU">Hungarian</option>
								  <option value="IS">Icelandic</option>
								  <option value="ID">Indonesian</option>
								  <option value="GA">Irish</option>
								  <option value="IT">Italian</option>
								  <option value="JA">Japanese</option>
								  <option value="JW">Javanese</option>
								  <option value="KO">Korean</option>
								  <option value="LA">Latin</option>
								  <option value="LV">Latvian</option>
								  <option value="LT">Lithuanian</option>
								  <option value="MK">Macedonian</option>
								  <option value="MS">Malay</option>
								  <option value="ML">Malayalam</option>
								  <option value="MT">Maltese</option>
								  <option value="MI">Maori</option>
								  <option value="MR">Marathi</option>
								  <option value="MN">Mongolian</option>
								  <option value="NE">Nepali</option>
								  <option value="NO">Norwegian</option>
								  <option value="FA">Persian</option>
								  <option value="PL">Polish</option>
								  <option value="PT">Portuguese</option>
								  <option value="PA">Punjabi</option>
								  <option value="QU">Quechua</option>
								  <option value="RO">Romanian</option>
								  <option value="RU">Russian</option>
								  <option value="SM">Samoan</option>
								  <option value="SR">Serbian</option>
								  <option value="SK">Slovak</option>
								  <option value="SL">Slovenian</option>
								  <option value="ES">Spanish</option>
								  <option value="SW">Swahili</option>
								  <option value="SV">Swedish </option>
								  <option value="TA">Tamil</option>
								  <option value="TT">Tatar</option>
								  <option value="TE">Telugu</option>
								  <option value="TH">Thai</option>
								  <option value="BO">Tibetan</option>
								  <option value="TO">Tonga</option>
								  <option value="TR">Turkish</option>
								  <option value="UK">Ukranian</option>
								  <option value="UR">Urdu</option>
								  <option value="UZ">Uzbek</option>
								  <option value="VI">Vietnamese</option>
								  <option value="CY">Welsh</option>
								  <option value="XH">Xhosa</option>
								</select>
							</div>

								<a d="'.$mId.'" e="movie" id="uploadSub" name="uploadSub" href="javascript:void(0)"><button style="border-radius:35px;"><i class="fa fa-upload"></i> Upload Subtitles</button></a>


								<p class="pull-right error" id="sub_error" style="display:none;color:red">Only .vtt formate supported at the time.</p>
								<p class="pull-right error" id="sub_success" style="display:none;color:green">Subtitles Uploded Successfully. Enjoy!</p>
							</div>
							<div class="error-msg pull-right">
							
							</div>
						</div>
					</div>

			
				<div class="basic_info ">
					<div id="rated">
						<h3>Rated</h3>
						<a href="javascript:void(0)">'.$rated.'</a>
					</div>
					<div id="imdb">
						<h3>Imdb</h3>
						<a href="javascript:void(0)"><i class="fa fa-star"></i> '.$Imdb_rating.'</a>
					</div>
					<div id="runtime">
						<h3>Runtime</h3>
						<a href="javascript:void(0)">'.$hour.' '.$minutes.'</a>
					</div>
					<div id="Genre">
						<h3>genre</h3>
						'.$allgen.'

					</div>
					<div id="storyLine">
						<h3>StoryLine</h3>
						<p>'.$overview.'</p>
					</div>
				</div>
				<!-- Basic info end -->
				<!-- Related -->
				<div class="related sliderDiv" >
			
					  '.$relatedBody.'
					
				</div>
				<!-- ENd Related -->
			</div>


		
		<!-- End Content -->
	</div>

<input type="button" id="testa" name="testa" value="test">
	
</div>
				<div id="cast">
					

					'.$castBody.'
				
				</div></div>';
			}?>

<!-- Youtube overlay for trailers -->
<div class="modal modal-fullscreen fade" id="youtube_overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 10000;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $title?> Trailer</h4>
      </div>
      <div class="modal-body">
        <iframe
			src="https://www.youtube.com/embed/<?php echo $trailer->site_key ?>">
			</iframe>
      </div>

    </div>
  </div>
</div>
<?php include 'footer.php' ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="<?php echo $absolutepath ?>includes/js/uploader/ajaxupload.js"></script>
<script src="<?php echo $absolutepath ?>includes/js/play_control.js"></script>
<script src="http://vjs.zencdn.net/6.2.0/video.js"></script>
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://www.gstatic.com/cv/js/sender/v1/cast_sender.js"></script>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/cromecast.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">

(function(window, videojs)
{
	
	$('#select2').select2();
		// srt uploader
	   	btnUpload1=$('#uploadSub');
        new AjaxUpload(btnUpload1, {
        action: Apath+'includes/processes/srtuploader.php',
        name: 'uploadfile1',
        onSubmit: function(file1, ext)
        {
           	if (! (ext && /^(vtt)$/.test(ext)))
           	{
                console.log('Wrong Format');
                $('#sub_error').show();
                setTimeout(function(){ $('#sub_error').fadeOut() }, 3000);
                return false;
            } 
        },
        onComplete: function(file1, response1)
        {
        	$('#sub_error').hide();
        	arr1 = response1.split(" | ");
            res1 = arr1[0];
            file2 = arr1[1];
        	//update to db
        	$.post(Apath+'includes/processes/sub_process.php',{action:'saveSub',type:"movies",mv_code:$('#mv_code').val(),lang:$('#select2').val(),title:file2.split('/').pop()},function showData(data)
        	{

        	})
           	var video=videojs('my-video');
				video.addRemoteTextTrack({
					kind: 'captions', 
					label:$('#select2').val(),
					src: Apath+'uploads/srt/'+file2 }, false);
				$('#sub_success').show();
        }
    })

   	//Uploader End
  var player = window.player = videojs('my-video',{'html5': { nativeTextTracks: false }});
  player.ready(function () 
  { 
    this.chromecast();
  });
}(window, window.videojs));
    
function showTrailer(title)
{
	$.post(Apath+'api/v1/analytics/videoPlay',{title:title,type:"movie-trailer"},function showData()
	{

	})
	$('#youtube_overlay').modal({backdrop: 'static', keyboard: false});
}



$(document).ready(function() 
{
  	$('.playshow').hide();			
  	$('#upsub').click(function()
	{
		$('.subtitle').show();
	})
	$('#close_sub').click(function()
	{
		$('.subtitle').hide();
	})

  	
	$.get(Apath+'api/v1/user/getplaylist',function showData(data)
	{
		if(data.status==200)
		{
			allData=data.response;
			for(i=0;i<allData.length;i++)
			{
				$('a[d="'+allData[i]['video_id']+'"][e="'+allData[i]['video_type']+'"]').html('<button><i class="fa fa-minus"></i> Remove From List</button>');
			}
		}
	})
	$('#youtube_overlay').on('hidden.bs.modal', function ()
    {
	    $('iframe').attr('src', $('iframe').attr('src'));
	})
    $("#lightSlider").lightSlider(
	{
		pager:false,
		autoWidth:true,
		loop:true,
		slideMargin:1,
		adaptiveHeight:true
	}); 
	$('#getBack').click(function()
	{
		var video=videojs('my-video');
		video.pause();
		$('.playshow').hide();
			$('.playhide').show();
	})

	$('#downLink').click(function()
	{
		var video=videojs('my-video');
		video.pause();
		$('.downshow').hide();
			$('.downhide').show();
	})
});

  function play(title)
  {
  	$.post(Apath+'api/v1/analytics/videoPlay',{title:title,type:"movie"},function showData()
	{

	})
	if($('#premium').val()==1)
	{
		// premium user. Do nthtng
	}
	else
	{
		//save Played

	}
  	$('html,body').animate({ scrollTop: 0 }, 'slow');
  	$('.playshow').show();
  	$('.playhide').hide();
  	var video=videojs('my-video');
  	video.play();
  }

</script>