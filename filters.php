<?php
include 'header.php';
include_once 'includes/processes/filter_process.php';
include_once "includes/mobile_detect/Mobile_Detect.php";
 ?>
 <?php 
	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '<div class="filters" style="padding-top: 10px;padding-left: 15px;padding-right: 15px; background: #2f3444;">';
	}
	else
	{
		echo '<div class="filters" style="padding-top: 10px;padding-left: 15px;padding-right: 15px;">';
	}
	?>
	<div class="" style="margin-top: 10px;">
		<div class="genre">
			<?php echo $fBody ?>

		</div>
	</div>

	<?php 
	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '
	<div class="row options filters">
		
			<div class="sorter">
				<label style="margin-top: 10px">Sort By: </label><br>
				<div class="btn-group" role="group" aria-label="...">
				  <a  href="javascript:void(0)" onclick="filter(1,this)" class="btn btn-default">Trending</a>
				  <a  href="javascript:void(0)" onclick="filter(2,this)" class="btn btn-default">Popularity</a>
				  <a  href="javascript:void(0)" onclick="filter(3,this)" class="btn btn-default">Recently Added</a>
				</div>
			</div>
		
		
			<div class="order">
				<label style="margin-top: 10px">Order By: </label><br>
				<div class="btn-group" role="group" aria-label="...">
						  <a  class="btn btn-default" onclick="filter(4,this)">asc</a>
			  <a  class="btn btn-default" onclick="filter(5,this)">desc</a>
				</div>
			</div>
		
	</div>'; 
	}
	else
	{
		echo '<div class="options pull-right filters">
		<div class="sorter">
			<label>Sort By: </label>
			<div class="btn-group filter" role="group" aria-label="...">
			  <a  href="javascript:void(0)" id="trending" name="trending" onclick="filter(1,this)" class="btn btn-default">Trending</a>
			  <a  href="javascript:void(0)" id="popular" name="popular" onclick="filter(2,this)" class="btn btn-default">Popularity</a>
			  <a  href="javascript:void(0)" id="recent" name="recent" onclick="filter(3,this)" class="btn btn-default">Recently Added</a>
			</div>
		</div>
		<div class="order">
		<label>Order By: </label>
			<div class="btn-group order_by_filter" role="group" aria-label="...">
			  <a  class="btn btn-default" onclick="filter(4,this)">asc</a>
			  <a  class="btn btn-default" onclick="filter(5,this)">desc</a>
			</div>
		</div>
	</div>';
	}
	?> 
</div>
