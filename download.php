<?php
include_once 'includes/config.php';
include 'api/classes/user_class.php';
include_once 'api/classes/analytic_class.php';

$hostname = $_SERVER['HTTP_HOST'];
// echo $hostname;
if($hostname == 'localhost')
{
    $csabosultepath = '/BB-beta';
    $absolutepath="http://".$_SERVER['SERVER_NAME'].$csabosultepath.'/';
}
else 
{
    $absolutepath="http://".$_SERVER['SERVER_NAME'].'/';
}

$uObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
$anObject=new analytic(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);

$result=$uObject->verifySessionuser();
if($result==1)
{
	$code=$_REQUEST['download_code'];
	$type=$_REQUEST['type'];
	// $size=$_REQUEST['size'];
	//get file details
	$fileData=$uObject->getFileinfo($type,$code);
	if($fileData==2)
	{
		header("location:".$absolutepath."index");
	}
	else
	{
		$allData=$fileData[0];
		$title=$allData['title'];
		if($type=="movie")
		{
			$url="http://media.beanbag.tv/movies/";
			$anObject->videoDownload($type,$_SERVER['HTTP_REFERER'],$title);
		}
		else
		{
			$url="http://media.beanbag.tv/shows/ep";
			$anObject->videoDownload($type,$_SERVER['HTTP_REFERER'],$title."-season ".$allData['season_number']);
		}

		$video_id=$allData['live_video_id'];

		$download_url=$url.$video_id.".mp4";
		$new_filename=$title.".mp4";

		$ch = curl_init($download_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_NOBODY, TRUE);
		$data = curl_exec($ch);
		$size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		curl_close($ch);

	header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$new_filename);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . $size);
ob_clean();
flush();
session_write_close();
readfile($download_url);
exit;
	}
}
else
{
	header("location:".$absolutepath."index");
}

	
?>