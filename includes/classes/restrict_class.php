<?php

class restricted extends PDO
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception)
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		return json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		return json_encode($success);
	}
    public $v_aud_ip,$v_aud_loc;

    public function chkrestricted()
    {

        $this->checkIpLoc();
        $user_country_data=explode(", ", $this->v_aud_loc);
        if($user_country_data)
        {

        }
        $user_country=$user_country_data[1];
        // get data from restricted
        $gQuery=$this->prepare('select * from restricted_countries');
        $gQuery->execute();

        if($gQuery->rowCount()>0)
        {
            $countires=array();
            $gData=$gQuery->fetchAll();
            for($i=0;$i<count($gData);$i++)
            {
                array_push($countires, $gData[$i]['country_name']);
            }
            if($gData[0]['status']==1)
            {
                
                //block
                if (in_array($user_country, $countires))
                {
                        header("location:die");
                }
                else
                {

                }
            }
            else
            {
                //approve
                if (in_array($user_country, $countires))
                {

                }
                else
                {
                    header("location:die");
                }
            }

        }
    }

    public function checkIpLoc()
    {
        $this->v_aud_ip=$_SERVER['REMOTE_ADDR'];
        $location = file_get_contents('http://geoip.nekudo.com/api/'.$this->v_aud_ip);
        if(isset($location))
        {

            $user_location = json_decode($location, true);



                $city=$user_location['city'];
                $country=$user_location['country']['name'];
                $this->v_aud_loc=$city.", ".$country;

        }
        else
        {
            $this->error('ERROR',__FUNCTION__,'Somthing bad happend. Please conatct tech team');
            exit();
        }

    }

}
?>
