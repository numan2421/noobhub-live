<?php

class filter extends PDO {

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception) 
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }


    public function getgenres()
    {
    	$gQuery=$this->prepare('select * from genres where active=1');
    	$gQuery->execute();
    	if($gQuery->rowCount()>0)
    	{
    		$gData=$gQuery->fetchAll();
    		return $gData;
    	}
    }



}//end of class
?>