<?php
class home extends PDO
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception) 
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		return json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		return json_encode($success);
	}
    public function getMovies()
    {
        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,overview,imdb_rating,year(release_date) as year,runtime from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by date_added desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
            $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
            return $this->success(200,__FUNCTION__,$movieData);
            exit();
        }
    }


    public function getCelebs()
    {
        $cQuery=$this->prepare('select * FROM `video_cast` where profile_path != "" group by tmdb_cast_id order by rand() limit 30');
        $cQuery->execute();
        if($cQuery->rowCount()>0)
        {
            $celebData=$cQuery->fetchAll(PDO::FETCH_ASSOC);
            // print_r($movieData);
            return $celebData;
            exit();
        }  
    }

    public function getGrossing()
    {
      $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,overview,imdb_rating,year(release_date) as year,runtime from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by revenue desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
            $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
            // print_r($movieData);
            return json_encode($movieData);
            exit();
        }  
    }
    public function getpopularmovies2()
    {

        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,imdb_rating,year(release_date) as year,rated from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 and year(release_date) = (YEAR(CURDATE())-2) order by popularity desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
        $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($movieData);
        exit();
        }  
    }
    public function getpopularmovies1()
    {

        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,imdb_rating,year(release_date) as year,rated from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 and year(release_date) = (YEAR(CURDATE())-1) order by popularity desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
        $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($movieData);
        exit();
        }  
    }


    public function getBanner()
    {
        $getQuery=$this->prepare('select mv.imdb_id,title,year(release_date) as year,"movie" as video_type,vi.filepath as backdrop from movies mv inner join video_images vi on vi.video_id=mv.mov_id where is_active=1 and is_featured=1 and vi.image_type="backdrop" group by vi.video_id ');

        $tQuery=$this->prepare('select tmdb_id as imdb_id,name as title,year(first_air_date) as year,"tv" as video_type,vi.filepath as backdrop from tv_shows tv inner join video_images vi on vi.video_id=tv.tv_id where is_featured=1 and vi.image_type="backdrop" group by vi.video_id');

        $getQuery->execute();
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

        return $obj_merged;
    }

    public function getvotedmov()
    {

        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,imdb_rating,year(release_date) as year,rated from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by imdb_rating desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
        $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($movieData);
        exit();
        }  
    }
    public function getvotedtv()
    {
        $tQuery=$this->prepare('select tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type FROM tv_shows where is_active=1  order by imdb_rating DESC LIMIT 0,'.INDEX_ROW_LIMIT);
        $tQuery->execute();
         if($tQuery->rowCount()>0)
        {
        $tvData=$tQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($tvData);
        exit();
        }  
    }

    public function getpopulartv()
    {
        $tQuery=$this->prepare('select tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type FROM tv_shows where is_active=1  order by popularity DESC LIMIT 0,'.INDEX_ROW_LIMIT);
        $tQuery->execute();
         if($tQuery->rowCount()>0)
        {
        $tvData=$tQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($tvData);
        exit();
        }  

    }
    public function getpopulartv2()
    {
        $tQuery=$this->prepare('select tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type FROM tv_shows where is_active=1 and year(first_air_date) = (YEAR(CURDATE())-2)  order by popularity DESC LIMIT 0,'.INDEX_ROW_LIMIT);
        $tQuery->execute();
         if($tQuery->rowCount()>0)
        {
        $tvData=$tQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($tvData);
        exit();
        }  
    }

    public function getpopulartv1()
    {
        $tQuery=$this->prepare('select tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type FROM tv_shows where is_active=1 and year(first_air_date) = (YEAR(CURDATE())-1)  order by popularity DESC LIMIT 0,'.INDEX_ROW_LIMIT);
        $tQuery->execute();
         if($tQuery->rowCount()>0)
        {
        $tvData=$tQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($tvData);
        exit();
        }  
    }

    public function getpopularmovies()
    {
        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,imdb_rating,year(release_date) as year,rated from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1  order by popularity desc limit 0,'.INDEX_ROW_LIMIT);
        $getQuery->execute();
        if($getQuery->rowCount()>0)
        {
        $movieData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
        // print_r($movieData);
        return json_encode($movieData);
        exit();
        }  
    }

    public function getpopular()
    {
        $getQuery=$this->prepare('select mv.imdb_id,mov_id,poster_path,title,imdb_rating,year(release_date) as year,rated,"movie" as video_type from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by popularity desc limit 0,'.INDEX_ROW_LIMIT/2);

        $tQuery=$this->prepare('select tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type FROM tv_shows where is_active=1  order by popularity DESC LIMIT 0,'.INDEX_ROW_LIMIT/2);

        $getQuery->execute();
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

        return json_encode($obj_merged);
    }

    
    public function getlatest()
    {
        //get movies
        $getQuery=$this->prepare('select mv.imdb_id,poster_path,title,imdb_rating,year(release_date) as year,rated from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by release_date desc limit 0,'.INDEX_ROW_LIMIT/2);
        // get tv
        $tQuery=$this->prepare('select DISTINCT(ts.tv_id) as tv_id,tv.tmdb_id as imdb_id,tv.poster as poster_path,tv.name as title,vote_average as imdb_rating,year(tv.first_air_date) as year,tv.rated,ts.air_date,"tv" as video_type FROM `tv_season` ts inner join tv_shows tv on tv.tv_id=ts.tv_id where air_date < CURDATE() and tv.is_active=1 order by air_date DESC LIMIT 0,'.INDEX_ROW_LIMIT/2);


        $getQuery->execute();
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

        return json_encode($obj_merged);
    }

    public function saveSub($title,$type,$id,$lang)
    {

        $iQuery=$this->prepare('insert into video_subtitle (video_id,video_type,subtitle,uploaded_by,language) values (:vid,:vtype,:sub,:upBy,:lang)');
        $iQuery->bindParam(':vid',$id,PDO::PARAM_INT);
        $iQuery->bindParam(':vtype',$type,PDO::PARAM_STR);
        $iQuery->bindParam(':sub',$title,PDO::PARAM_INT);
        $iQuery->bindParam(':lang',$lang,PDO::PARAM_INT);
        $iQuery->bindParam(':upBy',$_SESSION['userId'],PDO::PARAM_INT);
        $iQuery->execute();
        if($iQuery->rowCount())
        {
            return 'success';
            
        }
        else
        {
            return '2';
            
        }

    }




    public function latestTv()
    {
        $lQuery=$this->prepare('select tv.tmdb_id,tse.ep_id,ts.season_number,tse.episode_number,tv.vote_average,tv.name,tv.poster,year(tse.air_date) as year,episode_name,tv.tv_id,tv.rated from tv_season_episode tse inner join tv_season ts on ts.season_id=tse.season_id INNER join tv_shows tv on tv.tv_id=ts.tv_id where tse.air_date is not null and tse.season_id is not null and tse.air_date < CURDATE() and tse.is_active=1 order by tse.air_date desc,tse.episode_number desc limit 0,'.INDEX_ROW_LIMIT);
        $lQuery->execute(); 
        if($lQuery->rowCount()>0)
        {
            $lData=$lQuery->fetchAll();
            
            return json_encode($lData);
        }
    }



}
?>