<?php

class search extends PDO
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception) 
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		return json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		return json_encode($success);
	}

    public function searchkids($quer,$type)
    {
       $alldata=array();
        $mData;
        $tvData;
        if($type=="all")
        {
            if(isset($_REQUEST['mlimit']))
            {
                 $start=$_REQUEST['mlimit'];
                 $end=SERACH_VIEW_LIMIT/2; 
            }
            else
            {
                 $start=0;
                 $end=SERACH_VIEW_LIMIT/2;
            }
            if(isset($_REQUEST['tlimit']))
            {
                 $tvstart=$_REQUEST['tlimit'];
                 $tvend=SERACH_VIEW_LIMIT/2; 
            }
            else
            {
                $tvstart=0;
                $tvend=SERACH_VIEW_LIMIT/2; 
            }
            //get mv data
            $mQuery=$this->prepare('select * from movies where title like concat("%",:tit,"%") and is_active=1 and kids=1 order by release_date desc limit '.$start.','.$end);
            $mQuery->bindParam(':tit',$quer,PDO::PARAM_STR);
            $mQuery->execute();
            if($mQuery->rowCount()>0)
            {
                $mData=$mQuery->fetchAll();
            }
            else
            {
                $mData=array();
            }
            //get tv Data
            $tQuery=$this->prepare('select * from tv_shows where name like concat("%",:tit,"%") and kids=1 and is_active=1 order by first_air_date desc limit '.$tvstart.','.$tvend);
            $tQuery->bindParam(':tit',$quer,PDO::PARAM_STR);
            $tQuery->execute();
            if($tQuery)
            {
                $tvData=$tQuery->fetchAll();
            }
            else
            {
                $tvData=array();
            }

            $alldata=array('tv'=>$tvData,'mv'=>$mData);
            return json_encode($alldata);
        }
        elseif ($type=="tv")
        {
            # code...
        }
        elseif ($type=="movie")
        {
            # code...
        } 
    }

    public function search($quer,$type)
    {

        $alldata=array();
        $mData;
        $tvData;
        if($type=="all")
        {
            if(isset($_REQUEST['mlimit']))
            {
                 $start=$_REQUEST['mlimit'];
                 $end=SERACH_VIEW_LIMIT/2; 
            }
            else
            {
                 $start=0;
                 $end=SERACH_VIEW_LIMIT/2;
            }
            if(isset($_REQUEST['tlimit']))
            {
                 $tvstart=$_REQUEST['tlimit'];
                 $tvend=SERACH_VIEW_LIMIT/2; 
            }
            else
            {
                $tvstart=0;
                $tvend=SERACH_VIEW_LIMIT/2; 
            }
            //get mv data
            $mQuery=$this->prepare('select * from movies m inner join queue q on m.live_video_id=q.video_id where title like concat("%",:tit,"%") and is_active=1 order by release_date desc limit '.$start.','.$end);
            $mQuery->bindParam(':tit',$quer,PDO::PARAM_STR);
            $mQuery->execute();
            if($mQuery->rowCount()>0)
            {
                $mData=$mQuery->fetchAll();
            }
            else
            {
                $mData=array();
            }
            //get tv Data
            $tQuery=$this->prepare('select * from tv_shows where name like concat("%",:tit,"%") and is_active=1 order by first_air_date desc limit '.$tvstart.','.$tvend);
            $tQuery->bindParam(':tit',$quer,PDO::PARAM_STR);
            $tQuery->execute();
            if($tQuery)
            {
                $tvData=$tQuery->fetchAll();
            }
            else
            {
                $tvData=array();
            }

            // Get Actors Data

            $aQuery=$this->prepare('select * from video_cast where  cast_name is not null and (tmdb_character like concat("%",:tit,"%") or cast_name like concat("%",:tit,"%")) group by tmdb_cast_id limit 0,16');
            $aQuery->bindParam(':tit',$quer,PDO::PARAM_STR);
            $aQuery->execute();
            if($aQuery->rowCount()>0)
            {
                $aData= $aQuery->fetchAll();

            }
            else
            {
                $aData=array();
            }


            $alldata=array('tv'=>$tvData,'mv'=>$mData,'actor'=>$aData);
            return json_encode($alldata);
        }
        elseif ($type=="tv")
        {
            # code...
        }
        elseif ($type=="movie")
        {
            # code...
        }
            

    }









}//end of class


?>