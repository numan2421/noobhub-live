<?php

class validation extends PDO
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception)
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }
	public function sendEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
  		//   $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.billing@yandex.com";
		// $mail->Password = "@bbtv_billing321!";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(BILLING_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility

  public function chkinKids()
  {
    $cQuery=$this->prepare('select in_kids from users where user_id=:uid');
    $cQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
    $cQuery->execute();
    if($cQuery->rowCount()>0)
    {
      $UserData=$cQuery->fetchAll();
      $user_in_kids=$UserData[0]['in_kids'];
      return $user_in_kids;
    }
    else
    {
      return 0;
    }
  }
	public function updateInKids()
	{
		$uQuey=$this->prepare('update users set in_kids=1 where user_id=:uid');
		$uQuey->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
		$uQuey->execute();

	}


// Raiminder emails are commented if needed ucomment.
    public function checkExpiry($userid)
    {
    	$lQuery=$this->prepare('select * from users where user_id=:uid and is_approved=1');
		$lQuery->bindParam(':uid',$userid,PDO::PARAM_STR);

		$lQuery->execute();
		if($lQuery->rowCount() > 0)
		{
			//chk if approved or not
				$userData=$lQuery->fetchAll(PDO::FETCH_ASSOC);
				//check if on trial and trial expired or not
				if($userData[0]['is_paid']==0)
				{
					$joinDatetime=$userData[0]['join_date'];
					$datetimearray = explode(" ", $joinDatetime);
					$date = $datetimearray[0];
					$joindate= strtotime($date);
					$today = time();
					$datediff = $today - $joindate;
					$daysDiffenrce = floor($datediff / (60 * 60 * 24));
					$trialleft=TRIAL_LIMIT-$daysDiffenrce;
					$allData=array('userData'=>$userData,'trialleft'=>$trialleft);
						if($daysDiffenrce==(TRIAL_LIMIT-1))
						{
							//send email as reminder
							//email start
							// $userEmail=$userData[0]['email'];
							// $userName=$userData[0]['f_name'];
							// $body = "";
				   //          $body .="Dear ".$userName." , <br><br> ";
				   //          $body .="Your Free Trial account at ".SITE_TITLE." is expiring tomorrow. Please register as paid user to conitnue using our services.";
				   //          $body .="<br>Regards";
				   //          $body .="<br>";
				   //          $body .="<br><br>".SITE_TITLE;
				   //          $Request=$this->sendEmail('Trial Account Reminder' , $userEmail , $body);
							//email end
							return 1;
							exit();
						}
						else if($daysDiffenrce==TRIAL_LIMIT)
						{
							//send email as reminder
							//email start
							// $userEmail=$userData[0]['email'];
							// $userName=$userData[0]['f_name'];
							// $body = "";
				   //          $body .="Dear ".$userName." , <br><br> ";
				   //          $body .="Your Free Trial account at ".SITE_TITLE." is expiring today. Please register as paid user to conitnue using our services.";
				   //          $body .="<br>Regards";
				   //          $body .="<br>";
				   //          $body .="<br><br>".SITE_TITLE;
				   //          $Request=$this->sendEmail('Trial Account Reminder' , $userEmail , $body);
							//email end
							return 1;
							exit();
						}
						else if($daysDiffenrce > TRIAL_LIMIT)
						{
							//trial expired but let go
              //send email or not
              $userEmail=$userData[0]['email'];
              $userName=$userData[0]['f_name'];
              $body = "";
                    // $body .="Dear ".$userName." , <br><br> ";
                    // $body .="Your Free Trial account at ".SITE_TITLE." is expired. Please register as paid user to conitnue using our services.";
                    // $body .="<br>Regards";
                    // $body .="<br>";
                    // $body .="<br><br>".SITE_TITLE;
                    // $Request=$this->sendEmail('Trial Account Reminder' , $userEmail , $body);
              //email end
							return 1;
							exit();
						}
						else
						{
							return 1;
							exit();
						}

				}
				else
				{
					return 1;
					exit();
				}


		}
		else
		{
			return 3;
			exit();
		}

    }

}//end class
?>
