<?php

class details extends PDO
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception)
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		return json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		return json_encode($success);
	}

// movie Details
    public function getmDetails()
    {
        $alldata=array();
        $mid=$_GET['mv_code'];
        $movId;
        $mQuery=$this->prepare('select mov_id,title,release_date,poster_path,rank,overview,runtime,live_video_id,year(release_date) as year,rated,imdb_rating,size from movies mv inner join queue q on q.VIDEO_ID=mv.live_video_id where mv.imdb_id=:mId');
        $mQuery->bindParam(':mId',$mid,PDO::PARAM_INT);
        $mQuery->execute();
        if($mQuery->rowCount()>0)
        {
            //found
            $mData=$mQuery->fetchAll();
            $movId=$mData[0]['mov_id'];
        }
        else
        {
            //no data
            $mData="";
        }
        //get genres
        $gQuery=$this->prepare('select gen_tmdb_id,title from video_genres vg inner join genres g on g.tmdb_id=vg.gen_tmdb_id where vg.video_id=:vId and vg.video_type="movie"');
        $gQuery->bindParam(':vId',$movId,PDO::PARAM_INT);
        $gQuery->execute();
        if($gQuery->rowCount()>0)
        {
            $gData=$gQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            $gData="";
        }
        //get crew
        $crQuery=$this->prepare('select name,job,profile_path from video_crew where video_type="movie" and video_id=:vid');
        $crQuery->bindParam(':vid',$movId,PDO::PARAM_INT);
        $crQuery->execute();
        if($crQuery->rowCount()>0)
        {
            $crData=$crQuery->fetchAll();
        }
        else
        {
            $crData="";
        }
        //get cast
        $caQuery=$this->prepare('select cast_id,cast_name,tmdb_character,profile_path,tmdb_cast_id from video_cast where video_type="movie" and video_id=:vid ORDER by tmdb_order limit 0,10');
        $caQuery->bindParam(':vid',$movId,PDO::PARAM_INT);
        $caQuery->execute();
        if($caQuery->rowCount()>0)
        {
            $caData=$caQuery->fetchAll();
        }
        else
        {
            $caData="";
        }
        //get trailor
        $tquery=$this->prepare('select site_key from video_intro_videos where video_id=:vid and video_type="movie" and type="Trailer"');
        $tquery->bindParam(':vid',$movId,PDO::PARAM_INT);
        $tquery->execute();
        if($tquery->rowCount()>0)
        {
            $tData=$tquery->fetchAll();
        }
        else
        {
            $tData='';
        }
        //getbackdrop
        $bQuery=$this->prepare('select filepath from `video_images` where video_id=:vid and image_type="backdrop" and video_type="movie"');
        $bQuery->bindParam(':vid',$movId,PDO::PARAM_INT);
        $bQuery->execute();
        if($bQuery->rowCount()>0)
        {

            $backData=$bQuery->fetchAll();
            $randData=array_rand($backData,1);
            $bData=$backData[$randData];
        }
        else
        {
            $bData="";
        }
        //get subtitles
        $type="movies";


        $sQuery=$this->prepare('select * from video_subtitle where  video_type=:vtype and video_id=:vid order by date_added desc');
        
        $sQuery->bindParam(':vid',$mid,PDO::PARAM_STR);
        $sQuery->bindParam(':vtype',$type,PDO::PARAM_STR);
        $sQuery->execute();

        if($sQuery->rowCount()>0)
        {

            $subData=$sQuery->fetchAll();
            
        }
        else
        {
           
            $subData="";
        }

        //get related
        if($gData!='')
        {
            $allgens=array();
            for($k=0;$k<count($gData);$k++)
            {
                array_push($allgens, $gData[$k]['gen_tmdb_id']);
            }
            $finder=implode(',', $allgens);
            $rQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year, mv.runtime,mv.rank,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from movies mv inner join queue qu on mv.live_video_id = qu.video_id inner join video_genres vg on vg.video_id = mv.mov_id where vg.gen_tmdb_id in ('.$finder.') and vg.video_type="movie" and mv.mov_id <> :mid and mv.is_active=1
order by release_date desc limit 0,12');
            $rQuery->bindParam(':mid',$movId,PDO::PARAM_INT);
            $rQuery->execute();
            if($rQuery->rowCount()>0)
            {
                $rData=$rQuery->fetchAll();
            }
        }
        else
        {
            $rData="";
            //get related by somthing else
        }

        //get video file 
        $vQuery=$this->prepare('select * from videos where type="movie" and imdb_id=:iid');
        $vQuery->bindParam(':iid',$mid,PDO::PARAM_STR);
        $vQuery->execute();

        if($vQuery->rowCount()>0)
        {
            $videoData=$vQuery->fetchAll();
            $bPath=$videoData[0]['base_path'];
            $scid=$videoData[0]['scid'];
            $videourl=$bPath.$scid.".mp4";
         
        }
        else
        {
            $videourl="";
        }






        $alldata=array('mdetails'=>$mData,'genre'=>$gData,'crew'=>$crData,'cast'=>$caData,'back'=>$bData,'trailer'=>$tData,'related'=>$rData,'video'=>$videourl,'subtitle'=>$subData);

        return json_encode($alldata);
    }

    // show Detials
    public function gettDetails()
    {
        $alldata=array();
        //basic info
        $mid=$_GET['tv_code'];
        $mQuery=$this->prepare('select tv_id,name,first_air_date,poster,vote_average,rated,overview from tv_shows where tmdb_id=:mId');
        $mQuery->bindParam(':mId',$mid,PDO::PARAM_INT);
        $mQuery->execute();
        if($mQuery->rowCount()>0)
        {
            
            //found
            $mData=$mQuery->fetchAll();
            $tv_id=$mData[0]['tv_id'];
        }
        else
        {
    
            //no data
            $mData="";
        }

         //get genres
        $gQuery=$this->prepare('select gen_tmdb_id,title from video_genres vg inner join genres g on g.tmdb_id=vg.gen_tmdb_id where vg.video_id=:vId and vg.video_type="tv"');
        $gQuery->bindParam(':vId',$tv_id,PDO::PARAM_INT);
        $gQuery->execute();
        if($gQuery->rowCount()>0)
        {
            $gData=$gQuery->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            $gData="";
        }
     
          //get cast
        $caQuery=$this->prepare('select cast_id,cast_name,tmdb_character,profile_path,tmdb_cast_id from video_cast where video_type="tv" and video_id=:vid ORDER by tmdb_order limit 0,10');
        $caQuery->bindParam(':vid',$tv_id,PDO::PARAM_INT);
        $caQuery->execute();
        if($caQuery->rowCount()>0)
        {
            $caData=$caQuery->fetchAll();
        }
        else
        {
            $caData="";
        }
        //get trailor
        $tquery=$this->prepare('select site_key from video_intro_videos where video_id=:vid and video_type="tv" and type="Trailer"');
        $tquery->bindParam(':vid',$tv_id,PDO::PARAM_INT);
        $tquery->execute();
        if($tquery->rowCount()>0)
        {
            $tData=$tquery->fetchAll();
        }
        else
        {
            $tData='';
        }
         //getbackdrop
        $bQuery=$this->prepare('select filepath from `video_images` where video_id=:vid and image_type="backdrop" and video_type="tv"');
        $bQuery->bindParam(':vid',$tv_id,PDO::PARAM_INT);
        $bQuery->execute();
        if($bQuery->rowCount()>0)
        {
            $backData=$bQuery->fetchAll();
            $randData=array_rand($backData,1);
            $bData=$backData[$randData];
            // print_r($bData);

        }
        else
        {
            $bData="";
        }

 

         //get related
        if($gData!='')
        {
            $allgens=array();
            for($k=0;$k<count($gData);$k++)
            {
                array_push($allgens, $gData[$k]['gen_tmdb_id']);
            }
            $finder=implode(',', $allgens);
            $rQuery=$this->prepare('select distinct mv.name,mv.tmdb_id,year(mv.first_air_date) as year, mv.vote_average,mv.tv_id,mv.poster,mv.overview,video_type from tv_shows mv  inner join video_genres vg on vg.video_id = mv.tv_id where vg.gen_tmdb_id in ('.$finder.') and vg.video_type="tv" and mv.tv_id <> :mid and mv.is_active=1
order by first_air_date desc limit 0,12');
            $rQuery->bindParam(':mid',$tv_id,PDO::PARAM_INT);
            $rQuery->execute();
            if($rQuery->rowCount()>0)
            {
                $rData=$rQuery->fetchAll();
            }
            else
            {
                echo 'fucked';
            }
        }
        else
        {
            $rData="";
            //get related by somthing else
        }
       



        //get season details
        $sQuery=$this->prepare('select distinct * from tv_season where tv_id=:tid group by season_number order by season_number');
        $sQuery->bindParam(':tid',$tv_id,PDO::PARAM_INT);
        $sQuery->execute();

        if($sQuery->rowCount()>0)
        {
            $sData=$sQuery->fetchAll();
     
            if($sData[0]['season_number']==0)
            {
                $epdetails=$this->getepisodes($sData[count($sData)-1]['season_id']);
            }
            else
            {
                // echo getepisodes($sData[count($sData)]['season_id'];
                $epdetails=$this->getepisodes($sData[count($sData)-1]['season_id']);
            }

        }
        else
        {
            $sData='';
        }




        $alldata=array('mdetails'=>$mData,'genre'=>$gData,'cast'=>$caData,'back'=>$bData,'trailer'=>$tData,'seasons'=>$sData,'first_episode'=>$epdetails,'related'=>$rData);
        return json_encode($alldata);
    }

    public function getPerson($person_id)
    {
        //get Tv and movies details and merge to show 1 


        $cQuery=$this->prepare('select * from video_cast where tmdb_cast_id=:tid limit 0,1');
        $cQuery->bindParam(':tid',$person_id,PDO::PARAM_INT);
        $cQuery->execute();


        $cData=$cQuery->fetchAll();


        $pQuer=$this->prepare('select distinct mov_id,q.IMDB_ID as imdb_id,title,release_date,poster_path,rank,overview,runtime,live_video_id,year(release_date) as year,rated,imdb_rating,size,"movie" as video_type from video_cast vc inner join  movies mv on vc.video_id=mv.mov_id inner join queue q on q.VIDEO_ID=mv.live_video_id where tmdb_cast_id=:cid');



        $tQuery=$this->prepare('select distinct tv_id as mov_id,vote_average as imdb_rating,tv.tmdb_id as imdb_id,name as title,first_air_date as release_date,poster as poster_path,rated as rank,overview,year(first_air_date) as year,"tv" as video_type from video_cast vc inner join  tv_shows tv on vc.video_id=tv.tv_id  where tmdb_cast_id=:cid');
        //movies Data
        $pQuer->bindParam(':cid',$person_id,PDO::PARAM_INT);
        $tQuery->bindParam(':cid',$person_id,PDO::PARAM_INT);
        $pQuer->execute();
        $tQuery->execute();


        $mData=$pQuer->fetchAll();
        // $tData=[];
        $tData=$tQuery->fetchAll();

  

        $obj_merged=array_merge($mData,$tData);
        shuffle($obj_merged);


        $alldata=array('cast'=>$cData,'video'=>$obj_merged);

        return  $alldata; 
    }


    public function getepisodes($season_id)
    {

        $eQuery=$this->prepare('select  tmdb_id,ep_id,poster_path,overview,episode_name,episode_number,is_active,scid,air_date,base_path,tq.size from tv_season_episode tse left join videos v on v.imdb_id=tse.tmdb_id left join tvqueue tq on tq.video_id=v.scid  where season_id=:sid  group by tmdb_id order by episode_number');
        $eQuery->bindParam(':sid',$season_id,PDO::PARAM_INT);
        $eQuery->execute();

        if($eQuery->rowCount()>0)
        {

            $episodes=array();
            $eData=$eQuery->fetchAll();

            //check if have remainig time
            for($i=0;$i<count($eData);$i++)
            {
                $cQuery=$this->prepare('select * from user_watchlist where video_type="tv" and video_id=:vid and prof_id=:pid');
                $cQuery->bindParam(':vid',$eData[$i]['ep_id'],PDO::PARAM_INT);
                $cQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_STR);
                $cQuery->execute();
                if($cQuery->rowCount()>0)
                {

                    $videoData=$cQuery->fetchAll();
                    $video_duration=$videoData[0]['video_duration'];
                    $video_remaining=$videoData[0]['video_remaining'];
                    $eData[$i]['video_remaining']=$video_remaining;
                    $eData[$i]['video_duration']=$video_duration;
                    array_push($episodes, $eData[$i]);

                }
                else
                {
                    $video_duration=null;
                    $video_remaining=null;
                    $eData[$i]['video_remaining']=$video_remaining;
                    $eData[$i]['video_duration']=$video_duration;
                    array_push($episodes, $eData[$i]);
                }

              
            }
            return $eData;
        }
        else
        {

        }
    }
}
?>
