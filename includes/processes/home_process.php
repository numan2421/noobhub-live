	
<?php
include_once 'includes/config.php';

include_once 'includes/classes/home_class.php';

//get first 20 moviwa from all movies
	$homeObject=new home(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);

	$bannerData=$homeObject->getBanner();

	$allbannerbody=showbannerData($bannerData);


	$celebData=$homeObject->getCelebs();
	$celebBody=showCeleb($celebData);


	$moviesData=$homeObject->getMovies();
	$allData=json_decode($moviesData);
	$allMoviesData=$allData->response;
	$allmoviebody=showData($allMoviesData);
	

	//trending
	$trendData=json_decode($homeObject->getlatest());
	$trendingBody=showData($trendData);

	//popular title
	$popData=json_decode($homeObject->getpopular());
	$popbody=showData($popData);

	//highst grossing
	$grosData=json_decode($homeObject->getGrossing());
	$grossbody=showData($grosData);


	//popular movies
	$popmovData=json_decode($homeObject->getpopularmovies());
	$popmovbody=showData($popmovData);


	//popular movies in 2 year back from now
	$popmovData2=json_decode($homeObject->getpopularmovies2());
	$popmov2body=showData($popmovData2);


	//popular movies in 1 year back from now
	$popmovData1=json_decode($homeObject->getpopularmovies1());
	$popmov1body=showData($popmovData1);
	

	//most voted movies
	$votedData=json_decode($homeObject->getvotedmov());
	$votedBody=showData($votedData);



	//most popular tv_shows
	$poptvData=json_decode($homeObject->getpopulartv());
	$poptvbody=showData($poptvData);


	//most popular tv_shows -2 years from now
	$poptvData2=json_decode($homeObject->getpopulartv2());
	$poptv2body=showData($poptvData2);

		//most popular tv_shows -1 years from now
	$poptvData1=json_decode($homeObject->getpopulartv1());
	$poptv1body=showData($poptvData1);

	//most voted tv
	$votedtvData=json_decode($homeObject->getvotedtv());
	$votedtvBody=showData($votedtvData);

	// latest Tv 
	$ltv=json_decode($homeObject->latestTv());
	$lBody=showTvepisodes($ltv);

function showCeleb($data)
{
	$body='';
	for($i=0;$i<count($data);$i++)
	{

		$castname=preg_replace('/[^A-Za-z0-9\-]/', '', $data[$i]['cast_name']);
		$body .='<li><div class="item"><a class="person-celeb" title="'.$data[$i]['cast_name'].'" href="person/'.$data[$i]['tmdb_cast_id'].'/'.$castname.'"><img class="pavatar" src="https://image.tmdb.org/t/p/w150_and_h150_bestv2/'.$data[$i]['profile_path'].'" alt="'.$data[$i]['cast_name'].'"><person-name>'.$data[$i]['cast_name'].'</person-name></a></div>
						</li>';
	}
	return $body;
}

function showbannerData($Data)
{

	$body='';
	for($i=0;$i<count($Data);$i++)
	{


			if(isset($Data[$i]['video_type']) && $Data[$i]['video_type']=='tv')
			{
				$url='tv/'.$Data[$i]['imdb_id'].'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]['title'])));
			}
			else
			{
				$url='movies/'.$Data[$i]['imdb_id'].'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]['title'])));
			}
		$body .='<li title="movie_name">
					 <a href="'.$url.'">
					 	<div class="wrapper">  
					
						    <img  title="'.$Data[$i]['title'].'" src="https://image.tmdb.org/t/p/w780'.$Data[$i]['backdrop'].'" class=" image" alt="'.$Data[$i]['title'].'">
					
						    <div class="description">  
						       
						        <h3 class="description_content">'.$Data[$i]['title'].' <span class="year">('.$Data[$i]['year'].')</span></h3>  
						        
						    </div>  
						    <!-- end description div -->  
						</div> 
					 </a>
			  	</li>';
	}
		return $body;
}


function showTvepisodes($tv)
{
	$body='';
	
		$displayed=array();
		for($i=0;$i<count($tv);$i++)
		{
			if(in_array($tv[$i]->tv_id, $displayed))
			{

			}
			else
			{
			array_push($displayed, $tv[$i]->tv_id);
			$sNUmber = sprintf("%02d", $tv[$i]->season_number);
			$eNumber = sprintf("%02d", $tv[$i]->episode_number);

			$body .='<li><div class="item"><a href="tv/'.$tv[$i]->tmdb_id.'/season-'.$sNUmber.'/episode-'.$eNumber.'"><img width="132px" title="'.$tv[$i]->name.'" src="https://image.tmdb.org/t/p/w342'.$tv[$i]->poster.'" class=" image" alt="'.$tv[$i]->name.'"></a><a href="tv/'.$tv[$i]->tmdb_id.'/season-'.$sNUmber.'/episode-'.$eNumber.'"><div class="overlay"><span><h3>'.$tv[$i]->name.' </h3><small>('.$tv[$i]->year.')</small></span><div class="overlayFooter"><p class="seasonInfo">
								S'.$sNUmber.'E'.$eNumber.'
								</p>';
			if(isset($Data[$i]->rated))
			{
				$body.='<span class="rated">'.$tv[$i]->rated.'</span>';
			}
			else
			{
				
			}
			$body.='<span class="score"><i class="fa fa-star"></i> '.$tv[$i]->vote_average.'</span></div></div></a>
						</div></li>';
	       	}
		}
	
 
	return $body;

}
 function showData($Data)
 {
 		$body='';
	 	for($i=0;$i<count($Data);$i++)
		{
			
			$rating=($Data[$i]->imdb_rating/10)*100;
			$length=str_word_count($Data[$i]->title);
			if($length > 6)
			{
				$text=$Data[$i]->title;
				$limit=6;
				if (str_word_count($text, 0) > $limit) {
			          $words = str_word_count($text, 2);
			          $pos = array_keys($words);
			          $text = substr($text, 0, $pos[$limit]) . '...';
			      }
			      $title=$text;
			}
			else
			{
				$title=$Data[$i]->title;
			}
			if(isset($Data[$i]->video_type) && $Data[$i]->video_type=='tv')
			{
				$url='tv/'.$Data[$i]->imdb_id.'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]->title)));
			}
			else
			{
				$url='movies/'.$Data[$i]->imdb_id.'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]->title)));
			}
			
					$body .='<li title="'.$Data[$i]->title.' ('.$Data[$i]->year.')"><div class="item"><a href="'.$url.'"><img width="132px" title="'.$Data[$i]->title.'" src="https://image.tmdb.org/t/p/w342'.$Data[$i]->poster_path.'" class=" image" alt="'.$Data[$i]->title.'"></a><a href="'.$url.'"><div class="overlay"><span><h3>'.$title.' </h3><small>('.$Data[$i]->year.')</small></span><div class="overlayFooter">';
			if(isset($Data[$i]->rated))
			{
				$body.='<span class="rated">'.$Data[$i]->rated.'</span>';
			}
			else
			{
				$body.='<span class="rated">N/A</span>';	
			}
					$body.='<span class="score"><i class="fa fa-star"></i> '.$Data[$i]->imdb_rating.'</span></div></div></a>
						</div></li>';
		}
	
	return $body;
 }
?>		