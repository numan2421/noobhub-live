<?php
include_once 'includes/config.php';
include_once 'includes/classes/details_class.php';

	$dObject=new details(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	$edata=$dObject->getPerson($_REQUEST['person_id']);

	$videoData=$edata['video'];
	$cast=$edata['cast'][0];

	$allbody=showData($videoData,$absolutepath);
	// echo $allbody;


	$tBody ='<a href="javascript:void(0)"><img class="pavatar" src="https://image.tmdb.org/t/p/w150_and_h150_bestv2/'.$cast['profile_path'].'" alt="'.$cast['cast_name'].'"></a><h2>'.$cast['cast_name'].'</h2>';
	
	function showData($Data)
 	{
 		$body='';
 		
	 	for($i=0;$i<count($Data);$i++)
		{
			
			$rating=($Data[$i]['imdb_rating']/10)*100;
			$length=str_word_count($Data[$i]['title']);
			if($length > 6)
			{
				$text=$Data[$i]['title'];
				$limit=6;
				if (str_word_count($text, 0) > $limit) {
			          $words = str_word_count($text, 2);
			          $pos = array_keys($words);
			          $text = substr($text, 0, $pos[$limit]) . '...';
			      }
			      $title=$text;
			}
			else
			{
				$title=$Data[$i]['title'];
			}
			if(isset($Data[$i]['video_type']) && $Data[$i]['video_type']=='tv')
			{
				$url='../../tv/'.$Data[$i]['imdb_id'].'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]['title'])));
			}
			else
			{
				$url='../../movies/tt'.$Data[$i]['imdb_id'].'/'.str_replace(" ", "-", str_replace(".","",urlencode($Data[$i]['title'])));
			}
			if(isset($Data[$i]['rated']))
			{
				$rated=$Data[$i]['rated'];
			}
			else
			{
				$rated='N/A';	
			}
				$body .='<div title="'.$title.' ('.$Data[$i]['year'].')" class="item"><a href="'.$url.'"><img title="'.$title.'" src="https://image.tmdb.org/t/p/w342'.$Data[$i]['poster_path'].'" class=" image" alt="'.$title.'" width="132px"></a><a href="'.$url.'"><div class="overlay"><span><h3>'.$title.' </h3><small>('.$Data[$i]['year'].')</small></span><div class="overlayFooter" style="bottom:0"><span class="rated">'.$rated.'</span><span class="score"><i class="fa fa-star"></i> '.$Data[$i]['imdb_rating'].'</span></div></div></a></div>';
		}
	
	return $body;
 }



?>