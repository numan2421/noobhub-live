<?php
include_once 'includes/config.php';
include_once 'includes/classes/validation_class.php';
include_once 'api/classes/analytic_class.php';
require('includes/mailer/class.phpmailer.php');
require('includes/mailer/class.smtp.php');
//chk for account expiry
function chkExpiry($userId)
{
	$arrPDODriverOptions = array(PDO::ATTR_PERSISTENT => false,  PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

	$sObject=new validation(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	$accountCheck=$sObject->checkExpiry($userId);
	if($accountCheck==1)
	{
		return 1;
	}
	elseif ($accountCheck==2)
	{
		//user expired but let go in
		$exTime = time() - (60 * 60 * 24 * 7);
		setcookie("UserData", "", $exTime, '/');
		session_destroy();
		return 2;
	}
	else
	{
		return 3;
	}
}
function chkinKids()
{
	$arrPDODriverOptions = array(PDO::ATTR_PERSISTENT => false,  PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

	$sObject=new validation(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	return $sObject->chkinKids();

}

function addvisit()
{

	$arrPDODriverOptions = array(PDO::ATTR_PERSISTENT => false,  PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
	$sObject=new analytic(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	return $sObject->addvisit();
}


?>
