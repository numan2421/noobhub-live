<?php
include_once 'includes/config.php';
include_once 'includes/classes/details_class.php';
include_once 'api/classes/analytic_class.php';


$dObject=new details(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
$anObject=new analytic(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
if(isset($_GET['mv_code']))
{


	$anObject->videoClick('movie',$_SERVER['HTTP_REFERER'],$_GET['mv_code']);

	$basic_body='';
	$castBody='';
	$relatedBody='';
	
	$movie_details=json_decode($dObject->getmDetails());
	
	// Returned Data from class
	$mDetails=$movie_details->mdetails[0];
	$cast=$movie_details->cast;
	$crew=$movie_details->crew;
	$genre=$movie_details->genre;
	$video=$movie_details->video;
	$subtitle=$movie_details->subtitle;
	
	$backDrop=$movie_details->back;
	if(isset($movie_details->trailer[0]))
	{
		$trailer=$movie_details->trailer[0];
		$trailerBody='<a href="javascript:void(0)" onclick="showTrailer(\''.$mDetails->title.'\')"class="playhide"><button><i class="fa fa-youtube" aria-hidden="true"></i> Trailer</button></a>';
	}
	else
	{
		$trailer='';
		$trailerBody='';
	}

	$related=$movie_details->related;
	if(isset($backDrop->filepath) && $backDrop->filepath !="")
	{
		$backDrop_path='https://image.tmdb.org/t/p/w1280'.$backDrop->filepath;
	}
	else
	{
		$backDrop_path='';
	}
		//display basic info
		$allgen='';
		$mId=$mDetails->mov_id;
		$director='';

		$videolink_id=$mDetails->live_video_id;
		$title=$mDetails->title;
		$Imdb_rating=$mDetails->imdb_rating;
		$ryear=$mDetails->year;
		$size=$mDetails->size;
		if($mDetails->rated && $mDetails->rated != Null){
			$rated=$mDetails->rated;
		}
		else
		{
			$rated="N/A";
		}
		
		$rDate=explode('-',$mDetails->release_date)[0];
		$poster='https://image.tmdb.org/t/p/w1280'.$mDetails->poster_path;
		$rank=$mDetails->rank;
		$rank=($rank/10)*100;
			$t = $mDetails->runtime;
			
			$hour = floor($t/60) ? floor($t/60) .'h' : '';
			$minutes = $t%60 ? $t%60 .'m' : '';

			$overview=$mDetails->overview;
		//genres
		$allgen='';
		if($genre=="")
		{
			//no genres saved
			$allgen .='<a href="javascript:void(0)">N/A</a>';
		}
		else
		{
			
			for($i=0;$i<count($genre);$i++)
			{
				$allgen.='<a href="javascript:void(0)">'.$genre[$i]->title.'</a>';
			}
		}
		//cast get director
		if($crew!='')
		{
			for($j=0;$j<count($crew);$j++)
			{
				if($crew[$j]->job=="Director")
				{
					$director=$crew[$j]->name;
				}
			}
		}
		  	if($cast != '')
		  	{
		  		$castBody .='<div id="cast_details" class="sliderDiv">
						<h3>Cast</h3>
						<ul id="lightSlider3">';
						
		  		for($l=0;$l<count($cast);$l++)
		  		{

		  			$castname=preg_replace('/[^A-Za-z0-9\-]/', '', $cast[$l]->cast_name);
			  		if($cast[$l]->profile_path != "")
			  		{
			  			$castBody .='<li><div class="person">
				  		<a href="'.$absolutepath.'person/'.$cast[$l]->tmdb_cast_id.'/'. $castname.'"><img src="https://image.tmdb.org/t/p/w185/'.$cast[$l]->profile_path.'"></a>';
			  		}
			  		else
			  		{
			  			$initials=strtoupper(substr($cast[$l]->cast_name, 0, 2));
			  			$castBody .='<li><div class="person not-found">
				  		<a href="'.$absolutepath.'person/'.$cast[$l]->tmdb_cast_id.'/'. $castname.'"><span class="initials">'.$initials.'</span></a>';
			  		}

					$castBody .='<h2>'.$cast[$l]->cast_name.'</h2><p>'.$cast[$l]->tmdb_character.'</p><div></li>';
			  	}	
			  	$castBody .='	
						</ul>
					</div>';
		  	}
		  	else
		  	{
		  		$castBody.='';
		  	}
		  	
	 
	 // show related Data

	  	$relatedBody='';

	  	if($related != '')
	  	{
	  		$relatedBody.='<h3>You May Also like </h3>
				<ul id="lightSlider">';
	  		for($m=0;$m<count($related);$m++)
		  	{
				$relatedBody.='<li><a href="'.$absolutepath.'movies/'.$related[$m]->imdb_id.'/'.str_replace(" ", "-", str_replace(".", "", $related[$m]->title)).'"><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/'.$related[$m]->poster_path.'"></a></li>';

		  	}
		  	$relatedBody.='</ul>';
	  	}
	  	else
	  	{
	  		$relatedBody.='';
	  	}

	  




}







?>
