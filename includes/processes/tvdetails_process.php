<!-- get all information to be displayed on tv details page -->
<?php
include_once 'includes/config.php';
include_once 'includes/classes/details_class.php';
include_once 'api/classes/analytic_class.php';


$dObject=new details(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
$anObject=new analytic(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
if(isset($_REQUEST['chk']))
{
	
}
else
{
	if(isset($_GET['tv_code']))
	{
		$anObject->videoClick('tv',$_SERVER['HTTP_REFERER'],$_GET['tv_code']);
		$detect = new Mobile_Detect();
		$basic_body='';
		$castBody='';
		$sBody='';
		$eBody='';
		$tv_details=json_decode($dObject->gettDetails());

		// returned Data from class
		$mDetails=$tv_details->mdetails[0];
		$cast=$tv_details->cast;
		$genre=$tv_details->genre;
		$backDrop=$tv_details->back;
		$related=$tv_details->related;

		// trialer
		if($tv_details->trailer != "")
		{
			$trailer=$tv_details->trailer[0];
		}
		else
		{

		}

		// show Detials
		$seasons=$tv_details->seasons;
		$episodes=$tv_details->first_episode;
		$backDrop_path='https://image.tmdb.org/t/p/w1280'.$backDrop->filepath;

		$allgen='';
		$director='';
		$title=$mDetails->name;
		$showid=$mDetails->tv_id;
		if($mDetails->rated=="")
		{
			$rated="N/A";
		}
		else
		{
			$rated=$mDetails->rated;
		}
		
		$rDate=explode('-',$mDetails->first_air_date)[0];
		if($mDetails->poster==null)
		{
			$poster=$absolutepath.'admin/assets/img/blank_150.png';
		}
		else
		{
			$poster='https://image.tmdb.org/t/p/w300_and_h450_bestv2'.$mDetails->poster;
		}
		$rank=$mDetails->vote_average;
		
		$overview=$mDetails->overview;
		if($genre=="")
		{
			//no genres saved
			$allgen .='<a href="javascript:void(0)">N/A</a>';
		}
		else
		{	
			for($i=0;$i<count($genre);$i++)
			{
				$allgen.='<a href="javascript:void(0)">'.$genre[$i]->title.'</a>';
			}
		}


			//display cast
		  	if($cast != '')
		  	{
		  		$castBody .='<div id="cast_details" class="sliderDiv">
						<h3>Cast</h3>
						<ul id="lightSlider3">';
						
		  		for($l=0;$l<count($cast);$l++)
		  		{
		  			$castname=preg_replace('/[^A-Za-z0-9\-]/', '', $cast[$l]->cast_name);
			  		if($cast[$l]->profile_path != "")
			  		{
			  			$castBody .='<li><div class="person">
				  		<a href="'.$absolutepath.'person/'.$cast[$l]->tmdb_cast_id.'/'. $castname.'"><img src="https://image.tmdb.org/t/p/w185/'.$cast[$l]->profile_path.'"></a>';
			  		}
			  		else
			  		{
			  			$initials=strtoupper(substr($cast[$l]->cast_name, 0, 2));
			  			$castBody .='<li><div class="person not-found">
				  		<a href="'.$absolutepath.'person/'.$cast[$l]->tmdb_cast_id.'/'. $castname.'"><span class="initials">'.$initials.'</span></a>';
			  		}

					$castBody .='<h2>'.$cast[$l]->cast_name.'</h2><p>'.$cast[$l]->tmdb_character.'</p><div></li>';
			  	}	
			  	$castBody .='	
						</ul>
					</div>';
		  	}
		  	else
		  	{
		  		$castBody.='';
		  	}
			

			 //display show related Data
			$relatedBody='';
		  	if($related != '')
		  	{
		  		$relatedBody.='<h3>You May Also like </h3>
					<ul id="lightSlider">';
		  		for($m=0;$m<count($related);$m++)
			  	{
					$relatedBody.='<li><a href="'.$absolutepath.'tv/'.$related[$m]->tmdb_id.'/'.str_replace(" ", "-", $related[$m]->name).'"><img width="150" height="200" src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/'.$related[$m]->poster.'"></a></li>';

			  	}
			  	$relatedBody.='</ul>';
		  	}
		  	else
		  	{
		  		$relatedBody.='';
		  	}


		  	//display seasons
		  	for($r=0;$r<count($seasons);$r++)
		  	{
		  		if($seasons[$r]->season_number==0)
		  		{

		  		}
		  		else
		  		{
			  		if($seasons[$r]->air_date)
			  		{
				  		$time = strtotime($seasons[$r]->air_date);
				  		$newformat = date('Y-m-d',$time);
				  		$dateData=explode("-", $newformat);
				  		$year=$dateData[0];
				  		$month=substr(date("F", strtotime(date("Y") ."-". $dateData[1] ."-01")),0,3);
				  		$season_air_date=$month."-".$year;
				  		if($newformat>date('Y-m-d'))
				  		{
				  			//upcoming seasons
				  		}
				  		else
				  		{
				  			
				  			if($r==count($seasons)-1)
				  			
				  			{
				  				$sBody.='<a class="selected" href="javascript:void(0)" onclick="getepisodes('.$seasons[$r]->season_id.',this)">'.$seasons[$r]->season_number.'</a>';
				  			}
				  			else
				  			{
				  				$sBody.='<a href="javascript:void(0)" onclick="getepisodes('.$seasons[$r]->season_id.',this)">'.$seasons[$r]->season_number.'</a>';
				  			}

				  		}

				  	}
			  	}

		  	}



		//display episodes
		if(count($episodes)>0)
	  	{
	  		$upcoming=0;
	  		
		  	for ($k=0; $k < count($episodes); $k++)
		  	{
		  	

		  			if($episodes[$k]->poster_path==null)
					{
						$poster=$absolutepath.'includes/img/blank_150.png';
					}
					else
					{
						$poster='https://image.tmdb.org/t/p/w300'.$episodes[$k]->poster_path;
					}
					
				if($episodes[$k]->is_active==1)
				{

					$eBody.='<div class="hover10 column" id="episode">
						<figure><a href="javascript:void(0)" ><img width="300" height="170" src="'.$poster.'"></a></figure>
						<h2>'.$episodes[$k]->episode_number.'. '.$episodes[$k]->episode_name.'</h2>
						<p class="ttip">'.$episodes[$k]->air_date.'</p>
							<div class="play_buttons">
								<div class="row  downshow">
								<a href="javascript:void(0)" onclick="play(\''.$episodes[$k]->base_path.'\','.$episodes[$k]->scid.',\''.addslashes($episodes[$k]->episode_name).'\','.$episodes[$k]->episode_number.',\''.addslashes($title).'\','.$episodes[$k]->tmdb_id.');">
									<div class="col-md-6" id="play_button">	
									<i class="fa fa-2x fa-play-circle-o"></i>
									Play
									</div>
								</a>
								<a href="javascript:void(0)" class="downLink">
									<div class="col-md-6" id="down_button">
										<i class="fa fa-2x fa-download"></i>
									Download
									</div>
								</a>
							</div>


							<div class="row downhide">
								<div class="">
									<a href="'.$absolutepath.'download/tv/'.$episodes[$k]->ep_id.'"  title="Download '.$episodes[$k]->episode_name.'" >
										<div class="col-md-12" style="text-align:center">
											<i class="fa fa-2x fa-download"></i>
											'.$episodes[$k]->size.' - .mp4
										</div>
									</a>
								</div>
							</div>
						
								
									
								</div>
						
						</div>';
				}
				else
				{
					if($upcoming==0)
					{
						$eBody.='<div class="hover10 column" id="episode">
						<figure><a href="javascript:void(0)" ><img width="300" height="170" src="'.$poster.'"></a></figure>
						<h2>'.$episodes[$k]->episode_number.'. '.$episodes[$k]->episode_name.'</h2>
						<p class="ttip">Airing '.$episodes[$k]->air_date.'</p>	</div></div>';
						$upcoming=1;
					}
				}	
		  	}
	  	}
		else
		{
			$eBody.="<h1>No Episodes Found Yet.</h1>";
		} //Episode end
	}
}

?>
