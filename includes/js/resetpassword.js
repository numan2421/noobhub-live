$(function()
{
	$('.alert').hide();
	// client sided form validation
	$('#reset_form').validate(
	{
		rules:
		{
			new_password:
			{
				required:true,
				minlength:6
			},
			confirm_password:
			{
				required:true,
				minlength:6,
				equalTo:'#new_password'
			}
		},
		messages:
		{
			new_password:
			{
				required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Please add new passwrd',
				minlength:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Password should be minimum 6'
			},
			confirm_password:
			{
				required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; Please confirm Password',
				minlength:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; Password should be minimum 6',
				equalTo:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Password doen\'\nt match'
			}

		},
		highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element) 
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	$.post(Apath+'api/v1/user/resetPassword',$('#reset_form').serialize(),function showData(data)
        	{
        		if(data.status==200)
        		{
        			$('.success-msg').html('Your password has been reset to new. Click <a href='+Apath+'index>here</a> to login');
        			$('.alert').hide();
                    $('.alert-success').show();
        		}
        		else
        		{
        			$('.error-msg').html(data.description);
        			$('.alert').hide();
        			$('.alert-danger').show();
        		}
        	})
        }
	})
})