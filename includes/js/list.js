var sorterData=[];
var resultData=[];
$(function()
{
	//get my list data
	$.get(Apath+'api/v1/user/getmylist',function showData(data)
	{
		if(data.status==200)
		{
			alldata=data.response;
		
			
			showdata(alldata,1);
					for(k=0;k<alldata.length;k++)
					{
						sorterData.push(alldata[k]);
						resultData.push(alldata[k]);
					}
		}
		else
		{
			$('#my-list').html("<h1 style='text-align:center;'>You didn't added anything yet.</h1>");
			$('#sorter_div').hide();
		}
	})





})

// filters

function filter(factor,param)
{
	$(param).parent().children().removeClass('activefilter');
	$(param).addClass('activefilter');
	if(factor=="1")
	{
		sorterData.sort(compare_by_lastet);
		showdata(sorterData,1);
	}
	else if(factor=="2")
	{
		// console.log(sorterData);
		sorterData.sort(popularity);
		showdata(sorterData,1);
		showdata(sorterData,1);
	}
	else if(factor=="3")
	{
		sorterData.sort(date_added);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
	else if(factor=="4")
	{
		sorterData.sort(compare_by_title_asc);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
	else if(factor=="5")
	{
		sorterData.sort(compare_by_title_desc);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
}


function typefilter(factor,param)
{
	$('.all a').removeClass('activefilter');
	$(param).addClass('activefilter');
		displayer=factor;
		if(displayer==1)
		{
			sorterData=resultData;
			showdata(resultData,1);
		}
		else if(displayer==2)
		{
			tvonly=[];
			//tv only
			for(var j=0;j<resultData.length;j++)
			{
				if(resultData[j]['video_type']=="tv")
				{
					tvonly.push(resultData[j]);
				}
			}
			sorterData=tvonly;
			showdata(tvonly,1);
		}
		else if(displayer==3)
		{
			mvonly=[];
			for(var j=0;j<resultData.length;j++)
			{
				if(resultData[j]['video_type']=="movie")
				{
					mvonly.push(resultData[j]);
				}
			}
			sorterData=mvonly;
			showdata(mvonly,1);
		}
}

// filters functins
function popularity(a,b) 
{
	console.log(a.popularity);
  if (a.popularity < b.popularity)
    return 1;
  if (a.popularity > b.popularity)
    return -1;
  return 0;
}
function date_added(a,b) 
{
	
  if (a.date_added < b.date_added)
    return 1;
  if (a.date_added > b.date_added)
    return -1;
  return 0;
}
function compare_by_title_asc(a,b) {
  if (a.title < b.title)
    return -1;
  if (a.title > b.title)
    return 1;
  return 0;
}
function compare_by_title_desc(a,b) {
  if (a.title < b.title)
    return 1;
  if (a.title > b.title)
    return -1;
  return 0;
}
function compare_by_lastet(a,b) {
  if (a.release_date < b.release_date)
    return 1;
  if (a.release_date > b.release_date)
    return -1;
  return 0;
}
function compare_by_release_date(a,b) {
  if (a.year < b.year)
    return 1;
  if (a.year > b.year)
    return -1;
  	return 0;
}
function compare_by_rank(a,b) {
  if (a.imdb_rating < b.imdb_rating)
    return 1;
  if (a.imdb_rating > b.imdb_rating)
    return -1;
  return 0;
}


function showdata(alldata,check)
{

			var body='';
		for(i=0;i<alldata.length;i++)
		{
			video_url="movie";
			t = alldata[i]['runtime'];
			var h = Math.floor( t / 60)+'h';
			var m = (t % 60)+'m';
			runtime=h+' '+m;


			rating=alldata[i]['imdb_rating'];	
			if(alldata[i]['rated']==null)
			{
				rated="N/A";
			}
			else
			{
				rated=alldata[i]['rated'];
			}
			if(alldata[i]['video_type'] && alldata[i]['video_type']=="tv"){
				mvurl='tv/'+alldata[i]['imdb_id']+'/'+alldata[i]['title'].split(" ").join('-');
			}
			else
			{
				mvurl='movies/'+alldata[i]['imdb_id']+'/'+alldata[i]['title'].split(" ").join('-').split(".").join("");
			}
			
			body+='<div class="item"><a href="'+mvurl+'"><img src="https://image.tmdb.org/t/p/w300/'+alldata[i]['poster_path']+'" width="132px"></a><a href="'+mvurl+'"><div class="overlay"><span><h3>'+alldata[i]['title']+' </h3><small>('+alldata[i]['year']+')</small></span><div class="overlayFooter" style="bottom:0"><span class="rated">'+rated+'</span><span class="score"><i class="fa fa-star"></i> '+rating+'</span></div></div></a></div>';
		}
		if(check==1)
		{
			$('#my-list').html(body);
		}
		else
		{
			$('#my-list').append(body);
		}
	

}
// trunc and replace with .. if name gretaer then given length
String.prototype.trunc = String.prototype.trunc ||
      function(n){
          return (this.length > n) ? this.substr(0, n-1) + '....' : this;
      };
