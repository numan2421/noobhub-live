$(function()
{
    $('.alert').hide();
	$('#suggestions').hide();
	// client sided form validation
    $('#request_form').validate(
	{
		rules:
		{
			subject:
			{
				required:true,
			},
			message:
			{
				required:true,
			}
		},
		messages:
		{
			subject:"",
			message:""
		},
        // assign classes accordingly to the object
		highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorHighlight");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorHighlight");
            $(element).addClass("");
        }, errorPlacement: function (error, element) 
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	$('#arrow').hide();
            $('#processing').show();
        	//post data
        	$.post(Apath+'api/v1/user/saveRequest',$('#request_form').serialize(),function showData(data)
        	{
        			if(data.status==200)
        			{
                        $('#arrow').show();
                        $('#processing').hide();
                        $('.success-msg').html('Your response has been recorded. We willll get back to you in a day or two.');
        				$('.alert').hide();
                        $('.alert-success').show();
        			}
        			else
        			{
                        $('#arrow').show();
                        $('#processing').hide();
        				$('.error-msg').html('Contact reuest failed. Please contact at "support@noobhub.tv".');
                        $('.alert').hide();
                        $('.alert-danger').show();
        			}
        			
        		
        	})
        }
	})

	$('#request_submit').click(function()
	{
        $('#arrow').hide();
        $('#processing').show()
		// $('#loader').show();
		title=$('#title').val();
		imdb_link=$('#imdb_link').val();
		video_type=$('input[type="radio"]:checked').val();
		$.post(Apath+'api/v1/user/saveRequest',{title:title,imdb_link:imdb_link,video_type:video_type},function showData(data)
    	{
    		
    		if(data.status==200)
    		{

    			$('#mBody').html('<h1>Request Submitted</h1><br> we will keep you posted.');
				$('#overlay').modal('show');
				$('#loader').hide();
    		}
    		else 
    		{
    			$('#mBody').html('<h1>Request Submition Failed</h1><br> '+data.description);
				$('#overlay').modal('show');
				$('#loader').hide();
    		}
    	})
	})	
})