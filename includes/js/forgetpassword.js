$(function()
{
    $('.alert').hide();
    // form validation
	$('#forget_form').validate({
		rules:
		{
			user_email:
			{
				required:true,
				email:true
			}
		},
		messages:
		{
			user_email:
			{
				required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Please enter your email address',
				email:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Please enter a valid email address'
			}
		},highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element) 
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	$('#loader').show();
        	$.post(Apath+'api/v1/user/forgetpassword',$('#forget_form').serialize(),function showData(data)
        	{
        		
        		if(data.status==200)
        		{
        			$('.success-msg').html('An email has been sent to '+$('#user_email').val()+' with further instructions on how to reset your password.Check your spam folder if you don&#39;t see it.');
                    $('.alert-success').show();
        		}
        		else
        		{
                    $('.error-msg').html(data.description);
        			$('.alert-danger').show();
        		}

        	})
        }
	})
})