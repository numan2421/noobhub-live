var sorterData=[];
var query;
var type='';
var genres=[];
$(function()
{
	query=$('#query').val();
	data="";
	$('#start').val(0);
	getData(1);

	if(query=="popular")
	{
		$('.sorter a').removeClass('activefilter');
		$('#popular').addClass('activefilter');
	}
	else if(query=="latestep")
	{
		$('.filters').hide();
	}

	$(window).scroll(function ()
	{
	   if ($(window).scrollTop() >= $(document).height() - window.innerHeight)
	   {
	      getData(2);
	   }
	});

	$('input[type="checkbox"]').change(function()
	{
	
			if($(this).is(':checked'))
			{
				if($(this).val()==0)
				{	
					sorterData=[];
					$('.genres').prop("checked","");
					$('input:checkbox[value="0"]').attr("checked","checked");
					query=$('#query').val();
					
					data="";
					$('#start').val(0);
					getData(1);
				}
				else
				{	
					sorterData=[];
					//i am on movies go for first genre
					// uncheck all
					$('input:checkbox[value="0"]').removeAttr('checked');
					$('input:checkbox[value="0"]').prop('checked',"");
					chkGenre=$(this).val();
					genres.push(chkGenre);
					query=$('#query').val();
					data=genres;
				
					$('#start').val(0);
					getData(1)
				}
			}
			else
			{
				if($(this).val()==0)
				{	
					sorterData=[];
					$('.genres').prop("checked","");
					$('input:checkbox[value="0"]').attr("checked","checked");
					query=$('#query').val();
					
					data="";
					$('#start').val(0);
					getData(1);
				}
				else
				{	
					if($('.genres').is(":checked"))
					{
						sorterData=[];
						//i am on movies go for first genre
						// uncheck all
						chkGenre=$(this).val();
						var index = genres.indexOf(chkGenre);
						if (index > -1) {
						    genres.splice(index, 1);
						}
						query=$('#query').val();
						data=genres;
						
						$('#start').val(0);
						getData(1)
					}
					else
					{
						sorterData=[];
						$('.genres').prop("checked","");
						$('input:checkbox[value="0"]').attr("checked","checked");
						query=$('#query').val();
						
						data="";
						$('#start').val(0);
						getData(1);
					}
				
				}
			}
		

	
	})
})



function getData(chk)
{
		
		start=$('#start').val();
		$.post(Apath+'api/v1/video/discoverdata',{query:query,start:start,data:data},function showfunctionData(data)
		{
			if(data.status==200)
			{
				result_data=data.response;
				showdata(result_data,chk);
					
					for(k=0;k<result_data.length;k++)
					{
						sorterData.push(result_data[k]);
					}
				
			    $('#start').val(parseInt($('#start').val())+parseInt($('#limit').val()))
			}
			else if(data.status=="Error")
			{
				$('#noData').html('<div class="row" style="text-align:center;"><h3>No More Record Found</h3></div>');
			}
			

		})

}

	


function showdata(alldata,check)
{
		var body='';
		for(i=0;i<alldata.length;i++)
		{
				video_url="movie";
				t = alldata[i]['runtime'];
				var h = Math.floor( t / 60)+'h';
				var m = (t % 60)+'m';
				runtime=h+' '+m;


			rating=alldata[i]['imdb_rating'];	
			if(alldata[i]['rated']==null)
			{
				rated="N/A";
			}
			else
			{
				rated=alldata[i]['rated'];
			}
			if(alldata[i]['video_type'] && alldata[i]['video_type']=="tv"){
				mvurl=Apath+'tv/'+alldata[i]['imdb_id']+'/'+alldata[i]['title'].split(" ").join('-');
			}
			else
			{
				mvurl=Apath+'movies/'+alldata[i]['imdb_id']+'/'+alldata[i]['title'].split(" ").join('-').split(".").join("");;
			}
			
			body+='<div class="item"><a href="'+mvurl+'"><img src="https://image.tmdb.org/t/p/w300/'+alldata[i]['poster_path']+'" width="132px"></a><a href="'+mvurl+'"><div class="overlay"><span><h3>'+alldata[i]['title']+' </h3><small>('+alldata[i]['year']+')</small></span><div class="overlayFooter" style="bottom:0"><span class="rated">'+rated+'</span><span class="score"><i class="fa fa-star"></i> '+rating+'</span></div></div></a></div>';
		}
		if(check==1)
		{
			$('#video_data').html(body);
		}
		else
		{
			$('#video_data').append(body);
		}
	


	// return sorterData;
}

// filters
function filter(factor,param)
{
	$(param).parent().children().removeClass('activefilter');
	$(param).addClass('activefilter');
	if(factor=="1")
	{
		sorterData.sort(compare_by_lastet);
		showdata(sorterData,1);
	}
	else if(factor=="2")
	{
		// console.log(sorterData);
		sorterData.sort(popularity);
		showdata(sorterData,1);
		showdata(sorterData,1);
	}
	else if(factor=="3")
	{
		sorterData.sort(date_added);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
	else if(factor=="4")
	{
		sorterData.sort(compare_by_title_asc);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
	else if(factor=="5")
	{
		sorterData.sort(compare_by_title_desc);
		showdata(sorterData,1);
		// showdata(sorterData,1);
	}
}


// filters functions

function popularity(a,b) 
{
	console.log(a.popularity);
  if (a.popularity < b.popularity)
    return 1;
  if (a.popularity > b.popularity)
    return -1;
  return 0;
}
function date_added(a,b) 
{	
  if (a.date_added < b.date_added)
    return 1;
  if (a.date_added > b.date_added)
    return -1;
  return 0;
}
function compare_by_lastet(a,b) {

  if (a.release_date < b.release_date)
    return 1;
  if (a.release_date > b.release_date)
    return -1;
  return 0;
}
function compare_by_title_asc(a,b) {
  if (a.title < b.title)
    return -1;
  if (a.title > b.title)
    return 1;
  return 0;
}
function compare_by_title_desc(a,b) {
  if (a.title < b.title)
    return 1;
  if (a.title > b.title)
    return -1;
  return 0;
}