$('body').on('click', '#playlistadd', function()
{
    type=$(this).attr('e');
    id=$(this).attr('d');
    $.post(Apath+'api/v1/user/addplaylist',{type:type,id:id},function showData(data)
    {
        if(data.status==200)
        {

            if(data.response=="success")
            {
                //tick
                $('a[d="'+id+'"]').html('<button><i class="fa fa-minus"></i> Remove From List</button>');
            }
            else
            {
                //plus
                $('a[d="'+id+'"]').html('<button><i class="fa fa-plus"></i> My List</button>');
            }
        }
        else
        {

        }

    })
});

$(function()
{
    if($('#premium').val()==0)
    {
       // free use check total watched minutes in a day
        $.get(Apath+'api/v1/user/getfreeTotaltime',function showData(data)
        {
            if(data==1)
            {
                // expired
                // show message
                $('.alert-daily').show();
            }
            else
            {

            }
        })
    }
    $(window).on('beforeunload', function ()
    {
        //this will work only for Chrome
        pageCleanup();
    });

    $(window).on("unload", function ()
    {
        //this will work for other browsers
        pageCleanup();
    });
    $('#test').click(function()
    {
        pageCleanup();
    })

    $('#m_search').on('input',function()
    {

        $('.close-icon').show();
        $('.close-icon').addClass('active');
        $(this).css('width','180px');
        if($(this).val()=='')
        {

        }
        else
        {
            $('.page_body').hide();
            $('.filters').hide();
            $('.search_body').show();
        }
    })

    $('#m_search').focus(function()
    {
        
            $(this).css('width','180px');
        
    })
    $('#m_search').focusout(function()
    {
        if($(this).val()=='' && !$('.close-icon').hasClass('active'))
        {
          $(this).css('width','55px');
        }
    })

     $('.close-icon').click(function()
     {
        //search ends

        searchEnd($('#m_search').val(),"-","-");

        // remove search data and reset value
        $('#m_search').css('width','55px');
        $('#m_search').val('');
        $('.close-icon').removeClass('active');
        $('.page_body').show();
        $('.filters').show();
        $('.search_body').hide();
        $(this).hide();


     })
})



// var _wasPageCleanedUp = false;
function pageCleanup()
{
    $.get(Apath+'api/v1/user/addtimeout',function showout()
    {

    })
}

function searchEnd(keyword,type,title)
{
   var currenturl=window.location.pathname;
   $.post(Apath+'api/v1/analytics/search',{keyword:keyword,type:type,title:title,url:currenturl},function showData(data)
   {

   })
}
