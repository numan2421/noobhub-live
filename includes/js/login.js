$(function()
{	$('#loader').hide();
	//chk for signup settings
	$.get(Apath+'api/v1/admin/getsignupSettings',function showData(data)
	    {
	        if(data.response=="NO Data")
	        {
	            //first time check
	            // let it off
	            window.location.href=Apath+'login';
	        }
	        else
	        {
	            var allData=data.response;
	            if(allData[0]['value']==1)
	            {
	                // its ok
	                $('.signup_div').show();
	            }
	            else
	            {
	                //not active get the fuk out
	            	$('.signup_div').hide();
	            }
	        }
	    })


	$('#login_form').validate(
	{
		rules:
		{
			user_email:
			{
				required:true,
				email:true
			},
			user_pass:
			{
				required:true,
				minlength:6
			}
		},
		messages:
		{
			user_email:
			{
				required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;User email is required.',
				email:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Please enter a valid email address',
			},
			user_pass:
			{
				required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; Password is required.',
				minlength:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; Password too short',
			}
		},
		highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element)
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	$('#loader').show();
        	//post data
        	$.post(Apath+'api/v1/user/login',$('#login_form').serialize(),function showData(data)
        	{
        		if(data.status==200)
        		{
							
        			$('#loader').hide();
        			window.location.href=Apath+'home';
        		}
        		else
        		{
        			$('#mBody').html('<h1>ERROR</h1><br>'+data.description);
			        $('#overlay').modal('show');
					$('#loader').hide();
        		}
        	})
        }
	})
})
