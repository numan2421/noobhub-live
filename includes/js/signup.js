$(function()
{
	
	// validate signup_code
	var signupCode=$('#code').val();
	if(signupCode=="")
	{
		//code is null no need to run
		// chk if signup setting is active
		$.get(Apath+'api/v1/admin/getsignupSettings',function showData(data)
	    {
	        if(data.response=="NO Data")
	        {
	            //first time check
	            // let it off 
	            window.location.href=Apath+'login';
	        }
	        else
	        {
	            var allData=data.response;
	            if(allData[0]['value']==1)
	            {
	                //check the checkbox
	                // its ok
	            }
	            else
	            {
	                // uncheckcheckbox
	                //not active get the fuk out
	                window.location.href=Apath+'login';

	            }
	        }
	    })
	}
	else
	{
		$.post(Apath+'api/v1/user/validateCode',{code:signupCode},function showData(Data)
		{
			if(Data.status==200)
			{
				//do nothing
				var userData=Data.response;
				userEmail=userData[0]['user_email'];
				$('#user_email').val(userEmail);
			}
			else
			{
				$('#mBody').html('<h1>Error</h1><br>'+Data.description);
				$('#overlay').modal('show');
				window.location.href=Apath+'login';
			}
		})
	}
	



	$('#signup_form').validate(
	{
		rules:
		{
			f_name:"required",
			user_email:
			{
				required:true,
				email:true
			},
			user_pass:
			{
				required:true,
				minlength:6
			},
			user_pass_confirm:
			{
				required:true,
				minlength:6,
				equalTo:"#user_pass"
			}

		},
		messages:
		{
 			 f_name: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Full name is required.',
 			 user_email:
 			 {
 			 	required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Email address is required',
 			 	email:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Please enter a valid email address'
 			 },
 			 user_pass:
 			 {
 			 	required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Password is required',
 			 	minlength:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Password should be min 6 characters',
 			 },
 			 user_pass_confirm:
 			 {
 			 	required:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Confirmation Password is required',
 			 	minlength:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Password should be min 6 characters',
 			 	equalTo:'<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;Passwrods should be equal',
 			 }
		},
		 highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element) 
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	$('#mBody').html('PROCESSING. PLEASE WAIT...');
            $('#overlay').modal('show');
        	$.post(Apath+'api/v1/user/signup',$('#signup_form').serialize(),function showData(data)
        	{
        		if(data.status==200)
	        	{
	        		if($('#code').val()=="")
	        		{
	        			$('#mBody').html('<h1>Sign up Success</h1><br><span>'+data.response+'<span>');
			        	$('#overlay').modal('show');
	        		}
	        		else
	        		{
	        			$('#mBody').html('<h1>Sign up Success</h1><br>Please proceed to login and enjoy.');
			        	$('#overlay').modal('show');
	        		}
			        $('#signup_form')[0].reset();
			        // window.location.href='/';
        		}
        		else
        		{
        			$('#mBody').html('<h1>ERROR</h1><br><span>'+data.description+'</span>');
			        $('#overlay').modal('show');
        		}
        	})

        }
	})
})

var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 
  return query_string;
}();