$(function()
{
	$('#user_profile_form').validate(
	{
		rules:
		{
			user_name:
			{
				required:true
			},
			user_email:
			{
				required:true,
				email:true
			}
		},
		messages:
		{
			user_name:
			{
				required:""
			},
			user_email:
			{
				required:"",
				email:""
			}
		},
		highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element)
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	//post data'
        	$.post(Apath+'api/v1/user/updateProfile',$('#user_profile_form').serialize(),function showData(data)
    		{
    			if(data.status==200)
        		{
							
        			$('.clickable').removeClass('activediv');
					$('.show').css("cssText", "display: block !important;");
					$('.hide').css("cssText", "display: none !important;");
        		}
        		else
        		{
        			console.log('error');
        		}
    		})
        }
	})

	$('#password_form').validate(
	{
		rules:
		{
			curr_pass:
			{
				required:true,
				minlength:6	
			},
			new_pass:
			{
				required:true,
				minlength:6
			},
			new_re_pass:
			{
				required:true,
				equalTo:'#new_pass'
			}
		},
		messages:
		{
			curr_pass:
			{
				required:""
			},
			new_pass:
			{
				required:"",

			},
			new_re_pass:
			{
				required:"",
				equalTo:"New password doesn't match",
			}
		},
		highlight: function (element) {
            $(element).removeClass("");
            $(element).addClass("errorInput");
        },
        unhighlight: function (element) {
            $(element).removeClass("errorInput");
            $(element).addClass("");
        }, errorPlacement: function (error, element)
        {
        	element.after(error);
        },
   		submitHandler: function (form)
        {
        	//post data'
        	$.post(Apath+'api/v1/user/updatepassword',$('#password_form').serialize(),function showData(data)
    		{
    			if(data.status==200)
        		{	
        			$('.clickable').removeClass('activediv');
					$('.show').css("cssText", "display: block !important;");
					$('.hide').css("cssText", "display: none !important;");
        		}
        		else
        		{
        			$('#error').html(data.description);
        		}
    		})
        }
	})
})