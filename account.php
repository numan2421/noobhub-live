<?php
include_once "header.php";
 include 'search.php'
  ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/account.css">

<div class="container page_body">
	<div class="account">
		
		<div id="user_info" class="bordered">
			<div class="prof_images">
				<img src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&amp;s=120" class="avatar" width="120" height="120" alt="">
			</div>
			<div id="user_details">
				<h2><?php echo $_SESSION['user_name']?></h2>
				<h4>Trial Member</h4>
				<p>Thanks for joining <? echo SITE_TITLE ?> <br> You have access to all the videos for 30 days.</p>
			</div>
		</div>
<form id="user_profile_form" name="user_profile_form">	
		<div id="profile" class="bordered clickable">
			<h3>Profile</h3>
			<p class="show">Edit your name,email</p>
						
							<div class="hide">
								<span>your name</span>
								<div class="form-group">
									  <div class="inner-addon left-addon">
									    <i class="fa fa-user" aria-hidden="true"></i>
									    <input type="text" id="user_name" name="user_name" class="form-control" placeholder="Your name" value="<?php echo $_SESSION['user_name']?>" />
									</div>
								</div>
								<span>your email address</span>
								<div class="form-group">
									 
									  <div class="inner-addon left-addon">
									    <i class="fa fa-envelope" aria-hidden="true"></i>
									    <input type="text" id="user_email" name="user_email" class="form-control" placeholder="test@test.com" value="<?php echo $_SESSION['user_email'] ?>" />
									</div>
								</div>
			
								<div class="prof_buttons">
									<button class="text small secondary">Cancel</button>
									<button class="text small">Save</button>
								</div>
							</div>

		</div>
	</form>

	<form id="password_form" name="password_form">
		<div id="password" class="bordered clickable">
			<h3>Password</h3>
			<p class="show">Changed Password</p>
			<div class="hide">
								<span>your current password</span>
								<div class="form-group">
									  <div class="inner-addon left-addon">
									    <i class="fa fa-key" aria-hidden="true"></i>
									    <input type="password" id="curr_pass" name="curr_pass" class="form-control" placeholder="************" />
									</div>
								</div>
								<span>your new password</span>
								<div class="form-group">
									 
									  <div class="inner-addon left-addon">
									    <i class="fa fa-key" aria-hidden="true"></i>
									    <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="*************" />
									</div>
								</div>
								<span>re-enter new password</span>
								<div class="form-group">
									  <div class="inner-addon left-addon">
									    <i class="fa fa-key" aria-hidden="true"></i>
									    <input type="password" id="new_re_pass" name="new_re_pass" class="form-control" placeholder="**************" />
									</div>
								</div>
								<p class="error" id="error"><p>
								<div class="pass_buttons">
									<button type="submit" class="text small">Update Password</button>
								</div>
							</div>
		</div>
	</form>


		<div id="disagnostic" class="bordered clickable">
			<h3>Diagnostics</h3>
			
				<div class="hide">
				<p><!-- react-text: 185 -->You can test your internet connection speed from <!-- /react-text --><a href="//www.speedtest.net" target="_blank" rel="noopener noreferrer" class="link">Speedtest.net</a><!-- react-text: 187 --> or <!-- /react-text --><a href="https://fast.com" target="_blank" rel="noopener noreferrer" class="link">Fast.com</a></p><p><!-- react-text: 190 -->Find out what browser you're using and how to use a different one from <!-- /react-text --><a href="https://whatbrowser.org" target="_blank" rel="noopener noreferrer" class="link">WhatBrowser.org</a></p></p>
				</div>
		</div>



	</div> 
</div>


<?php include_once "footer.php"; ?>



<script type="text/javascript">
	$(function()
	{
		$('.clickable').click(function()
		{
			$('.clickable').removeClass('activediv');
			$('.show').css("cssText", "display: block !important;");
			$('.hide').css("cssText", "display: none !important;");
			if($(this).hasClass('activediv'))
			{
				//active display hide and hide showed elemendt
				$(this).children('.show').css("cssText", "display: block !important;");
				$(this).children('.hide').css("cssText", "display: none !important;");
				$(this).removeClass('activediv');
			}
			else
			{
				$(this).children('.show').css("cssText", "display: none !important;");
				$(this).children('.hide').css("cssText", "display: block !important;");
				$(this).addClass('activediv');
			}
		})
	})
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/account.js"></script>