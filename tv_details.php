<?php
	include_once "header.php";
	include_once "includes/mobile_detect/Mobile_Detect.php";
	include_once 'includes/processes/tvdetails_process.php';
?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether-theme-arrows.min.css">
<link href="http://vjs.zencdn.net/6.2.0/video-js.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/video.css">
<style type="text/css">

#close_sub
{
	cursor: pointer;
}
.select2-container
{
	width: 150px !important;
}
#lang
{
	width: 150px;
    margin: 12px;
}
.select2-selection--single{
	height: 30px !important;
	background: none !important;
    
    border-radius: 35px !important;
    border: 2px solid rgba(243,249,255,0.7) !important;
    opacity: .9;
}
.select2-selection__rendered
{
	color: white !important;
}
.select2-search__field
{
	color: black !important;
}
iframe
{
	width: 100%;
	height: 90%;
}
.modal
{}
.modal-header
{
	border-bottom: none !important;
}
.modal-dialog
{
	width: 100% !important;
}
.modal-content
{
	background-color: rgb(0, 0, 0) !important;
}
.modal-fullscreen .modal-dialog {
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
.vjs-menu-button-popup .vjs-menu
{
 width:10.5em;
}
@media (min-width: 768px) {
  .modal-fullscreen .modal-dialog {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .modal-fullscreen .modal-dialog {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .modal-fullscreen .modal-dialog {
     width: 1170px;
  }
}
.close {
    color: #fff !important;
    opacity: 9 !important;
}
.subtitle
{
	display: none;
}
#myModalLabel
{
	color: white;
	font-weight: bolder;
}

</style>
<script  type="text/javascript"  src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
			




<input type="hidden" id="ep_tmdb_id" name="ep_tmdb_id" value="">	
<input type="hidden" name="premium" id="premium" value=<?php echo $_SESSION['is_premium'] ?> >
<input type="hidden" name="expired" id="expired" value=<?php echo $_SESSION['daily_expired'] ?> >
<?php 
include 'search.php';
	$detect = new Mobile_Detect();
	if ($detect->isMobile())
	{
		echo '<div class="mobile page_body">
		 <div class="alert alert-info fade in alert-video">
  <div class="alert-Text"><strong>Warning!</strong> Playable time limit/video reached. <a href="'.$absolutepath.'upgrade">Click</a> to become premium user.</div>
</div>
				<div id="backdrop" class="playhide">
					<div id="bImage">
						<img src="'.$backDrop_path.'" />
						<div id="mvoverlay">
							<h1>'.$title.'<small>('.$rDate.')</small></h1>
						</div>
					</div>
				</div>

				<div id="player" class="playshow">
					<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">

				    
				  	</video>
				</div>

				<div id="details">
					<div class="buttons">
						<div id="btn1">
							<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						</div>
						<div id="btn2">
							<a class="playshow" href="javascript:void(0)" id="upsub" name="upsub"><button><i class="fa fa-plus"></i>Upload Subtitle</button></a>
							<a href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
							<a href="javascript:void(0)" onclick="showTrailer(\''.$title.'\')"><button><i class="fa fa-youtube" aria-hidden="true"></i> Trailer</button></a>
						</div>
					</div>

					<div id="playHeading" class="playshow" id="playing">
						

					</div>


					<div class="subtitle" style="padding:20px;">
					<h3>Upload Subtitle</h3>
					<p id="subInfo">You can download  .vtt subtitles for '.$title.' from the internet and upload it here.</p>
						<div>
							<div class="buttons" style="padding:0;    background: none;display: inline-flex;">
											<div id="lang">
								<select id="select2" data-placeholder="Choose a Language...">
								  <option value="AF">Afrikanns</option>
								  <option value="SQ">Albanian</option>
								  <option value="AR">Arabic</option>
								  <option value="HY">Armenian</option>
								  <option value="EU">Basque</option>
								  <option value="BN">Bengali</option>
								  <option value="BG">Bulgarian</option>
								  <option value="CA">Catalan</option>
								  <option value="KM">Cambodian</option>
								  <option value="ZH">Chinese (Mandarin)</option>
								  <option value="HR">Croation</option>
								  <option value="CS">Czech</option>
								  <option value="DA">Danish</option>
								  <option value="NL">Dutch</option>
								  <option value="EN" selected="selected">English</option>
								  <option value="ET" >Estonian</option>
								  <option value="FJ">Fiji</option>
								  <option value="FI">Finnish</option>
								  <option value="FR">French</option>
								  <option value="KA">Georgian</option>
								  <option value="DE">German</option>
								  <option value="EL">Greek</option>
								  <option value="GU">Gujarati</option>
								  <option value="HE">Hebrew</option>
								  <option value="HI">Hindi</option>
								  <option value="HU">Hungarian</option>
								  <option value="IS">Icelandic</option>
								  <option value="ID">Indonesian</option>
								  <option value="GA">Irish</option>
								  <option value="IT">Italian</option>
								  <option value="JA">Japanese</option>
								  <option value="JW">Javanese</option>
								  <option value="KO">Korean</option>
								  <option value="LA">Latin</option>
								  <option value="LV">Latvian</option>
								  <option value="LT">Lithuanian</option>
								  <option value="MK">Macedonian</option>
								  <option value="MS">Malay</option>
								  <option value="ML">Malayalam</option>
								  <option value="MT">Maltese</option>
								  <option value="MI">Maori</option>
								  <option value="MR">Marathi</option>
								  <option value="MN">Mongolian</option>
								  <option value="NE">Nepali</option>
								  <option value="NO">Norwegian</option>
								  <option value="FA">Persian</option>
								  <option value="PL">Polish</option>
								  <option value="PT">Portuguese</option>
								  <option value="PA">Punjabi</option>
								  <option value="QU">Quechua</option>
								  <option value="RO">Romanian</option>
								  <option value="RU">Russian</option>
								  <option value="SM">Samoan</option>
								  <option value="SR">Serbian</option>
								  <option value="SK">Slovak</option>
								  <option value="SL">Slovenian</option>
								  <option value="ES">Spanish</option>
								  <option value="SW">Swahili</option>
								  <option value="SV">Swedish </option>
								  <option value="TA">Tamil</option>
								  <option value="TT">Tatar</option>
								  <option value="TE">Telugu</option>
								  <option value="TH">Thai</option>
								  <option value="BO">Tibetan</option>
								  <option value="TO">Tonga</option>
								  <option value="TR">Turkish</option>
								  <option value="UK">Ukranian</option>
								  <option value="UR">Urdu</option>
								  <option value="UZ">Uzbek</option>
								  <option value="VI">Vietnamese</option>
								  <option value="CY">Welsh</option>
								  <option value="XH">Xhosa</option>
								</select>
							</div>
								<a d="" id="uploadSub" name="uploadSub" href="javascript:void(0)"><button style="border-radius:35px;"><i class="fa fa-upload"></i> Upload Subtitles</button></a>


								<p class="pull-right error" id="sub_error" style="display:none;color:red">Only .vtt formate supported at the time.</p>
								<p class="pull-right error" id="sub_success" style="display:none;color:green">Subtitles Uploded Successfully. Enjoy!</p>
							</div>
							<div class="error-msg pull-right">
							
							</div>
						</div>
					</div>

					
					<div class="basic_info" style="margin:0px ; padding:10px;">
						<div id="rated">
							<h3>Rated</h3>
							<a href="javascript:void(0)">'.$rated.'</a>
						</div>
						<div id="imdb">
							<h3>Imdb</h3>
							<a href="javascript:void(0)"><i class="fa fa-star"></i> '.$rank.'</a>
						</div>
						
						<div id="Genre" style="margin-right:0 !important;">
							<h3>genre</h3>
							'.$allgen.'

						</div>
						<div id="storyLine" style="margin-right:0 !important;">
							<h3>StoryLine</h3>
							<p>'.$overview.'</p>
						</div>
						<div class="seasons" style="margin-right:0 !important;">
							<div class="snav" style="margin-right:0 !important;">
							<h3 style="margin-left:0px;">Seasons</h3><br>
								'.$sBody.'
							</div>
						</div>
										<div class="episodes">
	
		



											'.$eBody.'
			
										</div>




					</div>
				</div>

						<div id="cast" class="row" style="width:100%;padding:0px;margin-left:0px;">
					<div id="cast_details" class="sliderDiv">
			
							'.$castBody.'
						
					</div>
				</div>


			</div>';
	}
	else
	{
echo '
<div class="page_body">
 <div class="alert alert-info fade in alert-video">
  <div class="alert-Text"><strong>Warning!</strong> Playable time limit/video reached. <a href="'.$absolutepath.'upgrade">Click</a> to become premium user.</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-8" >
	
	
		<!-- backdrop -->
			<div id="backdrop" class="playhide">
				<div id="bImage">
					<img src="'.$backDrop_path.'" />
					<div id="mvoverlay">
						<h1>'.$title.'<small>('.$rDate.')</small></h1>
					</div>
				</div>
			</div>
			


			<div id="player" class="playshow">
				<video id="my-video" class="video-js" controls preload="auto"data-setup="{}">';
			    	
			  echo '</video>
			  </div>

				<!-- All Details -->
				<div id="details">
					<div class="buttons">
						<div id="btn1">
							<a href="javascript:void(0)" class="playshow" id="getBack"><button><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
						</div>
						<div id="btn2">
							<a class="playshow" href="javascript:void(0)" id="upsub" name="upsub"><button><i class="fa fa-upload"></i>Upload Subtitle</button></a>
							<a d="'.$showid.'" e="tv" id="playlistadd" href="javascript:void(0)"><button><i class="fa fa-plus"></i> My List</button></a>
							<a class="playhide" href="javascript:void(0)" onclick="showTrailer(\''.$title.'\')"><button><i class="fa fa-youtube" aria-hidden="true"></i> Trailer</button></a>
						</div>
					
					</div>

					<div id="playHeading" class="playshow" style="display: block;padding: 20px;">
						<div><b>PLAYING</b></div>
						<div style="margin-top: 10px;" id="playing"></div>

					</div>
					<div class="subtitle" style="padding:20px;">
					<h3>Upload Subtitle</h3> <p id="close_sub" name="close_sub" class="pull-right">X</p>
					<p id="subInfo">You can download  .vtt subtitles for '.$title.' from the internet and upload it here.</p>
						<div>
							<div class="buttons" style="padding:0;    background: none;display: inline-flex;">
								<div id="lang">
								<select id="select2" data-placeholder="Choose a Language...">
								  <option value="AF">Afrikanns</option>
								  <option value="SQ">Albanian</option>
								  <option value="AR">Arabic</option>
								  <option value="HY">Armenian</option>
								  <option value="EU">Basque</option>
								  <option value="BN">Bengali</option>
								  <option value="BG">Bulgarian</option>
								  <option value="CA">Catalan</option>
								  <option value="KM">Cambodian</option>
								  <option value="ZH">Chinese (Mandarin)</option>
								  <option value="HR">Croation</option>
								  <option value="CS">Czech</option>
								  <option value="DA">Danish</option>
								  <option value="NL">Dutch</option>
								  <option value="EN" selected="selected">English</option>
								  <option value="ET" >Estonian</option>
								  <option value="FJ">Fiji</option>
								  <option value="FI">Finnish</option>
								  <option value="FR">French</option>
								  <option value="KA">Georgian</option>
								  <option value="DE">German</option>
								  <option value="EL">Greek</option>
								  <option value="GU">Gujarati</option>
								  <option value="HE">Hebrew</option>
								  <option value="HI">Hindi</option>
								  <option value="HU">Hungarian</option>
								  <option value="IS">Icelandic</option>
								  <option value="ID">Indonesian</option>
								  <option value="GA">Irish</option>
								  <option value="IT">Italian</option>
								  <option value="JA">Japanese</option>
								  <option value="JW">Javanese</option>
								  <option value="KO">Korean</option>
								  <option value="LA">Latin</option>
								  <option value="LV">Latvian</option>
								  <option value="LT">Lithuanian</option>
								  <option value="MK">Macedonian</option>
								  <option value="MS">Malay</option>
								  <option value="ML">Malayalam</option>
								  <option value="MT">Maltese</option>
								  <option value="MI">Maori</option>
								  <option value="MR">Marathi</option>
								  <option value="MN">Mongolian</option>
								  <option value="NE">Nepali</option>
								  <option value="NO">Norwegian</option>
								  <option value="FA">Persian</option>
								  <option value="PL">Polish</option>
								  <option value="PT">Portuguese</option>
								  <option value="PA">Punjabi</option>
								  <option value="QU">Quechua</option>
								  <option value="RO">Romanian</option>
								  <option value="RU">Russian</option>
								  <option value="SM">Samoan</option>
								  <option value="SR">Serbian</option>
								  <option value="SK">Slovak</option>
								  <option value="SL">Slovenian</option>
								  <option value="ES">Spanish</option>
								  <option value="SW">Swahili</option>
								  <option value="SV">Swedish </option>
								  <option value="TA">Tamil</option>
								  <option value="TT">Tatar</option>
								  <option value="TE">Telugu</option>
								  <option value="TH">Thai</option>
								  <option value="BO">Tibetan</option>
								  <option value="TO">Tonga</option>
								  <option value="TR">Turkish</option>
								  <option value="UK">Ukranian</option>
								  <option value="UR">Urdu</option>
								  <option value="UZ">Uzbek</option>
								  <option value="VI">Vietnamese</option>
								  <option value="CY">Welsh</option>
								  <option value="XH">Xhosa</option>
								</select>
							</div>
								<a d="" id="uploadSub" name="uploadSub" href="javascript:void(0)"><button style="border-radius:35px;"><i class="fa fa-upload"></i> Upload Subtitles</button></a>


								<p class="pull-right error" id="sub_error" style="display:none;color:red">Only .vtt formate supported at the time.</p>
								<p class="pull-right error" id="sub_success" style="display:none;color:green">Subtitles Uploded Successfully. Enjoy!</p>
							</div>
							<div class="error-msg pull-right">
							
							</div>
						</div>
					</div>

					<div class="basic_info playhide">
						<div id="rated">
							<h3>Rated</h3>
							<a href="javascript:void(0)">'.$rated.'</a>
						</div>
						<div id="imdb">
							<h3>Imdb</h3>
							<a href="javascript:void(0)"><i class="fa fa-star"></i> '.$rank.'</a>
						</div>
						
						<div id="Genre">
							<h3>genre</h3>
							'.$allgen.'

						</div>
						<div id="storyLine">
							<h3>StoryLine</h3>
							<p>'.$overview.'</p>
						</div>
					
					</div>

					<!-- Related -->
					<div class="related main sliderDiv">
				
						  '.$relatedBody.'
						
					</div>
					<!-- ENd Related -->
				
				</div>

</div>





					<!-- Basic info end -->
					
<div class="col-xs-12 col-sm-12 col-md-4">
			<div class="episodes">
				<div class="seasons">
							<div class="snav">
							<h3>Seasons</h3>
								'.$sBody.'
				
							</div>
				</div>
			<div class="" id="allEpisodes">


'.$eBody.'
			</div>
		</div>




</div>


			
			<!-- End Content -->
				<div id="cast" class="row">
					<div id="cast_details" class="sliderDiv">
			
							'.$castBody.'
						
					</div>
				</div></div>';
}
?>



<!-- youtubr overlay for trailers -->
<div class="modal modal-fullscreen fade" id="youtubeoverlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 10000;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $title?> Trailer</h4>
      </div>
      <div class="modal-body">
        <iframe
			src="https://www.youtube.com/embed/<?php echo $trailer->site_key ?>">
			</iframe>
      </div>

    </div>
  </div>
</div>
<?php include_once "footer.php";
  ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="http://vjs.zencdn.net/6.2.0/video.js"></script>
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://www.gstatic.com/cv/js/sender/v1/cast_sender.js"></script>

<script src="<?php echo $absolutepath ?>includes/js/uploader/ajaxupload.js"></script>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/cromecast.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
 
(function(window, videojs) 
{
	// chromecast
	var player = window.player = videojs('my-video',{'html5': { nativeTextTracks: false }});
	player.ready(function ()
	{ 
		this.chromecast();
	});
}(window, window.videojs));
   
$(function()
{
	$('.episodes').mCustomScrollbar();
	$('#upsub').click(function()
	{
		$('.subtitle').show();
	})
	$('#close_sub').click(function()
	{
		$('.subtitle').hide();
	})


	$('#select2').select2();
	// srt uploader
	 btnUpload1=$('#uploadSub');
        new AjaxUpload(btnUpload1, {
        action: Apath+'includes/processes/srtuploader.php',
        name: 'uploadfile1',
        onSubmit: function(file1, ext)
        {
           	if (! (ext && /^(vtt)$/.test(ext)))
           	{
                console.log('Wrong Format');
                $('#sub_error').show();
                setTimeout(function(){ $('#sub_error').fadeOut() }, 3000);
                return false;
            } 
        },
        onComplete: function(file1, response1)
        {
        	$('#sub_error').hide();
        	arr1 = response1.split(" | ");
            res1 = arr1[0];
            file2 = arr1[1];
           $.post(Apath+'includes/processes/sub_process.php',{action:'saveSub',type:"tv",mv_code:$('body').children().find('#uploadSub').attr('d'),lang:$('#select2').val(),title:file2.split('/').pop()},function showData(data)
        	{

        	})
        	
           	var video=videojs('my-video');
			video.addRemoteTextTrack({
				kind: 'captions', 
				label:$('#select2').val(),
				src: Apath+'uploads/srt/'+file2 }, false);
			$('#sub_success').show();
                
        }
    })
		  
})
   
function showTrailer(title)
{
	$.post(Apath+'api/v1/analytics/videoPlay',{title:title,type:"tv-trailer"},function showData()
	{

	})
	$('#youtubeoverlay').modal({backdrop: 'static', keyboard: false});
}
$(document).ready(function() 
{

  	$.get(Apath+'api/v1/user/getplaylist',function showData(data)
	{
		if(data.status==200)
		{
			allData=data.response;
			for(i=0;i<allData.length;i++)
			{
				$('a[d="'+allData[i]['video_id']+'"][e="'+allData[i]['video_type']+'"]').html('<button><i class="fa fa-minus"></i> Remove From List</button>');
			}
		}
	})
  	

  		$(document.body).on('click', '.downLink' ,function()
		{
			var video=videojs('my-video');
			video.pause();
			$(this).parent().hide();
  			$(this).parent().parent().children().last().show();
		})


		$('#youtubeoverlay').on('hidden.bs.modal', function () 
		{
		    $('iframe').attr('src', $('iframe').attr('src'));
		})

  	$('.playshow').hide();

    $("#lightSlider").lightSlider(
	{
		pager:false,
		autoWidth:true,
		loop:true,
		slideMargin:1,
		adaptiveHeight:true
	}); 



	$(document.body).on('click', '.hover10' ,function()
	{
		$('.error').hide();
		if($(this).find('img').hasClass('blur_active'))
		{
			console.log('dnt hide');
		}
		else
		{
			$('.downshow').show();
			$('.downhide').hide();
		}
		$('.play_buttons').hide();
		$('.hover10 figure').removeClass('activeep');
		$(this).find('figure').addClass('activeep');
		$('.hover10 img').removeClass('blur_active');
		$(this).find('img').addClass('blur_active');
		$(this).find('.play_buttons').show();
	})

	var video = videojs('my-video');
		video.on('pause', function() 
		{
		  	this.bigPlayButton.show();
		  // Now the issue is that we need to hide it again if we start playing
		  // So every time we do this, we can create a one-time listener for play events.
		  	video.one('play', function() {
		    this.bigPlayButton.hide();
		  });
		});

		$('#getBack').click(function()
		{
			var video=videojs('my-video');
			video.pause();
			$('.playshow').hide();
  			$('.playhide').show();
		})
});


  function play(bpath,scid,epname,epnumber,show,tmdb_id)
  {
  	if($('#expired')==0)
  	{
	  	if($('#is_premium')==0)
	  	{
	  		$('#alert-video').hide();
	  		videoTimer();
	  	}
	  	$('	')
	  	$('#uploadSub').attr('d',tmdb_id);
	  	$('html,body').animate({ scrollTop: 0 }, 'slow');
	  	sNumber=$('.snav').children('a.selected').html();
	  	$('#playing').html(show+' - S'+('0' + sNumber).slice(-2)+'E'+('0' + epnumber).slice(-2)+' - '+unescape(epname));
	  	url=bpath+scid+'.mp4';
	  	$('.playshow').show();
	  	$('.playhide').hide();
	  	var video=videojs('my-video');
		video.src(url);
			
		//get sub for playing video
		$.post(Apath+'api/v1/video/gettvsub',{tmdb_id:tmdb_id},function showData(data)
		{
			var video=videojs('my-video');
			sub=data;
			if(sub=="")
			{
				console.log("No subtitles");
				var oldTracks = player.remoteTextTracks();
				var i = oldTracks.length;
				while (i--) {
				  player.removeRemoteTextTrack(oldTracks[i]);
				}
			}
			else
			{
				$('.vjs-subs-caps-button').show();
					for(var k=0;k<sub.length;k++)
					{
						video.addRemoteTextTrack({
	  					kind: 'captions', 
	  					label:sub[k]['language'],
	  					src: Apath+'uploads/srt/'+sub[k]['subtitle'] });
					}
			}
		})
		video.load();
		video.play();
		// 
		$.post(Apath+'api/v1/analytics/videoPlay',{title:show+' - S'+('0' + sNumber).slice(-2)+'E'+('0' + epnumber).slice(-2)+' - '+epname,type:"tv"},function showData()
		{

		})
	}
  }

    function getepisodes(epid,param)
    {
    	$('.snav a').removeClass('selected');
 		$(param).addClass('selected');
 		$.post(Apath+'includes/processes/episodes_process.php',{chk:'ajax',epid:epid},function showData(data)
    	{
    		if(data != "null")
    		{
    			var eBody ='';
    			// console.log(data);
    			var allData=JSON.parse(data);

    		
    				for (k=0; k < allData.length; k++)
    				{
    					if(allData[k]['poster_path']==null)
    					{
    						poster=Apath+'includes/img/blank_150.png';
    					}
    					else
    					{
    						poster='https://image.tmdb.org/t/p/w300'+allData[k]['poster_path'];
    					}
    					video_duration=allData[k]['video_duration'];
						video_remaining=allData[k]['video_remaining'];
						if(video_remaining != null && video_duration != null)
						{
							// echo $video_duration-$video_remaining;
								remaining=((video_duration-video_remaining)*100)/video_duration;
						}
						else
						{
							remaining=0;
						}
						show=$('#mvoverlay').children('h1').html().split('<small>')[0];
					
						eBody+='<div class="hover10 column" id="episode"><figure><a href="javascript:void(0)" ><img width="300" height="170" src="'+poster+'"></a></figure><h2>'+allData[k]['episode_number']+'. '+allData[k]['episode_name']+'</h2><p class="ttip">'+allData[k]['air_date']+'</p><div class="play_buttons"><div class="row downshow"><a href="javascript:void(0)" onclick="play(\''+allData[k]['base_path']+'\','+allData[k]['scid']+',\''+escape(allData[k]['episode_name'])+'\','+allData[k]['episode_number']+',\''+show+'\','+allData[k]['tmdb_id']+');"><div class="col-md-6" id="play_button">	<i class="fa fa-2x fa-play-circle-o"></i>Play</div></a><a href="javascript:void(0)" class="downLink"><div class="col-md-6" id="down_button"><i class="fa fa-2x fa-download"></i>Download</div></a></div><div class="row downhide"><div class=""><a href="'+Apath+'download/tv/'+allData[k]['ep_id']+'"  title="Download '+allData[k]['episode_name']+'"><div class="col-md-12" style="text-align:center"><i class="fa fa-2x fa-download"></i> '+allData[k]['size']+' - .mp4</div></a></div></div></div></div>';
			  		}
			  	
		  		$('#allEpisodes').html(eBody);
    		}
    		else
    		{
    			$('#allEpisodes').html("<h1 style='text-align:center'>No Episodes Found Yet.</h1>");
    			
    		}
    	})
    }
</script>
<script src="<?php echo $absolutepath ?>includes/js/play_control.js"></script>