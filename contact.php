<style type="text/css">
  #processing
  {
    display: none;
  }
</style>
<?php

session_start();
if(isset($_SESSION['userId']))
{
   include 'header.php';

}
else
{
  include 'login-nav.php';
}

 ?>
<link rel="stylesheet" type="text/css" href="<?php $absolutepath ?>includes/css/contact.css">
<div class="container" style="padding-bottom: 15px;">
<div class="alert alert-danger">

  <p class="error-msg"></p>
</div>
<div class="alert alert-success alert-dismissable">

   <p class="success-msg"></p>
</div>
	<div class="" style="text-align: center;">
		<div class="heading">
     <?php
         if(isset($_SESSION['userId']))
        {
			     echo '<h1>Contact us / Request any movie or Tv show</h1>';
		    }
        else
        {
          echo '<h1>Contact</h1>';
        }
        ?>
    </div>
		  <div class="row" id="contactFrom">
    <div class="col-sm-6 col-sm-offset-3">
        <form id="request_form" name="request_form">
        <?php
        if(isset($_SESSION['userId']))
        {

        }
        else
        {
          echo '<div class="form-group">
            <span>Your email</span>
            <input type="text" id="guest_email" name="guest_email" class="form-control" id="" placeholder="&#xf003; Enter your email">
          </div>';
        }

        ?>  

          <div class="form-group">
            <span>Subject</span>
            <input type="text" id="subject" name="subject" class="form-control" id="" placeholder="&#xf02b; Enter Subject">
          </div>
        <?php
         if(isset($_SESSION['userId']))
        {
          echo '          

             <div class="form-group">
              <span>Type:</span>
             
                <label class="radio-inline"><input type="radio" name="video_type" value="1" checked="checked">Movie</label>
              <label class="radio-inline"><input type="radio" name="video_type" value="2">Tv Show</label>
             
          </div>

              <div class="form-group">
            <span>Title Suggestion:</span>
            <input type="text" class="form-control" id="title" name="title" placeholder="Enter your suggestion here">
          </div>

          <div class="form-group">
            <span>IMDB Link:</span>
           <input type="text" class="form-control" id="imdb_link" name="imdb_link" placeholder="IMDB link for suggested content">
          </div>
          ';
        }
        else
        {

          }

          ?>


       

       
    
         

  <div class="form-group">
    <span >Message</span>
    <textarea class="form-group" id="message" name="message" placeholder="&#xf075;  Enter Your valueable message"></textarea>
  </div>
  
<?php
         if(isset($_SESSION['userId']))
        {
        echo "<p id='contactNotification'>A human will contact you at  ". $_SESSION['user_email']." within 24 hours.
      If you're reporting a problem, it's a good idea to make your message detailed.</p>";
}
else{
  
}
?>
  <button type="submit" title="submit" class="btn btn-default"><i id="arrow" class="fa fa-arrow-right" aria-hidden="true"></i><img id="processing" src="https://hiretual.com/assets/img/loading.gif" width="15"></button>

          
      
 
  
</form>
    </div>
  </div>
	</div>
</div>
<?php include_once "footer.php" ?>
<script type="text/javascript" src="<?php $absolutepath ?>includes/js/contact.js"></script>