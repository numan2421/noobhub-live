<!-- All processes regaring to admin -->

<?php
session_start();
include_once '../../includes/config.php';
include_once '../classes/admin_class.php';
require('../classes/mailer/class.phpmailer.php');
require('../classes/mailer/class.smtp.php');

//Headers 
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
//End Headers

if(isset($_REQUEST['action']))
{
	switch ($_REQUEST['action']) {
		case 'signup':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			//Data Validation
			if(isset($_REQUEST['email']))
			{
				$adminObject->admin_email=$_REQUEST['email'];	
			}
			else
			{
				$adminObject->error('Error','signup','email missing');
				//'ERROR! email missing';
				break;
			}
			if(isset($_REQUEST['name']))
			{
				$adminObject->admin_name=$_REQUEST['name'];
			}
			else
			{
				$adminObject->error('Error','signup','Full name missing');
				//  'ERROR! first name missing';
				break;
			}
			if(isset($_REQUEST['username']))
			{
				$adminObject->admin_username=$_REQUEST['username'];
			}
			else
			{
				$adminObject->error('Error','signup','username missing');
				//  'username missing';
				break;
			}
			if(isset($_REQUEST['password']))
			{
				$adminObject->admin_password=md5($_REQUEST['password']);
			}
			else
			{
				$adminObject->error('Error','signup','password missing');
				//  'ERROR! password missing';
				break;
			}
			//End
			$adminObject->signup();
			

		break;
		case 'login':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['username']))
			{
				$adminObject->admin_username=$_REQUEST['username'];
			}
			else
			{
				$adminObject->error('Error','login','username missing');
				exit();

			}
			if(isset($_REQUEST['password']))
			{
				$adminObject->admin_password=md5($_REQUEST['password']);
			}
			else
			{
				$adminObject->error('Error','login','password missing');
				exit();
			}
			 $adminObject->login();
		break;

		case 'forgetpassword':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['email']))
			{
				$adminObject->admin_email=$_REQUEST['email'];
			}
			else
			{
				$adminObject->error('Error','forgetpassword','email missing');
				exit();
			}

			$adminObject->forgetPassword();
		break;

		case 'resetPassword':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['reset_code']))
			{
				$adminObject->reset_code=$_REQUEST['reset_code'];
			}
			else
			{
				$adminObject->error('Error','resetPassword','reset code missing');
				exit();
			}
			if(isset($_REQUEST['new_password']))
			{
				$adminObject->admin_password=md5($_REQUEST['new_password']);
			}
			else
			{
				$adminObject->error('Error','resetPassword','new password missing');
				exit();
			}
			$adminObject->resetPassword();
		break;

		case 'approveadmin':
		 $adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		 if(isset($_REQUEST['email']))
			{
				$adminObject->approve_email=$_REQUEST['email'];
			}
			else
			{
				$adminObject->error('Error','approveadmin','email missing');
				exit();
			}
			$adminObject->approveUser('admins');
		break;

		case 'approveUser':
		 $adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		 if(isset($_REQUEST['email']))
			{
				$adminObject->approve_email=$_REQUEST['email'];
			}
			else
			{
				$adminObject->error('Error','approveUser','email missing');
				exit();
			}
			$adminObject->approveUser('users');
		break;

		case 'pendingusers':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		if(isset($_REQUEST['user_type']))
			{
				switch ($_REQUEST['user_type']) 
				{

					case 'users':
						$customerData=$adminObject->getPendingusers('users');
						$adminObject->success(200,'getPendingusers',$customerData);
						break;
					case 'admin':
						$adminData=$adminObject->getPendingusers('admins');
						$adminObject->success(200,'getPendingusers',$adminData);
						break;
				}
			}
			else
			{	
				//get all pending users
				$adminObject->getPendingusers('');					
			}
		break;

		case 'userData':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['user_type']))
			{
				if(isset($_REQUEST['id']))
				{
					$adminObject->userId=$_REQUEST['id'];
					switch ($_REQUEST['user_type']) 
					{
	
						case 'customer':
							$customerData=$adminObject->getUserData('users','user_id');
							$adminObject->success(200,'getUserData',$customerData);
							break;
						case 'admin':
							$adminData=$adminObject->getUserData('admins','admin_id');
							$adminObject->success(200,'getUserData',$adminData);
							break;
					}
				}
				else
				{

					switch ($_REQUEST['user_type']) 
					{
	
						case 'customer':
							$customerData=$adminObject->getallusers('users');
							$adminObject->success(200,'getUserData',$customerData);
							break;
						case 'admins':
							$adminData=$adminObject->getallusers('admins');
							$adminObject->success(200,'getUserData',$adminData);
							break;
					}
					// $adminObject->error('Error','getUserData','user id missing');
					// exit();	
				}
					
			}
			else
			{
				$adminObject->error('Error','getUserData','user type missing');
				exit();	

			}
		break;

		case 'session':
		// print_r($_REQUEST['Data']);
		$allData=$_REQUEST['Data'];
			$_SESSION['admin_id']=$allData[0]['admin_id'];
			$_SESSION['admin_name']=$allData[0]['name'];
			$_SESSION['admin_type']=$allData[0]['type'];
			$_SESSION['admin_last_login']=$allData[0]['last_login'];

			if(isset($_SESSION['admin_id']))
			{
				echo 1;
				exit();
			}
			else
			{
				echo 2;
				exit();
			}
		break;


		case 'addgenre':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);

			if(isset($_REQUEST['genre']))
			{
				$adminObject->genre=$_REQUEST['genre'];
			}
			else
			{
				$adminObject->error('Error','addgenre','Genre Title missing');
				exit();	
			}
			if(isset($_REQUEST['tmdb_id']) && $_REQUEST['tmdb_id'] != "")
			{
				$adminObject->tmdb_id=$_REQUEST['tmdb_id'];
			}
			else
			{	
				$adminObject->tmdb_id=NULL;
			}
			if(isset($_REQUEST['page_action']) && $_REQUEST['page_action']=='save')
			{
				//save
				$adminObject->addgenre();
				
			}
			else
			{
				
				//edit
				$adminObject->genreId=$_REQUEST['genId'];
				$adminObject->editGenre();
			}
			
		break;

		case 'allGenres':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->allGenres();


		break;

		case 'removeGenre':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['id']))
			{
				$adminObject->genreId=$_REQUEST['id'];
			}
			else
			{
				$adminObject->error('Error','removeGenre','Genre Id is missing');
				exit();	
			}
			$adminObject->removeGenre();
		break;

		case 'reactiveGenre':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['id']))
			{
				$adminObject->genreId=$_REQUEST['id'];
			}
			else
			{
				$adminObject->error('Error','reactiveGenre','Genre Id is missing');
				exit();	
			}
			$adminObject->reactiveGenre();
		break;

		case 'saveOnlineData':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['data']))
			{
				$adminObject->genreData=$_REQUEST['data'];
			}
			else
			{
				$adminObject->error('Error','saveOnlineData','Online Genre Data Missing');
				exit();	
			}
			$adminObject->saveOnlineData();
		break;

		case 'updatetoSuper':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['id']))
			{
				$adminObject->adminId=$_REQUEST['id'];
			}
			else
			{
				$adminObject->error('Error','updatetoSuper','Admin Id is missing');
				exit();	
			}
				if(isset($_REQUEST['type']))
			{
				$adminObject->adminType=$_REQUEST['type'];
			}
			else
			{
				$adminObject->error('Error','updatetoSuper','Admin Type is missing');
				exit();	
			}
			$adminObject->updatetoSuper();
		break;

		case 'adminBysuper':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			//Data Validation
			if(isset($_REQUEST['email']))
			{
				$adminObject->admin_email=$_REQUEST['email'];	
			}
			else
			{
				$adminObject->error('Error','adminBysuper','email missing');
				//'ERROR! email missing';
				break;
			}
			if(isset($_REQUEST['name']))
			{
				$adminObject->admin_name=$_REQUEST['name'];
			}
			else
			{
				$adminObject->error('Error','adminBysuper','Full name missing');
				//  'ERROR! first name missing';
				break;
			}
			if(isset($_REQUEST['username']))
			{
				$adminObject->admin_username=$_REQUEST['username'];
			}
			else
			{
				$adminObject->error('Error','adminBysuper','username missing');
				//  'username missing';
				break;
			}
			if(isset($_REQUEST['password']))
			{
				$adminObject->admin_password=md5($_REQUEST['password']);
			}
			else
			{
				$adminObject->error('Error','adminBysuper','password missing');
				//  'ERROR! password missing';
				break;
			}
			if(isset($_REQUEST['admin_type']))
			{
				$adminObject->adminType=$_REQUEST['admin_type'];
			}
			else
			{
				$adminObject->error('Error','adminBysuper','Admin Type missing');
				//  'ERROR! password missing';
				break;
			}
			//End
			$adminObject->adminBysuper();
		break;

		case 'inviteUser':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			//Data Validation
			if(isset($_REQUEST['user_email']))
			{
				$adminObject->user_email=$_REQUEST['user_email'];	
			}
			else
			{
				$adminObject->error('Error','inviteUser','email missing');
				//'ERROR! email missing';
				break;
			}
			if(isset($_REQUEST['greeting_message']))
			{
				$adminObject->greeting_message=$_REQUEST['greeting_message'];	
			}
			else
			{
				$adminObject->error('Error','inviteUser','Greeting message missing');
				//'ERROR! email missing';
				break;
			}
			$adminObject->profiles=$_REQUEST['profiles'];
			$adminObject->inviteUser();

		break;

		case 'addCountires':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		
			// print_r($_REQUEST);
				if(isset($_REQUEST['countires']))
				{
					$adminObject->countires=$_REQUEST['countires'];
				}
				else
				{
					
				}
				$adminObject->addCountires();


		break;

		case 'addrestrictedCountires':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		
			// print_r($_REQUEST);
				if(isset($_REQUEST['web_countries']))
				{
					$adminObject->countires=$_REQUEST['web_countries'];
				}
				else
				{
					
				}
				$adminObject->addrestrictedCountires();


		break;

		case 'getCountries':
				$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
				$adminObject->getCountries();

		break;

		case 'getsignupSettings':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->getSignupsetting();
		break;


		case 'getmovie':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['mv_id'];
			$adminObject->getmovebyid();
		break;
		case 'updatemovies':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['mov_id'];
			$adminObject->kids=$_REQUEST['kids'];
			$adminObject->updatemovies();
		break;
		case 'deleteMovie':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['mv_id'];
			$adminObject->deleteMovie();
		break;
		case 'gettv':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['tv_id'];
			$adminObject->gettvbyid();
		break;
		case 'updatetv':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['tvId'];
			$adminObject->kids=$_REQUEST['kids'];
			$adminObject->updatetv();
		break;
		case 'deletetv':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->mov_id=$_REQUEST['mv_id'];
			$adminObject->deletetv();
		break;

		case 'addsignupSettings':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['status']))
			{
				$adminObject->setting_status=$_REQUEST['status'];	
			}
			else
			{
				$adminObject->error('Error','addsignupSettings','Setting status missing');
				//'ERROR! email missing';
				break;
			}

			if(isset($_REQUEST['setting_id']))
			{
				$adminObject->setting_id=$_REQUEST['setting_id'];	
			}
			else
			{
				$adminObject->error('Error','addsignupSettings','Setting id missing');
				//'ERROR! email missing';
				break;
			}
			$adminObject->addsignupSettings();
		break;

		case 'movieCleanup':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		$adminObject->mCleanup();
		break;


		case 'tvdeletedsync':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->tvdeletedsync();
		break;

		case 'getrestrictedCountries':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->getrestrictedCountries();

		break;
		case 'updateDeleted':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		
			$adminObject->updateDeleted();
		break;

		case 'movieSync':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->syncmovies2();
		break;
		case 'syncshowid':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
			$adminObject->syncshowid();
		break;

		case 'crewSync':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
			$adminObject->syncCrew();
		break;

		case 'tvcrewSync':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
			$adminObject->tvcrewSync();
		break;

		case 'tvSync':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);

			$adminObject->synctv();
		break;

		case 'mvDuplicates':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
			$adminObject->mvDuplicates();
		break;

		case 'getmissingtv':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->gettvmissing();
		break;

		case 'sendEmails':
			$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$adminObject->eBody=$_REQUEST['body'];
			$adminObject->emails=$_REQUEST['email'];
			$adminObject->eSubject=$_REQUEST['subject'];
			$adminObject->sendEmails();
		break;

		case 'shows_diff_from_sc':
				$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$adminObject->shows_diff_from_sc();
		break;

		case 'fixepisodes':
		$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
		$adminObject->fixEpisodes();
		break;

		
		default:
			# code...
		break;
	}
}
else
{
	$adminObject=new admin(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	//error action is missing
	$adminObject->error('Error','','action missing');
}





?>