<?php
session_start();
include_once '../../includes/config.php';
include_once '../classes/video_class.php';
require('../classes/mailer/class.phpmailer.php');
include('../classes/mailer/class.smtp.php');

//Headers 
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
//End Headers

if(isset($_REQUEST['action']))
{
	switch ($_REQUEST['action']) {
		case 'addmovie':
			$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['data']))
			{
			
					$allData=$_REQUEST['data'];
					//parse data object to get requierd detials. No need for validation as it is online db .
					$videoObject->type='movie';
					$videoObject->liveId=$_REQUEST['lid'];
					$videoObject->reviews=$allData['reviews'];
					if(isset($allData['production_companies']))
					{
						$videoObject->production=$allData['production_companies'];
					}
					else
					{
						$videoObject->production="";
					}
					
					$videoObject->images=$allData['images'];
					$videoObject->languages=$allData['spoken_languages'];
					
					if(isset($allData['videos']))
					{
						$videoObject->intro_videos=$allData['videos'];
					}
					else
					{
						$videoObject->intro_videos="";
					}
					$videoObject->poster=$allData['poster_path'];
					$videoObject->cast=$allData['credits']['cast'];
					$videoObject->genres=$allData['genres'];
					$videoObject->tmdb_id=$allData['id'];
					$videoObject->imdb_id=$allData['imdb_id'];
					$videoObject->ranking=$allData['vote_average'];
					$videoObject->title=$allData['original_title'];
					if(isset($allData['production_countries']))
					{
						$videoObject->country=$allData['production_countries'][0];
					}
					else
					{
						$videoObject->country="";
					}
					
					$videoObject->overview=$allData['overview'];
					$videoObject->runtime=$allData['runtime'];
					$videoObject->revenue=$allData['revenue'];
					$videoObject->budget=$allData['budget'];
					$videoObject->tagline=$allData['tagline'];
					$videoObject->release_date=$allData['release_date'];
					$videoObject->status=$allData['status'];

					//Save main video details
					$addMovieid=$videoObject->addmovie();
			}
			else
			{
				$videoObject->error('Error','addmovie','Movie data is  missing');
				exit();	
			}
			break;

			case 'addTv':
			$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['data']))
			{
					if(isset($_REQUEST['showid']) && $_REQUEST['showid'] !=0)
					{
						$iddetaikls=$_REQUEST['showid'];
						if (strpos($iddetaikls, '?') !== false) 
						{
						    $idInfo=explode("?", $iddetaikls);
							$videoObject->showID=$idInfo[1];
						}
						else
						{
							$videoObject->showID=$iddetaikls;
						}
						
					}
					else
					{
						$videoObject->error('Error','addtv','Show id is not correct');
						exit();
					}
					$allData=$_REQUEST['data'];
					//parse data object to get requierd detials. No need for validation as it is online db .
					$videoObject->type='tv';
					
					if(isset($allData['production_companies']))
					{
						$videoObject->production=$allData['production_companies'];
					}
					else
					{
						$videoObject->production="";
					}
					
					$videoObject->images=$allData['images'];
					$videoObject->languages=$allData['languages'];
					
					if(isset($allData['videos']))
					{
						$videoObject->intro_videos=$allData['videos'];
					}
					else
					{
						$videoObject->intro_videos="";
					}
					if(isset($allData['credits']['cast']))
					{
						$videoObject->cast=$allData['credits']['cast'];
					}
					else
					{
						$videoObject->cast='';
					}
					
					$videoObject->genres=$allData['genres'];
					$videoObject->tmdb_id=$allData['id'];
					$videoObject->ranking=$allData['vote_average'];
					$videoObject->title=$allData['original_name'];
					if(count($allData['origin_country'])>0)
					{
						$videoObject->country=$allData['origin_country'][0];
					}
					else
					{
						$videoObject->country="";
					}
					$videoObject->poster=$allData['poster_path'];
					$videoObject->overview=$allData['overview'];
					$videoObject->runtime=$allData['episode_run_time'][0];
					
			
					$videoObject->release_date=$allData['first_air_date'];
					$videoObject->last_air_date=$allData['last_air_date'];
					if(count($allData['seasons'])>0)
					{
						$videoObject->seasons=$allData['seasons'];
						$videoObject->number_of_seasons=$allData['number_of_seasons'];

					}
					else
					{
						$videoObject->seasons="";
						$videoObject->number_of_seasons=0;
					}

					if (isset($allData['networks']))
					{
						$videoObject->networks=$allData['networks'];
					}
					else
					{
						$videoObject->networks="";
					}
					$videoObject->status=$allData['status'];
					if($allData['in_production']==true)
					{
						$videoObject->in_production=true;
					}
					else
					{
						$videoObject->in_production=false;
					}
					$videoObject->tvtype=$allData['type'];

					//Save main video details
					$addMovieid=$videoObject->addTv();
			}
			else
			{
				$videoObject->error('Error','addtv','TV show data is  missing');	
			}
			break;

			case 'getallmovies':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['start'])&& isset($_REQUEST['end']))
				{
					$videoObject->getallmoviesData($_REQUEST['start'],$_REQUEST['end']);
				}
				else
				{
					$videoObject->getallmoviesData('','');	
				}
			break;

			case 'getalltv':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['start'])&& isset($_REQUEST['end']))
				{
				$videoObject->getalltvData($_REQUEST['start'],$_REQUEST['end']);	
				}
				else
				{
					$videoObject->getalltvData("","");	
				}

			break;

			case 'rTimer':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['tlength']))
				{
					$videoObject->vLength=$_REQUEST['tlength'];
				}
				else
				{
					$videoObject->error('Error','rTimer','Total length Missing');
					break;
				}
				if(isset($_REQUEST['rTime']))
				{
					$videoObject->rTime=$_REQUEST['rTime'];
				}
				else
				{
					//error
					$videoObject->error('Error','rTimer','Time missing');
					break;
				}
				if(isset($_REQUEST['video_id']))
				{
					$videoObject->video_id=$_REQUEST['video_id'];
				}
				else
				{
					//error
					$videoObject->error('Error','rTimer','video id missing');
					break;
				}
				if(isset($_REQUEST['video_type']))
				{
					$videoObject->type=$_REQUEST['video_type'];
				}
				else
				{
					//error
					$videoObject->error('Error','rTimer','video type missing');
					break;
				}
				$videoObject->rTimer();
			break;

			
			case 'getepisodes':

			$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['season_id']))
			{
				$episodes=$videoObject->getepisodes($_REQUEST['season_id']);
				$videoObject->success(200,'getepisodes',$episodes);
				break;
			}
			else
			{
				$videoObject->error('Error','getepisodes','Season id is misssing');
				break;	
			}
			break;
			
			case 'gettvdetails':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['tv_id']))
				{
					$videoObject->id=$_REQUEST['tv_id'];
				}
				else
				{
					$videoObject->error('Error','gettvdetails','Show id is missing');
					break;
				}
				$videoObject->gettvdetails();
			break;
			
			case 'searchTv':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['query']))
				{
					$videoObject->tvQuery=$_REQUEST['query'];
					$videoObject->searchTv($_REQUEST['start'],$_REQUEST['end']);
				}
			break;

			case 'tvlinkSync':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->tvlinkSync();
			break;

			case 'movielinksync':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->movielinksync();
			break;


			case 'searchmovies':
			$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['query']))
				{
					$videoObject->mvQuery=$_REQUEST['query'];
					$videoObject->searchMv($_REQUEST['start'],$_REQUEST['end']);
				}
			break;
			case 'test':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->test();
			break;
			case 'synctvepisodes':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->synctvepisodes();
			break;

			case 'discoverdata':
					$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
					if(isset($_REQUEST['start']))
					{
						$videoObject->start=$_REQUEST['start'];
					}
					else
					{
						$videoObject->start=0;
					}
					if(isset($_REQUEST['check']))
					{
						$videoObject->check=$_REQUEST['check'];
					}
					else
					{
						$videoObject->check="";
					}
					
					$query=$_REQUEST['query'];
					switch ($query) 
					{
						case 'trending':
							if($_REQUEST['data']=="")
							{
								$videoObject->gettrending();
							}
							else
							{
								$videoObject->getgenretrending();
							}
						break;
						case 'popular';
							if($_REQUEST['data']=="")
							{
								$videoObject->getpopular();
							}
							else
							{
								$videoObject->getgenrepopular();
							}
						break;
						case 'latestep':
							if($_REQUEST['data']=="")
							{
								$videoObject->getlatesttv();
							}
							else
							{

							}
						break;
						case 'recentmv':
							if($_REQUEST['data']=="")
							{
								$videoObject->getrecent();
							}
							else
							{
								$videoObject->getgenrerecent();
							}
						break;
						case 'highestgross':
							if($_REQUEST['data']=="")
							{
								$videoObject->highestgross();
							}
							else
							{
								$videoObject->highestgenregross();
							}
						break;
						case 'popular-movies':
							if($_REQUEST['data']=="")
							{
								$videoObject->popmovies();
							}
							else
							{
								$videoObject->popgenreMovies();
							}
						break;
						case 'movie-year2':
							if($_REQUEST['data']=="")
							{
								$videoObject->year2();
							}
							else
							{
								$videoObject->year2gen();
							}
						break;
						case 'movie-year1':
							if($_REQUEST['data']=="")
							{
								$videoObject->year1();
							}
							else
							{
								$videoObject->year1gen();
							}
						break;
						case 'most-voted-movies':
							if($_REQUEST['data']=="")
							{
								$videoObject->votedMovies();
							}
							else
							{
								$videoObject->votedgenreMovies();
							}
						break;
						case 'popular-tv':
							if($_REQUEST['data']=="")
							{
								$videoObject->poptv();
							}
							else
							{
								$videoObject->popgentv();
							}
						break;
						case 'tv-year2':
							if($_REQUEST['data']=="")
							{
								$videoObject->tvyear2();
							}
							else
							{
								$videoObject->tvgenreyear2();
							}
						break;
						case 'tv-year1':
							if($_REQUEST['data']=="")
							{
								$videoObject->tvyear1();
							}
							else
							{
								$videoObject->tvgenreyear1();
							}
						break;
						case 'most-voted-tv':
							if($_REQUEST['data']=="")
							{
								$videoObject->votedTv();
							}
							else
							{
								$videoObject->votedgenreTv();
							}
						break;
					}
			break;

			case 'getgenredata':
				if(isset($_REQUEST['query']))
				{
					$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
					if(isset($_REQUEST['start']))
					{
						$videoObject->start=$_REQUEST['start'];
					}
					else
					{
						$videoObject->start=0;
					}
					if(isset($_REQUEST['check']))
					{
						$videoObject->check=$_REQUEST['check'];
					}
					else
					{
						$videoObject->check="";
					}
					
					$query=$_REQUEST['query'];
					switch ($query) 
					{
						case 'allshows':
							//get all tv data	
							$videoObject->getalltv();
						break;
						case 'allmovies':
							//get all mv data	
							$videoObject->getallmovie();
						break;

						case 'genre':
							$videoObject->getallgenmovie();
						break;

						case 'genretv':
							$videoObject->getallgentv();
						break;


						
						case 'dreamwork':
							if(isset($_REQUEST['check']))
							{
								
								$videoObject->getproductin("dreamwork");
							}
							else
							{
								$videoObject->error('Error',"getgenredata","NO RECORD FOUND");
								exit();
							}
						break;
						case 'disney':
							if(isset($_REQUEST['check']))
							{
								
								$videoObject->getproductin("disney");
							}
							else
							{
								$videoObject->error('Error',"getgenredata","NO RECORD FOUND");
								exit();
							}
						break;
						case 'recent':
							$videoObject->getrecent();
						break;
						case 'latest':
							$videoObject->getlatest();
						break;

						case 'latesttv':
						$videoObject->getlatesttv();
						break;
						
						default:
							$videoObject->genid=$_REQUEST['query'];
							$videoObject->getgenData();
						break;
					}
				}
				else
				{
					$videoObject->error('Error','getgenredata','query missing');
					break;
				}

			break;

			
			case 'updateShowid':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->updateShowid();
			break;

			case 'gettvsub':
				$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				$videoObject->gettvsub($_REQUEST['tmdb_id']);
			break;

			case 'updatepg':
			$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$videoObject->updatepg();
			break;
		
		
		default:
			# code...
			break;
	}
}
else
{
	$videoObject=new video(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	//error action is missing
	$videoObject->error('Error','','action missing');
}


?>