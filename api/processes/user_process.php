<?php

include_once '../../includes/config.php';
include_once '../classes/user_class.php';
require('../classes/mailer/class.phpmailer.php');
include('../classes/mailer/class.smtp.php');

//Headers
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
//End Headers

if(isset($_REQUEST['action']))
{
	switch ($_REQUEST['action']) {
		case 'signup':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			//Server End Data Validation
			if(isset($_REQUEST['f_name']))
			{
				$userObject->f_name=$_REQUEST['f_name'];
			}
			else
			{
				$userObject->error('Error','signup','first name missing');
				break;
				//error First name is missing
			}
			if(isset($_REQUEST['user_email']))
			{
				$userObject->email=$_REQUEST['user_email'];
			}
			else
			{
				//ERROR Email missing
				$userObject->error('Error','signup','user email missing');
				break;
			}
			if(isset($_REQUEST['user_pass']))
			{
				$userObject->password=md5($_REQUEST['user_pass']);
			}
			else
			{
				//ERROR password is missing
				$userObject->error('Error','signup','user password missing');
				break;
			}
			$userObject->invite_code=$_REQUEST['code'];
			//Data Validation End
			if($_REQUEST['code']=="")
			{

				$userObject->signup();
			}
			else
			{
				$userObject->invited_signup();
			}

		break;

		case 'login':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			
			//chk for cookies
			//if cookie is set
			//cookie is not set
			if(isset($_REQUEST['user_email']))
			{
				$userObject->email=$_REQUEST['user_email'];
			}
			else
			{
				//ERROR username is missing
				$userObject->error('Error','login','User email missing');
				break;
			}
			if(isset($_REQUEST['user_pass']))
			{
				$userObject->password=md5($_REQUEST['user_pass']);
			}
			else
			{
				//ERROR username is password
				$userObject->error('Error','login','password missing');
				break;
			}
			if(isset($_REQUEST['remeber_me']))
			{
				$userObject->remember=$_REQUEST['remeber_me'];
			}
			$userObject->login();
			//end
		break;

		case 'forgetpassword':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['user_email']))
			{
				$userObject->email=$_REQUEST['user_email'];
			}
			else
			{
				//ERROR! email is missing
				$userObject->error('Error','forgetpassword','Email missing');
				break;

			}
			$userObject->forgetpassword();
		break;

		case 'resetPassword':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['reset_token']))
			{
				$userObject->reset_code=$_REQUEST['reset_token'];
			}
			else
			{
				$userObject->error('Error','resetPassword','reset code missing');
				exit();
			}
			if(isset($_REQUEST['new_password']))
			{
				$userObject->password=md5($_REQUEST['new_password']);
			}
			else
			{
				$userObject->error('Error','resetPassword','new password missing');
				exit();
			}
			$userObject->resetPassword();
		break;

		case 'addsubuser':
			$userObject=new user(HOSTDNS,DB_USER,DB_PASS,$arrPDODriverOptions);
			//Data Validation
				if(isset($_REQUEST['sub_email']))
				{
					$userObject->sub_email=$_REQUEST['sub_email'];
				}
				else
				{
					$userObject->error('Error','addsubuser','sub user email missing');
					exit();
				}
				if(isset($_REQUEST['first_name']))
				{
					$userObject->f_name=$_REQUEST['first_name'];
				}
				else
				{
					$userObject->error('Error','addsubuser','first name missing');
					exit();
				}
				if(isset($_REQUEST['username']))
				{
					$userObject->username=$_REQUEST['username'];
				}
				else
				{
					$userObject->error('Error','addsubuser','username missing');
					exit();
				}
				if(isset($_REQUEST['password']))
				{
					$userObject->password=md5($_REQUEST['password']);
				}
				else
				{
					$userObject->error('Error','addsubuser','password missing');
					exit();
				}
				if(isset($_REQUEST['parent_id']))
				{
					$userObject=$_REQUEST['parent_id'];
				}
				else
				{
					$userObject->pId=1;
				}
				if(isset($_REQUEST['middle_name']))
				{
					$userObject->m_name=$_REQUEST['middle_name'];
				}
				else
				{
					$userObject->m_name="";
				}
				if(isset($_REQUEST['last_name']))
				{
					$userObject->l_name=$_REQUEST['last_name'];
				}
				else
				{
					$userObject->l_name="";
				}
				//End
				$userObject->addsubuser();
		break;
		case 'profName':
		$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['pName']))
				{
					$userObject->pName=$_REQUEST['pName'];
				}
				else
				{
					$userObject->error('Error','profName','Profile Name missing');
					exit();
				}
			if(isset($_REQUEST['pId']))
				{
					$userObject->pId=$_REQUEST['pId'];
				}
				else
				{
					$userObject->error('Error','profName','Profile id missing');
					exit();
				}
			if(isset($_REQUEST['parent']))
			{
				$userObject->user_id=$_REQUEST['parent'];
			}
			else
			{
				$userObject->error('Error','profName','Parent id missing');
				exit();
			}
			$userObject->addProfileName();

		break;
		case 'addplaylist':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['id']))
			{
				$userObject->video_id=$_REQUEST['id'];
			}
			else
			{
				$userObject->error('Error','addplaylist','Video id missing');
				exit();
			}
			if(isset($_REQUEST['type']))
			{
				$userObject->video_type=$_REQUEST['type'];
			}
			else
			{
				$userObject->error('Error','addplaylist','Video type missing');
				exit();
			}
			$userObject->addPlaylist();
		break;

		case 'getfreeTotaltime':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->getfreeTotaltime();
		break;

		case 'getplaylist':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['id']))
			{
				$userObject->video_id=$_REQUEST['id'];
			}

			if(isset($_REQUEST['type']))
			{
				$userObject->video_type=$_REQUEST['type'];
			}

			$userObject->getplaylist();

		break;
		case 'saveRequest':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
				if(isset($_REQUEST['subject']))
				{
					$userObject->subject=$_REQUEST['subject'];
				}
				else
				{
					$userObject->error('Error','saveRequest','Subject is missing');
					exit();
				}
				if(isset($_REQUEST['message']))
				{
					$userObject->message=$_REQUEST['message'];
				}
				else
				{
					$userObject->error('Error','saveRequest','message is missing');
					exit();
				}
			$userObject->saveRequest();
		break;

		case 'getCompletedrequest':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['title']))
			{
				$userObject->title=$_REQUEST['title'];
			}
			else
			{
				$userObject->error('Error','Searchrequest','title is missing');
				exit();
			}
			if(isset($_REQUEST['imdb_link']))
			{
				$userObject->imdb_link=$_REQUEST['imdb_link'];
			}
			else
			{
				$userObject->error('Error','Searchrequest','imdb_link is missing');
				exit();
			}
			$userObject->getCompletedrequest();

		break;

		case 'markCompleterequests':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->markCompleterequests();
		break;

		case 'Searchrequest':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['title']))
			{
				$userObject->title=$_REQUEST['title'];
			}
			else
			{
				$userObject->error('Error','Searchrequest','title is missing');
				exit();
			}
			if(isset($_REQUEST['imdb_link']))
			{
				$userObject->imdb_link=$_REQUEST['imdb_link'];
			}
			else
			{
				$userObject->error('Error','Searchrequest','imdb_link is missing');
				exit();
			}
			$userObject->Searchrequest();

		break;
		case 'getallrequests':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->getallrequests();
		break;

		case 'validateCode':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if(isset($_REQUEST['code']))
			{
				$userObject->code=$_REQUEST['code'];
			}
			else
			{
				$userObject->error('Error','validateCode','Code missing');
				exit();
			}
			$userObject->ValidateCode();
		break;
		case 'addplaytimer':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->addplaytimer();
		break;
		
		case 'addtimeout':

			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->timeout();
		break;

		case 'exitkids':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->exitkids();
		break;

		case 'getmylist':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->getmylist();
		break;
		case 'updatepassword':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->updatepassword();
		break;
		case 'getVideolimit':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$userObject->getVideolimit();
		break;
		case 'updateProfile':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			if($_REQUEST['user_email']==$_SESSION['user_email'] && $_REQUEST['user_name']==$_SESSION['user_name'])
			{
				//old data dnt do anything
				$userObject->success(200,__FUNCTION__,'success');
			}
			else
			{	
				$userObject->username=$_REQUEST['user_name'];
				$userObject->email=$_REQUEST['user_email'];
				$userObject->updateProfile();

			}
		break;

		case 'setseesion':
			$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
			$_SESSION['profile_id']=$_REQUEST['pId'];
			$_SESSION['profile_image']=$_REQUEST['image'];
			$_SESSION['profile_name']=$_REQUEST['pName'];

			$userObject->success(200,__FUNCTION__,'success');
		exit();


		break;

		default:
			# code...
			break;
	}

}
else
{
	$userObject=new user(HOSTDNS, DB_USER, DB_PASS, $arrPDODriverOptions);
	//error action is missing
	$userObject->error('Error','','action missing');
}
?>
