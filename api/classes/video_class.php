<?php
define('CLASS_NAME', 'ADMIN');
class video extends PDO {

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception)
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }
    //class variable
public $video_id,$id,$type,$production,$reviews,$images,$languages,$intro_videos,$cast,$genres,$tmdb_id,$imdb_id,$ranking,$path,$title,$country,$overview,$runtime,$revenue,$first_air_date,$last_air_date,$in_production,$budget,$tagline,$release_date,$status,$networks,$seasons,$tvtype,$number_of_seasons,$episodes,$poster,$liveId,$rTime,$vLength,$tvQuery,$mvQuery,$start,$genid,$check,$showID;
    //utility functions
    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		echo json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		echo json_encode($success);
	}

	public function sendEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.support@yandex.com";
		// $mail->Password = "!bbtv_support321@";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(SUPPORT_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility
	public function sendbillingEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.billing@yandex.com";
		// $mail->Password = "@bbtv_billing321!";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(BILLING_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility

	//start tv addition
	public function addTv()
	{
			$chkQuery=$this->prepare('select * from tv_shows where name=:title or tmdb_id=:tid');
			$chkQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
			$chkQuery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_STR);
			$chkQuery->execute();
			if($chkQuery->rowCount()>0)
			{
				//Found Movie.
				$mData=$chkQuery->fetchAll(PDO::FETCH_ASSOC);
				$this->error('ERROR',__FUNCTION__,'TV show already available');
				exit();

			}
			else
			{
				//chk for uniqness of showid
				$cQuery=$this->prepare('select name from tv_shows where show_id=:sid');
				$cQuery->bindparam(':sid',$this->showID,PDO::PARAM_INT);
				$cQuery->execute();
				if($cQuery->rowCount()>0)
				{
					$showData=$cQuery->fetchAll();
					$showName=$showData[0]['name'];
					$this->error('ERROR',__FUNCTION__,'Show Id is already in user for '.$showName);
					exit();
				}
				else
				{
					$sQuery=$this->prepare('insert into tv_shows (name,tmdb_id,path,country,overview,episode_run_time,status,first_air_date,last_air_date,number_of_seasons,in_production,type,vote_average,poster,show_id) values (:title,:tId,:path,:country,:overview,:runTime,:status,:fDate,:lDate,:sNumber,:iProd,:type,:rank,:poster,:sId)');
					$sQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
					$sQuery->bindParam(':tId',$this->tmdb_id,PDO::PARAM_INT);
					$sQuery->bindParam(':path',$this->path,PDO::PARAM_STR);
					// print_r($this->country[0]['iso_3166_1']);
					$sQuery->bindParam(':country',$this->country,PDO::PARAM_STR);
					$sQuery->bindParam(':overview',$this->overview,PDO::PARAM_STR);
					$sQuery->bindParam(':runTime',$this->runtime,PDO::PARAM_INT);

					$sQuery->bindParam(':status',$this->status,PDO::PARAM_STR);

					$sQuery->bindParam(':rank',$this->ranking,PDO::PARAM_STR);
					$sQuery->bindParam(':fDate',$this->release_date,PDO::PARAM_STR);
					$sQuery->bindParam(':lDate',$this->last_air_date,PDO::PARAM_STR);
					$sQuery->bindParam(':sNumber',$this->number_of_seasons,PDO::PARAM_STR);
					$sQuery->bindParam(':iProd',$this->in_production,PDO::PARAM_STR);
					$sQuery->bindParam(':type',$this->tvtype,PDO::PARAM_STR);
					$sQuery->bindParam(':poster',$this->poster,PDO::PARAM_STR);
					$sQuery->bindParam(':sId',$this->showID,PDO::PARAM_INT);
					$sQuery->execute();
					if($sQuery->rowCount()>0)
					{
						$this->video_id=$this->lastInsertId();
						$addprod=$this->addproduction();
						if($addprod==1)
						{
							$addimages=$this->addimages();
							if($addimages==1)
							{
								$addlanguage=$this->addlanguage();
									if($addlanguage==1)
									{
										$addvideos=$this->addvideos();
										if($addvideos==1)
										{
											//saved
											//save cast
											$addcast=$this->addcast();
											if($addcast==1)
											{

												//saved
												// save genres
												$addgenres=$this->addgenres();
												if($addgenres==1)
												{

													$addNetworks=$this->addNetworks();
													if($addNetworks==1)
													{
														$addSeasons=$this->addSeasons();
														if($addSeasons==1)
														{
															$this->success(200,__FUNCTION__,'saved');
															exit();
														}
														else
														{
															//error
														}
													}
													else
													{
														//error delete
													}

												}
												else
												{
													//error delete all details
												}
											}
											else
											{

												//error
												//delete last inserted movie records
											}
										}
									}
									else
									{
										//delete entry
									}
							}
							else
							{
								//delete Entry
							}
						}
						else
						{
							//delete entry
						}

					}
				}
			}
	}


	//start add season
	public function addSeasons()
	{
		$videoId=$this->video_id;
		$tmdbId=$this->tmdb_id;
		//check if season is added
		$chkQuery=$this->prepare('select * from tv_season where tmdb_id=:tId and tv_id=:tvId');
		$sQuery=$this->prepare('insert into tv_season (season_number,poster_path,tmdb_id,episodes,tv_id,air_date) values (:sNumber,:pPath,:tmdb_id,:episodes,:tv_id,:airDate)');
		for($i=0;$i<count($this->seasons);$i++)
		{

			$season=$this->seasons[$i];
			$chkQuery->bindParam(':tId',$season['id'],PDO::PARAM_INT);
			$chkQuery->bindParam(':tvId',$videoId,PDO::PARAM_INT);
			$chkQuery->execute();
			if($chkQuery->rowCount()>0)
			{
				//found
			}
			else
			{

				$sQuery->bindParam(':sNumber',$season['season_number'],PDO::PARAM_STR);
				$sQuery->bindParam(':pPath',$season['poster_path'],PDO::PARAM_STR);
				$sQuery->bindParam(':episodes',$season['episode_count'],PDO::PARAM_STR);
				$sQuery->bindParam(':tmdb_id',$season['id'],PDO::PARAM_STR);
				$sQuery->bindParam(':tv_id',$videoId,PDO::PARAM_STR);
				$sQuery->bindParam(':airDate',$season['air_date'],PDO::PARAM_STR);
				$sQuery->execute();
				if($sQuery->rowCount()>0)
				{
					//saved save episodes next
					$lastId=$this->lastInsertId();
					$this->getSeasonDetails($lastId,$tmdbId,$season['season_number']);

				}
				else
				{
					// error revert all things
				}
			}
		}
		return 1;



	}
	// // end

	public function getSeasonDetails($lastid,$showID,$seasonNumber)
	{
		 $api=file_get_contents('https://api.themoviedb.org/3/tv/'.$showID.'/season/'.$seasonNumber.'?api_key=97e8434a62c5f9732e325e238b3aaf51&append_to_response=images,videos');
		 $allData = json_decode($api,true);

		 $episodes=$allData['episodes'];
		 $posters=$allData['images']['posters'];
		 $videos=$allData['videos'];

		//save season posters

    	$this->type="season";
    	$this->images=$allData['images'];

    	$this->addimages();
    	//posters saved
    	//videos
    	$this->intro_videos=$videos;
    	$this->addvideos();
    	//save videos
    	//espisodes
    	$this->episodes=$episodes;
    	$this->addepisodes($lastid,$showID);
    	//end

	}

	public function addepisodes($lastid,$stmdbId)
	{
		$chkQuery=$this->prepare('select ep_id from tv_season_episode where season_id=:sId and tmdb_id=:tid');
		for($i=0;$i<count($this->episodes);$i++)
		{
			$episode=$this->episodes[$i];
			$ep_tmdb_id=$episode['id'];
			$chkQuery->bindParam(':sId',$lastid,PDO::PARAM_INT);
			$chkQuery->bindParam(':tid',$ep_tmdb_id,PDO::PARAM_INT);
			$chkQuery->execute();
			if($chkQuery->rowCount()>0)
			{
				//episode already saved no need to do anything
			}
			else
			{
				//add new
				$sQuery=$this->prepare('insert into tv_season_episode (season_id,season_tmdb_id,air_date,overview,episode_name,episode_number,tmdb_id,poster_path,rank) values (:sid,:stid,:aDate,:overview,:eName,:eNmbr,:tId,:pPath,:rank)');
				$sQuery->bindParam(':sid',$lastid,PDO::PARAM_INT);
				$sQuery->bindParam(':stid',$stmdbId,PDO::PARAM_INT);
				$sQuery->bindParam(':aDate',$episode['air_date'],PDO::PARAM_STR);
				$sQuery->bindParam(':overview',$episode['overview'],PDO::PARAM_STR);
				$sQuery->bindParam(':eName',$episode['name'],PDO::PARAM_STR);
				$sQuery->bindParam(':eNmbr',$episode['episode_number'],PDO::PARAM_INT);
				$sQuery->bindParam(':tId',$episode['id'],PDO::PARAM_INT);
				$sQuery->bindParam(':pPath',$episode['still_path'],PDO::PARAM_STR);
				$sQuery->bindParam(':rank',$episode['vote_average'],PDO::PARAM_STR);
				$sQuery->execute();
			}
		}
		return 1;
	}
	//start Movie addition
	public function addmovie()
	{
			$chkQuery=$this->prepare('select * from movies where title=:title or tmdb_id=:tid or imdb_id=:iid');
			$chkQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
			$chkQuery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_STR);
			$chkQuery->bindParam(':iid',$this->imdb_id,PDO::PARAM_STR);
			$chkQuery->execute();
			if($chkQuery->rowCount()>0)
			{
				//Found Movie.
				$mData=$chkQuery->fetchAll(PDO::FETCH_ASSOC);
				$this->error('ERROR',__FUNCTION__,'already available');
				exit();

			}
			else
			{
				//save new
				$sQuery=$this->prepare('insert into movies (title,tmdb_id,imdb_id,path,live_video_id,release_date,country,overview,runtime,revenue,status,tagline,rank,budget,poster_path) values (:title,:tId,:iId,:path,:lid,:rData,:country,:overview,:runTime,:revenue,:status,:tagline,:rank,:budget,:pPath)');
				$sQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
				$sQuery->bindParam(':tId',$this->tmdb_id,PDO::PARAM_INT);
				$sQuery->bindParam(':iId',$this->imdb_id,PDO::PARAM_STR);
				$sQuery->bindParam(':path',$this->path,PDO::PARAM_STR);
				$sQuery->bindParam(':lid',$this->liveId,PDO::PARAM_STR);
				$sQuery->bindParam(':rData',$this->release_date,PDO::PARAM_STR);
				$sQuery->bindParam(':country',$this->country['iso_3166_1'],PDO::PARAM_STR);
				$sQuery->bindParam(':overview',$this->overview,PDO::PARAM_STR);
				$sQuery->bindParam(':runTime',$this->runtime,PDO::PARAM_INT);
				$sQuery->bindParam(':revenue',$this->revenue,PDO::PARAM_INT);
				$sQuery->bindParam(':status',$this->status,PDO::PARAM_STR);
				$sQuery->bindParam(':tagline',$this->tagline,PDO::PARAM_STR);
				$sQuery->bindParam(':rank',$this->ranking,PDO::PARAM_STR);
				$sQuery->bindParam(':budget',$this->budget,PDO::PARAM_INT);
				$sQuery->bindParam(':pPath',$this->poster,PDO::PARAM_INT);

				$sQuery->execute();
				if($sQuery->rowCount()>0)
				{
					$this->video_id=$this->lastInsertId();

					$addprod=$this->addproduction();
					if($addprod==1)
					{
						//saved
						// save reviews
						$addreview=$this->addreviews();
						if($addreview==1)
						{
							//saved
							//save images
							$addimages=$this->addimages();
							if($addimages==1)
							{

								//saved
								//save languagess
								$addlanguage=$this->addlanguage();
								if($addlanguage==1)
								{

									//saved
									//save videos
									$addvideos=$this->addvideos();
									if($addvideos==1)
									{

										//saved
										//save cast
										$addcast=$this->addcast();
										if($addcast==1)
										{

											//saved
											// save genres
											$addgenres=$this->addgenres();
											if($addgenres==1)
											{

												$this->success(200,__FUNCTION__,'saved');
												exit();
											}
											else
											{
												//error delete all details
											}
										}
										else
										{

											//error
											//delete last inserted movie records
										}
									}
									else
									{
										//error
										//delete last inserted movie records
									}

								}
								else
								{
									//error
									//delete last inserted movie records
								}
							}
							else
							{
								//error
							//delete last inserted movie records
							}
						}
						else
						{
							//error
							//delete last inserted movie records
						}
					}
					else
					{
						//error occuerd
						//delete last inserted movie record
					}

				}
				else
				{
					//main if
				}
			}


	}//End


	//start network tv
	public function addNetworks()
	{
	$sQuery=$this->prepare('insert into tv_networks (tmdb_id,net_name,tv_show_id) values (:tid,:nName,:tvId)' );
	$sQuery->bindParam(':tvId',$this->video_id,PDO::PARAM_INT);
	$sQuery->bindParam(':nName',$this->networks[0]['name'],PDO::PARAM_STR);
	$sQuery->bindParam(':tid',$this->networks[0]['id']);
	$sQuery->execute();
		if($sQuery->rowCount()>0)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
	//end

	//start addgenres

	public function addgenres()
	{
		$addQuery=$this->prepare('insert into video_genres (video_id,video_type,gen_tmdb_id) values (:vid,:vType,:gId) ');
		$genres=$this->genres;
		for($i=0;$i<count($genres);$i++)
		{
			$addQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			$addQuery->bindParam(':gId',$genres[$i]['id']);
			$addQuery->execute();
		}
		return 1;
	}
	//end

	//start add cast
	public function addcast()
	{
		$addQuery=$this->prepare('insert into video_cast (video_id,video_type,tmdb_character,tmdb_id,tmdb_cast_id,cast_name,tmdb_order,profile_path )values (:vid,:vType,:tCharacter,:tid,:tcid,:cName,:tOrder,:pPath)');
		$cast=$this->cast;
		for($i=0;$i<count($cast);$i++)
		{
			$addQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			$addQuery->bindParam(':tCharacter',$cast[$i]['character'],PDO::PARAM_INT);
			$addQuery->bindParam(':tid',$cast[$i]['credit_id'],PDO::PARAM_STR);
			$addQuery->bindParam(':tcid',$cast[$i]['id'],PDO::PARAM_INT);
			$addQuery->bindParam(':cName',$cast[$i]['name'],PDO::PARAM_STR);
			$addQuery->bindParam(':tOrder',$cast[$i]['order'],PDO::PARAM_INT);
			$addQuery->bindParam(':pPath',$cast[$i]['profile_path'],PDO::PARAM_STR);
			$addQuery->execute();

		}
		return 1;
	}
	//end

	//start addvideos
	public function addvideos()
	{
		$addQuery=$this->prepare('insert into video_intro_videos (video_type,video_id,tmdb_id,language,site_key,name,source_site,size,type) values (:vType,:vId,:tId,:lang,:sKey,:name,:sSite,:size,:type)');
		if($this->intro_videos=="")
		{
			return 1;
		}
		else
		{
			$videos=$this->intro_videos['results'];
			if(count($videos)>0)
			{
				for($i=0;$i<count($videos);$i++)
				{
					$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
					$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
					$addQuery->bindParam(':tId',$videos[$i]['id'],PDO::PARAM_STR);
					$addQuery->bindParam(':lang',$videos[$i]['iso_639_1'],PDO::PARAM_STR);
					$addQuery->bindParam(':sKey',$videos[$i]['key'],PDO::PARAM_STR);
					$addQuery->bindParam(':name',$videos[$i]['name'],PDO::PARAM_STR);
					$addQuery->bindParam(':sSite',$videos[$i]['site'],PDO::PARAM_STR);
					$addQuery->bindParam(':size',$videos[$i]['size'],PDO::PARAM_INT);
					$addQuery->bindParam(':type',$videos[$i]['type'],PDO::PARAM_STR);
					$addQuery->execute();

				}
				return 1;
			}
			else
			{
				//no videos
				return 1;
			}
		}

	}
	// end
	//start language
	public function addlanguage()
	{
		$addQuery=$this->prepare('insert into video_languages (video_id,video_type,name) values (:vId,:vType,:lan)');
		$languages=$this->languages;
		for($i=0;$i<count($languages);$i++)
		{
			$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			if($this->type=="tv")
			{
				$addQuery->bindParam(':lan',$languages[$i],PDO::PARAM_STR);
			}
			else
			{
				$addQuery->bindParam(':lan',$languages[$i]['iso_639_1'],PDO::PARAM_STR);
			}

			$addQuery->execute();
		}
		return 1;

	}
	//End

	//start add images
	public function addimages()
	{
		$posterVar='poster';
		$backVar='backdrop';
		$addQuery=$this->prepare('insert into video_images (image_type,filepath,video_id,video_type,language,aspect_ratio,height,width) values (:imgType,:fPath,:vId,:vType,:language,:aRatio,:height,:width)');
		$images=$this->images;
		if(isset($images['posters']) && count($images['posters'])>0)
		{
			$posters=$images['posters'];
			for($i=0;$i<count($posters);$i++)
			{

				$addQuery->bindParam(':imgType',$posterVar,PDO::PARAM_STR);
				$addQuery->bindParam(':fPath',$posters[$i]['file_path'],PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':language',$posters[$i]['iso_639_1'],PDO::PARAM_STR);
				$addQuery->bindParam(':aRatio',$posters[$i]['aspect_ratio'],PDO::PARAM_STR);
				$addQuery->bindParam(':height',$posters[$i]['height'],PDO::PARAM_INT);
				$addQuery->bindParam(':width',$posters[$i]['width'],PDO::PARAM_INT);
				$addQuery->execute();

			}
		}
		else
		{
			$posters=NULL;
		}
		if(isset($images['backdrops']) && count($images['backdrops'])>0)
		{
			$backdrops=$images['backdrops'];
			for($i=0;$i<count($backdrops);$i++)
			{

				$addQuery->bindParam(':imgType',$backVar,PDO::PARAM_STR);
				$addQuery->bindParam(':fPath',$backdrops[$i]['file_path'],PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':language',$backdrops[$i]['iso_639_1'],PDO::PARAM_STR);
				$addQuery->bindParam(':aRatio',$backdrops[$i]['aspect_ratio'],PDO::PARAM_STR);
				$addQuery->bindParam(':height',$backdrops[$i]['height'],PDO::PARAM_INT);
				$addQuery->bindParam(':width',$backdrops[$i]['width'],PDO::PARAM_INT);
				$addQuery->execute();
			}
		}
		else
		{
			$backdrops=NULL;
		}




		return 1;


	}
	// END

	//start add production
	public function addproduction()
	{
		//no need to check
		$addQuery=$this->prepare('insert into video_productions (video_type,video_id,company_name,company_tmdb_id) values (:vType,:vID,:company,:cid)');
		$production=$this->production;
		if($production=="")
		{
			return 1;
		}
		else
		{
			for($i=0;$i<count($production);$i++)
			{
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':vID',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':company',$production[$i]['name']);
				$addQuery->bindParam(':cid',$production[$i]['id']);
				$addQuery->execute();
			}
			return 1;
		}

	}//END

	//start Reviews
	public function addreviews()
	{
		$addQuery=$this->prepare('insert into video_reviews (video_type,video_id,author,content,tmdb_id) values (:vType,:vId,:author,:Content,:tId)');
		$reviews=$this->reviews;
		if($reviews['total_results']==0)
		{
			//no reviews
			return 1;
		}
		else
		{
			for($i=0;$i<count($reviews['results']);$i++)
			{
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':author',$reviews['results'][$i]['author']);
				$addQuery->bindParam(':Content',$reviews['results'][$i]['content']);
				$addQuery->bindParam(':tId',$reviews['results'][$i]['id']);
				$addQuery->execute();
			}
			return 1;
		}
		//

	}
	//End

	public function getallmoviesData($start,$limit)
	{
		if($start=='' && $limit == '')
		{
			$sQuery=$this->prepare('select imdb_id,mov_id,title,poster_path,rated,popularity from movies where is_active=1');
		}
		else
		{
			$sQuery=$this->prepare('select mov_id,title,poster_path,rated,popularity from movies where is_active=1 limit '.$start.', '.$limit);
		}

		$sQuery->execute();
		if($sQuery->rowCount()>0)
		{
			$moviesData=$sQuery->fetchAll(PDO::FETCH_ASSOC);
			$this->success(200,__FUNCTION__,$moviesData);
			exit();

		}
		else
		{
			$this->error('Error',__FUNCTION__,'Error!');
		}
	}

	public function getalltvData($start,$limit)
	{
		if($start=='' && $limit == '')
		{
			$sQuery=$this->prepare('select * from tv_shows where is_active=1');
		}
		else
		{
			$sQuery=$this->prepare('select * from tv_shows where is_active=1 limit '.$start.', '.$limit);
		}

		$sQuery->execute();
		if($sQuery->rowCount()>0)
		{
			$moviesData=$sQuery->fetchAll(PDO::FETCH_ASSOC);
			$this->success(200,__FUNCTION__,$moviesData);
			exit();

		}
		else
		{
			$this->error('Error',__FUNCTION__,'Error!');
			exit();
		}
	}


	public function rTimer()
	{
		//chk if flaged,if flaged remove session and cokkies and change password of profile,remove entry
			$cQuery=$this->prepare('select * from users where user_id=:uid');
			$cQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
			$cQuery->execute();
			if($cQuery->rowCount()>0)
			{
				$userData=$cQuery->fetchAll();
				if($userData[0]['max_screen_limit']==1)
				{
						// max limit reached logout
					//delete entry
					$dQuery=$this->prepare('delete from user_playing where profile_id=:pid');
					$dQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
					$dQuery->execute();

						//logout
						$exTime = time() - (60 * 60 * 24 * 7);
						setcookie("UserData", "", $exTime, '/');
						session_destroy();
				}
				else
				{

			          if($this->type=="tv")
			          {
			            $path="http://media.beanbag.tv/shows/ep".$this->video_id.".mp4";
			          }
			          else
			          {
			            $path="http://media.beanbag.tv/movies/".$this->video_id.".mp4";
			          }
			          
						$dQuery=$this->prepare('delete from user_playing where profile_id=:pid and video_type=:vtype and video_path=:vid');
						$dQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
						$dQuery->bindParam(':vid',$path,PDO::PARAM_STR);
						$dQuery->bindParam(':vtype',$this->type,PDO::PARAM_STR);
						$dQuery->execute();
			         
				}
			}

		//end
		$addQuery=$this->prepare("update user_watchlist set video_duration=:tLength , video_remaining=:rTime where video_id=:mId and video_type=:type and prof_id=:pid");
		$addQuery->bindParam(':tLength',$this->vLength,PDO::PARAM_STR);
		$addQuery->bindParam(':rTime',$this->rTime,PDO::PARAM_STR);
		$addQuery->bindParam(':mId',$this->video_id,PDO::PARAM_STR);
		$addQuery->bindParam(':type',$this->type,PDO::PARAM_STR);
		$addQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_STR);

		$addQuery->execute();
		if($addQuery->rowCount()>0)
		{
			

			$this->success(200,__FUNCTION__,'success');
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,$addQuery->errorInfo());
			exit();
		}


	}

	public function getallshows()
	{
		
	}

	public function getallmovie()
	{
		if($this->check=="kids")
		{
			$mvQuery=$this->prepare('select distinct mv.title,mv.rated,year(mv.release_date) as year,mv.poster_path,qu.imdb_rating,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" and mv.kids=1 order by mv.release_date desc limit  '.$this->start.',54');
		}
		else
		{
			$mvQuery=$this->prepare('select distinct mv.title,mv.imdb_id,mv.release_date,year(mv.release_date) as year,mv.date_added,mv.popularity,mv.poster_path,qu.imdb_rating,video_type,mv.rated from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by mv.release_date desc limit  '.$this->start.',54');
		}

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	$heading="All movies";
        	$alldata=array('heading'=>$heading,'data'=>$mvData);
        	$this->success(200,__FUNCTION__,$alldata);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function getrecent()
	{
	
		
			$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.date_added,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,mv.imdb_id,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by mv.date_added desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}






	public function getlatesttv()
	{
	
			$mvQuery=$this->prepare('select DISTINCT tv.tmdb_id as imdb_id,tse.air_date, year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.name as title,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,"tv" as video_type,tv.date_added from tv_shows tv left join tv_season ts on ts.tv_id = tv.tv_id left join tv_season_episode tse on tse.season_id=ts.season_id where tse.air_date < CURRENT_DATE and tv.is_active=1 order by tse.air_date desc limit '.$this->start.',54');
	

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}


	public function getlatest()
	{
		if($this->check=="kids")
		{
			$mvQuery=$this->prepare('select distinct mv.title,mv.rated,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and mv.kids=1 and vg.video_type="movie" order by mv.release_date desc limit  '.$this->start.',54');
		}
		else
		{
			$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by mv.release_date desc limit  '.$this->start.',54');
		}

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	$heading="All Latest";
        	$alldata=array('heading'=>$heading,'data'=>$mvData);
        	$this->success(200,__FUNCTION__,$alldata);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function getallgenmovie()
	{
		$sQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',54');
			$sQuery->execute();
			if($sQuery->rowCount()>0)
			{
				$sData=$sQuery->fetchAll();
				$alldata=array('data'=>$sData);
				$this->success(200,__FUNCTION__,$alldata);
				exit();
			}
	}


	public function getallgentv()
	{
		$sQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.first_air_date desc limit '.$this->start.',54');
			$sQuery->execute();
			if($sQuery->rowCount()>0)
			{
				$sData=$sQuery->fetchAll();
				$alldata=array('data'=>$sData);
				$this->success(200,__FUNCTION__,$alldata);
				exit();
			}
	}

	public function getgenrerecent()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by mv.date_added desc limit '.$this->start.',54');
		$getQuery->execute();
		$alltrendData=$getQuery->fetchAll();
		$this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}

	public function getgenretrending()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',27');
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id inner join tv_season ts on ts.tv_id=tv.tv_id where tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by ts.air_date desc limit '.$this->start.',27');
		$getQuery->execute();
		
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

       $this->success(200,__FUNCTION__,$obj_merged);
			exit();

	}

	public function getgenrepopular()
	{

		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by mv.popularity desc limit '.$this->start.',27');
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.popularity desc limit '.$this->start.',27');
		$getQuery->execute();
		
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

       $this->success(200,__FUNCTION__,$obj_merged);
			exit();

	}
	public function highestgenregross()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by mv.revenue desc limit '.$this->start.',54');	
		$getQuery->execute();
		 $alltrendData=$getQuery->fetchAll();
		   $this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}
	public function popgenreMovies()
	{
			$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie"   order by mv.popularity desc limit '.$this->start.',54');	
		$getQuery->execute();
		 $alltrendData=$getQuery->fetchAll();
		   $this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}
	public function year2gen()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" and year(mv.release_date) = year(current_date)-2 order by mv.popularity desc limit '.$this->start.',54');	
		$getQuery->execute();
		 $alltrendData=$getQuery->fetchAll();
		   $this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}
		public function year1gen()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" and year(mv.release_date) = year(current_date)-1 order by mv.popularity desc limit '.$this->start.',54');	
		$getQuery->execute();
		 $alltrendData=$getQuery->fetchAll();
		   $this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}
	public function votedgenreMovies()
	{
		$getQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.popularity,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type,mv.date_added,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="movie" order by qu.imdb_rating desc limit '.$this->start.',54');	
		$getQuery->execute();
		 $alltrendData=$getQuery->fetchAll();
		   $this->success(200,__FUNCTION__,$alltrendData);
			exit();
	}
	public function popgentv()
	{
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.popularity desc limit '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}

	}
	public function tvgenreyear2()
	{
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where and year(tv.first_air_date)=year(current_date)-2 and tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.popularity desc limit '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}

	}

	public function tvgenreyear1()
	{
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where and year(tv.first_air_date)=year(current_date)-1 and tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.popularity desc limit '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}

	}
	public function votedgenreTv()
	{
		$tQuery=$this->prepare('select distinct tv.tmdb_id as imdb_id, tv.name as title,year(tv.first_air_date) as year,tv.popularity,tv.poster as poster_path,tv.vote_average as imdb_rating,tv.overview,tv.first_air_date,video_type,tv.date_added from video_genres vg inner join tv_shows tv on tv.tv_id=vg.video_id  where tv.is_active=1 and vg.gen_tmdb_id IN (' . implode(',', $_REQUEST['data'] ). ')  and vg.video_type="tv" order by tv.vote_average desc limit '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}
	}

	public function votedTv()
	{
		$tQuery=$this->prepare('select DISTINCT tv_id,popularity,tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type,date_added FROM tv_shows where first_air_date < CURDATE() and is_active=1 order by vote_average DESC LIMIT '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}
	}

	public function tvyear1()
	{
		$tQuery=$this->prepare('select DISTINCT tv_id,popularity,tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type,date_added FROM tv_shows where first_air_date < CURDATE() and is_active=1 and year(first_air_date)=year(current_date)-1 order by popularity DESC LIMIT '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}	
	}

	public function tvyear2()
	{
		$tQuery=$this->prepare('select DISTINCT tv_id,popularity,tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type,date_added FROM tv_shows where first_air_date < CURDATE() and is_active=1 and year(first_air_date)=year(current_date)-2 order by popularity DESC LIMIT '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}	
	}

	public function poptv()
	{
		$tQuery=$this->prepare('select DISTINCT tv_id,popularity,tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type,date_added FROM tv_shows where first_air_date < CURDATE() and is_active=1 order by popularity DESC LIMIT '.$this->start.',54');
		$tQuery->execute();
		if($tQuery->rowCount()>0)
		{
			$tData=$tQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$tData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}

	}

	public function votedMovies()
	{
		$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.date_added,mv.runtime,mv.imdb_id,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type,mv.revenue,qu.imdb_rating from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by qu.imdb_rating desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function year1()
	{
			$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.date_added,mv.imdb_id,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" and year(mv.release_date) = year(current_date)-1 order by mv.popularity desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}



	public function year2()
	{
			$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.date_added,mv.runtime,mv.imdb_id,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" and year(mv.release_date) = year(current_date)-2 order by mv.popularity desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function popmovies()
	{
		$mvQuery=$this->prepare('select distinct mv.title,mv.imdb_id,year(mv.release_date) as year,mv.date_added,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.release_date,video_type,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by mv.popularity desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function highestgross()
	{

			$mvQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.date_added,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.date_added,mv.imdb_id,mv.release_date,video_type,mv.revenue from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.video_type="movie" order by mv.revenue desc limit  '.$this->start.',54');
		

        $mvQuery->execute();
        if($mvQuery->rowCount()>0)
        {
        	$mvData=$mvQuery->fetchAll();
        	
        	$this->success(200,__FUNCTION__,$mvData);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function getpopular()
	{
		$getQuery=$this->prepare('select mv.imdb_id,poster_path,popularity,title,imdb_rating,year(release_date) as year,rated,mv.date_added from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by popularity desc limit '.$this->start.',27');
        // get tv
        $tQuery=$this->prepare('select DISTINCT tv_id,popularity,tmdb_id as imdb_id,poster as poster_path,name as title,vote_average as imdb_rating,year(first_air_date) as year,rated,"tv" as video_type,date_added FROM tv_shows where first_air_date < CURDATE() and is_active=1 order by popularity DESC LIMIT '.$this->start.',27');


        $getQuery->execute();
        // print_r($getQuery->errorInfo());
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

       $this->success(200,__FUNCTION__,$obj_merged);
			exit();
	}


	public function gettrending()
	{
		$getQuery=$this->prepare('select mv.imdb_id,poster_path,popularity,title,imdb_rating,year(release_date) as year,rated,mv.date_added from movies mv inner join queue qu on mv.live_video_id = qu.video_id where is_active=1 order by release_date desc limit '.$this->start.',27');
        // get tv
        $tQuery=$this->prepare('select DISTINCT(ts.tv_id) as tv_id,popularity,tv.tmdb_id as imdb_id,tv.poster as poster_path,tv.name as title,vote_average as imdb_rating,year(tv.first_air_date) as year,tv.rated,ts.air_date,"tv" as video_type,tv.date_added FROM `tv_season` ts inner join tv_shows tv on tv.tv_id=ts.tv_id where air_date < CURDATE() and tv.is_active=1 order by air_date DESC LIMIT '.$this->start.',27');


        $getQuery->execute();
        // print_r($getQuery->errorInfo());
        $tQuery->execute();

        $alltrentv=$tQuery->fetchAll();
        $alltrendData=$getQuery->fetchAll();

        $obj_merged=array_merge($alltrentv,$alltrendData);
        shuffle($obj_merged);
       

       $this->success(200,__FUNCTION__,$obj_merged);
			exit();
	}
	public function getgenData()
	{


		$genre=$this->genid;

			if(strpos($genre, "-") !== false)
			{

				$genInfo=explode("-",$genre);

				//get heading
				$heading="Action & Thriller";
				//multiple available
			if($this->check=="kids")
			{
				$sQuery=$this->prepare('select distinct mv.title,mv.rated,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and (vg.gen_tmdb_id=:gId or vg.gen_tmdb_id=:gId2) and mv.kids=1  and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',54');
			}
			else
			{
				$sQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and (vg.gen_tmdb_id=:gId or vg.gen_tmdb_id=:gId2) and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',54');
			}

				$sQuery->bindParam(':gId',$genInfo[0],PDO::PARAM_INT);
				$sQuery->bindParam(':gId2',$genInfo[1],PDO::PARAM_INT);

			}
			else
			{

				$hQuery=$this->prepare('select title from genres where tmdb_id=:tid');
				$hQuery->bindParam(':tid',$genre,PDO::PARAM_INT);
				$hQuery->execute();
				$hData=$hQuery->fetchAll();

				$heading=$hData[0]['title'];
				//single
				if($this->check=="kids")
				{

					$sQuery=$this->prepare('select distinct mv.title,mv.rated,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and mv.kids=1 and vg.gen_tmdb_id=:gId  and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',54');
				}
				else
				{
					$sQuery=$this->prepare('select distinct mv.title,year(mv.release_date) as year,mv.runtime,mv.mov_id,mv.poster_path,qu.imdb_rating,mv.overview,mv.release_date,video_type from video_genres vg inner join movies mv on mv.mov_id=vg.video_id inner join queue qu on mv.live_video_id = qu.video_id where mv.is_active=1 and vg.gen_tmdb_id=:gId  and vg.video_type="movie" order by mv.release_date desc limit '.$this->start.',54');
				}

				$sQuery->bindParam(':gId',$genre,PDO::PARAM_INT);
			}
			$sQuery->execute();

			if($sQuery->rowCount()>0)
			{
				$genData=$sQuery->fetchAll();
				$alldata=array('heading'=>$heading,'data'=>$genData);
				$this->success(200,__FUNCTION__,$alldata);
				exit();
			}
			else
			{
				$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
				exit();
			}

	}
	public function getproductin($production)
	{
		$heading=ucfirst($production);
		 $kQuery=$this->prepare('select distinct mov_id,q.IMDB_RATING as imdb_rating,mv.rated,mv.release_date,mv.title,poster_path from movies mv inner join video_productions vp on vp.video_id=mv.mov_id inner join queue q on q.VIDEO_ID = mv.live_video_id where mv.kids=1 and mv.is_active=1 and vp.company_name like concat("%","'.$production.'","%") order by mv.release_date desc limit '.$this->start.',54');
        $kQuery->execute();
        if($kQuery->rowCount()>0)
			{
				$genData=$kQuery->fetchAll();
				$alldata=array('heading'=>$heading,'data'=>$genData);
				$this->success(200,__FUNCTION__,$alldata);
				exit();
			}
			else
			{
				$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
				exit();
			}
	}






	public function getalltv()
	{
		
			$tvQuery=$this->prepare('select distinct tv_id as mov_id,tmdb_id as imdb_id , poster as poster_path,name as title,overview,vote_average as imdb_rating,year(first_air_date) as year,rated,popularity,last_air_date as release_date,video_type,number_of_seasons as runtime,year(first_air_date) as year from tv_shows tv inner join video_genres vg on vg.video_id=tv.tv_id where vg.video_type="tv" and tv.is_active=1 order by release_date desc limit '.$this->start.',54');
		

        $tvQuery->execute();
        if($tvQuery->rowCount()>0)
        {
        	$tvData=$tvQuery->fetchAll();
        	$heading="All tv shows";
        	$alldata=array('heading'=>$heading,'data'=>$tvData);
        	$this->success(200,__FUNCTION__,$alldata);
			exit();

        }
        else
        {
        	$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
        }
	}

	public function searchTv($start,$end)
	{
		$sQuery=$this->prepare('select poster,name,tv_id from tv_shows where name like concat ("%",:query,"%") and is_active=1 limit '.$start.','.$end);
		$sQuery->bindParam(':query',$this->tvQuery,PDO::PARAM_STR);
		$sQuery->execute();
		if($sQuery->rowCount()>0)
		{
			$sData=$sQuery->fetchAll();
			$this->success(200,__FUNCTION__,$sData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}
	}

	public function searchMv($start,$end)
	{
		$sQuery=$this->prepare('select poster_path,title,mov_id from movies where title like concat ("%",:query,"%") and is_active=1 limit '.$start.','.$end);
		$sQuery->bindParam(':query',$this->mvQuery,PDO::PARAM_STR);
		$sQuery->execute();
		// print_r($sQuery->errorInfo());
		if($sQuery->rowCount()>0)
		{
			$sData=$sQuery->fetchAll();
			$this->success(200,__FUNCTION__,$sData);
			exit();
		}
		else
		{
			$this->error('Error',__FUNCTION__,"NO RECORD FOUND");
			exit();
		}
	}

	public function updatepg()
	{
		if(isset($_REQUEST['type']) && $_REQUEST['type']=="tv")
		{
			if($_REQUEST['rated']=="PG" or $_REQUEST['rated']=="PG-13" or $_REQUEST['rated']=="G" or $_REQUEST['rated']=="TV-Y" or $_REQUEST['rated']=="TV-Y7" or $_REQUEST['rated']=="TV-G" or $_REQUEST['rated']=="TV-PG")
			{
				$uQuery=$this->prepare('update tv_shows set rated=:rValue, kids=1 where name=:name and is_active=1');
			}
			else
			{
				$uQuery=$this->prepare('update tv_shows set rated=:rValue where name=:name and is_active=1');
			}

		 		$uQuery->bindParam(':rValue',$_REQUEST['rated'],PDO::PARAM_STR);
		 		$uQuery->bindParam(':name',$_REQUEST['tvname'],PDO::PARAM_STR);
		 		$uQuery->execute();
		 		if($uQuery->rowCount()>0)
		 		{
		 			echo 1;
		 			exit();
		 		}
		 		else
		 		{
		 			echo $_REQUEST['tvname'];
		 			exit();
		 		}
		}
		else
		{
				if($_REQUEST['rated']=="PG" or $_REQUEST['rated']=="PG-13" or $_REQUEST['rated']=="G")
				{
					$uQuery=$this->prepare('update movies set rated=:rValue , kids=1 where imdb_id=:iid and is_active=1');
				}
				else
				{
					$uQuery=$this->prepare('update movies set rated=:rValue where imdb_id=:iid and is_active=1');
					echo 'there';
				}

		 		$uQuery->bindParam(':rValue',$_REQUEST['rated'],PDO::PARAM_STR);
		 		$uQuery->bindParam(':iid',$_REQUEST['imdb_id'],PDO::PARAM_STR);
		 		$uQuery->execute();
		 		if($uQuery->rowCount()>0)
		 		{
		 			echo 1;
		 			exit();
		 		}
		 		else
		 		{
		 			echo $_REQUEST['imdb_id'];
		 			exit();
		 		}
		}



	}

	public function gettvdetails()
	{
		$alldata=array();
        //basic info
        $mid=$this->id;
        $mQuery=$this->prepare('select tv_id,name,first_air_date,poster,rated,overview from tv_shows where tv_id=:mId');
        $mQuery->bindParam(':mId',$mid,PDO::PARAM_INT);
        $mQuery->execute();
        if($mQuery->rowCount()>0)
        {
            //found
            $mData=$mQuery->fetchAll();
        }
        else
        {
            //no data
            $mData="";
        }
         //getbackdrop
        $bQuery=$this->prepare('select filepath from `video_images` where video_id=:vid and image_type="backdrop" and video_type="tv"');
        $bQuery->bindParam(':vid',$mid,PDO::PARAM_INT);
        $bQuery->execute();
        if($bQuery->rowCount()>0)
        {
            $backData=$bQuery->fetchAll();
            $randData=array_rand($backData,1);
            $bData=$backData[$randData];
            // print_r($bData);

        }
        else
        {
            $bData="";
        }
        //get season details
        $sQuery=$this->prepare('select * from tv_season where tv_id=:tid order by season_number');
        $sQuery->bindParam(':tid',$mid,PDO::PARAM_INT);
        $sQuery->execute();
        if($sQuery->rowCount()>0)
        {
            $sData=$sQuery->fetchAll();

            if($sData[0]['season_number']==0)
            {
                $epdetails=$this->getepisodes($sData[1]['season_id']);
            }
            else
            {
                $epdetails=$this->getepisodes($sData[0]['season_id']);
            }

        }
        else
        {
            $sData='';
        }
        $alldata=array('mdetails'=>$mData,'back'=>$bData,'seasons'=>$sData,'first_episode'=>$epdetails);
        $this->success(200,__FUNCTION__,$alldata);
		exit();

    }

    public function updateShowid()
    {
    		ini_set('max_execution_time', 500000);
			$location='../../tv_shows_missing_2.csv';
			$row = 1;
			if (($handle = fopen($location, "r")) !== FALSE)
			{
			    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
			    	$data = array_map("utf8_encode", $data);
			        $num = count($data);
			        $line_of_text[]=$data;
			    }
			    fclose($handle);
			}
			for($k=0;$k<count($line_of_text);$k++)
			{
				$tv_id=$line_of_text[$k][0];
				$tv_name=$line_of_text[$k][1];
				$showid=$line_of_text[$k][2];
				// print_r($showid);
				if(isset($showid) && $showid != 'multiple' && $showid !='')
				{
					$upQuery=$this->prepare('update tv_shows set show_id=:sid where tv_id=:tid');
					$upQuery->bindparam(':sid',$showid,PDO::PARAM_INT);
					$upQuery->bindparam(':tid',$tv_id,PDO::PARAM_INT);
					$upQuery->execute();
					if($upQuery->rowCount()>0)
					{
						echo 'updated '.$tv_name;
					}
				}
			}
    }


    public function synctvepisodes()
    {
    	ini_set('max_execution_time', 500000000);
    	$gQuery=$this->prepare('select * from tv_season_episode where is_active=1');
    	$gQuery->execute();
    	$gData=$gQuery->fetchAll();
    	for($i=0;$i<count($gData);$i++)
    	{
    		//chk for video
    		$cQuery=$this->prepare('select * from videos where imdb_id=:id');
    		$cQuery->bindParam(':id',$gData[$i]['tmdb_id'],PDO::PARAM_INT);
    		$cQuery->execute();
    		if($cQuery->rowCount()>0)
    		{

    		}
    		else
    		{
    			//update is_active to 0
    			$uQuery=$this->prepare('update tv_season_episode set is_active=0 where tmdb_id=:tid');
    			$uQuery->bindParam(':tid',$gData[$i]['tmdb_id'],PDO::PARAM_INT);
    			$uQuery->execute();
    		}
    	}
    }


    public function getepisodes($season_id)
    {

        $eQuery=$this->prepare('select poster_path,overview,episode_name,ep_id,video_duration,video_remaining from tv_season_episode tse left join user_watchlist uw on uw.video_id=tse.ep_id where season_id=:sid and is_active=1 order by ep_id');
        $eQuery->bindParam(':sid',$season_id,PDO::PARAM_INT);
        $eQuery->execute();

        if($eQuery->rowCount()>0)
        {

            $eData=$eQuery->fetchAll();
            return $eData;
        }
        else
        {

        }
    }

    public function movielinksync()
    {
    	ini_set('max_execution_time', 500000000);
    	$eQuery=$this->prepare('select ID,VIDEO_ID from queue where status=1 and is_deleted=0 and is_linked=0');
    	$eQuery->execute();
    	if($eQuery->rowCount()>0)
    	{
    		$downloaded=$eQuery->fetchAll();
    		$fQuery=$this->prepare('select id,imdb_id from videos where scid=:sid and type="movie"');
    		$cQuery=$this->prepare('select mov_id from movies where imdb_id=:iid and is_active=1');
    		for($i=0;$i<count($downloaded);$i++)
    		{
    			$fQuery->bindParam(':sid',$downloaded[$i]['VIDEO_ID'],PDO::PARAM_INT);
    			$fQuery->execute();
    			if($fQuery->rowCount()>0)
    			{
    				$videoData=$fQuery->fetchAll();
    				$cQuery->bindParam(':iid',$videoData[0]['imdb_id'],PDO::PARAM_INT);
    				$cQuery->execute();
    				if($cQuery->rowCount()>0)
    				{
    					echo 'Active '.$downloaded[$i]['ID']."</br>";
    					//updated linked
    					$uQuery=$this->prepare('update queue set is_linked=1 where ID=:id');
    					$uQuery->bindParam(':id',$downloaded[$i]['ID'],PDO::PARAM_INT);
    					$uQuery->execute();
    				}
    				else
    				{
    					echo 'Not Found for '.$downloaded[$i]['ID']."</br>";
    				}
    			}
    		}
    		echo 'All Done';
    	}
    }

    public function gettvsub($tmdb_id)
    {
    	 		$subQuery=$this->prepare('select * from video_subtitle where video_id=:vid and video_type="tv"');
                $subQuery->bindParam(':vid',$tmdb_id,PDO::PARAM_INT);
                $subQuery->execute();
                // print_r($subQuery->errorInfo());
                if($subQuery->rowCount()>0)
                {
                    $subData=$subQuery->fetchAll();
  
                    // print_r($subData);
                }
                else
                {
                    $subData= "";
                }
                echo json_encode($subData);
    }

    public function tvlinkSync()
    {
    	ini_set('max_execution_time', 500000000);
    	$eQuery=$this->prepare('select ID,VIDEO_ID from tvqueue where status=1 and is_deleted=0 and is_linked=0');
    	$eQuery->execute();
    	if($eQuery->rowCount()>0)
    	{
    		$downloaded=$eQuery->fetchAll();
    		//find if its linked
    		$fQuery=$this->prepare('select id,imdb_id from videos where scid=:sid and type="tv"');
    		$cQuery=$this->prepare('select ep_id from tv_season_episode where tmdb_id=:tid and is_active=1');
    		for($i=0;$i<count($downloaded);$i++)
    		{
    			$fQuery->bindParam(':sid',$downloaded[$i]['VIDEO_ID'],PDO::PARAM_INT);
    			$fQuery->execute();
    			if($fQuery->rowCount()>0)
    			{
    				//video file is available check if details are available and active
    				$videoData=$fQuery->fetchAll();
    				$cQuery->bindParam(':tid',$videoData[0]['imdb_id'],PDO::PARAM_INT);
    				$cQuery->execute();
    				if($cQuery->rowCount()>0)
    				{
    					echo 'Active '.$downloaded[$i]['ID']."</br>";
    					//updated linked
    					$uQuery=$this->prepare('update tvqueue set is_linked=1 where ID=:id');
    					$uQuery->bindParam(':id',$downloaded[$i]['ID'],PDO::PARAM_INT);
    					$uQuery->execute();
    				}
    				else
    				{
    					echo 'Not Found for '.$downloaded[$i]['ID']."</br>";
    				}

    			}
    		}
    		echo 'All Done';
    	}
    }





}//end class
?>
