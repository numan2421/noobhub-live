<?php
session_start();
define('CLASS_NAME', 'USER');
include_once 'analytic_class.php';
class user extends analytic
{

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception)
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    //class variables
    public $f_name,$m_name,$l_name,$email,$username,$password,$reset_code,$sub_email,$code,$pName,$pId,$user_id,$invite_code,$remember,$video_type,$video_id,$title,$imdb_link;

    //analytics variable
    public $v_aud_loc,$v_aud_Device,$v_aud_OS,$v_aud_browser,$v_aud_ip,$user_location,$subject,$message;

    //Utitlty Functions
    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		echo json_encode($error);
	}

	public function success($status,$action,$response)
	{
		if($action=='login')
		{
			$this->analytics_login();
			$this->addvisit();
		}
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		echo json_encode($success);
	}


	public function sendEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.support@yandex.com";
		// $mail->Password = "!bbtv_support321@";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(SUPPORT_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility
	public function sendnoreplyEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.support@yandex.com";
		// $mail->Password = "!bbtv_support321@";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(NO_REPLY_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility

	public function sendbillingEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.billing@yandex.com";
		// $mail->Password = "@bbtv_billing321!";
		// $mail->Port = "465";
		// $mail->SMTPDebug=4;
	    $mail->setFrom(BILLING_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);

	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    }
	    else
	    {
	      return 1;
	    }
	}//End Utility

	//class Functions
	public function invited_signup()
	{
		// server sided validation for code
		$cQuery=$this->prepare('select * from user_invites where invite_code=:iCode and is_active=1');
		$cQuery->bindParam(':iCode',$this->invite_code,PDO::PARAM_STR);
		$cQuery->execute();

		if($cQuery->rowCount()>0)
		{
			$inviteData=$cQuery->fetchAll(PDO::FETCH_ASSOC);
			//check if user already available;
			$chkQuery=$this->prepare('select user_id from users where email=:email');
			$chkQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
			$chkQuery->execute();
			if($chkQuery->rowCount() == 0)
			{
				// getuser ip
				$this->checkIpLoc();
				// get user device
				$this->checkDevice();
				//get user browser
				$this->checkBrowser();
				//get user os
				$this->checkOS();
				$invitedBy=$inviteData[0]['invited_by']."_".$inviteData[0]['invited_by_id'];
				$invited=1;
				//available. save new customer
				$sQuery=$this->prepare('insert into users (f_name,email,password,ip,location,os,browser,device,invited,invited_by,is_approved) values (:fName,:email,:pass,:ip,:loc,:os,:bro,:dev,:inv,:invBy,:approved)');
				//Data Binding
				$sQuery->bindParam(':fName',$this->f_name,PDO::PARAM_STR);
				$sQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
				$sQuery->bindParam(':pass',$this->password,PDO::PARAM_STR);
				$sQuery->bindParam(':ip',$this->v_aud_ip,PDO::PARAM_STR);
				$sQuery->bindParam(':loc',$this->v_aud_loc,PDO::PARAM_STR);
				$sQuery->bindParam(':os',$this->v_aud_OS,PDO::PARAM_STR);
				$sQuery->bindParam(':bro',$this->v_aud_browser,PDO::PARAM_STR);
				$sQuery->bindParam(':dev',$this->v_aud_Device,PDO::PARAM_STR);
				$sQuery->bindParam(':inv',$invited,PDO::PARAM_STR);
				$sQuery->bindParam(':invBy',$invitedBy,PDO::PARAM_STR);
				$sQuery->bindParam(':approved',$invited,PDO::PARAM_STR);
				$sQuery->execute();
				if($sQuery->rowCount()>0)
				{
					//save profiles
					$parent_id=$this->lastInsertId ();
					//unset signupcode and create user profiles
					$allowedProfile=0;
					$allowedProfile=$inviteData[0]['profiles'];
					$pQuery=$this->prepare('insert into user_profiles (prof_name,prof_parent_id,is_active) values (:pName,:pID,:active)');

					$active=1;

					$pQuery->bindParam(':pName',$this->f_name,PDO::PARAM_STR,10);
					$pQuery->bindParam(':pID',$parent_id,PDO::PARAM_STR,10);
					$pQuery->bindParam(':active',$active,PDO::PARAM_STR,10);
					$pQuery->execute();
					if($pQuery->rowCount()>0)
					{
						if($allowedProfile==0)
						{
							//only main profile
						}
						else
						{
							//extra profiles
							for($i=0;$i<$allowedProfile;$i++)
							{
								$active=0;
								$pName='profile'.($i+1);
								$pQuery->bindParam(':pName',$pName,PDO::PARAM_STR,10);
								$pQuery->bindParam(':pID',$parent_id,PDO::PARAM_STR,10);
								$pQuery->bindParam(':active',$active,PDO::PARAM_STR,10);
								$pQuery->execute();
							}
						}
						$upQuery=$this->prepare('update user_invites set is_active=0 where invite_code=:iCode');
						$upQuery->bindParam(':iCode',$this->invite_code,PDO::PARAM_STR);
						$upQuery->execute();
						//user Saved
						$this->success(200,__FUNCTION__,'saved');
						exit();
					}
					else
					{
						$this->error('ERROR',__FUNCTION__,$pQuery->errorInfo());
						exit();
					}
				}
				else
				{
					$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
					exit();
				}
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,"Email already taken");
				exit();
			}
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,"Code Expired or already used");
			exit();
		}
	}

	public function signup()
	{
		//chk if user name or email available
		$chkQuery=$this->prepare('select user_id from users where email=:email');
		$chkQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
		$chkQuery->execute();
		if($chkQuery->rowCount() == 0)
		{
			// getuser ip
			$this->checkIpLoc();
			if($this->v_aud_loc==", ")
			{
				$this->error('ERROR',__FUNCTION__,"something bad happend . Please contact tech team");
				exit();
			}

			// get user device
			$this->checkDevice();
			//get user browser
			$this->checkBrowser();
			//get user os
			$this->checkOS();
			// available. save new customer
			$sQuery=$this->prepare('insert into users (f_name,email,password,ip,location,os,browser,device) values (:fName,:email,:pass,:ip,:loc,:os,:bro,:dev)');
			//Data Binding
			$sQuery->bindParam(':fName',$this->f_name,PDO::PARAM_STR);
			$sQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
			$sQuery->bindParam(':pass',$this->password,PDO::PARAM_STR);
			$sQuery->bindParam(':ip',$this->v_aud_ip,PDO::PARAM_STR);
			$sQuery->bindParam(':loc',$this->v_aud_loc,PDO::PARAM_STR);
			$sQuery->bindParam(':os',$this->v_aud_OS,PDO::PARAM_STR);
			$sQuery->bindParam(':bro',$this->v_aud_browser,PDO::PARAM_STR);
			$sQuery->bindParam(':dev',$this->v_aud_Device,PDO::PARAM_STR);
			$sQuery->execute();

			if($sQuery->rowCount()>0)
			{
				$parent_id=$this->lastInsertId ();
				$pQuery=$this->prepare('insert into user_profiles (prof_name,prof_parent_id,is_active) values (:pName,:pID,:active)');
				$active=1;
				$pQuery->bindParam(':pName',$this->f_name,PDO::PARAM_STR,10);
				$pQuery->bindParam(':pID',$parent_id,PDO::PARAM_STR,10);
				$pQuery->bindParam(':active',$active,PDO::PARAM_STR,10);
				$pQuery->execute();
				if($pQuery->rowCount()>0)
				{
					$user_country_data=explode(", ", $this->v_aud_loc);
					$user_country=$user_country_data[1];
					$counriesData=array();
					$country_check='';

					//get approved and blocked countries
					$gQuery=$this->prepare('select * from signup_countries');
					$gQuery->execute();

					if($gQuery->rowCount()>0)
					{
							$countData=$gQuery->fetchAll();
							if($countData[0]['status']==1)
							{
									//blovked
								$country_check="blocked";
							}
							else
							{
									//approved
								$country_check="approved";
							}
						for($i=0;$i<count($countData);$i++)
						{
							array_push($counriesData, $countData[$i]['country']);
						}
					}

					if($country_check=="blocked")
					{
						if (in_array($user_country, $counriesData))
						{
							//user in blocked region

							//send an email to the admin
							$body = "";
				            $body .="Hi Admin , <br><br> ";
				            $body .=$this->f_name." from ".$this->v_aud_loc." with ip". $this->v_aud_ip. " using ".$this->v_aud_browser." browser from ".$this->v_aud_Device." has just registered from region ".$user_country.", please review the signup at <a href='".SITEURL."/admin/pending/users'>".SITE_TITLE."</a>";
				            $body .="<br><br>Thanks<br><br>NoobHub Tv";
				            $Request=$this->sendEmail('Account Signup on '.SITE_TITLE , ADMIN_EMAIL , $body);
							if($Request==1)
							{
								$this->success(200,__FUNCTION__,'Pending for admin approval');
								exit();
							}
							else
							{
								$this->error('ERROR',__FUNCTION__,"Somethign bad happend. Please contact tech team");
								exit();
							}

						}
						else
						{
							//user is approved
							$randString=md5(date("Y-m-d H:i:s"));
							$upQuery=$this->prepare('update users set activation_code=:aCode where user_id=:uid');
							$upQuery->bindParam(':aCode',$randString,PDO::PARAM_STR);
							$upQuery->bindParam(':uid',$parent_id,PDO::PARAM_STR);
							$upQuery->execute();

							if($upQuery->rowCount()>0)
							{
								//send an email to user
								$activationlink=SITEURL.'activate/user/'.$randString;
								$body = "";
					            $body .="Hi ".$this->f_name.", <br><br> ";
					            $body .="Welcome to ".SITE_TITLE.", there is one more step……<br>";
					            $body .="<br>Please click on the following link or copy paste in your browser";
					            $body .="<br><a href=".$activationlink.">".$activationlink."</a>";
					            $body .="<br><br>Thanks<br><br>NoobHub Tv";

					            $Request=$this->sendEmail('Account Activation' , $this->email , $body);

								if($Request==1)
								{
									$this->success(200,__FUNCTION__,'An email has been sent to your email address, please check your inbox to activate your membership');
								exit();
								}
								else
								{
									$this->error('ERROR',__FUNCTION__,"Somethign bad happend. Please contact tech team");
								exit();
								}

							}
							else
							{
								$this->error('ERROR',__FUNCTION__,'activation code error');
								exit();
							}
						}
					}
					else if($country_check=="approved")
					{
						if (in_array($user_country, $counriesData))
						{
							//user in approved region
							//user is approved
							$randString=md5(date("Y-m-d H:i:s"));
							$upQuery=$this->prepare('update users set activation_code=:aCode where user_id=:uid');
							$upQuery->bindParam(':aCode',$randString,PDO::PARAM_STR);
							$upQuery->bindParam(':uid',$parent_id,PDO::PARAM_STR);
							$upQuery->execute();

							if($upQuery->rowCount()>0)
							{
								//send an email to user
								$activationlink=SITEURL.'activate/user/'.$randString;
								$body = "";
					            $body .="Hi ".$this->f_name.", <br><br> ";
					            $body .="Welcome to ".SITE_TITLE.", there is one more step……<br>";
					            $body .="<br>Please click on the following link or copy paste in your browser";
					            $body .="<br><a href=".$activationlink.">".$activationlink."</a>";
					            $body .="<br><br>Thanks<br><br>NoobHub Tv";

					            $Request=$this->sendEmail('Account Activation' , $this->email , $body);

								if($Request==1)
								{
									$this->success(200,__FUNCTION__,'An email has been sent to your email address, please check your inbox to activate your membership');
								exit();
								}
								else
								{
									$this->error('ERROR',__FUNCTION__,"Somethign bad happend. Please contact tech team");
								exit();
								}

							}
							else
							{
								$this->error('ERROR',__FUNCTION__,'activation code error');
								exit();
							}

						}
						else
						{
							$randString=md5(date("Y-m-d H:i:s"));
						//send an email to the admin
							$activationlink=SITEURL.'activate/user/'.$randString;
							$body = "";
				            $body .="Hi Admin , <br><br> ";
				            $body .=$this->f_name." from ".$this->v_aud_loc." with ip". $this->v_aud_ip. " using ".$this->v_aud_browser." browser from ".$this->v_aud_Device." has just registered from region ".$user_country.", please review the signup at <a href='".SITEURL."/admin/pending/users'>".SITE_TITLE."</a>";
				            $body .="<br><br>Thanks<br><br> NoobHub Tv";
				            $Request=$this->sendEmail('Account Signup on '.SITE_TITLE , ADMIN_EMAIL , $body);
							if($Request==1)
							{
								$this->success(200,__FUNCTION__,'Pending for admin approval');
								exit();
							}
							else
							{
								$this->error('ERROR',__FUNCTION__,"Somethign bad happend. Please contact tech team");
								exit();
							}
						}
					}
					else
					{
							$randString=md5(date("Y-m-d H:i:s"));
							$upQuery=$this->prepare('update users set activation_code=:aCode where user_id=:uid');
							$upQuery->bindParam(':aCode',$randString,PDO::PARAM_STR);
							$upQuery->bindParam(':uid',$parent_id,PDO::PARAM_STR);
							$upQuery->execute();

							if($upQuery->rowCount()>0)
							{
								//send an email to user
								$activationlink=SITEURL.'activate/user/'.$randString;
								$body = "";
					            $body .="Hi ".$this->f_name.", <br><br> ";
					            $body .="Welcome to ".SITE_TITLE.", there is one more step……<br>";
					            $body .="<br>Please click on the following link or copy paste in your browser";
					            $body .="<br><a href=".$activationlink.">".$activationlink."</a>";
					            $body .="<br><br>Thanks<br><br>NoobHub Tv";

					            $Request=$this->sendEmail('Account Activation' , $this->email , $body);

								if($Request==1)
								{
									$this->success(200,__FUNCTION__,'An email has been sent to your email address, please check your inbox to activate your membership');
								exit();
								}
								else
								{
									$this->error('ERROR',__FUNCTION__,"Somethign bad happend. Please contact tech team");
								exit();
								}

							}
							else
							{
								$this->error('ERROR',__FUNCTION__,'activation code error');
								exit();
							}
					}

				}
				else
				{
					$this->error('ERROR',__FUNCTION__,$pQuery->errorInfo());
					exit();
				}

			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
				exit();
			}
		}
		else
		{
				$this->error('ERROR',__FUNCTION__,'Email already taken');
				exit();
		}
	}//End of signup

	//Start login
	public function login()
	{
		$lQuery=$this->prepare('select * from users where email=:uName and password=:pwrd and is_approved=1');
		$lQuery->bindParam(':uName',$this->email,PDO::PARAM_STR);
		$lQuery->bindParam(':pwrd',$this->password,PDO::PARAM_STR);
		$lQuery->execute();
		if($lQuery->rowCount() > 0)
		{
			//chk if approved or not
				$userData=$lQuery->fetchAll(PDO::FETCH_ASSOC);
				$userId=$userData[0]['user_id'];
				$user_email=$userData[0]['email'];
				$user_f_name=$userData[0]['f_name'];
        		$in_kids=$userData[0]['in_kids'];
					//get active profiles
				$pQuery=$this->prepare('select * from user_profiles where prof_parent_id=:pid and is_active=1');
				$pQuery->bindParam(':pid',$userId,PDO::PARAM_INT);
				$pQuery->execute();
				if($pQuery->rowCount()>0)
				{
					$profileData=$pQuery->fetchAll();

					$activeProfile=$profileData[0];
					if($this->remember==1)
					{
						//set cookie
		                $cookie=array("email" => $userData[0]['email'], 'username' => $userData[0]['f_name'],'id'=> $userData[0]['user_id'],'profile_id'=>$activeProfile['prof_id'],'profile_name'=>$activeProfile['prof_name'],'profile_image'=>'1');
		                $strUser = serialize($cookie);
		                $exTime = time() + (60 * 60 * 24 * 7);
		                setcookie("UserData", $strUser, $exTime, '/');
					}

					//check if on trial and trial expired or not
					if($userData[0]['is_premium']==0)
					{
						$joinDatetime=$userData[0]['join_date'];
						$datetimearray = explode(" ", $joinDatetime);
						$date = $datetimearray[0];
						$joindate= strtotime($date);
						$today = time();
						$datediff = $today - $joindate;
						$daysDiffenrce = floor($datediff / (60 * 60 * 24));
						$trialleft=TRIAL_LIMIT-$daysDiffenrce;
						$allData=array('userData'=>$userData,'trialleft'=>$trialleft);
							if($daysDiffenrce==(TRIAL_LIMIT-1))
							{
								//send email as reminder
								//email start
								// $userEmail=$userData[0]['email'];
								// $userName=$userData[0]['f_name'];

								// $body = "";
					   //          $body .="Dear ".$userName." , <br><br> ";
					   //          $body .="Your Free Trial account at ".SITE_TITLE." is expiring tomorrow. Please register as paid user to conitnue using our services.";
					   //          $body .="<br>Regards";
					   //          $body .="<br>";
					   //          $body .="<br><br>".SITE_TITLE;
					   //          $Request=$this->sendbillingEmail('Trial Account Reminder' , $userEmail , $body);
								//email end
								//session set
					      		$_SESSION['user_name']=$user_f_name;
								$_SESSION['user_email']=$user_email;
								$_SESSION['userId']=$userId;
                				$_SESSION['in_kids']=$in_kids;
                				$_SESSION['is_premium']=0;
								$_SESSION['profile_id']=$activeProfile['prof_id'];
								$_SESSION['profile_image']='1';
								$_SESSION['profile_name']=$activeProfile['prof_name'];
								//end
								$this->success(200,__FUNCTION__,$allData);
								exit();
							}
							else if($daysDiffenrce==TRIAL_LIMIT)
							{
								//send email as reminder
								//email start
								// $userEmail=$userData[0]['email'];
								// $userName=$userData[0]['f_name'];
								// $body = "";
					   //          $body .="Dear ".$userName." , <br><br> ";
					   //          $body .="Your Free Trial account at ".SITE_TITLE." is expiring today. Please register as paid user to conitnue using our services.";
					   //          $body .="<br>Regards";
					   //          $body .="<br>";
					   //          $body .="<br><br>".SITE_TITLE;
					   //          $Request=$this->sendbillingEmail('Trial Account Reminder' , $userEmail , $body);
								//email end
								//session set
				      			$_SESSION['user_name']=$user_f_name;
								$_SESSION['user_email']=$user_email;
								$_SESSION['userId']=$userId;
                				$_SESSION['in_kids']=$in_kids;
                				$_SESSION['is_premium']=0;
								$_SESSION['profile_id']=$activeProfile['prof_id'];
								$_SESSION['profile_image']='1';
								$_SESSION['profile_name']=$activeProfile['prof_name'];
								//end
								$this->success(200,__FUNCTION__,$allData);
								exit();
							}
							else if($daysDiffenrce > TRIAL_LIMIT)
							{
								//trial expired but let go to main page
               			
                //send mail or not ??
                				$userEmail=$userData[0]['email'];
								$userName=$userData[0]['f_name'];

								// $body = "";
					   //          $body .="Dear ".$userName." , <br><br> ";
					   //          $body .="Your Free Trial account at ".SITE_TITLE." is expired. Please register as paid user to conitnue using our services.";
					   //          $body .="<br>Regards";
					   //          $body .="<br>";
					   //          $body .="<br><br>".SITE_TITLE;
					   //          $Request=$this->sendbillingEmail('Trial Account Reminder' , $userEmail , $body);
                //mail end
                //session set
								$_SESSION['is_premium']=0;
					      		$_SESSION['user_name']=$user_f_name;
								$_SESSION['user_email']=$user_email;
								$_SESSION['userId']=$userId;
                				$_SESSION['in_kids']=$in_kids;
								$_SESSION['profile_id']=$activeProfile['prof_id'];
								$_SESSION['profile_image']='1';
								$_SESSION['profile_name']=$activeProfile['prof_name'];
								//end
								$this->success(200,__FUNCTION__,$allData);
								exit();
							}
							else
							{
								//get user active profile

								//session set
								$_SESSION['is_premium']=0;
					      		$_SESSION['user_name']=$user_f_name;
								$_SESSION['user_email']=$user_email;
								$_SESSION['userId']=$userId;
                				$_SESSION['in_kids']=$in_kids;
								$_SESSION['profile_id']=$activeProfile['prof_id'];
								$_SESSION['profile_image']='1';
								$_SESSION['profile_name']=$activeProfile['prof_name'];
								//end
								$this->success(200,__FUNCTION__,$allData);
								exit();
							}

					}
					else
					{

						//session set
						$_SESSION['is_premium']=1;
						$_SESSION['user_name']=$user_f_name;
						$_SESSION['user_email']=$user_email;
						$_SESSION['userId']=$userId;
            			$_SESSION['in_kids']=$in_kids;
						$_SESSION['profile_id']=$activeProfile['prof_id'];
						$_SESSION['profile_image']='1';
						$_SESSION['profile_name']=$activeProfile['prof_name'];
						//end
						$this->success(200,__FUNCTION__,'');
						exit();
					}
			}
			else
			{
				//no active profiles error
			}


		}
		else
		{
			$this->error('ERROR',__FUNCTION__,'username/password incorrect or waiting for approval');
			exit();
		}

	}//End login

	public function forgetpassword()
	{
		//check if email is available
		$chkQuery=$this->prepare('select * from users where email=:email and is_approved=1');
		$chkQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
		$chkQuery->execute();

		if($chkQuery->rowCount() > 0)
		{
			//email available generate reset link
			$userData=$chkQuery->fetchAll(PDO::FETCH_ASSOC);
			$userID=$userData[0]['user_id'];
			$userName=$userData[0]['f_name'];
			$randString=md5(date("Y-m-d H:i:s"));
			$resetlink=SITEURL.'reset/user/'.$randString;
			//update resetstring/code in customer
			$upQuery=$this->prepare('update users set reset_code=:rCode where user_id=:uId');
			$upQuery->bindParam(':uId',$userID,PDO::PARAM_STR);
			$upQuery->bindParam(':rCode',$randString,PDO::PARAM_STR);
			$upQuery->execute();
			if($upQuery)
			{
				//updated reset code. Send reset link in email
				$body = "";
	            $body .="Hey there!, <br><br> ";
	            $body .='<p>Someone told us you forgot your NoobHub password. If it was you, click the following link to choose a new password:</p>';
	            $body .="<a href=$resetlink>".$resetlink."</a><br>";
	            $body .="You didn't forget your password? Great, you can just ignore this email and nothing changes.";
	            $body .= "<br><br>Thanks for being a member!";
	            $body .= "<br><br>NoobHub";
				$response=$this->sendnoreplyEmail('Reset Password' , $this->email , $body);
				if($response==1)
		            {
		            	$this->success(200,__FUNCTION__,'email sent');
						exit();
		            }
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
				exit();
			}


		}
		else
		{
			//not available or waiting for admin approval
			$this->error('ERROR',__FUNCTION__,'The email address you entered is not associated with any account. Make sure you don&#39;t have any typos.');
				exit();
		}
	}//End forget password

	public function resetPassword()
	{
		//chk if reset code is available in db

		$pQuery=$this->prepare('select * from users where reset_code=:rCode');
		$pQuery->bindParam(':rCode',$this->reset_code,PDO::PARAM_STR);
		$pQuery->execute();
		if($pQuery->rowCount() > 0)
		{
			$userData=$pQuery->fetchAll(PDO::FETCH_ASSOC);
			$userId=$userData[0]['user_id'];
			//code available update new password
			$upQuery=$this->prepare('update users set password=:pwrd, reset_code= NULL,max_screen_limit=0 where user_id = :uid');
			$upQuery->bindParam(':pwrd',$this->password,PDO::PARAM_STR);
			$upQuery->bindParam(':uid',$userId,PDO::PARAM_STR);
			$upQuery->execute();
			if($upQuery->rowCount()>0)
			{
				$this->success(200,__FUNCTION__,'ok');
				exit();
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$upQuery->errorInfo());
				exit();
			}
		}
		else
		{
			//reset code is not available or expired
			$this->error('ERROR',__FUNCTION__,'Reset link is expired or not valid.');
			exit();
		}
	}//End restPassword

	//start add subuser
	public function addsubuser()
	{
		//chk if sub user is available in users/sub users
		$chkQuery=$this->prepare('select * from users, sub_users where user_email=:email and approved=1');
		$chkQuery->bindParam(':email',$this->sub_email,PDO::PARAM_STR);
		$chkQuery->execute();
		if($chkQuery->rowCount()==0)
		{

			//not found chk for username
			$chkQuery2=$this->prepare('select * from users, sub_users where username=:uName and approved=1');
			$chkQuery2->bindParam(':uName',$this->username,PDO::PARAM_STR);
			$chkQuery2->execute();
			if($chkQuery2->rowCount()==0)
			{
				$randString=md5(date("Y-m-d H:i:s"));
				//Not Found add new sub user
				$sQuery=$this->prepare('insert into sub_users (f_name,m_name,l_name,email,parent_user_id,signup_code) values (:fName,:mName,:lName,:eMail,:pUserId,:sCode)');
				$sQuery->bindParam(':fName',$this->f_name,PDO::PARAM_STR);
				$sQuery->bindParam(':mName',$this->m_name,PDO::PARAM_STR);
				$sQuery->bindParam(':lName',$this->l_name,PDO::PARAM_STR);
				$sQuery->bindParam(':sCode',$randString,PDO::PARAM_STR);
				$sQuery->bindParam(':email',$this->sub_email,PDO::PARAM_STR);
				$sQuery->bindParam(':pUserId',$this->pId,PDO::PARAM_INT);
				$sQuery->execute();
				if($sQuery->rowCount()>0)
				{
					//saved
					//create a link for username and password
					$link=SITEURL.'/signup/subuser/'.$randString;
					//update signupcode for user
					//email start
					$userName=$this->f_name." ".$this->m_name." ".$this->l_name ;
					$body = "";
		            $body .="Dear ".$userName." , <br><br> ";
		            $body .="Your Free account as a sub-user at ".SITE_TITLE." has been created. Please follow th link <a href=$resetlink>Signup</a> to sign up";
		            $body .="<br>Regards";
		            $body .="<br>";
		            $body .="<br><br>NoobHub Tv";

		            $Request=$this->sendEmail('Trial Account Reminder' , $userEmail , $body);
					//email end
				}
				else
				{

				}
			}
			else
			{
				//error Found username
				$this->error('ERROR',__FUNCTION__,'username is already taken');
				exit();
			}
		}
		else
		{
			//Found error
			$this->error('ERROR',__FUNCTION__,'Email is already taken');
			exit();
		}
	}//END
	public function addProfileName()
	{
		// chk if name already given to user's profiles
		$chkQuery=$this->prepare('select prof_id from user_profiles where prof_name=:pName and is_active=1 and prof_parent_id=:pid');
		$chkQuery->bindParam(':pName',$this->pName,PDO::PARAM_STR);
		$chkQuery->bindParam(':pid',$this->user_id,PDO::PARAM_INT);
		$chkQuery->execute();
		if($chkQuery->rowCount()>0)
		{
			//alreay available. please change name
			$this->error('ERROR',__FUNCTION__,'Profile name already taken.');
			exit();

		}
		else
		{
			//not available please save
			$sQuery=$this->prepare('update user_profiles set prof_name =:pName , is_active=1 where prof_id=:piid');
			$sQuery->bindParam(':pName',$this->pName,PDO::PARAM_STR);
			$sQuery->bindParam(':piid',$this->pId,PDO::PARAM_INT);
			$sQuery->execute();
			if($sQuery->rowCount()>0)
			{
				$this->success(200,__FUNCTION__,'saved');
				exit();
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
				exit();
			}
		}
	}

	public function updateProfile()
	{
		$uQuery=$this->prepare('update users set f_name=:fName, email=:email where user_id=:uid');
		$uQuery->bindParam(':fName',$this->username,PDO::PARAM_STR);
		$uQuery->bindParam(':email',$this->email,PDO::PARAM_STR);
		$uQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
		$uQuery->execute();
		if($uQuery->rowCount()>0)
		{
			$_SESSION['user_name']=$this->username;
			$_SESSION['user_email']=$this->email;
			$this->success(200,__FUNCTION__,'updated');
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$uQuery->errorInfo());
			exit();
		}
	}

	//start code validation
	public function ValidateCode()
	{
		$chkQuery=$this->prepare('select * from user_invites where invite_code=:icOde and is_active=1');
		$chkQuery->bindParam(':icOde',$this->code,PDO::PARAM_STR);
		$chkQuery->execute();
		if($chkQuery->rowCount()>0)
		{
			$userData=$chkQuery->fetchAll(PDO::FETCH_ASSOC);
			$this->success(200,__FUNCTION__,$userData);
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,'Invalid Url');
			exit();
		}
	}
	//end

	public function getmylist()
	{
		$gQuery=$this->prepare('select up.video_type,up.video_id,COALESCE(mv.title,ts.name) as title,COALESCE(mv.runtime,ts.number_of_seasons) as runtime,COALESCE(mv.rank,ts.vote_average) as imdb_rating,COALESCE(mv.poster_path,ts.poster) as poster_path,COALESCE(mv.mov_id,ts.tv_id) as id,COALESCE(mv.popularity,ts.popularity) as popularity,COALESCE(mv.imdb_id,ts.tmdb_id) as imdb_id , COALESCE(mv.release_date,ts.last_air_date) as release_date, COALESCE(mv.overview,ts.overview) as overview, COALESCE(year(mv.release_date),year(ts.first_air_date)) as year from user_playlist up left join movies mv ON (up.video_type= "movie" AND up.video_id = mv.mov_id) left join tv_shows ts ON (up.video_type = "tv" AND up.video_id = ts.tv_id) where up.profile_id=:pid');
		$gQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
		$gQuery->execute();
		if($gQuery->rowCount()>0)
		{
			$gData=$gQuery->fetchAll();
			$this->success(200,__FUNCTION__,$gData);
			exit();

		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$gQuery->errorInfo());
			exit();
		}
	}

	public function getplaylist()
	{
		if(isset($this->video_id) && isset($this->video_type))
		{
			$gQuery=$this->prepare('select video_id,video_type from user_playlist where profile_id=:pid and video_id=:vid and video_type=:vtype');
			$gQuery->bindParam(':vtype',$this->video_type,PDO::PARAM_STR);
			$gQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
		}
		if(isset($this->video_type))
		{
			$gQuery=$this->prepare('select video_id,video_type from user_playlist where profile_id=:pid  and video_type=:vtype');
			$gQuery->bindParam(':vtype',$this->video_type,PDO::PARAM_STR);

		}
		else
		{
			$gQuery=$this->prepare('select video_id,video_type from user_playlist where profile_id=:pid');
		}


		$gQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
		$gQuery->execute();
		if($gQuery->rowCount()>0)
		{
			$playlist=$gQuery->fetchAll();
			$this->success(200,__FUNCTION__,$playlist);
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$gQuery->errorInfo());
					exit();
		}
	}

	public function addPlaylist()
	{
		//chk if already in playlist
		$cQuery=$this->prepare('select * from user_playlist where video_type=:vtype and video_id=:vid and profile_id=:pid');
		$cQuery->bindParam(':vtype',$this->video_type,PDO::PARAM_STR);
		$cQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
		$cQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
		$cQuery->execute();
		if($cQuery)
		{
			if($cQuery->rowCount()>0)
			{
				//found // remove that
				$rQuery=$this->prepare('delete from user_playlist where video_type=:vtype and video_id=:vid and profile_id=:pid');
				$rQuery->bindParam(':vtype',$this->video_type,PDO::PARAM_STR);
				$rQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
				$rQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
				$rQuery->execute();

				if($rQuery->rowCount()>0)
				{
					$this->success(200,__FUNCTION__,'removed');
					exit();
				}
				else
				{
					$this->error('ERROR',__FUNCTION__,$rQuery->errorInfo());
					exit();
				}
			}
			else
			{
				//not found save new
				$sQuery=$this->prepare('insert into user_playlist (profile_id,video_type,video_id,profile_parent_id) values (:pid,:vtype,:vid,:ppid)');
				$sQuery->bindParam(':pid',$_SESSION['profile_id'],PDO::PARAM_INT);
				$sQuery->bindParam(':vtype',$this->video_type,PDO::PARAM_INT);
				$sQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
				$sQuery->bindParam(':ppid',$_SESSION['userId'],PDO::PARAM_INT);
				$sQuery->execute();
				if($sQuery->rowCount()>0)
				{
					$this->success(200,__FUNCTION__,'success');
					exit();
				}
				else
				{
					$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
					exit();
				}
			}
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$cQuery->errorInfo());
			exit();
		}
	}
	public function saveRequest()
	{
		//check if same request exists before by same user
		
			//not found save new
			$sQuery=$this->prepare('insert into content_request (from_user_id,from_user_email,title,imdb_link,type,subject,message) values (:userid,:userEmail,:title,:link,:type,:sub,:message)');
			
			if(isset($_SESSION['user_email']) && isset($_SESSION['userId']))
			{
				$email=$_SESSION['user_email'];
				$userid=$_SESSION['userId'];
				if($_REQUEST['video_type']==1)
				{
					$type="movie";
				}
				else
				{
					$type="tv";
				}
				if(isset($_REQUEST['title']))
				{
					$title=$_REQUEST['title'];
				}
				else
				{
					$title="";
				}
				if(isset($_REQUEST['imdb_link']))
				{
					$imdb=$_REQUEST['imdb_link'];
				}
				else
				{
					$imdb="";
				}
			}
			else
			{
				$email=$_REQUEST['guest_email'];
				$userId=0;
				$type="";
				$title="";
				$imdb="";
			}
			$sQuery->bindParam(':userid',$userId,PDO::PARAM_INT);
			$sQuery->bindParam(':userEmail',$email,PDO::PARAM_STR);
			$sQuery->bindParam(':title',$title,PDO::PARAM_STR);
			$sQuery->bindParam(':link',$imdb,PDO::PARAM_STR);
			$sQuery->bindParam(':type',$type,PDO::PARAM_STR);
			$sQuery->bindParam(':sub',$this->subject,PDO::PARAM_STR);
			$sQuery->bindParam(':message',$this->message,PDO::PARAM_STR);
			$sQuery->execute();
			if($sQuery->rowCount()>0)
			{
				//send admin an email
				if(isset($_SESSION['user_email']) && isset($_SESSION['userId']))
				{
					$body  =  "";
		            $body .= "Hi Admin , <br><br>";
		            if(isset($_REQUEST['title']) && $_REQUEST['imdb_link'])
					{
						 $body .= "NoobHub Tv user <b>".$email."</b> has just submitted a content request for <b>".$_REQUEST['title']."</b> with imdb link <b>".$_REQUEST['imdb_link']."</b> on ".SITE_TITLE. " Please review";
					}
					else
					{
						 $body .= "NoobHub Tv user <b>".$email."</b> has just submitted a contact request with subject <b>".$this->subject."</b> with message <b>".$this->message."</b> on ".SITE_TITLE. " Please review";
					}
				}
				else
				{
					$body  =  "";
		            $body .= "Hi Admin , <br><br>";
		            
						 $body .= "NoobHub Tv user <b>".$email."</b> has sent following message <br>";
						 $body .="<b>".$this->subject."</b><br>";
						 $body .="<p>".$this->message."</p><br><br>";
			


					

				}
	           
	            // $body .=$this->f_name." from ".$this->v_aud_loc." with ip". $this->v_aud_ip. " using ".$this->v_aud_browser." browser from ".$this->v_aud_Device." has just registered, please review the signup at <a href='".SITEURL."/admin/pending/users'>".SITE_TITLE."</a>";
	            $body .= "<br><br>Thanks<br><br>NoobHub Tv";
	            $Request=$this->sendEmail('Content Request '.SITE_TITLE , ADMIN_EMAIL , $body);
	            $Request=$this->sendEmail('Content Request '.SITE_TITLE , SUPPORT_EMAIL_HOST , $body);
				$this->success(200,__FUNCTION__,'success');
				exit();

			}
		}
	

	public function Searchrequest()
	{
		$imdb_link=$this->imdb_link;
		$linkData=explode("/", $imdb_link);
		$imdb_id=$linkData[4];
		if($_REQUEST['video_type']==1)
		{
			//movie
			//search for imdb_id in list
			$sQuery=$this->prepare('select * from movies where (imdb_id=:iid or title like concat("%",:title,"%")) and is_active=1');
			$sQuery->bindParam(':iid',$imdb_id,PDO::PARAM_STR);
			$sQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
		}
		else
		{
			//tv
			//find by title
			$sQuery=$this->prepare('select * from tv_shows where name like concat("%",:name,"%") and is_active=1');
			$sQuery->bindParam(':name',$this->title,PDO::PARAM_STR);
		}
		$sQuery->execute();
		if($sQuery->rowCount()>0)
		{
			//movie found retun movie data as suggestion
			$mData=$sQuery->fetchAll();
			$this->success(200,__FUNCTION__,$mData);
			exit();
		}
		else
		{
			//save request
			$this->saveRequest();
		}


	}

	function getVideolimit()
	{
		$gQuery=$this->prepare('select * from settings where setting_id=2');
		$gQuery->execute();
		if($gQuery->rowCount()>0)
		{
			$gData=$gQuery->fetchAll();
			$gValue=$gData[0]['value'];
			echo  $gValue*60;
		}
		else
		{
			echo  10;
		}
	}

	public function addplaytimer()
	{
		$pQuery=$this->prepare('insert into free_user_play_time (user_id,video_title,played_time) values (:uid,:vt,:pt)');
		
		// compare with max daily time

			$mQuery=$this->prepare('select * from settings where setting_id=2');
			$mQuery->execute();
			$timeData=$mQuery->fetchAll();
			$time=$timeData[0]['value']*60;
			if($_REQUEST['timer'] > $time)
			{
				$timer=$time;
			}
		else
			{
				$timer=$_REQUEST['timer'];
			}
		$pQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
		$pQuery->bindParam(':vt',$_REQUEST['title'],PDO::PARAM_STR);
		$pQuery->bindParam(':pt',$timer,PDO::PARAM_STR);
		$pQuery->execute();
		if($pQuery->rowCount()>0)
		{
			echo 1;
			exit();
		}
		else
		{
			echo 2;
			exit();
		}
	}

	public function markCompleterequests()
	{
		$allData=$_REQUEST['dataObject'];
		for($i=0;$i<count($allData);$i++)
		{
			$userEmail=$allData[$i]['from_user_email'];
			$title=$allData[$i]['title'];
			$imdb_link=$allData[$i]['imdb_link'];

			$body  =  "";
            $body .= "Dear NoobHub Tv user , <br><br>";
            $body .= "Content against ur request for <b>".$title."</b> with IMDB link <b>".$imdb_link."</b> has been added to NoobHub Tv. Please login to enjoy";
            // $body .=$this->f_name." from ".$this->v_aud_loc." with ip". $this->v_aud_ip. " using ".$this->v_aud_browser." browser from ".$this->v_aud_Device." has just registered, please review the signup at <a href='".SITEURL."/admin/pending/users'>".SITE_TITLE."</a>";
            $body .= "<br><br>Thanks<br><br>NoobHub Tv";
            $Request=$this->sendEmail('Content Request Completed on '.SITE_TITLE , $userEmail , $body);
            if($Request==1)
            {
            	$uQuery=$this->prepare('update content_request set status=1 where from_user_email=:email and title=:title and imdb_link=:ilink');
            	$uQuery->bindParam(':email',$userEmail,PDO::PARAM_STR);
            	$uQuery->bindParam(':title',$title,PDO::PARAM_STR);
            	$uQuery->bindParam(':ilink',$imdb_link,PDO::PARAM_STR);
            	$uQuery->execute();
            }

		}
		$this->success(200,__FUNCTION__,"success");
		exit();
	}

	public function updatepassword()
	{
		//compare current pass first
		$cQuery=$this->prepare('select user_id from users where password=:pwrd and user_id=:uid');
		$currPass=md5($_REQUEST['curr_pass']);
		$cQuery->bindParam(':pwrd',$currPass,PDO::PARAM_STR);
		$cQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
		$cQuery->execute();
		if($cQuery->rowCount()>0)
		{
			$uQuery=$this->prepare('update users set password=:pwrd where user_id=:uid');
			$password=md5($_REQUEST['new_pass']);
			$uQuery->bindParam(':pwrd',$password,PDO::PARAM_STR);
			$uQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
			$uQuery->execute();
			if($uQuery->rowCount()>0)
			{
				$this->success(200,__FUNCTION__,"updated");
				exit();
			}
			else
			{
				$this->error("ERROR",__FUNCTION__,$uQuery->errorInfo());
				exit();
			}
		}
		else
		{
			$this->error("ERROR",__FUNCTION__,"Current password is not correct");
			exit();
		}
		
	}

	public function getallrequests()
	{
		$aQuery=$this->prepare('select * from content_request');
		$aQuery->execute();
		if($aQuery->rowCount()>0)
		{
			$aData=$aQuery->fetchAll();
			$this->success(200,__FUNCTION__,$aData);
			exit();

		}
	}
	public function getCompletedrequest()
	{
		$aQuery=$this->prepare('select * from content_request where (title like concat("%",:title,"%") or imdb_link like concat("%",:link,"%")) and status=0');
		$aQuery->bindParam(':title',$this->title,PDO::PARAM_STR);
		$aQuery->bindParam(':link',$this->imdb_link,PDO::PARAM_STR);
		$aQuery->execute();
		if($aQuery->rowCount()>0)
		{
			$aData=$aQuery->fetchAll();
			$this->success(200,__FUNCTION__,$aData);
			exit();

		}
		else
		{
			$this->error("ERROR",__FUNCTION__,"No Active Requests !");
			exit();
		}
	}

  public function exitkids()
  {
    $uQuery=$this->prepare('update users set in_kids = 0 where user_id=:uid');
    $uQuery->bindParam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
    $uQuery->execute();
    if($uQuery->rowCount()>0)
    {
      //updated remove sessions
      $_SESSION['in_kids']=0;
      $this->success(200,__FUNCTION__,'success');
			exit();
    }
  }

  public function getFileinfo($type,$code)
  {
  	if($type=="movie")
  	{
  		$fQuery=$this->prepare('select title,live_video_id from movies where mov_id=:mid');
  	}
  	else
  	{
  		$fQuery=$this->prepare('select v.scid as live_video_id,tse.episode_name as title,season_number from videos v left join tv_season_episode tse on v.imdb_id=tse.tmdb_id left join tv_season ts on ts.season_id=tse.season_id where tse.ep_id=:mid');
  	}
  	$fQuery->bindParam(':mid',$code,PDO::PARAM_INT);
  	$fQuery->execute();
  	if($fQuery->rowCount()>0)
  	{
  		$fData=$fQuery->fetchAll();
  		return $fData;
  	}
  	else
  	{
  		return 2;
  	}
  }


  public function verifySessionuser()
  {
  	
  	if(isset($_SESSION['userId']))
  	{
  		$userId=$_SESSION['userId'];
  		//verfiy user's existance
  		$chkQuery=$this->prepare('select * from users where user_id=:uid and is_approved=1');
  		$chkQuery->bindParam(':uid',$userId,PDO::PARAM_INT);
  		$chkQuery->execute();
  		if($chkQuery->rowCount()>0)
  		{
  			return 1;
  		}
  		else
  		{
  			return 2;
  		}
  	}
  	else
  	{

  		return 2;

  	}
  }




	public function checkIpLoc()
    {

        $this->v_aud_ip=$_SERVER['REMOTE_ADDR'];
        $location = file_get_contents('http://geoip.nekudo.com/api/'.$this->v_aud_ip);
        if(isset($location))
        {
        	$user_location = json_decode($location, true);	
	        $city=utf8_decode($user_location['city']);
		    $country=$user_location['country']['name'];
	    	$this->v_aud_loc=$city.", ".$country;
        }
        else
        {
        	$this->error('ERROR',__FUNCTION__,'Somthing bad happend. Please conatct tech team');
			exit();
        }

    }	


	public function getfreeTotaltime()
	{
		$userId=$_SESSION['userId'];
		$gQuery=$this->prepare('select ROUND(SUM(played_time), 0) as sum from free_user_play_time where user_id=:uid and Date(date_added) = CURDATE()');
		$gQuery->bindParam(':uid',$userId,PDO::PARAM_INT);
		$gQuery->execute();
		if($gQuery->rowCount()>0)
		{
			$gData=$gQuery->fetchAll();
			$sData=$gData[0]['sum'];
			// compare with max time limit . get mac time limit
			$mQuery=$this->prepare('select * from settings where setting_id=3');
			$mQuery->execute();
			$timeData=$mQuery->fetchAll();
			$time=$timeData[0]['value']*60;
			
			if($sData >= $time)
			{
				$_SESSION['daily_expired']=1;
				echo 1;
				exit();
			}
			else
			{
				$_SESSION['daily_expired']=0;
				echo 2;
				exit();
			}
			// exit()
		}	
		else
		{
			// echo 0;

		}
	}


    public function checkDevice() {
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
        }

        if ($tablet_browser > 0) {
            // do something for tablet devices
            $this->v_aud_Device='tablet';

        } else if ($mobile_browser > 0) {
            // do something for mobile devices
            $this->v_aud_Device='mobile';

        } else {
            // do something for everything else
            $this->v_aud_Device='desktop';

        }
    }

    public function checkBrowser() {
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browser = "Unknown Browser";
        $browser_array = array(
            '/msie/i' => 'Internet Explorer',
            '/firefox/i' => 'Firefox',
            '/safari/i' => 'Safari',
            '/chrome/i' => 'Chrome',
            '/opera/i' => 'Opera',
            '/netscape/i' => 'Netscape',
            '/maxthon/i' => 'Maxthon',
            '/konqueror/i' => 'Konqueror',
            '/mobile/i' => 'Handheld Browser'
        );

        foreach ($browser_array as $regex => $value) {

            if (preg_match($regex, $this->user_agent)) {
                $browser = $value;
            }
        }
        $this->v_aud_browser = $browser;


    }

    public function checkOS() {
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $os_platform = "Unknown OS Platform";

        $os_array = array(
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/windows 7/i' => 'Windows 7',
            '/windows 8/i' => 'Windows 8',
            '/windows 8.1/i' => 'Windows 8.1',
            '/windows 10/i' => 'Windows 10',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($os_array as $regex => $value)
        {

            if (preg_match($regex, $this->user_agent))
            {
                $os_platform = $value;
            }
        }
        $this->v_aud_OS = $os_platform;


    }
}//End of class
?>
