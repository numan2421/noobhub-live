<?php
class analytic extends PDO {

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception) 
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }

    public $v_aud_loc,$v_aud_Device,$v_aud_OS,$v_aud_browser,$v_aud_ip,$user_location;

    public function analytics_login()
    {
    	//set cookie for start time
		$this->checkIpLoc();
    	$this->checkDevice();
    	$this->checkBrowser();
    	$this->checkOS();
    	// exit();
    	$insertQuery=$this->prepare('insert into analytics_login (user_id,referrer,user_email,location,ip,browser,device,os) values (:uid,:refrer,:umail,:loc,:ip,:brows,:dev,:os)');

    	$refrer=$_SERVER['HTTP_REFERER'];
    	$insertQuery->bindparam(':uid',$_SESSION['userId'],PDO::PARAM_STR);
    	$insertQuery->bindparam(':refrer',$refrer,PDO::PARAM_STR);
    	$insertQuery->bindparam(':umail',$_REQUEST['user_email'],PDO::PARAM_STR);
    	$insertQuery->bindparam(':loc',$this->v_aud_loc,PDO::PARAM_STR);
    	$insertQuery->bindparam(':ip',$this->v_aud_ip,PDO::PARAM_STR);
    	$insertQuery->bindparam(':brows',$this->v_aud_browser,PDO::PARAM_STR);
    	$insertQuery->bindparam(':dev',$this->v_aud_Device,PDO::PARAM_STR);
    	$insertQuery->bindparam(':os',$this->v_aud_OS,PDO::PARAM_STR);
    	$insertQuery->execute();

    	$lastId=$this->lastInsertId();
        $_SESSION['login_id']=$lastId;
    	return $lastId;
    }

    public function addvisit()
    {
    	$iQuery=$this->prepare('insert into analytics_user_visits (user_id,login_id) values (:uid,:lid)');
    	$iQuery->bindparam(':uid',$_SESSION['userId'],PDO::PARAM_INT);
    	$iQuery->bindparam(':lid',$_SESSION['login_id'],PDO::PARAM_INT);
    	$iQuery->execute();

    	$lastId=$this->lastInsertId();

        $_SESSION['visit_id']=$lastId;
    	$time_start = microtime(true);
    	$cookie=array('user_email'=>$_SESSION['user_email'],"time_in"=>$time_start,'row_id'=>$lastId);
		$strUser = serialize($cookie);
		$exTime = time() + (60 * 60 * 24 * 7);
		setcookie("user_logged_in", $strUser, $exTime, '/');
    }

    public function timeout()
    {
    	if(isset($_COOKIE['user_logged_in']))
        {
        	$allData=unserialize($_COOKIE['user_logged_in']);
        	$timeout=microtime(true);
        	$user_email=$allData['user_email'];
        	$row_id=$allData['row_id'];
        	$time_in=$allData['time_in'];

        	$diff=$time_in-$timeout;

		    $hours = (int) ($diff / 60 / 60);
		    $minutes = (int) ($diff / 60) - $hours * 60;
		    $seconds = (int) $diff - $hours * 60 * 60 - $minutes * 60;
			
		    // echo $minutes;
			$totalTime=$hours.":".$minutes.":".$seconds;
			// if($minutes != "-0")
			// {
			// 	$totalTime=$hours.":".explode("-",$minutes)[1].":".explode("-", $seconds)[1];
			// }
			// else
			// {
			// 	$totalTime="00:00:".explode("-", $seconds)[1];
			// }
            $totalTime=str_replace("-", "", $totalTime);
       
        	$uQuery=$this->prepare('update analytics_user_visits set online_time=:otime where id=:aId');
        	$uQuery->bindparam(':aId',$row_id,PDO::PARAM_STR);
        	$uQuery->bindparam(':otime',$totalTime,PDO::PARAM_STR);
        	$uQuery->execute();

   //      	$exTime = time() - (60 * 60 * 24 * 7);
			// setcookie("user_logged_in", "", $exTime, '/');
        }
        else
        {

        }
    }

    public function videoClick($type,$page,$id)
    {
        //get Video info
        if($type=="movie")
        {   
            $mQuery=$this->prepare('select title from movies where imdb_id=:iid');
            $mQuery->bindparam(':iid',$id,PDO::PARAM_INT);
            $mQuery->execute();
            $mvData=$mQuery->fetchAll();
            $title=$mvData[0]['title'];

        }
        else
        {

            $tQuery=$this->prepare('select name from tv_shows where tmdb_id=:tid');
            $tQuery->bindparam(':tid',$id,PDO::PARAM_INT);
            $tQuery->execute();

            $tvData=$tQuery->fetchAll();
      
            $title=$tvData[0]['name'];
        }

        $sQuery=$this->prepare('insert into analytics_video_click (page,video_type,video_title,visit_id) values (:page,:vType,:vTitle,:visit_id)');
        $sQuery->bindparam(':page',$page,PDO::PARAM_STR);
        $sQuery->bindparam(':vType',$type,PDO::PARAM_STR);
        $sQuery->bindparam(':vTitle',$title,PDO::PARAM_STR);
        $sQuery->bindparam(':visit_id',$_SESSION['visit_id'],PDO::PARAM_INT);
        $sQuery->execute();

    }


    public function videoDownload($type,$Page,$title)
    {
        $sQuery=$this->prepare('insert into analytics_video_download (page,video_type,video_title,visit_id) values (:page,:vType,:vTitle,:visit_id)');
        $sQuery->bindparam(':page',$Page,PDO::PARAM_STR);
        $sQuery->bindparam(':vType',$type,PDO::PARAM_STR);
        $sQuery->bindparam(':vTitle',$title,PDO::PARAM_STR);
        $sQuery->bindparam(':visit_id',$_SESSION['visit_id'],PDO::PARAM_INT);
        $sQuery->execute();
    }


    public function videoplay($type,$Page,$title)
    {
        $sQuery=$this->prepare('insert into analytics_video_play (page,video_type,video_title,visit_id) values (:page,:vType,:vTitle,:visit_id)');
        $sQuery->bindparam(':page',$Page,PDO::PARAM_STR);
        $sQuery->bindparam(':vType',$type,PDO::PARAM_STR);
        $sQuery->bindparam(':vTitle',$title,PDO::PARAM_STR);
        $sQuery->bindparam(':visit_id',$_SESSION['visit_id'],PDO::PARAM_INT);
        $sQuery->execute();
    }


    public function search($type,$Keyword,$title,$page)
    {
        $sQuery=$this->prepare('insert into analytics_search (page,clicked_title_type,clicked_title,visit_id,keyword) values (:page,:vType,:vTitle,:visit_id,:key)');
        $sQuery->bindparam(':page',$page,PDO::PARAM_STR);
        $sQuery->bindparam(':key',$Keyword,PDO::PARAM_STR);
        $sQuery->bindparam(':vType',$type,PDO::PARAM_STR);
        $sQuery->bindparam(':vTitle',$title,PDO::PARAM_STR);
        $sQuery->bindparam(':visit_id',$_SESSION['visit_id'],PDO::PARAM_INT);
        $sQuery->execute();

    }

   


}//end of class
?>