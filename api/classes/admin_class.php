<?php
define('CLASS_NAME', 'ADMIN');
class admin extends PDO {

    public function __construct($dsn, $username = '', $password = '', $driver_options) {
        set_exception_handler(array(__CLASS__, 'exception_handler'));   // Temporarily change the PHP exception handler while we . . .

        parent::__construct($dsn, $username, $password, $driver_options);  // . . . create a PDO object

        restore_exception_handler(); // Change the exception handler back to whatever it was before
    }

    public static function exception_handler($exception) 
    {
        die("<strong>Uncaught Exception:</strong> " . $exception->getMessage());
    }


//class variables
    public $admin_email,$admin_name,$admin_username,$admin_password,$genre,$tmdb_id,$genreId,$genreData,$adminId,$adminType,$user_email,$greeting_message,$setting_status,$setting_id,$countires,$data_type,$production,$reviews,$images,$languages,$intro_videos,$cast,$genres,$credits,$video_id,$type,$episodes,$mov_id,$kids,$eBody,$emails,$eSubject;

    //Utitlty Functions
    public function error($code,$action,$error)
	{
		$error=array('status'=>$code,'action'=>$action,'description'=>$error);
		echo json_encode($error);
	}

	public function success($status,$action,$response)
	{
		$success=array('status'=>$status,'action'=>$action,'response'=>$response);
		echo json_encode($success);
	}

	public function sendEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.support@yandex.com";
		// $mail->Password = "!bbtv_support321@";
		// $mail->Port = "465"; 
		// $mail->SMTPDebug=4;
	    $mail->setFrom(SUPPORT_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);
	   
	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    } 
	    else
	    {
	      return 1;
	    } 
	}//End Utility
	public function sendbillingEmail($subject,$address,$emailBody)
	{
		// exit();
		$mail = new PHPMailer();
		// $mail->IsSMTP();
		// $mail->Host = "smtp.yandex.com";
		// $mail->SMTPAuth = true;
		// $mail->SMTPSecure = "ssl";
		// $mail->Username = "bbtv.billing@yandex.com";
		// $mail->Password = "@bbtv_billing321!";
		// $mail->Port = "465"; 
		// $mail->SMTPDebug=4;
	    $mail->setFrom(BILLING_EMAIL_HOST, SITE_TITLE);
	    // $mail->FromName = SITE_TITLE;
	    $mail->Subject    = $subject;
	   	$mail->msgHTML($emailBody);
	   
	 	//add reciever's address
	    $mail->AddAddress($address, "");
		// send as HTML
	    $mail->IsHTML(true);
	    if(!$mail->Send())
	    {
	       echo "Mailer Error: " . $mail->ErrorInfo;
	    } 
	    else
	    {
	      return 1;
	    } 
	}//End Utility


    //Class Functions
    public function signup()
    {

    	//chk if email already available
		$chkQUery=$this->prepare('select admin_id from admins where email=:Email');
		$chkQUery->bindParam(':Email',$this->admin_email,PDO::PARAM_STR);
		$chkQUery->execute();
		$count=$chkQUery->rowCount();
		if($count == 0)
		{
			//chk if userName is available
			$nameQuery=$this->prepare('select admin_id from admins where username=:uname');
			$nameQuery->bindParam(':uname',$this->admin_username,PDO::PARAM_STR);
			$nameQuery->execute();
			if($nameQuery->rowCount()>0)
			{
				//userName is already saved not available
			$this->error('ERROR',__FUNCTION__,'username already taken');
			exit();
			}
			else
			{
				//Not Available. Add New Admin
				$signupQuery=$this->prepare('insert into admins (name,email,username,password) values (:Name,:email,:uName,:pass)');
				$signupQuery->bindParam(':Name',$this->admin_name,PDO::PARAM_STR);
				$signupQuery->bindParam(':email',$this->admin_email,PDO::PARAM_STR);
				$signupQuery->bindParam(':uName',$this->admin_username,PDO::PARAM_STR);
				$signupQuery->bindParam(':pass',$this->admin_password,PDO::PARAM_STR);
				$approveLink=SITEURL.'/approve/admin/'.$this->admin_email;
				$signupQuery->execute();//Query Execution
				if($signupQuery->rowCount()>0)
				{

		            	$this->success(200,__FUNCTION__,'saved');
						exit();
		         
					
				}
				else
				{
					//error saving PDO error displayed
					$this->error('ERROR',__FUNCTION__,$signupQuery->errorInfo());
					exit();
			
				}	
			}
		}
		else
		{
			//Already Available
			$this->error('ERROR',__FUNCTION__,'email already taken');
			exit();
		}
    }//End Signup

    //login start
	public function login()
	{
		$logQuery=$this->prepare('select * from admins where username=:userName and password=:password and active=1');
		$logQuery->bindParam(':userName',$this->admin_username,PDO::PARAM_STR);
		$logQuery->bindParam(':password',$this->admin_password,PDO::PARAM_STR);
		$logQuery->execute();
		$count=$logQuery->rowCount();
		if($count>0)
		{
			//available chk if approved or not 
			$adminData=$logQuery->fetchAll(PDO::FETCH_ASSOC);
			$approved=$adminData[0]['approved'];
			$adminId=$adminData[0]['admin_id'];
			if($approved==0)
			{
				$this->success(200,__FUNCTION__,'waiting for approvel');
				exit();
			}
			else
			{
				//update last login
				$upQuery=$this->prepare('update admins set last_login=NOW() where admin_id=:aId');
				$upQuery->bindParam(':aId',$adminId,PDO::PARAM_INT);
				$upQuery->execute();
				//set session data
				$_SESSION['admin_id']=$adminData[0]['admin_id'];	
				$this->success(200,__FUNCTION__,$adminData);
				exit();
			}
			
		}
		else
		{
			//not available
			$this->error('Error',__FUNCTION__,'incorrect email/password');
			exit();
		}
	}//login end

	//forgetPassword start
	public function forgetPassword()
	{
		//chk if email available
		$chkQUery=$this->prepare('select admin_id,name from admins where email=:email and approved=1 and active=1');
		$chkQUery->bindParam(':email',$this->admin_email,PDO::PARAM_STR);
		$chkQUery->execute();
		if($chkQUery->rowCount() > 0)
		{
			$adminData=$chkQUery->fetchAll(PDO::FETCH_ASSOC);
			$adminName=$adminData[0]['name'];
			//email available generate reset link 
			$randString=md5(date("Y-m-d H:i:s"));
			// print_r($_SERVER);
			// exit();
			$resetlink=SITEURL.'/reset/admin/'.$randString;
			//save Randstring against admin
			$sQuery=$this->prepare('update admins set reset_code = :rCode where email=:email');
			$sQuery->bindParam(':email',$this->admin_email,PDO::PARAM_STR);
			$sQuery->bindParam(':rCode',$randString,PDO::PARAM_STR);
			$sQuery->execute();
			if($sQuery->rowCount()>0)
			{
				//reste code saved send email to admin with reset link
				$body = "";
	            $body .="Dear  ".$adminName.", <br><br> ";
	            $body .= "<h1><a href=$resetlink>Reset Link</a></h1>  is ur resetlink. Please click to reset password.";
	            $body .= "<br>Thank you";
	            $body .= "<br><br>James Cordan ";
				$response=$this->sendEmail('Reset Password' , $this->admin_email , $body);
				if($response==1)
		            {
		            	$this->success(200,__FUNCTION__,'Email Sent');
						exit();
		            }

			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
				exit();
			}
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,'email not available or pending for approval');
			exit();
		}
	}//End forgetpass

	//start reset pass
	public function resetPassword()
	{
		//chk if reset code is available in db
		$pQuery=$this->prepare('select * from admins where reset_code=:rCode and approved=1 and active=1');
		$pQuery->bindParam(':rCode',$this->reset_code,PDO::PARAM_STR);
		$pQuery->execute();
		if($pQuery->rowCount() > 0)
		{

			$adminData=$pQuery->fetchAll(PDO::FETCH_ASSOC);
			$adminId=$adminData[0]['admin_id'];
			//code available update new password
			$upQuery=$this->prepare('update admins set password=:pwrd , reset_code=NULL where admin_id=:aId');
			$upQuery->bindParam(':aId',$adminId,PDO::PARAM_STR);
			$upQuery->bindParam(':pwrd',$this->admin_password,PDO::PARAM_STR);
			$upQuery->execute();
			if($upQuery->rowCount()>0)
			{
				$this->success(200,__FUNCTION__,'Password Reset 	');
				exit();
			}
			else
			{
				//Error
				$this->error('ERROR',__FUNCTION__,$upQuery->errorInfo());
				exit();
			}
		}
		else
		{
			//reset code is not available or expired
			$this->error('ERROR',__FUNCTION__,'reset code not available/Account Temporarily Blocked');
			exit();
		}
	}//End
	//start approve users
	public function approveUser($tablename)
	{
		$getQuery=$this->prepare('select * from '.$tablename.' where email=:email');
		$getQuery->bindParam(':email',$this->approve_email,PDO::PARAM_STR);
		$getQuery->execute();
		// print_r($getQuery->errorInfo());
		if($getQuery->rowCount()>0)
		{
			//email available
			$userData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
			// print_r($userData);
			if($tablename=="admins")
			{
				$upQuery=$this->prepare('update '.$tablename.' set approved=1 where email=:eMail');
				$userName=$userData[0]['name'];
				$upQuery->bindParam(':eMail',$this->approve_email,PDO::PARAM_STR);
				$upQuery->execute();
				if($upQuery)
				{
					$this->success(200,__FUNCTION__,"approved");
					exit();
				}
				else
				{
					$this->error('ERROR',__FUNCTION__,$upQuery->errorInfo());
					exit();
				}
			}
			else
			{
				//Email Body
				$randString=md5(date("Y-m-d H:i:s"));
				//update activation code
				$upQuery=$this->prepare('update users set activation_code=:aCode where user_id=:uid');
				$upQuery->bindParam(':aCode',$randString,PDO::PARAM_STR);
				$upQuery->bindParam(':uid',$userData[0]['user_id'],PDO::PARAM_INT);
				$upQuery->execute();
				if($upQuery->rowCount()>0)
				{
					$activationlink=SITEURL.'activate/user/'.$randString;
					$body = "";
		            $body .="Hi ".$userData[0]['f_name'].", <br><br> ";
		            $body .="Welcome to ".SITE_TITLE.", there is one more step……<br>";
		            $body .="<br>Please click on the following link or copy paste in your browser";
		            $body .="<br><a href=".$activationlink.">".$activationlink."</a>";
		            $body .="<br><br>Thanks,<br><br> James Cordan";

		            $Request=$this->sendEmail('Account Activation' , $userData[0]['email'] , $body);
		            
					if($Request==1)
					{
						$this->success(200,__FUNCTION__,"approved");
						exit();
					}
				}
				else
				{
					$this->error('ERROR',__FUNCTION__,'Error');
					exit();
				}
			}
			

		}
		else
		{
			//Error email not avialable
			$this->error('ERROR',__FUNCTION__,'incorrect email');
			exit();
		}
	} //END approve users

	//start pending users
	public function getPendingusers($user)
	{

		if($user=="")
		{
			//get all pending users
		
			$customerData=$this->getPendingusers('users');
			$adminData=$this->getPendingusers('admins');
			$allData=array('admin'=>$adminData,'user'=>$customerData);
		
			$this->success(200,__FUNCTION__,$allData);
			exit();
		}
		else if($user=="users")
		{	
			
			$getQuery=$this->prepare('select * from users where is_approved=0 and activation_code is NULL');
		}
		else if($user=="admins")
		{
			//pending admins 
			$getQuery=$this->prepare('select * from admins where approved=0');
		}
			
			$getQuery->execute();
			if($getQuery)
			{
				$UserData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
				
				return $UserData;
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$getQuery->errorInfo());
				exit();
			}
	}//end pending users
	//start user Data
	public function getUserData($user)
	{
		 if($user=="users")
		{	
			$tableName='users';
			$entity='user_id';
		}
		else if($user=="admins")
		{
			$tableName='admins';	
			$entity='admin_id';
		}
		$getQuery=$this->prepare('select * from '.$tableName.' where '.$entity.'=:userId');
		$getQuery->bindParam(':userId',$this->userId,PDO::PARAM_INT);
		$getQuery->execute();
		
		$userData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
		return $userData;	
	}//End of userData 

	//get all users
	public function getallusers($type)
	{
		if($type=="users")
		{	
			$tableName='users';
			
		}
		else if($type=="admins")
		{
			$tableName='admins';	
			
		}
		$getQuery=$this->prepare('select * from '.$tableName);


		$getQuery->execute();
	
		$userData=$getQuery->fetchAll(PDO::FETCH_ASSOC);
		return $userData;	
	}
	//END

	//start save genre
	public function addgenre()
	{
		//chk if already available
		$chkQUery=$this->prepare('select * from genres where (tmdb_id=:tid or title=:gTitle) and active=1');
		$chkQUery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_INT);
		$chkQUery->bindParam(':gTitle',$this->genre,PDO::PARAM_STR);
		$chkQUery->execute();

		if($chkQUery->rowCount()>0)
		{
			//genre available

			$this->error('ERROR',__FUNCTION__,'Genre already available');
			exit();
		}
		else
		{
			//not available save new
			$sQuery=$this->prepare('insert into genres (title,tmdb_id) values (:gTitle,:tid)');
			$sQuery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_INT);
			$sQuery->bindParam(':gTitle',$this->genre,PDO::PARAM_STR);
			$sQuery->execute();

			if($sQuery->rowCount()>0)
			{
				//saved
				$this->success(200,__FUNCTION__,"Genre Added");
				exit();
			}
			else
			{
				//error
				$this->error('ERROR',__FUNCTION__,$sQuery->errorInfo());
				exit();
			}
		}


	}
	//end

	//start allgenres
	public function allGenres()
	{
		$gQuery=$this->prepare('select * from genres');
		$gQuery->execute();
		
		if($gQuery)
		{
			$genData=$gQuery->fetchAll(PDO::FETCH_ASSOC);
			$this->success(200,__FUNCTION__,$genData);
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$gQuery->errorInfo());
			exit();
		}
	}
	//end

	//Strt edit genre
	public function editGenre()
	{
		$updQuery=$this->prepare('update genres set title=:title , tmdb_id=:tId where gen_id=:gId');
		$updQuery->bindParam(':tId',$this->tmdb_id,PDO::PARAM_INT);
		$updQuery->bindParam(':title',$this->genre,PDO::PARAM_STR);
		$updQuery->bindParam(':gId',$this->genreId,PDO::PARAM_INT);
		$updQuery->execute();
		if($updQuery->rowCount()>0)
		{
			$this->success(200,__FUNCTION__,"updated");
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$updQuery->errorInfo());
			exit();
		}
	}//End

	//start genre deactive
	public function removeGenre()
	{
		$active=0;
		$rQuery=$this->prepare('update genres set active=:act where gen_id=:gId');
		$rQuery->bindParam(':act',$active,PDO::PARAM_INT);
		$rQuery->bindParam(':gId',$this->genreId,PDO::PARAM_INT);
		$rQuery->execute();
		if($rQuery->rowCount()>0)
		{
			$this->success(200,__FUNCTION__,"removed");
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$rQuery->errorInfo());
			exit();
		}
	}//END

	//start reactive
	public function reactiveGenre()
	{
		$active=1;
		$rQuery=$this->prepare('update genres set active=:act where gen_id=:gId');
		$rQuery->bindParam(':act',$active,PDO::PARAM_INT);
		$rQuery->bindParam(':gId',$this->genreId,PDO::PARAM_INT);
		$rQuery->execute();
		if($rQuery->rowCount()>0)
		{
			$this->success(200,__FUNCTION__,"updated");
			exit();
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$rQuery->errorInfo());
			exit();
		}
	}
	//ENd

	//get genres online start
	public function saveOnlineData()
	{
	
		$chkQUery=$this->prepare('select * from genres where tmdb_id=:tid or title=:gTitle');
		$sQuery=$this->prepare('insert into genres (title,tmdb_id) values (:gTitle,:tid)');

		for($i=0;$i<count($this->genreData);$i++)
		{
			$this->tmdb_id=$this->genreData[$i]['id'];
			$this->genre=$this->genreData[$i]['name'];
			$chkQUery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_INT);
			$chkQUery->bindParam(':gTitle',$this->genre,PDO::PARAM_STR);
			$chkQUery->execute();
			if($chkQUery->rowCount()>0)
			{
				//do nothing
			}
			else
			{
				$sQuery->bindParam(':tid',$this->tmdb_id,PDO::PARAM_INT);
				$sQuery->bindParam(':gTitle',$this->genre,PDO::PARAM_STR);
				$sQuery->execute();
			}
		}	
		$this->success(200,__FUNCTION__,"added");
		exit();		
	}//end
	//updatetoSuper

	public function updatetoSuper()
	{
		$upQuery=$this->prepare('update admins set type=:type where admin_id=:aID');
		$upQuery->bindParam(':aID',$this->adminId,PDO::PARAM_INT);
		$upQuery->bindParam(':type',$this->adminType,PDO::PARAM_INT);
		$upQuery->execute();
		if($upQuery->rowCount()>0)
		{
			$this->success(200,__FUNCTION__,"updated");
			exit();		
		}
		else
		{
			$this->error('ERROR',__FUNCTION__,$upQuery->errorInfo());
			exit();
		}
	}
	//end

	//start admin by super admin

	public function adminBysuper()
	{
		$chkQUery=$this->prepare('select admin_id from admins where email=:Email');
		$chkQUery->bindParam(':Email',$this->admin_email,PDO::PARAM_STR);
		$chkQUery->execute();
		$count=$chkQUery->rowCount();
		if($count == 0)
		{
			//chk if userName is available
			$nameQuery=$this->prepare('select admin_id from admins where username=:uname');
			$nameQuery->bindParam(':uname',$this->admin_username,PDO::PARAM_STR);
			$nameQuery->execute();
			if($nameQuery->rowCount()>0)
			{
				//userName is already saved not available
			$this->error('ERROR',__FUNCTION__,'username already taken');
			exit();
			}
			else
			{
				//Not Available. Add New Admin
				$signupQuery=$this->prepare('insert into admins (name,email,username,password,approved,type) values (:Name,:email,:uName,:pass,:approv,:type)');
				$approved=1;
				$type=$this->adminType;
				$signupQuery->bindParam(':Name',$this->admin_name,PDO::PARAM_STR);
				$signupQuery->bindParam(':email',$this->admin_email,PDO::PARAM_STR);
				$signupQuery->bindParam(':uName',$this->admin_username,PDO::PARAM_STR);
				$signupQuery->bindParam(':pass',$this->admin_password,PDO::PARAM_STR);
				$signupQuery->bindParam(':approv',$approved,PDO::PARAM_STR);
				$signupQuery->bindParam(':type',$type,PDO::PARAM_STR);
		
				$signupQuery->execute();//Query Execution
				if($signupQuery->rowCount()>0)
				{

		            	$this->success(200,__FUNCTION__,'saved');
						exit();
		         
					
				}
				else
				{
					//error saving PDO error displayed
					$this->error('ERROR',__FUNCTION__,$signupQuery->errorInfo());
					exit();
			
				}	
			}
		}
		else
		{
			//Already Available
			$this->error('ERROR',__FUNCTION__,'email already taken');
			exit();
		}
	}
	//End

	//start user invite
	public function inviteUser()
	{
		$chkQuery=$this->prepare('select invite_id from user_invites where user_email=:email');
		$chkQuery->bindParam(':email',$this->user_email,PDO::PARAM_STR);
		$chkQuery->execute();
		if($chkQuery->rowCount()>0)
		{
			// invite already sent
			$this->error('ERROR',__FUNCTION__,'Invite already sent');
			exit();
		}
		else
		{
			//add new invite
			$randString=md5(date("Y-m-d H:i:s"));
			$link=SITEURL."signup/".$randString;
			$addQuery=$this->prepare('insert into user_invites (user_email,greeting_message,profiles,invite_code) values (:email,:gmsg,:profl,:iCode)');
			$addQuery->bindParam(':email',$this->user_email,PDO::PARAM_STR);
			$addQuery->bindParam(':gmsg',$this->greeting_message,PDO::PARAM_STR);
			$addQuery->bindParam(':profl',$this->profiles,PDO::PARAM_INT);
			$addQuery->bindParam(':iCode',$randString,PDO::PARAM_INT);
			$addQuery->execute();
			if($addQuery->rowCount()>0)
			{
				//added send email
				$body = "";
	            $body .="Dear , <br><br> ";
	            $body .= $this->greeting_message;
	            $body .= "Please click the link below.<br>";
	            $body .= "<a href='".$link."'>".$link."</a>";
	            $body .= "<br>Thank you";
	            $body .= "<br><br>James Cordan";
				$response=$this->sendEmail('Invitation' , $this->user_email , $body);
				if($response==1)
		            {
		            	$this->success(200,__FUNCTION__,'Email Sent');
						exit();
		            }
			}
			else
			{
				$this->error('ERROR',__FUNCTION__,$addQuery->errorInfo());
				exit();
			}

		}
	}
	//end

	//start get signup setting
	public function getSignupsetting()
	{
		$sQuery=$this->prepare('select value from settings where setting_id=1');
		$sQuery->execute();
		$sData=$sQuery->fetchAll(PDO::FETCH_ASSOC);
		if($sQuery->rowCount()>0)
		{
			$this->success(200,__FUNCTION__,$sData);
			exit();	
		}	
		else
		{
			$this->success(200,__FUNCTION__,"NO Data");
			exit();	
		}
	}
	// end

	//start add signup settings
	public function addsignupSettings()
	{
			//chk if already available
		$chkQUery=$this->prepare('select * from settings where setting_id=:sId');
		$chkQUery->bindParam(':sId',$this->setting_id,PDO::PARAM_INT);
		$chkQUery->execute();
		if($chkQUery->rowCount()>0)
		{
			//found update
			$upQuery=$this->prepare('update settings set value=:sValue , date_updated=now() where setting_id=:sId');
			$upQuery->bindParam(':sId',$this->setting_id,PDO::PARAM_INT);
			$upQuery->bindParam(':sValue',$this->setting_status,PDO::PARAM_STR);
			$upQuery->execute();
			if($upQuery->rowCount()>0)
			{
				//updated
				$this->success(200,__FUNCTION__,'success');
				exit();	

			}
			else
			{
				//error occured
				$this->error('ERROR',__FUNCTION__,$upQuery->errorInfo());
				exit();	
			}
		}
		else
		{
			//not found insert new 1
			$insertQuery=$this->prepare('insert into settings (name,value,setting_id) values (:sName,:sValue,:sId)');
			$sName="Open User Sigup";
			$insertQuery->bindParam(':sId',$this->setting_id,PDO::PARAM_INT);
			$insertQuery->bindParam(':sValue',$this->setting_status,PDO::PARAM_INT);
			$insertQuery->bindParam(':sName',$sName,PDO::PARAM_INT);
			$insertQuery->execute();
			print_r($insertQuery->errorInfo());
			if($insertQuery->rowCount()>0)
			{
				//added
				$this->success(200,__FUNCTION__,'success');
				exit();	

			}
			else
			{
				//error occured
				$this->error('ERROR',__FUNCTION__,$insertQuery->errorInfo());
				exit();	
			}

		}

	}
	//end

	public function syncMovies()
	{
		//get data from queue
		$qQuery=$this->prepare('select imdb_id from movies where is_active = 0');
		$qQuery->execute();
		if($qQuery->rowCount()>0)
		{
			// unsynced movies
			$qData=$qQuery->fetchAll(PDO::FETCH_ASSOC);
			
			//loop on thi data
			for($i=0;$i<count($qData);$i++)
			{
				
				$imdbId=$qData[$i]['imdb_id'];
				$orgid=explode("tt", $imdbId);
				$compReId=$orgid[1];
				//find in queue
				$fQuery=$this->prepare('select * from queue where IMDB_ID=:iid and status=1 and is_deleted=0 and height <> 0 and width <> 0');
				$fQuery->bindParam(':iid',$compReId,PDO::PARAM_STR);
				$fQuery->execute();
				if($fQuery->rowCount()>0)
				{
					
				
					$mData=$fQuery->fetchAll(PDO::FETCH_ASSOC);
					$type="";
					$mv_title="";
					$mv_id="";
					$duration;
					$IMDB;
					//found chk if result is 1 or 2 
					if($fQuery->rowCount()==1)
					{
						// only 1 record
						//check the resolution and save that in videos folder
						$width=$mData[0]['width'];
						$height=$mData[0]['height'];
						$type=$this->getmType($height);
						$mv_id=$mData[0]['VIDEO_ID'];
						$mv_title=$mData[0]['MOVIE_TITLE'];
						$duration=$mData[0]['duration'];
						$IMDB="tt".$mData[0]['IMDB_ID'];
					}
					else
					{
						//more then 1 record
						$counter=$fQuery->rowCount();
						$this->sortArrayByKey($mData,"height",false,false);
						$bestresolution=$mData[0];
						$type=$this->getmType($bestresolution['height']);
						$mv_id=$bestresolution['VIDEO_ID'];
						$mv_title=$bestresolution['MOVIE_TITLE'];
						$duration=$bestresolution['duration'];
						$IMDB="tt".$bestresolution['IMDB_ID'];
					}
					$bPath="http://media.beanbag.tv/movies/";
					$video_type='movie';
					$mtype=$mv_id.".mp4";

					//save in videos table
					$sQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
					$sQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
					$sQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
					$sQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
					$sQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
					$sQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
					$sQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
					$sQuery->execute();

					if($sQuery->rowCount()>0)
					{
						//update movie active status
						$upQuery=$this->prepare('update movies set is_active=1 where imdb_id=:id');
						$upQuery->bindParam(':id',$IMDB,PDO::PARAM_STR);
						$upQuery->execute();
					}
					else
					{
						echo $mv_title." ".$mv_id." ".$duration." ".$type;
						print_r($sQuery->errorInfo());
					}
				}
				else
				{
					//no data found in queue
					// echo 'no data in quer';

				}
			}
			$this->success(200,__FUNCTION__,'success');
			exit();	
		}
		else
		{
			// echo 'no data';
		}
	}
	public function is_utf8($string) 
	{
 		 return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
	}
	public function utf8_to_html ($data) 
	{
    return preg_replace(
        array (
            '/ä/',
            '/ö/',
            '/ü/',
            '/é/',
            '/à/',
            '/è/'
        ),
        array (
            '&auml;',
            '&ouml;',
            '&uuml;',
            '&eacute;',
            '&agrave;',
            '&egrave;'
        ),
        $data 
    );
}

	public function syncmovies2()
	{
				ini_set('max_execution_time', 500000);
				$fQuery=$this->prepare('select * from queue where status=1 and is_deleted=0 and height <> 0 and width <> 0');
				$fQuery->execute();
				if($fQuery->rowCount()>0)
				{
					$mData=$fQuery->fetchAll(PDO::FETCH_ASSOC);
					for($i=0;$i<count($mData);$i++)
					{
						$IMDB="tt".$mData[$i]['IMDB_ID'];
						$queueId=$mData[$i]['ID'];
						//check for details in movies
						$mQuery=$this->prepare('select * from movies where imdb_id=:iid');
						$mQuery->bindParam(':iid',$IMDB,PDO::PARAM_INT);
						$mQuery->execute();
						if($mQuery->rowCount()>0)
						{
							//details available
							//check if is active or not
							$movieDetails=$mQuery->fetchAll();
							if($movieDetails[0]['is_active']==1)
							{
								$height=$mData[$i]['height'];
								$type=$this->getmType($height);
								$mv_id=$mData[$i]['VIDEO_ID'];
								$duration=$mData[$i]['duration'];
								$IMDB="tt".$mData[$i]['IMDB_ID'];
								$bPath="http://media.beanbag.tv/movies/";
								$video_type='movie';
								$mtype=$mv_id.".mp4";
								//movie is already active compare resolution
								//get saved resolution from videos
								$vQuery=$this->prepare('select * from videos where imdb_id=:iid');
								$vQuery->bindParam(':iid',$IMDB,PDO::PARAM_INT);
								$vQuery->execute();
								if($vQuery->rowCount()>0)
								{
									$video=$vQuery->fetchAll();
									if (isset($video[0]['p360']))
			                        {
			                           $savedHeight=360;
			                        }
			                         else if (isset($video[0]['p480'])) {
			                             $savedHeight=480;
			                        }
			                         else if (isset($video[0]['p720'])) {
			                             $savedHeight=720;
			                        }
			                         else if (isset($video[0]['p1080'])) {
			                            $savedHeight=1080;
			                        }
			                         else if (isset($video[0]['p1440'])) {
			                            $savedHeight=1440;
			                        }
									if($savedHeight >= $height)
									{
										//no need to change already perfectly saved	
										$delQuery=$this->prepare('update queue set is_deleted=1 where Id=:id');
										$delQuery->bindParam(':id',$queueId,PDO::PARAM_INT);
										$delQuery->execute();
									}
									else
									{
										//update new video in videos 
										$upQuery=$this->prepare('update videos set scid=:sid , '.$type.'=:mType,duration=:duration where imdb_id=:iid');
										$upQuery->bindParam(':sid',$mv_id,PDO::PARAM_INT);
										$upQuery->bindParam(':mType',$mType,PDO::PARAM_INT);
										$upQuery->bindParam(':duration',$duration,PDO::PARAM_INT);
										//delete previous from queue
										$delQuery=$this->prepare('update queue set is_deleted=1 where VIDEO_ID=:vid');
										$delQuery->bindParam(':id',$video[$i]['scid'],PDO::PARAM_INT);
										$delQuery->execute();
										
									}
								}
								else
								{
									//this is error now movie is active but down't have file in videos
								}
							}
							else
							{
								
								$height=$mData[$i]['height'];
								$type=$this->getmType($height);
								$mv_id=$mData[$i]['VIDEO_ID'];
								$duration=$mData[$i]['duration'];
								$IMDB="tt".$mData[$i]['IMDB_ID'];
								$bPath="http://media.beanbag.tv/movies/";
								$video_type='movie';
								$mtype=$mv_id.".mp4";
								// movie is not active 
								$sQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
								$sQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
								$sQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
								$sQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
								$sQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
								$sQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
								$sQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
								$sQuery->execute();

								if($sQuery->rowCount()>0)
								{
									//update movie active status
									$upQuery=$this->prepare('update movies set is_active=1 , live_video_id=:lid where imdb_id=:id');
									$upQuery->bindParam(':id',$IMDB,PDO::PARAM_STR);
									$upQuery->bindParam(':lid',$mv_id,PDO::PARAM_INT);
									$upQuery->execute();
								}
								else
								{
									echo $mv_title." ".$mv_id." ".$duration." ".$type;
									print_r($sQuery->errorInfo());
								}
							}
						}
						else
						{
							$mv_id=$mData[$i]['VIDEO_ID'];
							$live_video_id=$mv_id;
							$this->type="movie";
							//no details available
							//get details from tmdb
							$api=file_get_contents('https://api.themoviedb.org/3/movie/'.$IMDB.'?api_key=97e8434a62c5f9732e325e238b3aaf51&append_to_response=images,videos,reviews,credits');
	    					$movieData = json_decode($api,true);
	    					$this->production=$movieData['production_companies'];
						    $this->images=$movieData['images'];
						    $this->videos=$movieData['videos'];
						    $this->reviews=$movieData['reviews'];
						    $this->credits=$movieData['credits']['cast'];
						    $this->languages=$movieData['spoken_languages'];
						    $this->genres=$movieData['genres'];

						    //variables
						    $tmdb_id=$movieData['id'];
							$imdb_id=$movieData['imdb_id'];
							$ranking=$movieData['vote_average'];
							$title=$movieData['title'];
							$overview=$movieData['overview'];
							$runtime=$movieData['runtime'];
							$revenue=$movieData['revenue'];
							$budget=$movieData['budget'];
							$tagline=$movieData['tagline'];
							$release_date=$movieData['release_date'];
							$status=$movieData['status'];
							$poster=$movieData['poster_path'];
							if(isset($movieData['production_countries']))
							{
								$country=$movieData['production_countries'][0]['iso_3166_1'];
							}
							else
							{
								$country="";
							}
							//get pg information from imdb
							$pginfo_api=file_get_contents('http://www.omdbapi.com/?i='.$imdb_id);
							$pgData=json_decode($pginfo_api);
							//object data extraction	
							$mvPg=$pgData->Rated;
							if($mvPg=="G" or $mvPg=="PG" or $mvPg=="PG-13")
							{
								$kids=1;
							}
							else
							{
								$kids=0;
							}
								$saveQuery=$this->prepare('insert into movies (title,tmdb_id,imdb_id,path,live_video_id,release_date,country,overview,runtime,revenue,status,tagline,rank,budget,rated,kids,poster_path) values (:title,:tId,:iId,:path,:lid,:rData,:country,:overview,:runTime,:revenue,:status,:tagline,:rank,:budget,:rated,:kids,:pPath)');
							if($this->is_utf8($overview))
							{
								$overview=mb_convert_encoding($overview, 'HTML-ENTITIES', 'UTF-8');
								echo mb_detect_encoding($overview);
							}
							else
							{
							
							}
							if($this->is_utf8($title))
							{
								$title=utf8_decode($title);
							}
							else
							{
								
							}
							
								$saveQuery->bindParam(':title',$title,PDO::PARAM_STR);
								$saveQuery->bindParam(':tId',$tmdb_id,PDO::PARAM_INT);
								$saveQuery->bindParam(':iId',$imdb_id,PDO::PARAM_STR);
								$saveQuery->bindParam(':path',$path,PDO::PARAM_STR);
								$saveQuery->bindParam(':lid',$live_video_id,PDO::PARAM_STR);
								$saveQuery->bindParam(':rData',$release_date,PDO::PARAM_STR);
								$saveQuery->bindParam(':country',$country,PDO::PARAM_STR);
								$saveQuery->bindParam(':overview',$overview,PDO::PARAM_STR);
								$saveQuery->bindParam(':runTime',$runtime,PDO::PARAM_INT);
								$saveQuery->bindParam(':revenue',$revenue,PDO::PARAM_INT);
								$saveQuery->bindParam(':status',$status,PDO::PARAM_STR);
								$saveQuery->bindParam(':tagline',$tagline,PDO::PARAM_STR);
								$saveQuery->bindParam(':rank',$ranking,PDO::PARAM_STR);
								$saveQuery->bindParam(':budget',$budget,PDO::PARAM_INT);
								$saveQuery->bindParam(':rated',$mvPg,PDO::PARAM_INT);
								$saveQuery->bindParam(':kids',$kids,PDO::PARAM_INT);
								$saveQuery->bindParam(':pPath',$poster,PDO::PARAM_INT);
								$saveQuery->execute();
								if($saveQuery->rowCount()>0)
								{
									$this->video_id=$this->lastInsertId();
									$addprod=$this->addproduction();
									if($addprod==1)
									{
										//saved
										// save reviews
										$addreview=$this->addreviews();
										if($addreview==1)
										{
											//saved
											//save images
											$addimages=$this->addimages();
											if($addimages==1)
											{
												
												//saved
												//save languagess
												$addlanguage=$this->addlanguage();
												if($addlanguage==1)
												{
													
													//saved 
													//save videos
													$addvideos=$this->addvideos();	
													if($addvideos==1)
													{
														
														//saved 
														//save cast
														$addcast=$this->addcast();
														if($addcast==1)
														{
															
															//saved 
															// save genres
															$addgenres=$this->addgenres();
															if($addgenres==1)
															{
																//video information is saved 
																// link video file now
																$height=$mData[$i]['height'];
																$type=$this->getmType($height);
																$mv_id=$mData[$i]['VIDEO_ID'];
																$duration=$mData[$i]['duration'];
																$IMDB="tt".$mData[$i]['IMDB_ID'];
																$bPath="http://media.beanbag.tv/movies/";
																$video_type='movie';
																$mtype=$mv_id.".mp4";
																// movie is not active 
																$sQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
																$sQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
																$sQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
																$sQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
																$sQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
																$sQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
																$sQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
																$sQuery->execute();

																if($sQuery->rowCount()>0)
																{
																	//update movie active status
																	$upQuery=$this->prepare('update movies set is_active=1 , live_video_id=:lid where imdb_id=:id');
																	$upQuery->bindParam(':id',$IMDB,PDO::PARAM_STR);
																	$upQuery->bindParam(':lid',$mv_id,PDO::PARAM_INT);
																	$upQuery->execute();
																	
																}
																else
																{
																	echo $mv_title." ".$mv_id." ".$duration." ".$type;
																	print_r($sQuery->errorInfo());
																}
															}
															else
															{
																//error delete all details 
															}
														}
														else
														{

															//error
															//delete last inserted movie records
														}
													}
													else
													{
														//error
														//delete last inserted movie records
													}

												}
												else
												{
													//error
													//delete last inserted movie records
												}
											}
											else
											{
												//error
											//delete last inserted movie records
											}
										}
										else
										{
											//error
											//delete last inserted movie records
										}
									}
									else
									{
										//error occuerd
										//delete last inserted movie record
									}
								}
								else
								{
									print_r($movieData);
									print_r($saveQuery->ErrorInfo());
									exit();
								}

						}
					}
				}
				else
				{
					//no data found in queue
					// echo 'no data in quer';

				}

	}


		//start addgenres

	public function addgenres()
	{
		$addQuery=$this->prepare('insert into video_genres (video_id,video_type,gen_tmdb_id) values (:vid,:vType,:gId) ');
		$genres=$this->genres;
		for($i=0;$i<count($genres);$i++)
		{
			$addQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			$addQuery->bindParam(':gId',$genres[$i]['id']);
			$addQuery->execute();
		}
		return 1;
	}
	//end

	//start add cast
	public function addcast()
	{
		$addQuery=$this->prepare('insert into video_cast (video_id,video_type,tmdb_character,tmdb_id,tmdb_cast_id,cast_name,tmdb_order,profile_path )values (:vid,:vType,:tCharacter,:tid,:tcid,:cName,:tOrder,:pPath)');
		$cast=$this->credits;
		for($i=0;$i<count($cast);$i++)
		{
			$addQuery->bindParam(':vid',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			$addQuery->bindParam(':tCharacter',$cast[$i]['character'],PDO::PARAM_INT);
			$addQuery->bindParam(':tid',$cast[$i]['credit_id'],PDO::PARAM_STR);
			$addQuery->bindParam(':tcid',$cast[$i]['id'],PDO::PARAM_INT);
			$addQuery->bindParam(':cName',$cast[$i]['name'],PDO::PARAM_STR);
			$addQuery->bindParam(':tOrder',$cast[$i]['order'],PDO::PARAM_INT);
			$addQuery->bindParam(':pPath',$cast[$i]['profile_path'],PDO::PARAM_STR);
			$addQuery->execute();
			
		}
		return 1;
	}
	//end

	//start addvideos
	public function addvideos()
	{
		$addQuery=$this->prepare('insert into video_intro_videos (video_type,video_id,tmdb_id,language,site_key,name,source_site,size,type) values (:vType,:vId,:tId,:lang,:sKey,:name,:sSite,:size,:type)');
		if($this->intro_videos=="")
		{
			return 1;
		}
		else
		{
			$videos=$this->intro_videos['results'];
			if(count($videos)>0)
			{
				for($i=0;$i<count($videos);$i++)
				{
					$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
					$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
					$addQuery->bindParam(':tId',$videos[$i]['id'],PDO::PARAM_STR);
					$addQuery->bindParam(':lang',$videos[$i]['iso_639_1'],PDO::PARAM_STR);
					$addQuery->bindParam(':sKey',$videos[$i]['key'],PDO::PARAM_STR);
					$addQuery->bindParam(':name',$videos[$i]['name'],PDO::PARAM_STR);
					$addQuery->bindParam(':sSite',$videos[$i]['site'],PDO::PARAM_STR);
					$addQuery->bindParam(':size',$videos[$i]['size'],PDO::PARAM_INT);
					$addQuery->bindParam(':type',$videos[$i]['type'],PDO::PARAM_STR);
					$addQuery->execute();

				}
				return 1;
			}
			else
			{
				//no videos
				return 1;
			}
		}
		
	}
	// end
	//start language
	public function addlanguage()
	{
		$addQuery=$this->prepare('insert into video_languages (video_id,video_type,name) values (:vId,:vType,:lan)');
		$languages=$this->languages;
		for($i=0;$i<count($languages);$i++)
		{
			$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
			$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
			if($this->type=="tv")
			{
				$addQuery->bindParam(':lan',$languages[$i],PDO::PARAM_STR);
			}
			else
			{
				$addQuery->bindParam(':lan',$languages[$i]['iso_639_1'],PDO::PARAM_STR);
			}
			
			$addQuery->execute();
		}
		return 1;
		
	}
	//End

	//start add images
	public function addimages()
	{
		$posterVar='poster';
		$backVar='backdrop';
		$addQuery=$this->prepare('insert into video_images (image_type,filepath,video_id,video_type,language,aspect_ratio,height,width) values (:imgType,:fPath,:vId,:vType,:language,:aRatio,:height,:width)');
		$images=$this->images;
		if(isset($images['posters']) && count($images['posters'])>0)
		{
			$posters=$images['posters'];
			for($i=0;$i<count($posters);$i++)
			{

				$addQuery->bindParam(':imgType',$posterVar,PDO::PARAM_STR);
				$addQuery->bindParam(':fPath',$posters[$i]['file_path'],PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':language',$posters[$i]['iso_639_1'],PDO::PARAM_STR);
				$addQuery->bindParam(':aRatio',$posters[$i]['aspect_ratio'],PDO::PARAM_STR);
				$addQuery->bindParam(':height',$posters[$i]['height'],PDO::PARAM_INT);
				$addQuery->bindParam(':width',$posters[$i]['width'],PDO::PARAM_INT);
				$addQuery->execute();

			}
		}
		else
		{
			$posters=NULL;
		}
		if(isset($images['backdrops']) && count($images['backdrops'])>0)
		{
			$backdrops=$images['backdrops'];
			for($i=0;$i<count($backdrops);$i++)
			{

				$addQuery->bindParam(':imgType',$backVar,PDO::PARAM_STR);
				$addQuery->bindParam(':fPath',$backdrops[$i]['file_path'],PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':language',$backdrops[$i]['iso_639_1'],PDO::PARAM_STR);
				$addQuery->bindParam(':aRatio',$backdrops[$i]['aspect_ratio'],PDO::PARAM_STR);
				$addQuery->bindParam(':height',$backdrops[$i]['height'],PDO::PARAM_INT);
				$addQuery->bindParam(':width',$backdrops[$i]['width'],PDO::PARAM_INT);
				$addQuery->execute();
			}
		}
		else
		{
			$backdrops=NULL;
		}
		
	
		
	
		return 1;


	}
	// END

	//start add production
	public function addproduction()
	{
		//no need to check 
		$addQuery=$this->prepare('insert into video_productions (video_type,video_id,company_name,company_tmdb_id) values (:vType,:vID,:company,:cid)');
		$production=$this->production;
		if($production=="")
		{
			return 1;
		}
		else
		{
			for($i=0;$i<count($production);$i++)
			{	
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':vID',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':company',$production[$i]['name']);
				$addQuery->bindParam(':cid',$production[$i]['id']);
				$addQuery->execute();	
			}
			return 1;
		}
	
	}//END

	//start Reviews
	public function addreviews()
	{
		$addQuery=$this->prepare('insert into video_reviews (video_type,video_id,author,content,tmdb_id) values (:vType,:vId,:author,:Content,:tId)');
		$reviews=$this->reviews;
		if($reviews['total_results']==0)
		{
			//no reviews
			return 1;
		}
		else
		{
			for($i=0;$i<count($reviews['results']);$i++)
			{
				$addQuery->bindParam(':vType',$this->type,PDO::PARAM_STR);
				$addQuery->bindParam(':vId',$this->video_id,PDO::PARAM_INT);
				$addQuery->bindParam(':author',$reviews['results'][$i]['author']);
				$addQuery->bindParam(':Content',$reviews['results'][$i]['content']);
				$addQuery->bindParam(':tId',$reviews['results'][$i]['id']);
				$addQuery->execute();
			}
			return 1;
		}
		// 
		
	}
	//End

	public function sortArrayByKey(&$array,$key,$string = false,$asc = true)
	{

	    if($string){

	        usort($array,function ($a, $b) use(&$key,&$asc)
	        {

	            if($asc)    return strcmp($a[$key]*$a['width'], $b[$key]*$b['width']);
	            else       return strcmp(strtolower($b[$key]*$b['width']), strtolower($a[$key]*$a['width']));
	        });
	    }else{
	        usort($array,function ($a, $b) use(&$key,&$asc)
	        {
	            if($a[$key] == $b[$key]){return 0;}
	            if($asc) return ($a[$key]*$a['width'] < $b[$key]*$b['width']) ? -1 : 1;
	            else     return ($a[$key]*$a['width'] > $b[$key]*$b['width']) ? -1 : 1;

	        });
	    }
	}

	public function getmType($height)
	{
		if($height>0 && $height <=360)
		{
			return 'p360';
		}
		elseif ($height>360 && $height <=480)
		{
			return 'p480';
		}
		elseif ($height>480 && $height <=720)
		{
			return 'p720';
		}
		elseif ($height>720 && $height <=1080)
		{
			return 'p1080';
		}
		elseif ($height>1080)
		{
			return 'p1440';
		}
	}
	public function tvcrewSync()
	{
		ini_set('max_execution_time', 500000);
		//get all movies details
		$gQuery=$this->prepare('select tmdb_id,tv_id from tv_shows');
		$sQuer=$this->prepare('insert into video_crew (video_id,video_type,job,tmdb_crew_id,tmdb_person_id,name,department,profile_path) values (:vId,:vType,:job,:tCrew,:tProf,:name,:dept,:pPath)');
		$gQuery->execute();
		if($gQuery)
		{
			$allTmdbid=$gQuery->fetchAll();
			for($i=0;$i<count($allTmdbid);$i++)
			{
				$tmdb_id=$allTmdbid[$i]['tmdb_id'];
				$api=file_get_contents('https://api.themoviedb.org/3/tv/'.$tmdb_id.'/credits?api_key=97e8434a62c5f9732e325e238b3aaf51');
				$allData = json_decode($api,true);
				if(count($allData)>0)
				{
					$crew_info=$allData['crew'];
					for($j=0;$j<count($crew_info);$j++)
					{
						if($crew_info[$j]['job']=='Producer' || $crew_info[$j]['job']=='Director')
						{
							$job=$crew_info[$j]['job'];
							$name=$crew_info[$j]['name'];
							$video_id=$allTmdbid[$i]['tv_id'];
							$video_type="tv";
							$tmdb_crew_id=$crew_info[$j]['credit_id'];
							$tmdb_person_id=$crew_info[$j]['id'];
							$department=$crew_info[$j]['department'];
							$poster_path=$crew_info[$j]['profile_path'];
							//data binding
							$sQuer->bindParam(':vId',$video_id,PDO::PARAM_INT);
							$sQuer->bindParam(':vType',$video_type,PDO::PARAM_INT);
							$sQuer->bindParam(':job',$job,PDO::PARAM_INT);
							$sQuer->bindParam(':tCrew',$tmdb_crew_id,PDO::PARAM_INT);
							$sQuer->bindParam(':tProf',$tmdb_person_id,PDO::PARAM_INT);
							$sQuer->bindParam(':name',$name,PDO::PARAM_INT);
							$sQuer->bindParam(':dept',$department,PDO::PARAM_INT);
							$sQuer->bindParam(':pPath',$poster_path,PDO::PARAM_INT);
							$sQuer->execute();
							if($sQuer->rowCount()>0)
							{

							}
							else
							{
								print_r($sQuer->errorInfo());
								print_r('error on '.$allTmdbid[$i]['tmdb_id']);
								exit();
							}

						}	
					}
				}

			}
			$this->success(200,__FUNCTION__,'Synced');
			exit();
			
		}
	}
	


	public function syncCrew()
	{	ini_set('max_execution_time', 500000);
		//get all movies details
		$gQuery=$this->prepare('select tmdb_id,mov_id from movies');
		$sQuer=$this->prepare('insert into video_crew (video_id,video_type,job,tmdb_crew_id,tmdb_person_id,name,department,profile_path) values (:vId,:vType,:job,:tCrew,:tProf,:name,:dept,:pPath)');
		$gQuery->execute();
		if($gQuery)
		{
			$allTmdbid=$gQuery->fetchAll();
			for($i=0;$i<count($allTmdbid);$i++)
			{
				
				$tmdb_id=$allTmdbid[$i]['tmdb_id'];
				
				if($tmdb_id==444145)
				{

				}
				else
				{	
					$api=file_get_contents('https://api.themoviedb.org/3/movie/'.$tmdb_id.'/credits?api_key=97e8434a62c5f9732e325e238b3aaf51');
					$allData = json_decode($api,true);
					if(count($allData)>0)
					{
						$crew_info=$allData['crew'];
						for($j=0;$j<count($crew_info);$j++)
						{
							if($crew_info[$j]['job']=='Producer' || $crew_info[$j]['job']=='Director')
							{
								$job=$crew_info[$j]['job'];
								$name=$crew_info[$j]['name'];
								$video_id=$allTmdbid[$i]['mov_id'];
								$video_type="movie";
								$tmdb_crew_id=$crew_info[$j]['credit_id'];
								$tmdb_person_id=$crew_info[$j]['id'];
								$department=$crew_info[$j]['department'];
								$poster_path=$crew_info[$j]['profile_path'];

								//data binding
								$sQuer->bindParam(':vId',$video_id,PDO::PARAM_INT);
								$sQuer->bindParam(':vType',$video_type,PDO::PARAM_INT);
								$sQuer->bindParam(':job',$job,PDO::PARAM_INT);
								$sQuer->bindParam(':tCrew',$tmdb_crew_id,PDO::PARAM_INT);
								$sQuer->bindParam(':tProf',$tmdb_person_id,PDO::PARAM_INT);
								$sQuer->bindParam(':name',$name,PDO::PARAM_INT);
								$sQuer->bindParam(':dept',$department,PDO::PARAM_INT);
								$sQuer->bindParam(':pPath',$poster_path,PDO::PARAM_INT);
								$sQuer->execute();

								if($sQuer->rowCount()>0)
								{

								}
								else
								{
									print_r($sQuer->errorInfo());
									print_r('error on '.$allTmdbid[$i]['tmdb_id']);
									exit();
								}

							}

							
						}
					}
				}

			}
			$this->success(200,__FUNCTION__,'Synced');
			exit();
			
		}
	}


	public function syncshowid()
	{
			ini_set('max_execution_time', 500000);
			$location='../../sc_out_seasons.csv';
			$row = 1;
			if (($handle = fopen($location, "r")) !== FALSE) 
			{
			    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
			    	$data = array_map("utf8_encode", $data);
			        $num = count($data);
			        $line_of_text[]=$data;
			    }
			    fclose($handle);
			}
			// print_r($line_of_text);
			for($k=1;$k<count($line_of_text);$k++)
			{
		
					
					$iddetaikls=$line_of_text[$k][4];
					$idInfo=explode("?", $iddetaikls);
					$showid=$idInfo[1];
					$showName=$line_of_text[$k][0];
					if (strpos($showName, '(') !== false) 
					{
					 $showName=explode(' (', $showName)[0];
					}
					else
					{
  						
  					}
					//find the season 
					$fQuery=$this->prepare('select tv_id from tv_shows where name like concat("%",:sName,"%")');
					$fQuery->bindParam(':sName',$showName,PDO::PARAM_STR);
					$fQuery->execute();
					if($fQuery->rowCount()==1)
					{
						$tvData=$fQuery->fetchAll();
						$tv_id=$tvData[0]['tv_id'];
						// update showid in tv_shows
						$upQuery=$this->prepare('update tv_shows set show_id=:sid where tv_id=:tid');
						$upQuery->bindParam(':sid',$showid,PDO::PARAM_INT,10);
						$upQuery->bindParam(':tid',$tv_id,PDO::PARAM_INT,10);
						$upQuery->execute();
						if($upQuery->rowCount()>0)
						{

						}
						else
						{
							print_r($upQuery->errorInfo());
						}
					}
					else if($fQuery->rowCount()>1)
					{
						echo $showName.' Greater';
					}
					else
					{
						echo $showName.' Not Found';
					}
						
					

				


			}



				// $getQuery=$this->prepare('select * from tv_shows');
				// $getQuery->execute();
				// if($getQuery)
				// {
				// 	$allData=$getQuery->fetchAll();
				// 	$showid=0;
				// 	$notfound=0;
				// 	for($i=0;$i<count($allData);$i++)
				// 	{
				// 		$tv_title=$allData[$i]['name'];
				// 		$tv_id=$allData[$i]['tv_id'];
				// 		for($k=0;$k<count($line_of_text);$k++)
				// 		{
					
				// 			if(strtoupper($line_of_text[$k][1])==strtoupper($tv_title))
				// 			{
				// 				$iddetaikls=$line_of_text[$k][5];
				// 				$idInfo=explode("?", $iddetaikls);
				// 				$showid=$idInfo[1];
				// 				// 
				// 				// update showid in tv_shows
				// 				$upQuery=$this->prepare('update tv_shows set show_id=:sid where tv_id=:tid');
				// 				$upQuery->bindParam(':sid',$showid,PDO::PARAM_INT,10);
				// 				$upQuery->bindParam(':tid',$tv_id,PDO::PARAM_INT,10);
				// 				$upQuery->execute();
				// 				if($upQuery->rowCount()>0)
				// 				{

				// 				}
				// 				else
				// 				{
				// 					print_r($upQuery->errorInfo());
				// 				}
				// 			}
				// 			else
				// 			{
								
				// 			}	


				// 		}
						

				// 	}
				// }
	}

	public function synctv()
	{
	
		$notfound=array();
		$error=array();
		$missing=array();
		ini_set('max_execution_time', 5000000);

			// $getQuery=$this->prepare('select * from tvqueue where status=1');
			$getQuery=$this->prepare('select * from tvqueue where status=1 and SHOW_ID=228');
			$getQuery->execute();
			
			if($getQuery->rowCount()>0)
			{	
				
				$findQuer=$this->prepare('select tse.* from tv_season_episode tse inner join tv_season ts on ts.season_id=tse.season_id inner join tv_shows tsh on tsh.tv_id = ts.tv_id where tsh.show_id=:sId and ts.season_number=:sNumber and tse.episode_number=:eNumber');
				$allData=$getQuery->fetchAll();
				for($j=0;$j<count($allData);$j++)
				{
					$showId=$allData[$j]['SHOW_ID'];	
					$epDetail=explode("x", $allData[$j]['EPISODE']);
					$sNumber=$epDetail[0];
					$epNumber=ltrim($epDetail[1], '0');
					// print_r($epDetail);
					// echo $showId;
					// find $episode
					$findQuer->bindParam(':sId',$showId,PDO::PARAM_INT);
					$findQuer->bindParam(':sNumber',$sNumber,PDO::PARAM_INT);
					$findQuer->bindParam(':eNumber',$epNumber,PDO::PARAM_INT);
					$findQuer->execute();
					if($findQuer->rowCount()>0)
					{
						
						//episode found
						$eDetails=$findQuer->fetchAll();
						
						$width=$allData[$j]['width'];
						$height=$allData[$j]['height'];
						$type=$this->getmType($height);
						$mv_id=$allData[$j]['VIDEO_ID'];

						$bPath="http://media.beanbag.tv/shows/ep";
						$video_type='tv';
						$mtype=$mv_id.".mp4";
						$duration=$allData[$j]['duration'];
						$IMDB=$eDetails[0]['tmdb_id'];

						//chk if availabkle in videos 
						$chkQuery=$this->prepare('select * from videos where imdb_id=:iid');
						$chkQuery->bindParam(':iid',$IMDB,PDO::PARAM_INT);
						$chkQuery->execute();

						if($chkQuery->rowCount()>0)
						{
							
							//available compare resolution 
							$video=$chkQuery->fetchAll();
	                        if (isset($video[0]['p360']))
	                        {
	                           $savedHeight=360;
	                           $table="p360";
	                        }
	                         else if (isset($video[0]['p480'])) {
	                             $savedHeight=480;
	                            $table="p480";
	                        }
	                         else if (isset($video[0]['p720'])) {
	                             $savedHeight=720;
	                             $table="p720";
	                        }
	                         else if (isset($video[0]['p1080'])) {
	                            $savedHeight=1080;
	                            $table="1080";
	                        }
	                         else if (isset($video[0]['p1440'])) {
	                            $savedHeight=1440;
	                            $table="p1440";
	                        }
	                        if($savedHeight > $height)
	                        {
	                            //no need to change already perfectly saved
	                            // update is active 
	                            $upQuery=$this->prepare('update tv_season_episode set is_active=1 where ep_id=:id');
								$upQuery->bindParam(':id',$eDetails[0]['ep_id'],PDO::PARAM_STR);
								$upQuery->execute();
								//update tv show
								$upQuery2=$this->prepare('update tv_shows set is_active=1 where show_id=:sid');
								$upQuery2->bindParam(':sid',$showId,PDO::PARAM_STR);
								$upQuery2->execute(); 
	                        }
	                        else
	                        {
	                            //update new height
	                            $upQuery=$this->prepare('update videos set '.$table.' = Null, scid = :sid , '.$type.'= :mType, duration=:duration where imdb_id=:iid');
	                            $upQuery->bindParam(':sid',$mv_id,PDO::PARAM_INT);
	                            $upQuery->bindParam(':mType',$mtype,PDO::PARAM_INT);
	                            $upQuery->bindParam(':duration',$duration,PDO::PARAM_INT);
	                            $upQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
	                            $upQuery->execute();
	                            if($upQuery->rowCount()>0)
	                            {
	                                // echo 'completly linked';
	                                $upQuery=$this->prepare('update tv_season_episode set is_active=1 where ep_id=:id');
									$upQuery->bindParam(':id',$eDetails[0]['ep_id'],PDO::PARAM_STR);
									$upQuery->execute();
									//update tv show
									$upQuery2=$this->prepare('update tv_shows set is_active=1 where show_id=:sid');
									$upQuery2->bindParam(':sid',$showId,PDO::PARAM_STR);
									$upQuery2->execute(); 
	                            }
	                            else
	                            {
	                                // print_r($upQuery->errorInfo());
	                            }
	                        }
						}
						else
						{
							// not available update
							$sQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
							$sQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
							$sQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
							$sQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
							$sQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
							$sQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
							$sQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
							$sQuery->execute();

							if($sQuery->rowCount()>0)
							{
								//update movie active status
								$upQuery=$this->prepare('update tv_season_episode set is_active=1 where ep_id=:id');
								$upQuery->bindParam(':id',$eDetails[0]['ep_id'],PDO::PARAM_STR);
								$upQuery->execute();
								//update tv show
								$upQuery2=$this->prepare('update tv_shows set is_active=1 where show_id=:sid');
								$upQuery2->bindParam(':sid',$showId,PDO::PARAM_STR);
								$upQuery2->execute();
							}
							else
							{
								print_r($sQuery->errorInfo());
								array_push($error, $showId."X".$sNumber."X".$epNumber);
							}
						}


						
						
					}
					else
					{
						echo 'i m on tmdb';
						exit();
						//get from tmdb
						$width=$allData[$j]['width'];
						$height=$allData[$j]['height'];
						$type=$this->getmType($height);
						$duration=$allData[$j]['duration'];
						$mv_id=$allData[$j]['VIDEO_ID'];
						$tmdbid=0;
						$tv_id=0;
						//get From tmdb save and link
						//get tmdbid for tv show
						$tQuery=$this->prepare('select tv_id,tmdb_id from tv_shows where show_id=:sid');
						$tQuery->bindParam(':sid',$showId,PDO::PARAM_INT);
						$tQuery->execute();
						if($tQuery->rowCount()>0)
						{
							$tmdbiddata=$tQuery->fetchAll();
							$tmdbid=$tmdbiddata[0]['tmdb_id'];
							$tv_id=$tmdbiddata[0]['tv_id'];
													// chk if shows requierd season is available
							$chkQuery=$this->prepare('select tse.season_id,tse.tmdb_id from tv_season tse inner join tv_shows ts on ts.tv_id = tse.tv_id where ts.show_id=:sid and tse.season_number=:sNumber');
							$chkQuery->bindParam(':sid',$showId,PDO::PARAM_INT);
							$chkQuery->bindParam(':sNumber',$sNumber,PDO::PARAM_INT);
							$chkQuery->execute();
							if($chkQuery->rowCount() > 0)
							{
								$seasonData=$chkQuery->fetchAll();
								$seasonid=$seasonData[0]['season_id'];
								$season_tmdb_id=$seasonData[0]['tmdb_id'];
								//season available get episode details from tmdb
								if($this->get_http_response_code('https://api.themoviedb.org/3/tv/'.$tmdbid.'/season/'.$sNumber.'/episode/'.$epNumber.'?api_key=97e8434a62c5f9732e325e238b3aaf51') != "200")
								{
								    // echo "Episode not present in TMDB";
								}
								else
								{
									$api=file_get_contents('https://api.themoviedb.org/3/tv/'.$tmdbid.'/season/'.$sNumber.'/episode/'.$epNumber.'?api_key=97e8434a62c5f9732e325e238b3aaf51');
		                			$epData = json_decode($api,true);
		                			$epQuery=$this->prepare('insert into tv_season_episode (season_id,season_tmdb_id,air_date,overview,episode_name,episode_number,tmdb_id,poster_path,rank) values (:sid,:stid,:aDate,:overview,:eName,:eNmbr,:tId,:pPath,:rank)');
					                $epQuery->bindParam(':sid',$seasonid,PDO::PARAM_INT);
					                $epQuery->bindParam(':stid',$season_tmdb_id,PDO::PARAM_INT);
					                $epQuery->bindParam(':aDate',$epData['air_date'],PDO::PARAM_STR);
					                $epQuery->bindParam(':overview',$epData['overview'],PDO::PARAM_STR);
					                $epQuery->bindParam(':eName',$epData['name'],PDO::PARAM_STR);
					                $epQuery->bindParam(':eNmbr',$epData['episode_number'],PDO::PARAM_INT);
					                $epQuery->bindParam(':tId',$epData['id'],PDO::PARAM_INT);
					                $epQuery->bindParam(':pPath',$epData['still_path'],PDO::PARAM_STR);
					                $epQuery->bindParam(':rank',$epData['vote_average'],PDO::PARAM_STR);
					                $epQuery->execute();
					                if($epQuery->rowCount()>0)
					                {
										$bPath="http://media.beanbag.tv/shows/ep";
										$video_type='tv';
										$mtype=$mv_id.".mp4";
										$IMDB=$epData['id'];
					                    // link video file
					                        $vQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
					                        $vQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
					                        $vQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
					                        $vQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
					                        $vQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
					                        $vQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
					                        $vQuery->bindParam(':iid',$IMDB,PDO::PARAM_STR);
					                    
					                        $vQuery->execute();
					                        if($vQuery->rowCount()>0)
					                        {
					                             // echo "Episode for show ".$showId." linked";
					                        }
					                        else
					                        {
					                             // echo $vQuery->errorInfo();
					                        }
					                }
					                else
					                {
					                    // echo 'BIG ISSUE';
					                }
				            	}

							}
							else
							{
								//get season info from tmdb
								if($this->get_http_response_code('https://api.themoviedb.org/3/tv/'.$tmdbid.'/season/'.$sNumber.'?api_key=97e8434a62c5f9732e325e238b3aaf51&append_to_response=images,videos') != "200")
								{
								    // echo "Episode not present in TMDB";
								}
								else
								{
									$api=file_get_contents('https://api.themoviedb.org/3/tv/'.$tmdbid.'/season/'.$sNumber.'?api_key=97e8434a62c5f9732e325e238b3aaf51&append_to_response=images,videos');
	            					$allData = json_decode($api,true);

	            					$poster=$allData['poster_path'];
						            $season_tmdb_id=$allData['id'];
						            $episodes_count=count($allData['episodes']);
						            $air_date=$allData['air_date'];
						            $sQuery=$this->prepare('insert into tv_season (season_number,poster_path,tmdb_id,episodes,tv_id,air_date) values (:sNumber,:pPath,:tmdb_id,:episodes,:tv_id,:airDate)');
	            					$sQuery->bindParam(':sNumber',$sNumber,PDO::PARAM_STR);
						            $sQuery->bindParam(':pPath',$poster,PDO::PARAM_STR);
						            $sQuery->bindParam(':episodes',$episodes_count,PDO::PARAM_STR);
						            $sQuery->bindParam(':tmdb_id',$season_tmdb_id,PDO::PARAM_STR);
						            $sQuery->bindParam(':tv_id',$tv_id,PDO::PARAM_STR);
						            $sQuery->bindParam(':airDate',$air_date,PDO::PARAM_STR);
						            $sQuery->execute();
						            if($sQuery->rowCount()>0)
						            {
						            	// print_r($allData);
						                //saved save episodes next
						                $this->video_id=$this->lastInsertId();
						                $this->episodes=$allData['episodes'];
						                $this->images=$allData['images'];
						                $this->videos=$allData['videos'];

						                print_r($this->images);
						                print_r($this->videos);
						                // save images
								                $addimages=$this->addimages();
								                if($addimages==1)
								                {
								                	//add videos
								                	$addvideos=$this->addvideos();
								                	if($addvideos==1)
								                	{
								                		echo "i am adding images";
								                		//add images
			    										$chkQuery=$this->prepare('select ep_id from tv_season_episode where season_id=:sId and tmdb_id=:tid');
														for($i=0;$i<count($this->episodes);$i++)
														{
															$episode=$this->episodes[$i];
															$ep_tmdb_id=$episode['id'];
															$chkQuery->bindParam(':sId',$lastid,PDO::PARAM_INT);
															$chkQuery->bindParam(':tid',$ep_tmdb_id,PDO::PARAM_INT);
															$chkQuery->execute();
															if($chkQuery->rowCount()>0)
															{
																//episode already saved no need to do anything
															}
															else
															{	
																//add new 
																$sQuery=$this->prepare('insert into tv_season_episode (season_id,season_tmdb_id,air_date,overview,episode_name,episode_number,tmdb_id,poster_path,rank) values (:sid,:stid,:aDate,:overview,:eName,:eNmbr,:tId,:pPath,:rank)');
																$sQuery->bindParam(':sid',$this->video_id,PDO::PARAM_INT);
																$sQuery->bindParam(':stid',$season_tmdb_id,PDO::PARAM_INT);
																$sQuery->bindParam(':aDate',$episode['air_date'],PDO::PARAM_STR);
																$sQuery->bindParam(':overview',$episode['overview'],PDO::PARAM_STR);
																$sQuery->bindParam(':eName',$episode['name'],PDO::PARAM_STR);
																$sQuery->bindParam(':eNmbr',$episode['episode_number'],PDO::PARAM_INT);
																$sQuery->bindParam(':tId',$episode['id'],PDO::PARAM_INT);
																$sQuery->bindParam(':pPath',$episode['still_path'],PDO::PARAM_STR);
																$sQuery->bindParam(':rank',$episode['vote_average'],PDO::PARAM_STR);
																$sQuery->execute();
															}
														}
								                	}
								                }
						                	
															echo $tmdbid." ".$sNumber." Please";
									                      	$sQuery=$this->prepare('select * from tv_season where tmdb_id=:tid and season_number=:sNumber');
											                $sQuery->bindparam(':tid',$season_tmdb_id,PDO::PARAM_INT);
											                $sQuery->bindparam(':sNumber',$sNumber,PDO::PARAM_INT);
											                $sQuery->execute();
											                if($sQuery->rowCount()>0)
											                {
											                    $seasonData=$sQuery->fetchAll();
											                    $seasonId=$seasonData[0]['season_id'];
											                    //find desired episode in episodes
											                    $eQuery=$this->prepare('select * from tv_season_episode where season_id=:sid and episode_number=:eNumber');
											                    $eQuery->bindparam(':sid',$seasonId,PDO::PARAM_INT);
											                    $eQuery->bindparam(':eNumber',$epNumber,PDO::PARAM_INT);
											                    $eQuery->execute();
											                    if($eQuery->rowCount()>0)
											                    {
											                        $epData=$eQuery->fetchAll();
											                        $ep_tmdb_id=$epData[0]['tmdb_id'];
											                        $epid=$epData[0]['ep_id'];
											                        $vQuery=$this->prepare('insert into videos (base_path,type,scid,'.$type.',duration,imdb_id) values (:bPath,:type,:sId,:mType,:dur,:iid)');
											                        $vQuery->bindParam(':bPath',$bPath,PDO::PARAM_STR);
											                        $vQuery->bindParam(':type',$video_type,PDO::PARAM_STR);
											                        $vQuery->bindParam(':sId',$mv_id,PDO::PARAM_STR);
											                        $vQuery->bindParam(':mType',$mtype,PDO::PARAM_STR);
											                        $vQuery->bindParam(':dur',$duration,PDO::PARAM_STR);
											                        $vQuery->bindParam(':iid',$ep_tmdb_id,PDO::PARAM_STR);
											                        $vQuery->execute();
											                        if($vQuery->rowCount()>0)
											                        {
											                            //update movie active status
											                            $upQuery=$this->prepare('update tv_season_episode set is_active=1 where ep_id=:id');
											                            $upQuery->bindParam(':id',$epid,PDO::PARAM_STR);
											                            $upQuery->execute();
											                            //update tv show
											                            $upQuery2=$this->prepare('update tv_shows set is_active=1 where show_id=:sid');
											                            $upQuery2->bindParam(':sid',$show_id,PDO::PARAM_STR);
											                            $upQuery2->execute();
											                            echo $episode." for show ".$show_id." linked";
											                        }
											                    }
											                    else
											                    {
											                        echo 'Episode missing on TMDB';
											                    }
											                }
											                else
											                {
											                	echo 'i am fucked';
											                }
						            }
						            else
						            {
						            	echo 'not inserted';
						            }
								}
							}
							
							
						}
						else
						{
							// echo 'SHow with id'.$showId." NOt Found";
						}
				



					}

				}//end of loop
			}	//tvQueue if
			else
			{
				// array_push($missing, $showId."X".$sNumber."X".$epNumber);
			}
			
			print_r($notfound);
			print_r($missing);
			print_r($error);
	}

public function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

	public function addepisodes($lastid,$stmdbId)
	{
		$chkQuery=$this->prepare('select ep_id from tv_season_episode where season_id=:sId and tmdb_id=:tid');
		for($i=0;$i<count($this->episodes);$i++)
		{
			$episode=$this->episodes[$i];
			$ep_tmdb_id=$episode['id'];
			$chkQuery->bindParam(':sId',$lastid,PDO::PARAM_INT);
			$chkQuery->bindParam(':tid',$ep_tmdb_id,PDO::PARAM_INT);
			$chkQuery->execute();
			if($chkQuery->rowCount()>0)
			{
				//episode already saved no need to do anything
			}
			else
			{	
				//add new 
				$sQuery=$this->prepare('insert into tv_season_episode (season_id,season_tmdb_id,air_date,overview,episode_name,episode_number,tmdb_id,poster_path,rank) values (:sid,:stid,:aDate,:overview,:eName,:eNmbr,:tId,:pPath,:rank)');
				$sQuery->bindParam(':sid',$lastid,PDO::PARAM_INT);
				$sQuery->bindParam(':stid',$stmdbId,PDO::PARAM_INT);
				$sQuery->bindParam(':aDate',$episode['air_date'],PDO::PARAM_STR);
				$sQuery->bindParam(':overview',$episode['overview'],PDO::PARAM_STR);
				$sQuery->bindParam(':eName',$episode['name'],PDO::PARAM_STR);
				$sQuery->bindParam(':eNmbr',$episode['episode_number'],PDO::PARAM_INT);
				$sQuery->bindParam(':tId',$episode['id'],PDO::PARAM_INT);
				$sQuery->bindParam(':pPath',$episode['still_path'],PDO::PARAM_STR);
				$sQuery->bindParam(':rank',$episode['vote_average'],PDO::PARAM_STR);
				$sQuery->execute();
			}
		}
		return 1;
	}
	public function shows_diff_from_sc()
	{
				$allData=array();
				ini_set('max_execution_time', 500000);
				$row = 1;
				if (($handle = fopen("../../chiling_shows.csv", "r")) !== FALSE) {
			  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			  {
			    $num = count($data);
			    $row++;
				$show_id=$data[0];
				$show_name=$data[1];
			
				//find show id in db

				$fQuery=$this->prepare('select tv_id from tv_shows where show_id=:sid and is_active=1');
				$fQuery->bindParam(':sid',$show_id,PDO::PARAM_INT);
				$fQuery->execute();
				if($fQuery->rowCount()>0)
				{
					//it is found
				}
				else
				{
					// it is not found
					// add this to csv
					$dataArray=array('Tv Show Title'=>$show_name);
					array_push($allData, $dataArray);
				}
			  }
			  fclose($handle);
			  $this->outputCsv($allData);
			}
	}
	public function mvDuplicates()
	{
		$allData=array();
		ini_set('max_execution_time', 500000);
		$row = 1;
			if (($handle = fopen("../../tv_shows.csv", "r")) !== FALSE) {
			  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			  {
			    $num = count($data);
			    $row++;
				$show_id=$data[1];
				if($show_id=="Show Id")
				{

				}
				elseif ($show_id==0) {
					
				}
				else
				{
					// echo $show_id."<br>";
					$gQuery=$this->prepare('select video_id from tvqueue where show_id=:sid');
					$gQuery->bindParam(':sid',$show_id,PDO::PARAM_INT);
					$gQuery->execute();		
					if($gQuery->rowCount()>0)
					{
						$video=$gQuery->fetchAll();
						$base_path='/home/ubuntu/Documents/media/shows/ep';
						for($i=0;$i<count($video);$i++)
						{
							$videoPath=$base_path.$video[$i]['video_id'].'.mp4';
							$dataArray=array('tv_ep_link'=>$videoPath);
							array_push($allData, $dataArray);
						}
						
				    		
					}    
				}
			    // $show_id=$data[];
			    // $gQuery=$this->prepare('select video_id from tvqueue where show_id=:sid');
			    // $gQuery->bindParam(':sid'->$show)		    
		   //  		$base_path='/home/ubuntu/Documents/media/movies/';
		   //  		$videoPath=$base_path.$video_id.'.mp4';
		   //  		// echo $videoPath;
						
					//
					// array_push($allData, $dataArray);
			    	
			
			    
			  }
			  fclose($handle);
			  $this->outputCsv($allData);
			}
		// $getQuery=$this->prepare('select  MAX(MOVIE_TITLE),COUNT(*) as occurance, IMDB_ID FROM `queue` where status=1 GROUP by IMDB_ID HAVING occurance > 1 order by occurance desc');
		// $getQuery->execute();
		// if($getQuery->rowCount()>0)
		// {
		// 	$allCSVdata=array();
		// 	$dupData=$getQuery->fetchAll();
		// 	for($i=0;$i<count($dupData);$i++)
		// 	{
		// 		$imdb_id=$dupData[$i]['IMDB_ID'];
		// 		//find records for this id
		// 		$gQuery=$this->prepare('select * from queue where IMDB_ID=:iId and status=1');
		// 		$gQuery->bindParam(':iId',$imdb_id,PDO::PARAM_INT);
		// 		$gQuery->execute();
		// 		if($gQuery->rowCount()>0)
		// 		{

		// 			$allData=$gQuery->fetchAll();
		// 			$counter=count($allData);
		// 			//now check which is bigger and which needs to be deleted.
		// 			$this->sortArrayByKey($allData,"height",false,false);
		// 			// 1st element is to be save rest needs to go 
		// 			for($k=0;$k<count($allData);$k++)
		// 			{
		// 				// calculate filesize
		// 				$base_path='http://media.beanbag.tv/movies/';
		// 				$videoPath=$base_path.$allData[$k]['VIDEO_ID'].'.mp4';
		// 				$byte_size=$this->curl_get_file_size($videoPath);
		// 				$actualsize=$this->formatSizeUnits($byte_size);
		// 				if($k==0)
		// 				{

		// 					$dataArray=array('movie_title'=>$allData[$k]['MOVIE_TITLE'],'height'=>$allData[$k]['height'],'width'=>$allData[$k]['width'],'imdb_id'=>$allData[$k]['IMDB_ID'],'video_id'=>$allData[$k]['VIDEO_ID'],'filesize'=>$actualsize,'status'=>'Save this');
		// 				}
		// 				else
		// 				{
							
		// 					$dataArray=array('movie_title'=>$allData[$k]['MOVIE_TITLE'],'height'=>$allData[$k]['height'],'width'=>$allData[$k]['width'],'imdb_id'=>$allData[$k]['IMDB_ID'],'video_id'=>$allData[$k]['VIDEO_ID'],'filesize'=>$actualsize,'status'=>'Delete this');
		// 					// if(!unlink($videoPath))
		// 					// {
		// 					// 	echo 'error on '.$videoPath;
		// 					// 	exit();
		// 					// }
		// 					// else
		// 					// {
		// 					// 	//update deleted
		// 					// 	$upQuery=$this->prepare('update queue set is_deleted=1 where video_id=:vid');
		// 					// 	$upQuery->bindParam(':vid',$allData[$k]['VIDEO_ID'],PDO::PARAM_INT);
		// 					// 	$upQuery->execute();
		// 					// }
	
	}
	
	function outputCsv($data) {
        # Generate CSV data from array
        $fh = fopen('D://shows_to_be_deleted.csv', 'w'); # don't create a file, attempt
                                         # to use memory instead

        # write out the headers
        fputcsv($fh, array_keys(current($data)));

        # write out the data
        foreach ( $data as $row ) {
                fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
}
 function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' kB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}
function curl_get_file_size( $url ) {
  // Assume failure.
  $result = -1;

  $curl = curl_init( $url );

  // Issue a HEAD request and follow any redirects.
  curl_setopt( $curl, CURLOPT_NOBODY, true );
  curl_setopt( $curl, CURLOPT_HEADER, true );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );


  $data = curl_exec( $curl );
  curl_close( $curl );

  if( $data ) {
    $content_length = "unknown";
    $status = "unknown";

    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
      $status = (int)$matches[1];
    }

    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
      $content_length = (int)$matches[1];
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if( $status == 200 || ($status > 300 && $status <= 308) ) {
      $result = $content_length;
    }
  }

  return $result;
}

public function gettvmissing()
{

	$allData=array();
	//get all tv shows with their seasons
	$getQuery=$this->prepare('select * from tv_shows ts inner join tv_season tse on ts.tv_id=tse.tv_id where ts.is_active=1 and tse.season_number <> 0 and tse.air_date < now()');
	$getQuery->execute();
	$allseasonsData=$getQuery->fetchAll();

	//loop on each season and check if all have episodes linked
	for($i=0;$i<count($allseasonsData);$i++)
	{
		$linked=0;
		$unlinked=0;
		print_r($allseasonsData[$i]['episodes']."<br>");
		$total_episodes=$allseasonsData[$i]['episodes'];
		$seasonid=$allseasonsData[$i]['season_id'];
		$epquery=$this->prepare('select is_active from tv_season_episode where season_id = :sid');
		$epquery->bindParam(':sid',$seasonid,PDO::PARAM_INT);
		$epquery->execute();
		$epData=$epquery->fetchAll();

		for($k=0;$k<count($epData);$k++)
		{
			if($epData[$k]['is_active']==1)
			{
				$linked++;
			}
			else
			{
				$unlinked++;
			}
		}
		$seasoninfo=array('Season name'=>$allseasonsData[$i]['name'],'season number'=>$allseasonsData[$i]['season_number'],'Total episodes'=>$total_episodes,'linked'=>$linked,'unlinked'=>$unlinked);
		array_push($allData, $seasoninfo);

	}
	$this->outputCsv($allData);
}



public function addCountires()
{
	$check;
	
	if ($_REQUEST['data_type']==1)
	{
		// blocked list
		$check=1;
	}
	else
	{
		//approved list
		$check=2;
	}
//delete if any data and insert new 
	$deleteQuery=$this->prepare('delete  from signup_countries');
	
	$deleteQuery->execute();
	if($deleteQuery)
	{
		//save new data
		
		$sQuery=$this->prepare('insert into signup_countries (country,status) values (:country,:status)');
		for($i=0;$i<count($this->countires);$i++)
		{
			$sQuery->bindParam(':country',$this->countires[$i],PDO::PARAM_STR);
			$sQuery->bindParam(':status',$check,PDO::PARAM_INT);
			$sQuery->execute();
			
		}
		$this->success(200,__FUNCTION__,'Updated');
				exit();
	}

}

public function addrestrictedCountires()
{
	if ($_REQUEST['web_type']==1)
	{
		// blocked list
		$check=1;
	}
	else
	{
		//approved list
		$check=2;
	}
	$deleteQuery=$this->prepare('delete  from restricted_countries');
	$deleteQuery->execute();
	if($deleteQuery)
	{
		//save new data
		
		$sQuery=$this->prepare('insert into restricted_countries (country_name,status) values (:country,:status)');
		for($i=0;$i<count($this->countires);$i++)
		{
			$sQuery->bindParam(':country',$this->countires[$i],PDO::PARAM_STR);
			$sQuery->bindParam(':status',$check,PDO::PARAM_INT);
			$sQuery->execute();
			
		}
		$this->success(200,__FUNCTION__,'Updated');
				exit();
	}
}

public function getCountries()
{

	$gQuery=$this->prepare('select * from signup_countries');
	$gQuery->execute();
	
	if($gQuery->rowCount()>0)
	{
		$countData=$gQuery->fetchAll();
		$this->success(200,__FUNCTION__,$countData);
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'No Data');
	}
}

public function fileExists($path){
    return (@fopen($path,"r")==true);
}

public function updatetv()
{
	$uQuery=$this->prepare('update tv_shows set kids=:kids where tv_id=:mId');
	$uQuery->bindParam(':kids',$this->kids,PDO::PARAM_INT);
	$uQuery->bindParam(':mId',$this->mov_id,PDO::PARAM_INT);
	$uQuery->execute();
	if($uQuery->rowCount()>0)
	{
		$this->success(200,__FUNCTION__,'updated');
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'ERROR');
	}
}

public function gettvbyid()
{
	$gQuery=$this->prepare('select name,poster,tv_id,show_id,kids from tv_shows where tv_id=:mid');
	$gQuery->bindParam(':mid',$this->mov_id,PDO::PARAM_INT);
	$gQuery->execute();
	if($gQuery->rowCount()>0)
	{
		$movData=$gQuery->fetchAll();
		$this->success(200,__FUNCTION__,$movData);
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'No Data');
	}
}

public function getmovebyid()
{
	$gQuery=$this->prepare('select title,poster_path,mov_id,live_video_id,kids from movies where mov_id=:mid');
	$gQuery->bindParam(':mid',$this->mov_id,PDO::PARAM_INT);
	$gQuery->execute();
	if($gQuery->rowCount()>0)
	{
		$movData=$gQuery->fetchAll();
		$this->success(200,__FUNCTION__,$movData);
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'No Data');
	}
}
public function updatemovies()
{
	$uQuery=$this->prepare('update movies set kids=:kids where mov_id=:mId');
	$uQuery->bindParam(':kids',$this->kids,PDO::PARAM_INT);
	$uQuery->bindParam(':mId',$this->mov_id,PDO::PARAM_INT);
	$uQuery->execute();
	if($uQuery->rowCount()>0)
	{
		$this->success(200,__FUNCTION__,'updated');
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'ERROR');
	}

}
public function deletetv()
{
	//get showid
	$sQuery=$this->prepare('select show_id from tv_shows where tv_id=:tid');
	$sQuery->bindParam(':tid',$this->mov_id,PDO::PARAM_INT);
	$sQuery->execute();
	if($sQuery)
	{
		$sData=$sQuery->fetchAll();
		$showid=$sData[0]['show_id'];
	}


	$dQuery=$this->prepare('delete t,s,e from tv_shows t inner join tv_season s on t.tv_id=s.tv_id inner join tv_season_episode e on s.season_id=e.season_id where t.tv_id=:tid');
	$dQuery->bindParam(':tid',$this->mov_id,PDO::PARAM_INT);
	$dQuery->execute();
	if($dQuery)
	{

		$uQuery=$this->prepare('update tvqueue set is_deleted=1 where show_id=:sid');
		$uQuery->bindParam(':sid',$showid,PDO::PARAM_INT);
		$uQuery->execute();

		$dQuery1=$this->prepare('delete from video_cast where video_type="tv" and video_id=:vid');
		$dQuery2=$this->prepare('delete from video_reviews where video_type="tv" and video_id=:vid');
		$dQuery3=$this->prepare('delete from video_images where video_type="tv" and video_id=:vid');
		$dQuery4=$this->prepare('delete from video_intro_videos where video_type="tv" and video_id=:vid');
		$dQuery5=$this->prepare('delete from video_crew where video_type="tv" and video_id=:vid');
		$dQuery6=$this->prepare('delete from video_productions where video_type="tv" and video_id=:vid');
		$dQuery7=$this->prepare('delete from video_genres where video_type="tv" and video_id=:vid');
		$dQuery8=$this->prepare('delete from video_languages where video_type="tv" and video_id=:vid');
		$dQuery9=$this->prepare('delete from user_playlist where video_type="tv" and video_id=:vid');

		$dQuery1->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery1->execute();
	

		$dQuery2->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery2->execute();

		$dQuery3->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery3->execute();

		$dQuery4->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery4->execute();

		$dQuery5->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery5->execute();

		$dQuery6->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery6->execute();

		$dQuery7->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery7->execute();

		$dQuery8->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery8->execute();

		$dQuery9->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery9->execute();

		
		$this->success(200,__FUNCTION__,'deleted');
		exit();
	}
}





public function deleteMovie()
{

	$mQuery=$this->prepare('select live_video_id from movies where mov_id=:mid');
	$mQuery->bindParam(':mid',$this->mov_id,PDO::PARAM_INT);
	$mQuery->execute();
	if($mQuery)
	{
		$mData=$mQuery->fetchAll();
		$live_id=$mData[0]['live_video_id'];
	}


	$dQuery=$this->prepare('delete from movies where mov_id=:mId');
	$dQuery->bindParam(':mId',$this->mov_id,PDO::PARAM_INT);
	$dQuery->execute();
	if($dQuery->rowCount()>0)
	{
		//update queue 
		$uQuery=$this->prepare('update queue set is_deleted=1 where VIDEO_ID=:vid');
		$uQuery->bindParam(':vid',$live_id,PDO::PARAM_INT);
		$uQuery->execute();
		

		$dQuery1=$this->prepare('delete from video_cast where video_type="movie" and video_id=:vid');
		$dQuery2=$this->prepare('delete from video_reviews where video_type="movie" and video_id=:vid');
		$dQuery3=$this->prepare('delete from video_images where video_type="movie" and video_id=:vid');
		$dQuery4=$this->prepare('delete from video_intro_videos where video_type="movie" and video_id=:vid');
		$dQuery5=$this->prepare('delete from video_crew where video_type="movie" and video_id=:vid');
		$dQuery6=$this->prepare('delete from video_productions where video_type="movie" and video_id=:vid');
		$dQuery7=$this->prepare('delete from video_genres where video_type="movie" and video_id=:vid');
		$dQuery8=$this->prepare('delete from video_languages where video_type="movie" and video_id=:vid');
		$dQuery9=$this->prepare('delete from user_playlist where video_type="movie" and video_id=:vid');

		
		$dQuery1->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery1->execute();
	

		$dQuery2->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery2->execute();

		$dQuery3->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery3->execute();

		$dQuery4->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery4->execute();

		$dQuery5->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery5->execute();

		$dQuery6->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery6->execute();

		$dQuery7->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery7->execute();

		$dQuery8->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery8->execute();

		$dQuery9->bindParam(':vid',$this->mov_id,PDO::PARAM_INT);
		$dQuery9->execute();

		
		$this->success(200,__FUNCTION__,'deleted');
		exit();
	}
}







public function updateDeleted()
{

		
		ini_set('max_execution_time', 500000);
		$row = 1;
			if (($handle = fopen("../../deleted.csv", "r")) !== FALSE) 
			{
			  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			  {
			    $num = count($data);
			    $row++;
				$videoFile=$data[0]."mp4";
				$basepath='http://media.beanbag.tv/movies/';
				$video=$basepath.$videoFile;
				// echo $video;
				if($this->fileExists($basepath.$videoFile))
				{
					echo 'exist';
					
				}
				else
				{
					//update is_deleted in queue
					$upQuery=$this->prepare('update queue set is_deleted=1 where video_id=:vid');
					$upQuery->bindParam(':vid',$data[0],PDO::PARAM_INT);
					$upQuery->execute();
				}
			    	
			
			    
			  }
			  fclose($handle);
				print_r("All Done");
			  
			}
}

public function getrestrictedCountries()
{
	$cQuery=$this->prepare('select * from restricted_countries');
	$cQuery->execute();
	if($cQuery->rowCount()>0)
	{
		$cData=$cQuery->fetchAll();
		$this->success(200,__FUNCTION__,$cData);
		exit();
	}
	else
	{
		$this->error('ERROR',__FUNCTION__,'No Data');
	}
}

public function tvdeletedsync()
{	

	ini_set('max_execution_time', 500000);
	$location='../../tv_delete.csv';
	$row = 1;
	if (($handle = fopen($location, "r")) !== FALSE) 
	{
	    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
	    	$data = array_map("utf8_encode", $data);
	        $num = count($data);
	        $line_of_text[]=$data;
	    }
	    fclose($handle);
	}


	for($i=1;$i<count($line_of_text);$i++)
	{
		$showName=$line_of_text[$i][0];
		$showid=$line_of_text[$i][1];

		
		//find by show id
		$dQuery=$this->prepare('delete t,s,e from tv_shows t inner join tv_season s on t.tv_id=s.tv_id inner join tv_season_episode e on s.season_id=e.season_id where show_id=0');
		$dQuery->bindParam(':sname',$showName,PDO::PARAM_STR);
		$dQuery->execute();

		if($dQuery->rowCount()>0)
		{
			// update episodes in tvqueue
			$upQuery=$this->prepare('update tvqueue set is_deleted=1 where show_id=:sid');
			$upQuery->bindParam(':sid',$showid,PDO::PARAM_INT);
			$upQuery->execute();
			if($upQuery)
			{
				echo 'All deleted files are synced';
			}
		}
		else
		{
			// error
		}

	}

}

public function mCleanup()
{
	ini_set('memory_limit','1600M');
	ini_set('max_execution_time', 500000);
	$sQuery=$this->prepare('select * from queue where status=1 and is_deleted = 0');
	$sQuery->execute();
	if($sQuery)
	{
		$qData=$sQuery->fetchAll();
		for($i=0;$i<count($qData);$i++)
		{
			$video_id=$qData[0]['VIDEO_ID'];
			//find in movies
			$fQuery=$this->prepare('select * from videos v inner join movies m  on m.live_video_id=v.scid where v.type="movie" and v.scid=:lid and m.is_active=1');
			$fQuery->bindParam(':lid',$video_id,PDO::PARAM_INT);
			$fQuery->execute();
			if($fQuery->rowCount()>0)
			{
				//movie is available and 
			}
			else
			{
				//update is_deleted to b 1
				$uQuery=$this->prepare('update queue set is_deleted=1 where VIDEO_ID=:vid');
				$uQuery->bindParam(':vid',$video_id,PDO::PARAM_INT);
				$uQuery->execute();
			}

		}
	}
}
public function sendEmails()
{
	if($this->emails=="all")
	{
		//get all users emails
		$users=$this->prepare('select email from users ');
		$users->execute();
		if($users->rowCount()>0)
		{
			$emails=$users->fetchAll();
			$emails=$this->emails;
			$body = $this->eBody;
			for($i=0;$i<count($emails);$i++)
			{
				$Request=$this->sendEmail($this->eSubject , $emails[$i][0] , $body);
			}     
		}
	}
	else
	{
		$emails=$this->emails;
		$body = $this->eBody;
		for($i=0;$i<count($emails);$i++)
		{
			$Request=$this->sendEmail($this->eSubject , $emails[$i] , $body);
		}            

        
        
		
			$this->success(200,__FUNCTION__,"Emails Sent");
			exit();
		

	}
}


public function epCleanup()
{
	ini_set('memory_limit','1500M');
	ini_set('max_execution_time', 500000);
	$sQuery=$this->prepare('select * from tvqueue where status=1 and is_deleted = 0');
	$sQuery->execute();
	if($sQuery)
	{
		$qData=$sQuery->fetchAll();
		for($i=0;$i<count($qData);$i++)
		{
			$video_id=$qData[0]['VIDEO_ID'];
			//find in movies
			$fQuery=$this->prepare('select * from videos v inner join tv_season_episode tse on tse.tmdb_id=v.imdb_id where v.type="tv" and v.scid=:lid and tse.is_active=1');
			$fQuery->bindParam(':lid',$video_id,PDO::PARAM_INT);
			$fQuery->execute();
			if($fQuery->rowCount()>0)
			{
				//movie is available and 
			}
			else
			{
				//update is_deleted to b 1
				$uQuery=$this->prepare('update tvqueue set is_deleted=1 where VIDEO_ID=:vid');
				$uQuery->bindParam(':vid',$video_id,PDO::PARAM_INT);
				$uQuery->execute();
			}

		}
	}
}


public function fixEpisodes()
{
	ini_set('display_error', '1');
	ini_set('memory_limit','1500M');
	ini_set('max_execution_time', 500000);
	$cQuery=$this->prepare('select episode_number,episode_name,tmdb_id from tv_season_episode where is_active=1');
	$cQuery->execute();
	if($cQuery->rowCount()>0)
	{
		$cData=$cQuery->fetchAll();
		for($i=0;$i<count($cData);$i++)
		{
		
			$fQuery=$this->prepare('select id from videos where imdb_id=:iid and type="tv"');
			$fQuery->bindParam(':iid',$cData[$i]['tmdb_id'],PDO::PARAM_INT);
			$fQuery->execute();
			if($fQuery->rowCount()>0)
			{
				//video is available and linked no need to do anything
			}
			else
			{
				echo $cData[$i]['episode_name']." ".$cData[$i]['episode_number']." ".$cData[$i]['tmdb_id'];
			}
		}
	}
	
}


}//Class End
?>