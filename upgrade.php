<?php include 'header.php';?>
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/upgrade.css">
<div class="container" style="padding-top: 10px;">

	<div class="section col-xs-6" id="premium-count" style="margin-right: 8px;width:49%;border: 2px solid rgba(243,249,255,0.2);
    border-radius: 16px;">
		<h2>Premium MemberShip</h2>
		<div class="" id="stats">
			<div id="stat"  style="margin: 0">
				<h3>8050</h3>
				<span>Movies</span>
			</div>
			<div id="stat">
				<h3>8050</h3>
				<span>Tv shows</span>
			</div>
			<div id="stat">
				<h3>338050</h3>
				<span>episodes</span>
			</div>
		</div>

		<div class="" id="allpackage">
			<h2>Start your free 3-day trial today</h2>
			<div class="separator"></div>
				<div class="checkout">
					<span class="step">1</span>
					<h3>Select a package</h3>
				</div>
				<div class="packages" >
						<ul class="list-group">
						  <li class="list-group-item activePackage"><span class="left">1 month premium</span><span class="mid"> </span><span class="right">$9.99</span></li>
						  <li class="list-group-item"><span class="left">3 month premium</span><span class="mid">(save 15%)</span><span class="right">$25.99</span></li>
						  <li class="list-group-item"><span class="left">6 month premium</span><span class="mid">(save 25%)</span><span class="right">$45.99</span></li>
						</ul>
				</div>
				
					<span class="step">2</span>
					<h3 id="heading3">Enter card information</h3>
				
				<div class="card_info">
					<ul class="list-group">
						<li class="list-group-item">
							<div class="form-group">
								  <label for="usr">card number: </label>
								  <div class="inner-addon left-addon">
								    <i class="fa fa-credit-card" aria-hidden="true"></i>
								    <input type="text" id="crdnumber" class="form-control" placeholder="#### ## ##### #####" />
								</div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="form-group half-field">
								  <label for="usr">expiry date: </label>
								  <div class="inner-addon left-addon">
								    <i class="fa fa-calendar" aria-hidden="true"></i>
								    <input type="date" id="expdate" class="form-control" placeholder="02/2012" />
								</div>
							</div>
							<div class="form-group half-field" style="margin-left: 10px;">
								  <label for="usr">security code: <i title="text about security" class="fa fa-question-circle-o" aria-hidden="true"></i></label>
								  <div class="inner-addon left-addon">
								    <i class="fa fa-lock" aria-hidden="true"></i>
								    <input type="password" id="crdPassword" class="form-control" placeholder="21321231321" />
								</div>
							</div>
						</li>
					</ul>
				</div>


				<span class="step">3</span>
				<h3 id="heading3">confirm</h3>
				<div class="confirm">
					<p>An authorization for a charge of USD 9.99 will be made on your card. After your trial ends on Sat Jul 22 2017, your card will be charged USD 9.99 every 1 month. You can cancel anytime.</p>
				</div>

				<div class="submitbutton">
					<button class="text">Upgrade</button>
				</div>
			</div>

		

		</div>

		<div class="col-xs-6 section" style="width:49%; border: 2px solid rgba(243,249,255,0.2);
    border-radius: 16px;">
				<h2>FAQ</h2>
			<div class="separator"></div>

			<div class="question">
				<p>What payment options can I use?</p>
			</div>
			<div class="ans">
				<p>Credit and debits cards only. PayPal or Bitcoin is not an option currently.</p>
			</div>
			<div class="question">
				<p>Can I cancel the trial or subscription?</p>
			</div>
			<div class="ans">
				<p>Absolutely, you can cancel online anytime, no commitments.</p>
			</div>
			<div class="question">
				<p>Should I get this?</p>
			</div>
			<div class="ans">
				<p>Definitely. We think it is awesome. </p>
			</div>
			<div class="question">
				<p>How many people can I share my account with?</p>
			</div>
			<div class="ans">
				<p>You can use your account on 3 devices/screens at the same time so you shouldn't share your account with more than 2 people.</p>
			</div>
		</div>
	</div> 




<script type="text/javascript">
	$(function()
	{
		$('.packages li').click(function()
		{
			$('.packages li').removeClass('activePackage');
			$(this).addClass('activePackage');
		})
	})

</script>