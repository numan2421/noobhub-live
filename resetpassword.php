
<style type="text/css">
    .error-msg
{
        text-align: center;
    color: #a94442;
}
.success-msg
{
    text-align: center;
    color: #3c763d;
}
</style>
<?php 

include ('login-nav.php') ;
if(isset($_GET['reset_code']))
{

}
else
{
    header('location:index');
}

?>
<link rel="stylesheet" href="<?php echo $absolutepath ?>includes/css/login.css">



<div class="container">
<div class="alert alert-danger">

  <p class="error-msg"></p>
</div>
<div class="alert alert-success alert-dismissable">
  
   <p class="success-msg"></p>
</div>
    <div class="login">
        <h2>Reset Password</h2>
        <form name="reset_form" id="reset_form">
        <div class="form">
         <input type="hidden" name="reset_token" id="reset_token" value="<?php echo $_GET['reset_code']?>">
            <span>Set a new password</span>
            <div class="form-group">
                  <div class="inner-addon left-addon">
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <input type="password" id="new_password" name="new_password" class="form-control form-field" />
                </div>
            </div>
            <div class="labels">
                <span class="left">Confirm your new password</span>
            
            </div>
            <div class="form-group">
                  <div class="inner-addon left-addon">
                    <i class="fa fa-key" aria-hidden="true"></i>
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control form-field"  />
                </div>
            </div>
            <button type="submit" title="submit" class="btn btn-default"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        </div>
        </form>

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/resetpassword.js"></script>


