<?php
include_once "header.php";
include 'search.php';

?>
  <link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/support.css">



<?php 
  $detect = new Mobile_Detect();
  if (!$detect->isMobile())
  {
	echo '<div class="page_body"><h1>Support</h1>

	<span><p>You can search this page by pressing Ctrl+F (or Cmd+F on a Mac).</p></span>


	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Announcements > &nbsp;	</span>
			<p class="row-desc">Browser crashes on Xbox after a few minutes of watching a video</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>There has been a recent software update to Xbox One (and similar Xbox models) which has a bug that crashes the browser when you&#39;
re watching a large video. This issue is not isolated to this site and will happen on other sites that stream large videos.</p>
			<p>Unfortunately, there is no way we can fix it because only Microsoft can make changes on their software. There are workarounds we can offer:</p>
			<ul id="solution">
				<li><p>Make sure you&#39;
re upgraded to the latest software version of Xbox. (<a href="http://support.xbox.com/en-US/xbox-one/apps/troubleshoot-movies-and-tv-on-xbox-one">Instructions here </a>).</p></li>
				<li><p>Download, install and use another browser that is not Edge.</p></li>
				<li><p>There&#39;
s an <a href="https://play.google.com/store/apps/details?id=com.instantbits.cast.webvideo" target="_blank" rel="noopener noreferrer">Android app</a> which will let you cast the videos to your Xbox.</p></li>
			</ul>
			<p>Please contact and let us know if it starts working for you, like if an Xbox software update fixes it.</p>
		
		</div>
	</div>
	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Content > &nbsp;	</span>
			<p class="row-desc">When do you get new content? Can I request stuff to be added?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>TV shows are updated every day with new episodes.</p>
			<p>New TV shows are added every several weeks.</p>
			<p>New movies are added every week as they become available to us. If it&#39;
s a movie still in theaters, it&#39;
ll be available to us when it&#39;
s released on iTunes/DVD. You can find out this date by googling "[movie name] dvd release date".</p>
			<p>Most movies are released to non-theatrical sources after about 3 or 6 months of their theatrical release.</p>
			<p><p>You can request a TV show or movie you want to be added here by submitting <a href="<?php echo $absolutepath ?>contact">this form.</a></p></p>
		
		</div>
	</div>
	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">My Account > &nbsp;	</span>
			<p class="row-desc">How do I delete or deactivate my account?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>Atm, there is no way to delete or deactivate an account.</p>
	
			<p>If you have a good reason to deactivate your account, you can contact us.</p>
		
		</div>
	</div>
		<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">My Account > &nbsp;	</span>
			<p class="row-desc">How do I change or reset my password?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>If you know your current password and want to change it, click the user icon at the top-right corner of the page, under "My Account" section you can find change password section. Alternatively, simply <a href="<?php echo $absolutepath?>account">click here.</a></p>
	
			<p>If you have a good reason to deactivate your account, you can contact us.</p>
			
		</div>
	</div>
	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Video > &nbsp;	</span>
			<p class="row-desc">How do I download videos on my iPhone or iPad?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>Downloading videos on your iPhone or iPad is different from the normal download process. You have to download the videos using an app that supports downloading files from the internet, like the free VLC app.</p>
			<ul id="solution">
				<li><p>Download and install <a href="https://itunes.apple.com/app/vlc-for-mobile/id650377962" target="_blank" rel="noopener noreferrer">VLC from the App Store</a>.</p></li>
				<li><p>Play the video you want to download as you normally would.</p></li>
				<li><p>Scroll down a bit and click the "Download video in VLC app" link.</p></li>
				<li><p>This should start downloading the movie or TV episode.</p></li>
			</ul>
			
			
		</div>
	</div>
	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Video > &nbsp;	</span>
			<p class="row-desc">Are there any keyboard shortcuts for controlling the video?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>Focus the video player by clicking on it once, then you can use the following shortcuts:</p>
			<ul id="solution">
				<li><p>Space bar – toggles play/pause</p></li>
				<li><p>Right and Left arrow keys – seek the video forwards and backwards by 5 seconds</p></li>
				<li><p>Up and Down arrow keys – increase and decrease the volume</p></li>
				<li><p>M key – toggles mute/unmute</p></li>
				<li><p>F key – toggles fullscreen off and on</p></li>
				<li><p>0-9 number keys – skip to a percentage of the video (0 is 0% and 9 is 90%)</p></li>
				<li><p>Double-clicking the video player with the mouse — toggles fullscreen off and on
</p></li>

			</ul>
			
			
		</div>
	</div>


	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Video > &nbsp;	</span>
			<p class="row-desc">How do I add subtitles for a video?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>Download subtitles from any site on the internet (like <a href="https://subscene.com" target="_blank" rel="noopener noreferrer">subscene.com</a>, <a href="http://www.yifysubtitles.com" target="_blank" rel="noopener noreferrer">yifysubtitles.com</a>, <a href="http://keepsubtiles.com" target="_blank" rel="noopener noreferrer">keepsubtiles.com</a> or <a href="http://www.tvsubtitles.net" target="_blank" rel="noopener noreferrer">tvsubtitles.net</a>). If the downloaded file is a .zip file, extract it. You should look for the .srt file.</p>
	
			<p>Select subtitle language (i.e English) Upload the downloaded .srt file using the "Upload Subtitles" button. This button is located under the video player and is visible when a movie or TV episode is playing, scroll down a bit if you don&#39;
t find it.</p>


			<p>Once uploaded, click the CC button in the video player and select DESIRED. If you don&#39;
t see the CC button, you&#39;
ll have to full screen the video then find a icon/button in the bottom-right corner that will enable the subtitles. This icon usually looks like a speech bubble.</p>


		</div>
	</div>

	<div class="support-scetion">
		<div class="row-heading">
			<h3 class="support-row"><span id="row-heading">Video > &nbsp;	</span>
			<p class="row-desc">How do I play videos on my big TV?</p></h3>
		</div>

		<div id="section-ans" class="answer">
			<p>Depending on your TV and streaming devices, you have got multiple options here.</p>

			<h2>TV with USB flash drive support</h2>
			<p>You can download the video and copy it to a flash drive which you can plug into the TV.</p>

			<h2>TV with a browser app</h2>
			<p>If your TV has a browser app like Chrome, you can browse the site on your TV like you&#39;
re doing now.</p>

			<h2>TV with an HDMI cable</h2>
			<p>You can mirror your display to your TV by connecting your TV and the device you watch videos on using a HDMI cable.</p>

			<h2>Chromecast</h2>
			<p>You can use chromecast to watch video on remote device. If you have chromecast device available and conncted the icon will be displayed on bottom right corner of the player while video playing.</p>



		
		</div>
	</div>







<hr>
			<a href="<?php echo $absolutepath ?>contact">Is there anything else we can help you with? Click here to contact a human.</a>

</div>';
}
else
{
	//i m on mobile
	echo '
	<div class="mobile">
		<div class="page_body">
			<h3>Support</h3>
			<p>You can search this page by pressing Ctrl+F (or Cmd+F on a Mac).</p>


			<div class="question support-scetion">
			<h3 class=""> Announcements > <span class="sec-info">Browser crashes on Xbox after a few minutes of watching a video</span></h3>
			<div id="section-ans" class="answer">
			<p>There has been a recent software update to Xbox One (and similar Xbox models) which has a bug that crashes the browser when you&#39;
re watching a large video. This issue is not isolated to this site and will happen on other sites that stream large videos.</p>
			<p>Unfortunately, there is no way we can fix it because only Microsoft can make changes on their software. There are workarounds we can offer:</p>
			<ul id="solution">
				<li><p>Make sure you&#39;
re upgraded to the latest software version of Xbox. (<a href="http://support.xbox.com/en-US/xbox-one/apps/troubleshoot-movies-and-tv-on-xbox-one">Instructions here </a>).</p></li>
				<li><p>Download, install and use another browser that is not Edge.</p></li>
				<li><p>There&#39;
s an <a href="https://play.google.com/store/apps/details?id=com.instantbits.cast.webvideo" target="_blank" rel="noopener noreferrer">Android app</a> which will let you cast the videos to your Xbox.</p></li>
			</ul>
			<p>Please contact and let us know if it starts working for you, like if an Xbox software update fixes it.</p>
		
		</div>
			</div>


				<div class="question support-scetion">
			<h3 class=""> Content > <span class="sec-info">When do you get new content? Can I request stuff to be added?</span></h3>
			<div id="section-ans" class="answer">
			<p>TV shows are updated every day with new episodes.</p>
			<p>New TV shows are added every several weeks.</p>
			<p>New movies are added every week as they become available to us. If it&#39;
s a movie still in theaters, it&#39;
ll be available to us when it&#39;
s released on iTunes/DVD. You can find out this date by googling "[movie name] dvd release date".</p>
			<p>Most movies are released to non-theatrical sources after about 3 or 6 months of their theatrical release.</p>
			<p><p>You can request a TV show or movie you want to be added here by submitting <a href="<?php echo $absolutepath ?>contact">this form.</a></p></p>
		
		</div>
			</div>


			<div class="question support-scetion">
			<h3 class=""> My Account > <span class="sec-info">How do I change or reset my password?</span></h3>
			<div id="section-ans" class="answer">
			<p>If you know your current password and want to change it, click the user icon at the top-right corner of the page, under "My Account" section you can find change password section. Alternatively, simply <a href="<?php echo $absolutepath?>account">click here.</a></p>
	
			<p>If you have a good reason to deactivate your account, you can contact us.</p>
			
		</div>
			</div>


			<div class="question support-scetion">
			<h3 class=""> My Account > <span class="sec-info">How do I delete or deactivate my account?</span></h3>
			<div id="section-ans" class="answer">
			<p>Atm, there is no way to delete or deactivate an account.</p>
	
			<p>If you have a good reason to deactivate your account, you can contact us.</p>
		
		</div>
			</div>


			<div class="question support-scetion">
			<h3 class=""> Video > <span class="sec-info">How do I download videos on my iPhone or iPad?</span></h3>
			
		<div id="section-ans" class="answer">
			<p>Downloading videos on your iPhone or iPad is different from the normal download process. You have to download the videos using an app that supports downloading files from the internet, like the free VLC app.</p>
			<ul id="solution">
				<li><p>Download and install <a href="https://itunes.apple.com/app/vlc-for-mobile/id650377962" target="_blank" rel="noopener noreferrer">VLC from the App Store</a>.</p></li>
				<li><p>Play the video you want to download as you normally would.</p></li>
				<li><p>Scroll down a bit and click the "Download video in VLC app" link.</p></li>
				<li><p>This should start downloading the movie or TV episode.</p></li>
			</ul>
			
			
		</div>
			</div>



			<div class="question support-scetion">
			<h3 class=""> Video > <span class="sec-info">Are there any keyboard shortcuts for controlling the video?</span></h3>
			
		<div id="section-ans" class="answer">
			<p>Focus the video player by clicking on it once, then you can use the following shortcuts:</p>
			<ul id="solution">
				<li><p>Space bar – toggles play/pause</p></li>
				<li><p>Right and Left arrow keys – seek the video forwards and backwards by 5 seconds</p></li>
				<li><p>Up and Down arrow keys – increase and decrease the volume</p></li>
				<li><p>M key – toggles mute/unmute</p></li>
				<li><p>F key – toggles fullscreen off and on</p></li>
				<li><p>0-9 number keys – skip to a percentage of the video (0 is 0% and 9 is 90%)</p></li>
				<li><p>Double-clicking the video player with the mouse — toggles fullscreen off and on
</p></li>

			</ul>
			
			
		</div>
			</div>




						<div class="question support-scetion">
			<h3 class=""> Video > <span class="sec-info">How do I add subtitles for a video?</span></h3>
			
	<div id="section-ans" class="answer">
			<p>Download subtitles from any site on the internet (like <a href="https://subscene.com" target="_blank" rel="noopener noreferrer">subscene.com</a>, <a href="http://www.yifysubtitles.com" target="_blank" rel="noopener noreferrer">yifysubtitles.com</a>, <a href="http://keepsubtiles.com" target="_blank" rel="noopener noreferrer">keepsubtiles.com</a> or <a href="http://www.tvsubtitles.net" target="_blank" rel="noopener noreferrer">tvsubtitles.net</a>). If the downloaded file is a .zip file, extract it. You should look for the .srt file.</p>
	
			<p>Select subtitle language (i.e English) Upload the downloaded .srt file using the "Upload Subtitles" button. This button is located under the video player and is visible when a movie or TV episode is playing, scroll down a bit if you don&#39;
t find it.</p>


			<p>Once uploaded, click the CC button in the video player and select DESIRED. If you don&#39;
t see the CC button, you&#39;
ll have to full screen the video then find a icon/button in the bottom-right corner that will enable the subtitles. This icon usually looks like a speech bubble.</p>


		</div>
			</div>



						<div class="question support-scetion">
			<h3 class=""> Video > <span class="sec-info">How do I play videos on my big TV?</span></h3>
			

		<div id="section-ans" class="answer">
			<p>Depending on your TV and streaming devices, you have got multiple options here.</p>

			<h2>TV with USB flash drive support</h2>
			<p>You can download the video and copy it to a flash drive which you can plug into the TV.</p>

			<h2>TV with a browser app</h2>
			<p>If your TV has a browser app like Chrome, you can browse the site on your TV like you&#39;
re doing now.</p>

			<h2>TV with an HDMI cable</h2>
			<p>You can mirror your display to your TV by connecting your TV and the device you watch videos on using a HDMI cable.</p>

			<h2>Chromecast</h2>
			<p>You can use chromecast to watch video on remote device. If you have chromecast device available and conncted the icon will be displayed on bottom right corner of the player while video playing.</p>



		
		</div>
			</div>



<hr>
			<a href="<?php echo $absolutepath ?>contact">Is there anything else we can help you with? Click here to contact a human.</a>

	</div></div>';

}
?>




<?php include_once "footer.php"; ?>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/support.js"></script>