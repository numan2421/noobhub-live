<?php
ob_start();
session_start();
include 'includes/config.php';
include 'includes/processes/validation_process.php';
include 'includes/mobile_detect/Mobile_Detect.php';
// Enviornment Setup
$hostname = $_SERVER['HTTP_HOST'];
if($hostname == 'localhost') 
{
    $csabosultepath = '/noobhub';
    $absolutepath="http://".$_SERVER['SERVER_NAME'].$csabosultepath.'/';
}
else 
{
    $absolutepath="http://".$_SERVER['SERVER_NAME']."/";
}
// echo $absolutepath;
if(isset($_SESSION['userId']) && isset($_SESSION['profile_id']))
{
  //check if in kids
  if(isset($_SESSION['in_kids']) && $_SESSION['in_kids']==1)
  {
    //go to kids
    header("location:".$absolutepath."kids");
  }
  else
  {
    //do nothing
    $in_kids=chkinKids();
    if($in_kids==1)
    {
      header("location:".$absolutepath."kids");
    }
    else
    {

    }
  }

}
else
{
    if(isset($_COOKIE['UserData']))
    {
        $allData=unserialize($_COOKIE['UserData']);
        $user_email=$allData['email'];
        $user_name=$allData['username'];
        $userid=$allData['id'];
        $_SESSION['user_name']=$user_name;
        $_SESSION['user_email']=$user_email;
        $_SESSION['userId']=$userid;
        $_SESSION['profile_id']=$allData['profile_id'];
        $_SESSION['profile_image']=$allData['profile_image'];
        $_SESSION['profile_name']=$allData['profile_name'];

        $expiryChk=chkExpiry($userid);
        if($expiryChk==1)
        {
            //already on home
            if($in_kids==1)
            {
              //go to kids
              header("location:".$absolutepath."kids");
            }
            else
            {
                //stay on home
            }
        }
        elseif ($expiryChk==2)
        {
             header("location:".$absolutepath."index");
        }
        else
        {
            //error
        }
    }
    else
    {
        header("location:".$absolutepath."index");
    }
}
$currentFile = $_SERVER["PHP_SELF"];
$parts = Explode('/', $currentFile);
$currect_nav_var = $parts[count($parts) - 1];
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Josefin+Sans" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/lightslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/slider.css">
<link rel="shortcut icon" type="image/png" href="<?php echo $absolutepath?>includes/img/favicon.ico"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
var Apath="";
if(window.location.host=="localhost") 
{
    Apath="http://localhost/noobhub/";
}

else
{
    Apath="http://noobhub.tv/";
}
apiimgurl='https://image.tmdb.org/t/p/'
</script>

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NoobHub TV</title>
    <input type="hidden" name="premium" id="premium" value=<?php echo $_SESSION['is_premium'] ?> >
<?php 
  $detect = new Mobile_Detect();
  if ($detect->isMobile())
  {
    echo '<nav class="navbar navbar-fixed-top mobile" style="background-color: #282c39;    z-index: 9999;" >
<ul class="nav navbar-nav navbar-right pull-right" style="margin: 7.5px 15px; ">
        <li class="menu-half"><form class=" navbar-left" >
            <div class="">
              <input type="search" placeholder="Search" id="m_search" name="m_search" a="">
              <button class="close-icon" type="reset"></button>
            </div>
        </form></li>
        <li class="dropdown menu-half" style="margin-top: 0;    float: right;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 2px;"><img class="avatar" src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&w=32&r=pg" width="45" height="45"></a>
          <ul class="dropdown-menu">
            <li><a href="'.$absolutepath.'account"><i class="fa fa-user"></i> My Account</a></li>
            <li><a href="'.$absolutepath.'upgrade"><i class="fa fa-star"></i> Upgrade</a></li>
            <li><a href="'.$absolutepath.'contact"><i class="fa fa-comments"></i> Contact/Request</a></li>
            <li><a href="'.$absolutepath.'support"><i class="fa fa-question-circle"></i> Support</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="'.$absolutepath.'logout"><i class="fa fa-sign-out"></i> Logout</a></li>
          </ul>
        </li>
        <!-- <li><img class="avatar" src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&w=32&r=pg" width="45" height="45"></li> -->
      </ul>


</nav>
<ul class="nav navbar-nav right-nav mobile_right-nav navbar-fixed-top" style="background-color: #282c39">
        <li class="menu-half '.(($currect_nav_var=="home.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'home"><i class="fa fa-home"></i> Home</a></li>
          <li class="menu-half '.(($currect_nav_var=="movies.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'movies"><i class="fa fa-film"></i> Movies</a></li>
          <li class="menu-half '.(($currect_nav_var=="shows.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'shows"><i class="fa fa-tv"></i> Tv Shows</a></li>
          <li class="menu-half '.(($currect_nav_var=="list.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'list"><i class="fa fa-list"></i> My List</a></li>
        <!-- <li class="menu-half"><a href="#"><i class="fa fa-child" aria-hidden="true"></i> Kids</a></li> -->
</ul>';
  }
  else
  {
    echo '<nav class="navbar navbar-fixed-top" style="background-color: #282c39">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border: 1px solid #1d1b24;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <a class="navbar-brand" href="#"><img src="http://beanbag.lk/images/logo.png" width="140"></a> -->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav right-nav">';
        echo '<li class="menu-half '.(($currect_nav_var=="home.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'home"><i class="fa fa-home"></i> Home</a></li>
          <li class="menu-half '.(($currect_nav_var=="movies.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'movies"><i class="fa fa-film"></i> Movies</a></li>
          <li class="menu-half '.(($currect_nav_var=="shows.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'shows"><i class="fa fa-tv"></i> Tv Shows</a></li>
          <li class="menu-half '.(($currect_nav_var=="list.php") ? 'activeLi' : '').'"><a href="'.$absolutepath.'list"><i class="fa fa-list"></i> My List</a></li>';
       
        
      echo '</ul>



     <ul class="nav navbar-nav navbar-right pull-right">
        <li class="menu-half"><form class=" navbar-left" >
            <div class="">
              <input type="search" placeholder="Search" id="m_search" name="m_search" a="">
               <button class="close-icon" type="reset"></button>
            </div>
        </form></li>
        <li class="dropdown menu-half" style="margin-top: 0;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 2px;"><img class="avatar" src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&w=32&r=pg" width="45" height="45"></a>
          <ul class="dropdown-menu">
            <li><a href="'.$absolutepath.'account"><i class="fa fa-user"></i> My Account</a></li>
            <li><a href="'.$absolutepath.'Upgrade"><i class="fa fa-star"></i> Upgrade</a></li>
            <li><a href="'.$absolutepath.'contact"><i class="fa fa-comments"></i> Contact/Request</a></li>
            <li><a href="'.$absolutepath.'support"><i class="fa fa-question-circle"></i> Support</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="'.$absolutepath.'logout"><i class="fa fa-sign-out"></i> Logout</a></li>
          </ul>
        </li>
        <!-- <li><img class="avatar" src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&w=32&r=pg" width="45" height="45"></li> -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  <div class="alert alert-info fade in alert-daily">
  <div class="alert-Text"><strong>Warning!</strong> Total Video play limit/day reached. <a href="'.$absolutepath.'upgrade">Click</a> to become premium user.</div>
</div>
</nav>';
  }?>
</head>

<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/header.js"></script>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/search.js"></script>





