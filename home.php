<?php
include 'header.php';
include 'includes/processes/home_process.php';
?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
<style type="text/css">
#lightSlider img
{
	width: 100%;

}
a.person
{
	    display: inline-block;
margin: 0 2px;
}
img.pavatar
{
display: inline-block;
vertical-align: middle;
border-radius: 50%;
background: rgba(0,6,12,0.5);
border: 2px rgba(243,249,255,0.8) solid;
height: 75px;
width: 75px;
margin: 10px 15px;
box-shadow: 0 2px 12px rgba(0,6,12,0.5);
}
#random_actor ul.lSSlide
{
	height: 150px !important;
}
.person-celeb
{
	height: 200px;
}
</style>
<?php include 'search.php' ?>
<div class="page_body">
		<div class="slider sliderDiv">
			<ul id="lightSlider">
	  	  		<?php echo $allbannerbody ?>
			</ul>
		</div>

		

			<div class="movData col-md-12">
				<div id="tredning" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Trending Titles</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/trending">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $trendingBody?>	
					</ul>
				</div>
				<div id="most_popular" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular titles</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/popular">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $popbody ?>
					</ul>
				</div>
				<div id="recent_episodes" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Recent Added Episodes</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/latestep">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $lBody ?>
					</ul>
				</div>
				<div id="recent_movies" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Recently Added Movies</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/recentmv">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $allmoviebody ?>
					</ul>
				</div>
				<div id="high_gross_movies" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Highest Grossing Movies Of All Time</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/highestgross">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $grossbody ?>
					</ul>
				</div>
				<div id="most_popular" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Movies</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/popular-movies">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $popmovbody ?>
					</ul>
				</div>
				<div id="most_popular_2015" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Movies Released in <?php echo date('Y', strtotime('-2 years')) ?> </h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/movie-year2">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $popmov2body ?>
					</ul>
				</div>
				<div id="most_popular_2016" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Movies Released in <?php echo date('Y', strtotime('-1 years')) ?></h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/movie-year1">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $popmov1body?>
					</ul>
				</div>
				<div id="most_voted" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Voted Movies Of All Time</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/most-voted-movies">See All > </a></span>
					</div>
					<ul class="listing">
							<?php echo $votedBody ?>
					</ul>
				</div>
				<div id="most_popular_tv" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Tv Shows</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/popular-tv">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $poptvbody ?>
					</ul>
				</div>
				<div id="most_popular_tv_2015" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Tv Shows Aired in <?php echo date('Y', strtotime('-2 years')) ?></h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/tv-year2">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $poptv2body ?>
					</ul>
				</div>
				<div id="most_popular_tv_2016" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Popular Tv Shows Aired in <?php echo date('Y', strtotime('-1 years')) ?></h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/tv-year1">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $poptv1body ?> 
					</ul>
				</div>
				<div id="most_voted" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Most Voted Tv Shows OF All Time</h2>
						<span class="seeAll"><a href="<?php $absolutepath?>discover/most-voted-tv">See All > </a></span>
					</div>
					<ul class="listing">
						<?php echo $votedtvBody?>
					</ul>
				</div>
				<div id="random_actor" class="sliderDiv dataDiv">
					<div class="heading">
						<h2>Random Actors</h2>
					</div>
					<ul class="listing">
						<?php echo $celebBody?>
					</ul>
				</div>


			</div>
</div>
<?php include 'footer.php' ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
 	// Main slider Hover
	$(document).on("mouseenter", "#lightSlider li", function(e) 
	{
		$(this).children().find('.year').show();
	});
	$(document).on("mouseleave", "#lightSlider li", function(e)
	{
		$(this).children().find('.year').hide();
	});
    $("#lightSlider").lightSlider(
	{
		auto:true,
		pager:true,
		item:3,
		slideMove:3,
		autoWidth:false,
		loop:true,
		pause:4000,
		pauseOnHover:true,
		slideMargin:0, 
		responsive : [
            {
                breakpoint:800,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
	});  

	$('.listing').lightSlider(
	{
		pager:false,
		autoWidth:true,
		loop:false,
		slideMargin:3,
	})
  });


</script>