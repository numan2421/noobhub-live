<?php
	include_once "login-nav.php"
?>
<style type="text/css">
.forgot:hover
{
	color: white;
}
</style>
<link rel="stylesheet" type="text/css" href="includes/css/login.css">
<div class="container">
	<div class="login">
		<h2>Have an account ? Log In</h2>
		<form name="login_form" id="login_form">
		<div class="form">
			<span>your username or email address</span>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-envelope" aria-hidden="true"></i>
				    <input type="text" id="user_email" name="user_email" class="form-control form-field" placeholder="Email Address"/>
				</div>
			</div>
			<div class="labels">
				<span class="left">your password</span>
				<a href="forgetpassword"><span class="right forgot">forgot?</span></a>
			</div>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-key" aria-hidden="true"></i>
				    <input type="password" id="user_pass" name="user_pass" class="form-control form-field" placeholder="Password" />
				</div>
			</div>
			<button type="submit" title="submit" class="btn btn-default"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
		</div>
		</form>
		<span><p>Start your free trial by  <a href="<?php echo $absolutepath?>signup">Signing Up</a></p></span>
	</div>
</div>
<?php include_once "footer.php" ?>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/login.js"></script>