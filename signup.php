<?php
	include_once "login-nav.php"
?>
<style type="text/css">
	.modal
	{
		    z-index: 99999 !important;
	}
</style>
<link rel="stylesheet" type="text/css" href="includes/css/login.css">
<div class="container">
	<div class="login">
		<h2>Sign up for new account !</h2>
		<form name="signup_form" id="signup_form">
		<input type="hidden" name="code" id="code" value="<?php echo isset($_GET['invite_code']) ? $_GET['invite_code'] : '' ;?>" name="">
		<div class="form">
			
		<span>Full Name</span>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-user" aria-hidden="true"></i>
				    <input type="text" id="f_name" name="f_name" class="form-control form-field" placeholder="Full Name"/>
				</div>
			</div>

			<span>Email</span>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-envelope" aria-hidden="true"></i>
				    <input type="text" id="user_email" name="user_email" class="form-control form-field" placeholder="Email Address"/>
				</div>
			</div>
			<div class="labels">
				<span class="left">password</span>
			</div>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-key" aria-hidden="true"></i>
				    <input type="password" id="user_pass" name="user_pass" class="form-control form-field" placeholder="Password" />
				</div>
			</div>
			<span>Confirm Password</span>
			<div class="form-group">
				  <div class="inner-addon left-addon">
				    <i class="fa fa-key" aria-hidden="true"></i>
				    <input type="password" id="user_pass_confirm" name="user_pass_confirm" class="form-control form-field" placeholder="Confirm Password" />
				</div>
			</div>
			<button type="submit" title="submit" class="btn btn-default"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
		</div>
		</form>
		<span><p>Already a member ? <a href="<?php echo $absolutepath?>login">Login Here</a></p></span>
	</div>


  	<div class="modal fade" id="overlay">
  <div class="modal-dialog">
    <div class="modal-content" style="background: #1d1b24;">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
        <h4 id="mTitle" class="modal-title"><a href="javascript:void(0)">
							<?php echo SITE_TITLE ?>
		</a></h4>
      </div>
      <div id="mBody" class="modal-body" style="text-align: center;">
        <p></p>
      </div>
    </div>
  </div>
</div>

</div>





<?php include_once "footer.php" ?>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/signup.js"></script>