<?php
// unset sessions and cookies 
session_start();
ob_start();
	               
$exTime = time() - (60 * 60 * 24 * 7);
setcookie("UserData", "", $exTime, '/');
session_destroy();
header("location:index");

?>