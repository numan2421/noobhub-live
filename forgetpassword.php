<style type="text/css">
    .logo p,.alert p
{
    margin-top: 0 !important; 
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 { padding: 0 !important;}
.error-msg
{
        text-align: center;
    color: #a94442;
}
.success-msg
{
    text-align: center;
    color: #3c763d;
}
</style>
<?php
    include_once "login-nav.php"
?>
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath ?>includes/css/forgot.css">

<div class="container">
<div class="alert alert-danger">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <p class="error-msg"></p>
</div>
<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <p class="success-msg"></p>
</div>
    <div class="login">
        <h2>Forgot password ?</h2>
        <form id="forget_form" name="forget_form">
        <div class="form">
            <span>your username or email</span>
            <div class="form-group col-md-3">
                  <div class="inner-addon left-addon">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <input type="text" id="user_email" name="user_email" class="form-control form-field" placeholder="Email Address"/>
                </div>

               
            </div>
                <div class="col-md-1">
                     <button type="submit" title="submit" class="btn btn-default"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>

            
            
        </div>
        </form>
       <p>Still having trouble identifying your account?  <a href="<?php echo $absolutepath?>contact">Contact us </a></p>
    </div>
</div>
<?php include_once "footer.php" ?>
<script type="text/javascript" src="<?php echo $absolutepath ?>includes/js/forgetpassword.js"></script>


