<?php
ob_start();
session_start();
include_once 'includes/mobile_detect/Mobile_Detect.php';
include 'includes/config.php';
include 'includes/processes/restricted_process.php';
include 'includes/processes/validation_process.php';


$hostname = $_SERVER['HTTP_HOST'];
if($hostname == 'localhost')
{
    $csabosultepath = '/noobhub';
    $absolutepath="http://".$_SERVER['SERVER_NAME'].$csabosultepath.'/';
}
else
{
    $absolutepath="http://".$_SERVER['SERVER_NAME']."/";
}

if(isset($_SESSION['userId']))
{
    if(isset($_SESSION['profile_id']))
    {
      if(isset($_SESSION['in_kids']) && $_SESSION['in_kids']==1)
      {
    
        //go to kids
        header("location:".$absolutepath."kids");
      }
      else
      {    
        //save visit
        addvisit();
        header("location:".$absolutepath."home");
      }
    }
    else
    {
        if(isset($_COOKIE['UserData']))
        {
            $allData=unserialize($_COOKIE['UserData']);
            $user_email=$allData['email'];
            $user_name=$allData['username'];
            $userid=$allData['id'];
            $_SESSION['user_name']=$user_name;
            $_SESSION['user_email']=$user_email;
            $_SESSION['userId']=$userid;
            $_SESSION['profile_id']=$allData['profile_id'];
            $_SESSION['profile_image']=$allData['profile_image'];
            $_SESSION['profile_name']=$allData['profile_name'];
            $in_kids=chkinKids();
            $expiryChk=chkExpiry($userid);
            if($expiryChk==1)
            {
              if($in_kids==1)
              {
                //go to kids
                header("location:".$absolutepath."kids");
              }
              else
              {
                  //go to home
                  //save visit
                  addvisit();
                  header("location:".$absolutepath."home");
              }
            }
            elseif ($expiryChk==2)
            {
                 header("location:index");
            }
            else
            {
                //error
            }
        }
        else
        {
            //do nothing
        }
    }
}
else
{
    // do nothing
}
?>


<!-- global path for prety urls -->
<script type="text/javascript">
var Apath="";

if(window.location.host=="localhost")
{
    Apath="http://localhost/noobhub/"
}
else
{
   Apath="http://noobhub.tv/";
}

</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Josefin+Sans" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/lightslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo $absolutepath?>includes/css/nav_style.css">
<link rel="shortcut icon" type="image/png" href="<?php echo $absolutepath?>includes/img/favicon.ico"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
.container
{
  margin-top:30px; 
}
a:active, a:hover {
    outline-width: 0;
}
.logo:hover
{
  transform: scale(1.04) translateY(1px);
}
.logo
{
transition: .3s transform;
padding: 0 20px;
display: inline-flex;
text-transform: none;
border: 3px solid #298EEA !important;
font: 700 24px/38px Josefin Sans,sans-serif;
padding: 0 12px;
cursor: pointer;
color: white;
}  
.hub
{
color: #298EEA;
}
p
{
margin: 0;
}
.navbar-fixed-top {
min-height: 70px;
}
.navbar-nav > li > a {
padding-top: 0px;
padding-bottom: 0px;
line-height: 70px;
}

@media (max-width: 767px) {
    .navbar-nav > li > a {
    line-height: 20px;
    padding-top: 10px;
    padding-bottom: 10px;}
}
</style>
<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NoobHub TV</title>

<?php 
  $detect = new Mobile_Detect();
  if ($detect->isMobile())
  {
    echo '<nav class="navbar navbar-fixed-top mobile" style="background-color: #282c39">
        <div class="navbar-header">
      <a class="navbar-brand" href="'.$absolutepath.'"><span class="logo"><p class="noob">Noob</p><p class="hub">Hub</p></span></a>
    </div>
      <ul class="nav navbar-nav navbar-right pull-right" style="margin: 7.5px 15px; ">  
      </ul>
</nav>
';
  }
  else
  {
    echo '<nav class="navbar navbar-fixed-top" style="background-color: #282c39">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border: 1px solid #1d1b24;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="'.$absolutepath.'"><span class="logo"><p class="noob">Noob</p><p class="hub">Hub</p></span></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<ul class="nav navbar-nav navbar-right pull-right">
 
          
        <!-- <li><img class="avatar" src="https://www.gravatar.com/avatar/883602c977cf8440fa9c9c5414cf3cb5?d=mm&w=32&r=pg" width="45" height="45"></li> -->
      </ul>


     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>';
  }?>

</head>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>